<?php
require_once "connect.php";

?>
<div class='left_box menu'>
  <div class='lbox_content'>
    <div class='lbox_head'>

      <ul id='main_menu'>

        <?php

        $lQuery = "SELECT * FROM s_item_brand ORDER BY sib_order ASC";
        $lBrandsRes = $db->query($lQuery);

        // pruchod vsemi znackami
        while ($lBrandRow = $lBrandsRes->fetch_assoc()) {
          $lIsActive = isset($GLOBALS[GL_BRAND]) && $GLOBALS[GL_BRAND]["sib_code"] == $lBrandRow["sib_code"];

          echo "<li class='li_brand" . ($lIsActive ? " in" : "") . "'>";
          echo "<a href='" . WR . $lBrandRow["sib_url"] . "'>" . $lBrandRow["sib_name"] . "</a></li>";

          echo getMenuItemsCateg(WR . $lBrandRow["sib_url"],
            "sit_brand = " . $lBrandRow["sib_code"] . " AND sit_head IS NULL");
        }

        echo "<li style='margin:10px 0px 5px 0px; border:none; height:2px; padding-top:0px;background-color:#540c0c'></li>";

        echo "<li><a href='" . WR_BAZAR . "'>Bazar</a></li>";
        echo "<li><a href='" . WR . "slevovy-system'>Slevový systém</a></li>";
        echo "<li><a href='" . WR_FOR . "'>Fórum</a></li>";
        echo "<li><a href='" . WR_CONTACT . "'>Kontakt</a></li>";
        ?>
      </ul>

      <div class='lbox_tail'></div>
    </div>
  </div>
</div>