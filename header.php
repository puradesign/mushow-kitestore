<?php
// logo
echo "<a href='" . WR . "'>";
echo "<img id='logo' alt='Mushow Kite Store Logo' ";
echo "src='" . getBrowserPath(F_IMG . "logo.png") . "'/></a>";

// kruhy nalevo a napravo
echo "<img id='left_circles' src='" . getBrowserPath(F_IMG . "left_circles_overlay.png") . "' alt='overlay'/>";
echo "<img id='right_circles' src='" . getBrowserPath(F_IMG . "right_circles_overlay.png") . "' alt='overlay'/>";


// telo headeru
echo "<div id='header_body'>";

// rychly kontakt
echo "<div style='position:absolute;top:0px;left:250px;width:160px;text-align:right;'>";
echo "<span style='color:#995959'>hotline: <a href='tel:+420608858479'>+420 608 858479</a></span>";
echo "</div>";

// znacky
$lActBrand = isset($GLOBALS[GL_BRAND]) ? $GLOBALS[GL_BRAND]["sib_code"] : 0;

echo "<div id='brands_div' class='brands'>";

echo "<div><a href='" . WR . "slingshot'><img src='" . WR_IMG . "slingshot.png' alt='slingshot logo'/><img src='" . WR_IMG . "slingshot2.png' alt='slingshot logo' class='";
echo ($lActBrand == 1 ? "img-in" : "img-hover") . "'/></a></div>";

echo "<div><a href='" . WR . "mushow'><img src='" . WR_IMG . "mushow.png' alt='mushow logo'/><img src='" . WR_IMG . "mushow2.png' alt='mushow logo' class='";
echo ($lActBrand == 2 ? "img-in" : "img-hover") . "'/></a></div>";

echo "<div><a href='" . WR . "ikon'><img src='" . WR_IMG . "ikon.png' alt='ikon logo'/><img src='" . WR_IMG . "ikon2.png' alt='ikon logo' class='";
echo ($lActBrand == 13 ? "img-in" : "img-hover") . "'/></a></div>";

echo "<div><a href='" . WR . "oakley'><img src='" . WR_IMG . "oakley.png' alt='oakley logo'/><img src='" . WR_IMG . "oakley2.png' alt='oakley logo' class='";
echo ($lActBrand == 6 ? "img-in" : "img-hover") . "'/></a></div>";

echo "<div><a href='" . WR . "rideengine'><img src='" . WR_IMG . "rideengine.png' alt='rideengine logo'/><img src='" . WR_IMG . "rideengine2.png' alt='rideengine logo' class='";
echo ($lActBrand == 5 ? "img-in" : "img-hover") . "'/></a></div>";


echo "</div>";

/* ------------------------- HEAD FOTOSS -------------------*/

echo "<div id='header_pics'>";
//echo "<li><img src='' alt='empty'/></li>";
$lPath = F_IMG . "head_fotos";

if (isset($GLOBALS[GL_BRAND]))
  $lPath = $lPath . DIRECTORY_SEPARATOR . $GLOBALS[GL_BRAND]["sib_url"];

$dir_handle = @opendir($lPath);

if (!$dir_handle) {
  @closedir($dir_handle);
  $lPath = F_IMG . "head_fotos";
  $dir_handle = @opendir(F_IMG . "head_fotos");
}

$list = array();
// projdeme obsah slozky a zapamatujeme si zvlast soubory a slozky
while ($file = readdir($dir_handle)) {
  if ($file == "." || $file == ".." || is_dir($lPath . DIRECTORY_SEPARATOR . $file))
    continue;

  if (preg_match("/^\./", $file))
    continue;

  $list[] = $file;
}

closedir($dir_handle);

sort($list);

foreach ($list as $file)
  echo "<img src='" . getBrowserPath($lPath . DIRECTORY_SEPARATOR . $file) . "' alt='header pic' />\n";

echo "</div>";

echo "<div id='search'>";
echo "	<form method='get' action='" . WR_SEARCH . "'>";
echo "		<input type='text' id='searchRight' name='search' value='Hledat..' class='input_out'/>";
echo "		<input type='submit' value='' id='search_butt'/>";
echo "	</form>";
echo "<a href='" . WR_BAZAR . "' style='margin-left:30px'>Bazar</a>";
echo "<a href='" . WR_FOR . "'>Fórum</a>";
echo "</div>";


echo "<a name='head'></a>";
echo "</div>";


/* ------------------------- cesta -------------------*/

$GLOBALS["pv"]->toHtml();

?>