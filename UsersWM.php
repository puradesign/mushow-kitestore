<?php

class UsersWM extends WebModule {
  var $mAction;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    if (!empty($_GET['action']) && $_GET['action'] == 'delete') {
      $userid = intval(@$_GET["user_id"]);
      $query = "SELECT * FROM user WHERE u_code = '$userid'";
      $result = $GLOBALS["db"]->query($query);
      $userRow = $result->fetch_assoc();

      if (!$userRow) {
        $GLOBALS["rv"]->addError("Uživatel nenalezen.");
        return true;
      }

      // check orders
      $query = "SELECT * FROM shop_order WHERE `or_user`=$userid";
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();
      if ($row) {
        $GLOBALS["rv"]->addError("Uživatele nelze smazat, má vytvořené objednávky.");
        return true;
      }

      $query = "DELETE FROM user WHERE `u_code`='$userid'";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Uživatele se nepodařilo smazat.");
        return true;
      }

      $GLOBALS["rv"]->addError("Uživatel smazán.");
      return true;
    }

    return true;
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Uživatelé";
  }

  /**
   * Definuje tlacitko pro pridani
   */
  function defineHeaderButton() {
    echo "<div class='right_top_button add_button'>";
    echo "<a href='" . WR . "registrace'>";
    echo "Nový uživatel";
    echo "</a></div>";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    $limit = 1000;
    $offset = 0;

    // slozeni podminky
    $whereCond = new WhereCondition();

    $lTable = "user LEFT JOIN user_address ON u_main_addr = ua_code";

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $result = $itemsBrowser->executeQuery($lTable, $whereCond, "ua_surname ASC, ua_name ASC", "");
    if (!$result)
      die(getRText("err9") . $result->error);

    echo "<table class='brw_table' style='width: 520px'>";
    echo "<thead><th>Jméno</th><th>nick</th><th></th><th></th><th></th></thead>";

    // vykresleni polozek
    while ($row = $result->fetch_assoc()) {
      echo "<tr>";
      echo "<td style='width:100%'>" . $row["ua_surname"] . " " . $row["ua_name"] . "</td>";
      echo "<td>" . $row["u_nick"] . "</td>";
      //echo "<td>".$row["u_mail"]."</td>";
      echo "<td><a href='" . WR . "edit-profile?user_id=" . $row["u_code"] . "'>Editovat</a></td>";
      echo "<td><a href='" . WR_ORDER . "?user=" . $row["u_code"] . "'>Objednávky</a></td>";
      echo "<td><a href='" . WR . "users?action=delete&user_id=" . $row["u_code"] .
        "' onClick='if(!confirm(\"Opravdu smazat uživatele " . $row["ua_surname"] . " " . $row["ua_name"] . "?\")) return false;'" .
        " title='Smazat'>X</a></td></tr>\n";
    }

    echo "</table>\n";
  }
}
?>