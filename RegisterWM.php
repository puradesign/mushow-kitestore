<?php
require_once "StoreUtil.php";

class RegisterWM extends WebModule {
  var $mAction;
  var $mActRow;
  var $mUrl = WR;
  var $mUser = 0;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {

    if (!isset($_GET["action"]))
      $this->mAction = "add";
    else
      $this->mAction = $_GET["action"];

    // kontrola akce
    if ($this->mAction != "add" && $this->mAction != "edit") {
      $GLOBALS["rv"]->addError("Chyba. Pravděpodobně byla ručně upravena url stránky.");
      $this->setForOutput(false);
      return false;
    }

    $this->mUrl = WR . ($this->mAction == "add" ? "registrace" : "edit-profile");

    if ($this->mAction == "edit") {
      $this->mUser = $_SESSION[SN_CODE];

      // editace uzivatele adminem
      if (isLoggedAdmin() && isset($_GET["user_id"])) {
        $this->mUser = (int) $_GET["user_id"];

        $this->mUrl .= "?user_id=" . $this->mUser;
      }

      $query = "SELECT * FROM user LEFT JOIN user_address ON u_main_addr = user_address.ua_code WHERE u_code=" . $this->mUser;
      $result = $GLOBALS["db"]->query($query);

      $row = $result->fetch_assoc();
      $this->mActRow = $row;

      if (empty($_POST)) {
        $_POST["u_nick"] = $row["u_nick"];
        $_POST["email1"] = "hehe" . $row["u_nick"] . "haha";
        $_POST["u_mail"] = $row["u_mail"];
        $_POST["ua_company"] = $row["ua_company"];
        $_POST["ua_name"] = $row["ua_name"];
        $_POST["ua_surname"] = $row["ua_surname"];
        $_POST["ua_street"] = $row["ua_street"];
        $_POST["ua_town"] = $row["ua_town"];
        $_POST["ua_psc"] = $row["ua_psc"];
        $_POST["ua_state"] = $row["ua_state"];
        $_POST["ua_tel"] = $row["ua_tel"];
        $_POST["ua_ico"] = $row["ua_ico"];
        $_POST["ua_dic"] = $row["ua_dic"];
      }
    }

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    // spam kontrola
    if (!doSpamCheck("u_nick"))
      return true;

    if (!isLoggedAdmin()) {
      // tvar hesla
      if (!preg_match("/^[a-zA-Z0-9]*$/", $_POST['u_passwd'])) {
        $GLOBALS["rv"]->addError("Zadejte prosím platné heslo. Použijte písmena a číslice.");
        $this->getFormElement('u_passwd')->mValid = false;
      }

      // shoda hesel
      if ($_POST['u_passwd'] != $_POST['u_passwd2']) {
        $GLOBALS["rv"]->addError("Hesla se musí shodovat.");
        $this->getFormElement('u_passwd')->mValid = false;
        $this->getFormElement('u_passwd2')->mValid = false;
      }

      // tvar emailove adresy
      if (!preg_match("/^.+@.+\.[a-zA-Z]+$/", $_POST['u_mail'])) {
        $GLOBALS["rv"]->addError("Emailová adresa má neplatný tvar.");
        $this->getFormElement('u_mail')->mValid = false;
      }

      // tvar prihl. jmena
      if (!preg_match("/^[a-zA-Z0-9\-]+$/", $_POST['u_nick'])) {
        $GLOBALS["rv"]->addError("Zadejte prosím platné uživatelské jméno. Použijte písmena, číslice a pomlčku.");
        $this->getFormElement('u_nick')->mValid = false;
      }
    }

    if (!$GLOBALS["rv"]->isOk())
      return true;

    $nick = alterTextForDB($_POST["u_nick"]);
    $mail = alterTextForDB($_POST["u_mail"]);
    $passwd = MD5($_POST["u_passwd"]);
    $company = alterTextForDB($_POST["ua_company"]);
    $name = alterTextForDB($_POST["ua_name"]);
    $surname = alterTextForDB($_POST["ua_surname"]);
    $street = alterTextForDB($_POST["ua_street"]);
    $town = alterTextForDB($_POST["ua_town"]);
    $psc = alterTextForDB($_POST["ua_psc"]);
    $state = $_POST["ua_state"];
    $tel = alterTextForDB($_POST["ua_tel"]);
    $ico = alterTextForDB($_POST["ua_ico"]);
    $dic = alterTextForDB($_POST["ua_dic"]);

    $lCond = new WhereCondition();

    // vlozeni
    if ($this->mAction == "add") {

      if (!isLoggedAdmin()) {
        // jedinecnost uziv. jmena
        $query = "SELECT u_code FROM user WHERE u_nick='" . $nick . "'";
        $result = $GLOBALS["db"]->query($query);

        if ($result->num_rows != 0) {
          $GLOBALS["rv"]->addError("Toto přihlašovací jméno je již obsazeno, zvolte prosím jiné.");
          return true;
        }

        $query = "SELECT u_code FROM user WHERE u_mail='" . $mail . "'";
        $result = $GLOBALS["db"]->query($query);

        if ($result->num_rows != 0) {
          $GLOBALS["rv"]->addError("Účet s touto emailovou adresou již existuje.");
          return true;
        }
      }

      $lActivationStr = isLoggedAdmin() ? "" : MD5("hehehe" . $nick . "he" . $mail);

      // vlozeni do DB
      $query = "INSERT INTO user (`u_nick`, `u_mail`, `u_passwd`, `u_ip`, `u_activation`, `u_date_create`)";
      $query .= " VALUES ('" . $nick . "', '" . $mail . "'";
      $query .= ", '$passwd', '" . getIP() . "', '$lActivationStr', NOW())";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      $lUserCode = $GLOBALS["db"]->insert_id;

      // -----------------
      // vlozeni adresy

      $query = "INSERT INTO user_address (`ua_abbr`, `ua_user`, `ua_name`, `ua_surname`, `ua_company`";
      $query .= ", `ua_street`, `ua_town`, `ua_psc`, `ua_state`, `ua_tel`, `ua_ico`, `ua_dic`)";
      $query .= " VALUES ('main_addr_$lUserCode', $lUserCode, '$name', '$surname', '$company'";
      $query .= ", '$street', '$town', '$psc', '$state', '$tel', '$ico', '$dic')";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      // update
      $query = "UPDATE user SET u_main_addr = '" . $GLOBALS["db"]->insert_id . "' WHERE u_code=$lUserCode";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      // -----------------

      if (!isLoggedAdmin()) {
        // zaslani aktivacniho emailu
        $subj = "Aktivace nového účtu na mushow.cz/kitestore";

        $lPlain = "Dobrý den,\n\n";
        $lPlain .= "při registraci v našem eshopu byla zadána tato emailová adresa jako kontaktní. Pro dokončení ";
        $lPlain .= "registrace klikněte na následující odkaz, čímž aktivujete Váš účet a můžete se do něj následně přihlásit: \n\n";
        $lPlain .= WR . "activate/$lActivationStr\n\n";
        $lPlain .= "Pokud jste si registraci nevyžádali, pak tento email ignorujte.";

        $lHtml = "<p>Dobrý den,<br/><br/>";
        $lHtml .= "při registraci v našem eshopu byla zadána tato emailová adresa jako kontaktní. Pro dokončení ";
        $lHtml .= "registrace klikněte na následující odkaz, čímž aktivujete Váš účet a můžete se do něj následně přihlásit: <br/><br/>";
        $lHtml .= "<a href='" . WR . "activate/$lActivationStr'>" . WR . "activate/$lActivationStr</a><br/><br/>";
        $lHtml .= "Pokud jste si registraci nevyžádali, pak tento email ignorujte.</p>";

        if (sendMail($mail, $subj, $lPlain, $lHtml)) {
          $GLOBALS["rv"]->addInfo("Na Vaši emailovou adresu byla zaslána zpráva s odkazem pro aktivaci Vašeho účtu.");
        } else {
          $GLOBALS["rv"]->addError("Nepodařilo se odeslat aktivační email, pokud potíže přetrvají obraťte se prosím na správce stránek.");
          return true;
        }
      }

      if ($GLOBALS['wm'] == $this && !isLoggedAdmin()) {
        $_POST = array();

        // presmerovani na prihlaseni
        require_once "LoginWM.php";
        $GLOBALS["wm"] = new LoginWM(LOGIN);
        $GLOBALS["wm"]->reactOnActionLow();
      }

      $GLOBALS["rv"]->addInfo("Účet úspěšně vytvořen.");
      $this->setForOutput(false);
      return false;
    }

    // editace
    elseif ($this->mAction == "edit") {

      // jedinecnost uziv. jmena
      $query = "SELECT u_code FROM user WHERE u_code != " . $this->mActRow["u_code"] . " AND u_nick='" . $nick . "'";
      $result = $GLOBALS["db"]->query($query);

      if ($result->num_rows != 0) {
        $GLOBALS["rv"]->addError("Toto přihlašovací jméno je již obsazeno, zvolte prosím jiné.");
        return true;
      }

      if (!isLoggedAdmin()) {
        $query = "SELECT u_code FROM user WHERE u_code != " . $this->mActRow["u_code"] . " AND u_mail='" . $mail . "'";
        $result = $GLOBALS["db"]->query($query);

        if ($result->num_rows != 0) {
          $GLOBALS["rv"]->addError("Účet s touto emailovou adresou již existuje.");
          return true;
        }
      }

      // update v DB prihlas. udaju
      $query = "UPDATE user SET `u_nick` = '$nick', `u_mail` = '$mail'";
      if ($_POST["u_passwd"] != null && $_POST["u_passwd"] != "")
        $query .= ", `u_passwd` = '$passwd'";
      $query .= " WHERE `u_code` = " . $this->mActRow["u_code"];
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      // update v DB fakturacnich udaju
      $query = "UPDATE user_address SET `ua_company` = '$company', `ua_name` = '$name'";
      $query .= ", `ua_surname` = '$surname', `ua_street` = '$street', `ua_town` = '$town'";
      $query .= ", `ua_psc` = '$psc', `ua_state` = '$state', `ua_tel` = '$tel'";
      $query .= ", `ua_ico` = '$ico', `ua_dic` = '$dic'";
      $query .= " WHERE `ua_code` = " . $this->mActRow["u_main_addr"];
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      $GLOBALS["rv"]->addInfo("Vaše údaje byly úspěšně upraveny.");

      return true;
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {

    if ($this->mAction == "add")
      return "Registrace";
    elseif ($this->mAction == "edit")
      return "Editace profilu";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $lPromptWidth = 120;
    $lInputWidth = 200;
    $lInputWidth2 = 120;
    $whereCond = new WhereCondition();
    $lIsEdit = $this->mAction == "edit";

    $lEF = new PresentElement("<fieldset class='form'>");
    $this->addElement($lEF);

    // Prihlasovaci jmeno
    $lEF = new EditField("u_nick", "Přihlašovací jméno", $lPromptWidth, !isLoggedAdmin(), $lInputWidth2, 30);
    $lEF->addFieldAttr("onblur", "reactOnBlurField(this)");
    $this->addElement($lEF);

    // Email
    $lEF = new EditField("u_mail", "Email", $lPromptWidth, !isLoggedAdmin(), $lInputWidth, 50);
    $this->addElement($lEF);

    // Heslo
    $lEF = new EditField("u_passwd", "Heslo", $lPromptWidth, !$lIsEdit && !isLoggedAdmin(), $lInputWidth2, 20);
    $lEF->setMinLength(6);
    $lEF->setSuffix("<span class='note'>(min. 6 znaků, kombinace písmen a číslic)</span>");
    $lEF->setInputType("password");
    $this->addElement($lEF);

    // Heslo znovu
    $lEF = new EditField("u_passwd2", "Heslo znovu", $lPromptWidth, !$lIsEdit && !isLoggedAdmin(), $lInputWidth2, 20);
    $lEF->setMinLength(6);
    $lEF->setSuffix("<span class='note'>(kontrola zadaného hesla)</span>");
    $lEF->setInputType("password");
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset><fieldset class='form'>");
    $this->addElement($lEF);

    // Firma
    $lEF = new EditField("ua_company", "Firma", $lPromptWidth, false, $lInputWidth2, 50);
    $this->addElement($lEF);

    // Jmeno
    $lEF = new EditField("ua_name", "Jméno", $lPromptWidth, true, $lInputWidth2, 30);
    $this->addElement($lEF);

    // Prijmeni
    $lEF = new EditField("ua_surname", "Příjmení", $lPromptWidth, true, $lInputWidth2, 30);
    $this->addElement($lEF);

    // Ulice
    $lEF = new EditField("ua_street", "Ulice a č.p.", $lPromptWidth, true, $lInputWidth, 100);
    $this->addElement($lEF);

    // Mesto
    $lEF = new EditField("ua_town", " Město", $lPromptWidth, true, $lInputWidth2, 100);
    $this->addElement($lEF);

    // PSC
    $lEF = new EditField("ua_psc", "PSČ", $lPromptWidth, !isLoggedAdmin(), 50, 10);
    $this->addElement($lEF);

    // Stat
    $lEF = new EditCombo("ua_state", "Stát", $lPromptWidth, false, 200);
    fillStatesCombo($lEF);
    $this->addElement($lEF);

    // Telefon
    $lEF = new EditField("ua_tel", " Telefon", $lPromptWidth, !isLoggedAdmin(), $lInputWidth2, 20);
    $this->addElement($lEF);

    // ICO
    $lEF = new EditField("ua_ico", "IČO", $lPromptWidth, false, $lInputWidth2, 20);
    $this->addElement($lEF);

    // DIC
    $lEF = new EditField("ua_dic", "DIČ", $lPromptWidth, false, $lInputWidth2, 20);
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset><fieldset class='form'>");
    $this->addElement($lEF);
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {

    if ($this->mAction == "add")
      echo "<p>Pro registraci nového zákazníka, poskytněte prosím vaše fakturační údaje:</p>";
    else
      echo "<p>Zde můžete upravit vaše přihlašovací a fakturační údaje:</p>";

    //echo "<fieldset class='form'>";
    echo "  <form method='post' id='edit_form' action='" . $this->mUrl . "'>";
    printSpamCheck1();
    $this->printElements();
    printSpamCheck2();

    if ($this->mAction == "add" && !isLoggedAdmin()) {
      echo " <div class='' style='height:100%;width: 100%;'>";
      echo " <label for='conditions'>Souhlasím s obchodními podmínkami </label>";
      echo "(<a href='" . WR_PODMINKY . "' target='_blank'>podrobnosti zde</a>)&nbsp;&nbsp;";
      echo "<input type='checkbox' name='conditions' id='conditions'/></div>";
      ?>
      <div class='' style='height:100%;width: 100%; margin: 5px 0;'>
        <label for='gdpr'>Souhlasím se zpracováním osobních údajů </label>
        (<a href='<?= WR_GDPR ?>' target="_blank">podrobnosti zde</a>)
        <input type='checkbox' name='gdpr' id='gdpr' />
      </div>

      <?php

    }

    echo " <div class='td_left' style='height:100%;width: 120px;'>";
    echo " <input type='submit' class='submit' value='" . ($this->mAction == "add" ? "Odeslat registraci" : "Změnit údaje") . "'";
    echo "  onclick='return validateRegister(this.form)'/></div>";

    ?>

    </fieldset>
    </form>

    <p>(<span style='color:#551310;font-weight:bold;font-size:14px;'>*</span>Položky označené hvězdičkou jsou povinné.)</p>

    <script>
      function validateRegister(form) {
        if (!form.conditions.checked) {
          alert("Musíte potvrdit, že souhlasíte s obchodními podmínkami.");
          return false;
        }

        if (!form.gdpr.checked) {
          alert("Musíte potvrdit, že souhlasíte se zpracováním osobních údajů.");
          return false;
        }

        return true;
      };
    </script>

    <?php
  }
}
?>