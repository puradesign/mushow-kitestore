<?php
require_once "SimpleImage.php";
require_once "ArticleUtil.php";

class NewsletterEditWM extends WebModule {
  var $action;
  var $mHeader = "Přidat newsletter";
  var $mActRow;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError("Nedostatečná práva pro vstup na tuto stránku.");
      $this->setForOutput(false);
      return false;
    }

    $this->action = "add";

    if (isset($_GET["item"]))
      $this->action = "edit";

    if (($this->action != "add" && $this->action != "edit") ||
      ($this->action == "edit" && (!isset($_GET["item"]) || !is_numeric($_GET["item"])))) {
      $GLOBALS["rv"]->addError("Chyba. Pravděpodobně byla ručně upravena url stránky.");
      $this->setForOutput(false);
      return false;
    }

    // editace
    if ($this->action == "edit") {

      $query = "SELECT * FROM newsletter WHERE nl_id=" . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);

      $row = $result->fetch_assoc();

      $this->mActRow = $row;
      $this->mHeader = alterHtmlTextToPlain($row["nl_subject"]) . " - editace";

      if (empty($_POST)) {
        $_POST["nl_subject"] = alterHtmlTextToPlain($row["nl_subject"]);
        $_POST["nl_text"] = $row["nl_text"];
      }
    } elseif ($this->action == "add") {

    }

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    $subject = alterTextForDB($_POST["nl_subject"]);
    $text = $_POST["nl_text"];
    $test_email = alterTextForDB(@$_POST["test_email"]);

    // preview newsletter
    if (isset($_POST["preview"])) {
      echo $this->getNewsletterHtml($text, $subject, $this->mActRow['nl_id']);
      exit;
    }
    // test email
    elseif (isset($_POST["test"])) {
      if (empty($_POST["test_email"])) {
        $GLOBALS["rv"]->addError("Vyplň adresu, na kterou má být testovací email odeslán.");
        return false;
      }

      sendNewsletter($test_email, $subject, $this->getNewsletterHtml($text, $subject, $this->mActRow['nl_id']));

      $GLOBALS["rv"]->addInfo("Testovací email odeslán na adresu $test_email.");
    }
    // save email
    else {
      $lCond = new WhereCondition();

      // vlozeni
      if ($this->action == "add") {
        $query = "INSERT INTO newsletter (nl_subject, nl_text, nl_date_create) VALUES ";
        $query .= "('$subject', '$text', '" . date('Y-m-d H:i:s') . "')";

        $result = $GLOBALS["db"]->query($query);

        $id = $GLOBALS["db"]->insert_id;

        header("location: " . WR . "?m=" . NS_EDIT . "&item=$id");

        return false;
      }

      // editace
      elseif ($this->action == "edit") {

        // upraveni odkazu na obrazky aby se otviraly v shadowboxu
        $text = addShadowboxToLinks($text);

        // update v DB
        $query = "UPDATE newsletter SET 
          			`nl_subject` = '$subject',
          			nl_text = '$text'
                 WHERE `nl_id` = '" . $this->mActRow['nl_id'] . "'";
        $result = $GLOBALS["db"]->query($query);

        $GLOBALS["rv"]->addInfo("Změny uloženy.");
      }

      // ------
      // send newsletter
      if (isset($_POST["send"])) {
        $query = "SELECT * FROM user WHERE u_unsubscribed = 0";
        $result = $GLOBALS["db"]->query($query);

        $sentCount = 0;

        // get users
        while ($row = $result->fetch_assoc()) {
          $address = $row["u_mail"];

          // check if newsletter wasn't already sent to this address
          $query = "(SELECT esh_address as address FROM email_history WHERE esh_address = '$address'
                	AND esh_newsletter = '" . $this->mActRow['nl_id'] . "') UNION
                	(SELECT es_address as address FROM email_stack WHERE es_address = '$address'
                	AND es_newsletter = '" . $this->mActRow['nl_id'] . "')";
          $resultHist = $GLOBALS["db"]->query($query);
          $rowHist = $resultHist->fetch_assoc();
          if ($rowHist)
            continue;


          $htmlText = addSlashes($this->getNewsletterHtml($_POST["nl_text"], $subject, $this->mActRow['nl_id'], $row));

          // save to stack for later sending
          $query = "INSERT INTO email_stack (es_address, es_subject, es_text, es_newsletter,es_date) VALUES
               		 ('$address', '$subject', '$htmlText', '" . $this->mActRow['nl_id'] . "', '" . date('Y-m-d H:i:s') . "')";
          $result2 = $GLOBALS["db"]->query($query);

          $sentCount++;
        }

        $GLOBALS["rv"]->addInfo("Newsletter bude odeslán na $sentCount adres.");
      }
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {

    return $this->mHeader;
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $whereCond = new WhereCondition();
    $lOffset = 100;

    // Nazev
    $lEF = new EditField("nl_subject", "Předmět emailu", $lOffset, true,
      500, 100);
    $this->addElement($lEF);

    // Text
    $lEF = new EditCKText("nl_text", "Text", true);
    $this->addElement($lEF);

    // Testovaci email
    $lEF = new EditField("test_email", "Email pro test", $lOffset, false,
      200, 100);
    $this->addElement($lEF);
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    //	 echo "<p>Přidání/editace inzerátu.</p>";

    if ($this->action == "edit") {
      // emails to be sent and already sent
      $query = "SELECT count(*) as count FROM email_stack WHERE es_newsletter = " . $this->mActRow["nl_id"];
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();
      $emailsToSent = $row["count"];

      $query = "SELECT count(*) as count FROM email_history WHERE esh_newsletter = " . $this->mActRow["nl_id"];
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();
      $emailsSent = $row["count"];
    }

    // users to be sent
    $query = "SELECT count(distinct u_mail) as count FROM user WHERE u_unsubscribed = 0";
    $result = $GLOBALS["db"]->query($query);
    $row = $result->fetch_assoc();
    $emailsCount = $row["count"] - $emailsSent - $emailsToSent;

    echo "  <form method='post' id='edit_form' action='" . WR . "?m=" . NS_EDIT . "&amp;action=" . $this->action;
    echo ($this->action != "add" ? "&amp;item=" . $_GET["item"] : "") . "'";
    echo " style='margin: 0 0 0 -184px; width: 700px;position: relative; z-index: 20'>";

    echo "  <fieldset class='form' style='width: 100%;'>";
    $this->printElements();

    echo "  <div class='' style='padding-top:5px; padding-bottom:3px'><div class='td_left' style='height:100%;width: 650px;'>";
    echo "  <input type='submit' class='submit' value='Uložit' name='save'/>";
    echo "  <input type='submit' class='submit' value='Náhled' name='preview'/>";

    if ($this->action == "edit") {
      echo "  <input type='submit' class='submit submitTest' value='Test email' name='test'/>";
    }

    echo "  <input type='submit' class='submit submitCancel' value='Zpět' name='cancel' onClick='document.location.href=\"" . WR . "?m=" . NS_BROWSER . "\";return false;'/>";

    echo " </div>";

    if ($this->action == "edit") {
      echo "<p style='float: left; margin: 20px 10px; width: 200px; font-size: 14px; line-height: 22px'>
        		Čeká na odeslání: <strong>$emailsToSent</strong><br/>
        		Odesláno: <strong>$emailsSent</strong>
              </p>";
      echo "  <input type='submit' class='submit submitSend' onClick='if (!confirm(\"Opravdu odeslat email? Email bude odeslán na $emailsCount adres\")) return false;' 
        		value='ODESLAT NEWSLETTER ($emailsCount)' name='send' style='float: right; font-size: 14px; margin: 20px'/>";
    }

    echo "</fieldset>";

    echo " </form>";
  }

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {
    echo "<script type='text/javascript' src='" . F_CKEDITOR . "ckeditor.js?reload'></script>\n";
    //echo "<script type='text/javascript' src='".WR_SCRIPT."ckeditor_conf.js'></script>\n";
    echo "<script type='text/javascript'>
    	$(document).ready(function() {
    	
    		$('.submit').click(function() {
  				if ($(this).attr('name') == 'preview') {
  					var w = window.open('about:blank','newsletter_popup','toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,width=800,height=500,left = 100,top = 50');
  					$(this).closest('form').attr('target', 'newsletter_popup');
  					//return false;
  				}
  				else {
  					$(this).closest('form').removeAttr('target');
  				}
  			});
    	
        	CKEDITOR.config.customConfig = '" . WR_SCRIPT . "ckeditor_conf.js';
        	
        	CKEDITOR.replace('nl_text', {
                height : 350,
                resize_maxWidth : 700,
                resize_minWidth : 700,
                width : 700,
                contentsCss : '" . WR . "' + 'css/newsletter.css?reload'
            });
        });
      </script>";
  }

  /**
   * Zobrazeni emailu
   */
  function getNewsletterHtml($html, $subject, $id, $user = null) {
    $key = $email = null;
    if (!empty($user)) {
      $email = $user["u_mail"];
      $key = sha1($user['u_date_create'] . $email . "solenicko" . $user['u_code']);
    }

    return "<html>
    		<head>
    			<meta http-equiv='content-type' content='text/html; charset=utf-8' />
    			<meta http-equiv='content-language' content='cs' />
    			<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
    			<title>$subject</title>
    			
    			<style type='text/css'>
    				#outlook a {padding:0;}
    				body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
    				.ExternalClass {width:100%;}
    				.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    				#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
    				
    				img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; max-width: 600px}
					a img {border:none;}
					.image_fix {display:block;}
					p {margin: 1em 0;}
					table td {border-collapse: collapse;}
					p { line-height: 21px; font-size: 14px;}
					h2 { font-size: 17px; color: #00b0a1; text-align: left; text-decoration: underline}
					h3 { font-size: 17px; color: #7c1212; text-align: left; text-decoration: underline}
					h4 { font-size: 15px; margin: 0 0 10px 0;color: #555; text-align: left; text-decoration: none}
					
					a { color: #00b0a1; text-decoration: underline; }
    			</style>
    		</head>
            <body style=\"font-family:Helvetica, sans-serif; font-size:13px; line-height: 18px;padding: 0; min-height: 100%; 
    				background: url('" . WR . "img/newsletter_bg.png');color:#333; margin: 0; width: 100% !important; min-width: 100%;\">
    			<table width='100%' border='0' cellspacing='0' cellpadding='0' style=' width: 100% !important; min-width: 100%;'>
                <tr><td align='center' style='background: url(\"" . WR . "img/newsletter_bg.png\");'>
    			<div style=' width: 600px; padding: 5px 20px 20px 20px; background: #fff; margin: 0;min-height: 400px; text-align: left;'>
    				<div style='display:none;'>$html</div>
    				<p style='text-align: center; font-size: 11px; color: #888; margin: 0'>
    					Nezobrazuje se Vám email správně? <a href='" . WR . "newsletter/$id' style='color:#16837a'>Klikněte zde.</a></p>
    				<h1 style='display:block; font-size: 0; width: 600px; height:74px; padding:0; margin:5px 0 25px 0;
    							background: url(\"" . WR . "img/newsletter_top.jpg\") no-repeat'>
    					Mushow newsletter</h1>
                    
                    $html
                    
                    <div style='width: 600px; margin: 45px 0; height: 30px '>
                    	<a href='http://mushow.cz' style='display: block; width:170px; margin:0; padding: 10px; 
                    		border-radius: 10px; background: #00b0a1; color: #fff; text-align: center; float: left;
                    		text-decoration: none; font-weight: 600; font-size: 15px'>Mushow News</a>
                    		
                    	<a href='http://mushow.cz/kitestore' style='display: block; width:170px; margin:0 0 0 30px; padding: 10px 0; 
                    		border-radius: 10px; background: #7c1212; color: #fff; text-align: center; float: left;
                    		text-decoration: none; font-weight: 600; font-size: 15px'>Mushow e-shop</a>
                    		
                    	<a href='http://www.facebook.com/pages/Mushow-community/361467200304' 
                    		style='display: block; width:170px; margin:0 0 0 30px; padding: 10px 0; 
                    		border-radius: 10px; background: #185ea0; color: #fff; text-align: center; float: left;
                    		text-decoration: none; font-weight: 600; font-size: 15px'>Mushow facebook</a>
                    </div>
                    
                    <div style='display:block; font-size: 0; width: 600px; height:34px; padding:0; margin:5px 0;
    							background: url(\"" . WR . "img/newsletter_foot.jpg\") no-repeat'>
    					Mushow newsletter</div>
    				<p style='text-align: center; font-size: 11px; color: #888; margin: 0'>
    					Nechcete dostávat tyto emaily? <a href='" . WR . "unsubscribe/?key=$key&email=$email' style='color:#16837a'>Odhlásit se z odběru novinek.</a></p>
                </div>
                </td></tr></table>
            </body> 
    	</html>";
  }
}
?>