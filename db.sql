-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 29, 2014 at 02:15 PM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `kitestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `advert`
--

CREATE TABLE `advert` (
  `ad_code` int(11) NOT NULL AUTO_INCREMENT,
  `ad_user` int(11) DEFAULT NULL,
  `ad_title` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ad_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ad_email` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ad_text` text COLLATE utf8_czech_ci NOT NULL,
  `ad_phone` varchar(15) COLLATE utf8_czech_ci DEFAULT NULL,
  `ad_price` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `ad_type` int(11) NOT NULL DEFAULT '0',
  `ad_categ` int(11) NOT NULL DEFAULT '0',
  `ad_place` int(11) NOT NULL DEFAULT '0',
  `ad_date_create` datetime NOT NULL,
  `ad_ip` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`ad_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `advert`
--

INSERT INTO `advert` (`ad_code`, `ad_user`, `ad_title`, `ad_name`, `ad_email`, `ad_text`, `ad_phone`, `ad_price`, `ad_type`, `ad_categ`, `ad_place`, `ad_date_create`, `ad_ip`) VALUES
(1, 1, 'test', 'peeta kulisakkk', 'testkk', 'dsad heeej\r<br />sd\r<br /> sd\r<br />  ds ds ds dsd  thehyrj tuk uj h', '44', '12.500 Kc', 0, 2, 0, '2010-10-08 21:01:31', '::1'),
(4, 4, 'blabla', 'dsd dsd', 'terihei@gmail.com', 'blabla', 'dsd', 'blabla', 1, 2, 0, '2012-10-18 12:20:46', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `advert_categ`
--

CREATE TABLE `advert_categ` (
  `adc_code` int(11) NOT NULL AUTO_INCREMENT,
  `adc_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`adc_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `advert_categ`
--

INSERT INTO `advert_categ` (`adc_code`, `adc_name`) VALUES
(1, 'Kites'),
(2, 'Boards'),
(3, 'Ostatní');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `a_code` int(11) NOT NULL AUTO_INCREMENT,
  `a_user` int(11) NOT NULL,
  `a_title` varchar(80) COLLATE utf8_czech_ci NOT NULL,
  `a_description` varchar(500) COLLATE utf8_czech_ci DEFAULT NULL,
  `a_text` text COLLATE utf8_czech_ci,
  `a_publish` tinyint(1) DEFAULT NULL,
  `a_type` int(11) NOT NULL DEFAULT '0',
  `a_date_create` datetime NOT NULL,
  `a_date_update` datetime NOT NULL,
  `a_for_comments` tinyint(1) NOT NULL DEFAULT '1',
  `a_url` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `a_ip` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`a_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`a_code`, `a_user`, `a_title`, `a_description`, `a_text`, `a_publish`, `a_type`, `a_date_create`, `a_date_update`, `a_for_comments`, `a_url`, `a_ip`) VALUES
(1, 1, 'Kontakt', 'Mushow kitestore kontakt', '<h3 style="margin-top:40px">\r\n	Mushow s.r.o.</h3>\r\n<p style="margin-bottom:40px">\r\n	Email: <a href="mailto:obchod@mushow.cz">obchod@mushow.cz</a></p>\r\n', NULL, 0, '2010-11-01 18:37:38', '2010-11-01 19:06:20', 1, 'kontakt', NULL),
(2, 1, 'Obchodní podmínky', 'obchodní podmínky eshopu Mushow kitestore', '<div align="justify">\r\n<b>1) VŠEOBECNÁ USTANOVENÍ</b><br />\r\n<b>\r\n</b>Kupující učiněním závazné objednávky stvrzuje, že akceptuje\r\nObchodní podmínky pro dodávku zboží vyhlášené prodávajícím. Vztahy mezi\r\nkupujícím a prodávajícím se řídí těmito obchodními podmínkami, které\r\njsou zároveň pro obě strany závazné.<br />\r\n<br />\r\n<b>\r\n2) OBJEDNÁNÍ ZBOŽÍ A SLUŽEB</b><br />\r\n<b>\r\n</b>Nabízíme vám možnost nákupu přímo z Vašeho domova, kde můžete\r\nnakupovat nonstop. Objednávat si můžete jednoduše a pohodlně pomocí\r\nnákupního košíku, telefonu, e-mailu, či písemně na naši adrese.<br />\r\n\r\nPředmětem smlouvy je pouze zboží uvedené v kupní smlouvě – objednávce.\r\nRozměry, váha, výkony, kapacita a ostatní údaje uvedené na našich\r\nstránkách, v katalozích, prospektech a jiných tiskovinách jsou údaji\r\nnezávaznými a vycházejícími z údajů výrobců. V případě nesrovnalostí\r\nVás samozřejmě budeme kontaktovat.<br />\r\nZavazujeme se, že svým odběratelům budeme dodávat jen zboží v\r\nperfektním stavu a v souladu se specifikacemi či vlastnostmi obvyklými\r\npro daný druh zboží vyhovujících daným normám, předpisům a nařízením\r\nplatným na území České republiky a zároveň řádně vybavené, záručními listy a seznamy pozáručních servisních\r\nstředisek je-li to pro daný druh zboží obvyklé.<br />\r\nPodmínkou pro naplnění platnosti naší elektronické objednávky je\r\nvyplnění veškerých požadovaných údajů a náležitostí uvedených v\r\nobjednávkovém formuláři. Objednávka je zároveň návrhem kupní smlouvy,\r\nkdy samotná kupní smlouva posléze vzniká samotným dodáním zboží. K\r\nuzavření kupní smlouvy se vyžaduje formální potvrzení objednávky\r\nprodávajícím. V jednotlivých, zejména cenově náročnějších případech, si\r\nprodávající vyhrazuje právo k vzniku smlouvy potvrzením objednávky\r\nnejlépe osobně nebo telefonicky a úhradu finanční zálohy kupujícím.<br />\r\nV případě, že v průběhu doby, kdy bylo zboží objednáno, došlo k výrazné\r\nzměně kurzu zahraniční měny nebo ke změně ceny či dodávaného sortimentu\r\nze strany dodavatele, má naše firma právo objednávku po dohodě s\r\nkupujícím modifikovat nebo od ní jednostranně s okamžitou platností\r\nodstoupit. Stejné oprávnění si vyhrazujeme i v případě, kdy výrobce\r\npřestane dodávat objednaný produkt nebo uvede na trh novou verzi\r\nproduktu popřípadě výrazným způsobem změní cenu produktu.<br />\r\n<br />\r\n<b>3) POTVRZENÍ OBJEDNÁVKY</b><br />\r\nObjednávka je přijata do 24 hodin, potvrzení objednávky vám zašleme\r\ne-mailem, o odeslání budete taktéž informováni e-mailem. V případě\r\nnejasností Vás budeme samozřejmě kontaktovat.<br />\r\n<br />\r\n<b>4) ZRUŠENÍ OBJEDNÁVKY</b><br />\r\nKaždou objednávku můžete do 24 hodin zrušit a to telefonicky, či\r\ne-mailem a to bez udání důvodu. Stačí uvést jméno, e-mail a popis\r\nobjednaného zboží či služby.<br />\r\n\r\n<br />\r\n<b>5) BALNÉ A POŠTOVNÉ</b><br />\r\nObjednané zboží vám zašleme poštou jako dobírku. V případě, že jste na\r\nnaší rozvozové trase bude o dodání informováni telefonicky nebo\r\nemailem. Je možno zvolit i jiný způsob dopravy a to dle specifikací\r\njednotlivé objednávky. Cena poštovného se může lišit i podle váhy\r\nobjednaného zboží, způsou přepravy (expresní dodánní) nebo na základě\r\ndodání zboží do zahraničí.<br />\r\n<br />\r\n<b>6) DODACÍ LHŮTA</b><br />\r\nDodací lhůta je od 2-15 dnů, není-li uvedeno jinak. V případě, že\r\nněkteré zboží nebude skladem, budeme Vás neprodleně kontaktovat.<br />\r\n<br />\r\n<b>\r\n7) VÝMĚNA ZBOŽÍ</b><br />\r\nV případě potřeby Vám nepoužité a nepoškozené zboží vyměníme za jiný\r\ndruh. Zboží stačí zaslat doporučeným balíkem (ne na dobírku) na naši\r\nadresu. Náklady spojené s vyměňováním nese v plné výši kupující. Na\r\nvýměnu zboží nebude brán zřetel, zhodnotí li prodávající tuto službu\r\njako zneužití této obchodní politiky ze strany objednatele.<br />\r\n<br />\r\n\r\n<b>9) REKLAMACE A ZÁRUKA</b><br />\r\nPřípadné reklamace vyřídíme k Vaší spokojenosti individuální dohodou s\r\nVámi a v souladu s platným právním řádem. Kupující je povinen zboží po\r\njeho převzetí prohlédnout tak, aby zjistil případné vady a poškození.\r\nPřípadné vady je kupující povinen neprodleně hlásit naší firmě. Za vady\r\nvzniklé přepravcem neručíme.<br />\r\n<br />\r\nNa veškeré zboží se vztahuje zákonná lhůta 24 měsíců, pokud není uvedeno jinak. Záruka se vztahuje pouze na výrobní vady.<br />\r\n<br />\r\n<b>Záruka se nevztahuje na:</b><br />\r\na) vady vzniklé běžným používáním<br />\r\nb) nesprávným použitím výrobku<br />\r\nc) nesprávným skladováním<br />\r\n<br />\r\n\r\nTato záruka neplatí jestli ji Mushow s.r.o. zhodnotí jako zneužívání této obch. politiky.<br />\r\nZáruka neplatí u zboží nakupovaného mimo www.kite-shop.cz.<br />\r\n<br />\r\n<b>Postup při reklamaci:</b><br />\r\n<br />\r\n1) informujte nás o reklamaci telefonicky, e-mailem, či písemně<br />\r\n2) zboží zašlete jako doporučený balík (ne na dobírku) na naši adresu<br />\r\n3) do zásilky uveďte důvod reklamace, vaši adresu<br />\r\n4) doklad o nabytí reklamovaného zboží v našem obchodě<br />\r\n<br />\r\n\r\nVaši reklamaci vyřídíme co nejrychleji, nejpozději do 30 dnů od jejího\r\nvzniku, tedy převzetí zboží naší firmou. V případě delších reklamací\r\nVás budeme neprodleně informovat o stavu reklamace.<br />\r\n<br />\r\n<br />\r\n<div align="center">\r\n<a title="reklam_rad" name="reklam_rad"></a><u><b><span style="font-size: 10pt">REKLAMAČNÍ ŘÁD</span></b><br />\r\n</u>\r\n</div>\r\n<br />\r\nReklamační řád obsahuje informace pro zákazníka při uplatňování reklamace zboží nakoupeného na www.kite-shop.cz<br />\r\n<br />\r\n<b>Povinnosti kupujícího</b><br />\r\nKupující je povinen dodané zboží při převzetí prohlédnout a bez zbytečného odkladu informovat prodávajícího o zjištěných závadách.<br />\r\n\r\n<br />\r\n<b>Kupující může oprávněnou reklamaci podat jednou z následujících možností:</b><br />\r\na) Poštou na adresu provozovatele za použití reklamačního formuláře.<br />\r\nb) Osobním doručením.<br />\r\n<br />\r\n<b>Kupující je povinen uvést:</b><br />\r\na) Celé své jméno, adresu bydliště a alespoň jeden funkční tel. kontakt.<br />\r\nb) Co nejvýstižnější popis závad a jejich projevů.<br />\r\nc) Doložit doklad o vlastnictví zboží (přikládaná faktura, PPD).<br />\r\n<br />\r\n\r\n<b>Povinnosti prodávajícího</b><br />\r\nProdávající rozhodne o reklamaci nejpozději do 5 pracovních dnů a vyrozumí o tom kupujícího elektronickou poštou, pokud se s kupujícím nedohodne jinak. <br />\r\nReklamace včetně vady bude vyřízena bez zbytečného odkladu, nejpozději do jednoho měsíce ode dne uplatnění reklamace, pokud se prodávající s kupujícím nedohodne jinak.<br />\r\n<br />\r\n<b>Reklamace uplatněná v záruční době</b><br />\r\nTento reklamační řád byl zpracován dle Občanského zákoníku a vztahuje se na zboží jehož reklamace byla uplatněna v záruční době.<br />\r\nKe každému zboží je přikládána faktura nebo PPD, který může sloužit zároveň jako záruční, pokud tento není přiložen (závislé na výrobci). Převzetí zboží a souhlas se záručními podmínkami stvrzuje zákazník podpisem faktury nebo PPD. Pokud není zboží osobně odebráno, rozumí se převzetím zboží okamžik, kdy zboží přebírá od dopravce.<br />\r\nPokud odběratel zjistí jakýkoliv rozdíl mezi fakturou, PPD a skutečně dodaným zbožím (v druhu nebo množství) nebo neobdržel se zásilkou správně vyplněnou fakturu nebo PPD, je povinen podat ihned (nejpozději do 72 hodin) písemně zprávu adresovanou na jméno obchodníka, který vyhotovil fakturu. Pokud tak neučiní, vystavuje se nebezpečí, že mu pozdější případná reklamace nebude uznána.<br />\r\n<br />\r\n<b>Záruční podmínky</b><br />\r\n\r\nDélka záruky je standardně 24 měsíců (mimo jiné je vyznačena na záručním listě) kde je i výrobní číslo výrobku. Tato doba začíná dnem vystavení dokladu o prodeji a prodlužuje se o dobu, po kterou byl výrobek v záruční opravně. V případě výměny se záruční doba neprodlužuje. Zákazník dostane nový záruční list, kde bude uvedeno nové i původní výrobní číslo. Další případná reklamace se bude uplatňovat na základě faktury nebo PPD. Ke každé položce musí být připojen přesný popis závad a četnost výskytu.<br />\r\nServisní středisko po vyřízení reklamace vyzve zákazníka k odběru zboží, případně jej zašle na své náklady a riziko zpět.<br />\r\n<br />\r\n<b>Nárok na uplatnění záruky zaniká v následujících případech:</b><br />\r\n- poškozením zboží při přepravě (tyto škody je nutno řešit s dopravcem při převzetí),<br />\r\n- porušením ochranných pečetí a nálepek, pokud na výrobku jsou,<br />\r\n- neodborným zacházením či obsluhou, použitím, které jsou v rozporu s uživatelskou příručkou, <br />\r\n- používáním zboží v podmínkách, které neodpovídají svými parametry parametrům uvedeným v dokumentaci,<br />\r\n- zboží bylo poškozeno živly,<br />\r\n\r\n- zboží bylo poškozeno nadměrným zatěžováním nebo používáním v rozporu s podmínkami uvedenými v dokumentaci,<br />\r\n- při opakovaném dodání neúplných průvodních dokladů (faktura, PPD atd.) je dodavatel oprávněn účtovat odběrateli poplatek 100,- Kč za dohledání těchto dokladů.<br />\r\nDodavatel si vyhrazuje právo nahradit vadné a neopravitelné zboží jiným se stejnými parametry.<br />\r\n<br />\r\n<br />\r\n<span style="font-size: 10pt">Reklamace zboží:</span><br />\r\n<b>Rozhodnete-li se pro vrácení zboží v garanční lhůtě 14 dní, prosíme Vás o dodržení všech níže uvedených podmínek:</b><br />\r\n1. zboží nesmí být použito <br />\r\n2. zboží nesmí být žádným způsobem poškozeno<br />\r\n3. zboží musí být v originálním obalu a kompletní (včetně příslušenství, návodu, atd.) <br />\r\n\r\n4. společně s vraceným zbožím je třeba zaslat také veškeré doklady vydané při prodeji<br />\r\n5. poštovné spojené se zasláním zboží zpět k prodejci hradí kupující, vracené zboží by mělo být při přepravě pojištěno <br />\r\n6. zboží nesmí být zasíláno na dobírku, takováto zásilka nebude přijata.<br />\r\n<br />\r\n<b>Pří uplatnění výměny nebo vrácení zboží postupujte takto:<br />\r\n</b>1. Zákazník nejprve vyplní reklamační protokol, který najde v sekci Obchodní podmínky. Tento protokol obsahuje všechny důležité informace, které jsou nezbytné pro bezproblémové vyřízení reklamace. <br />\r\n2. Vyplněný protokol je třeba vytisknout a opatřit podpisem.<br />\r\n3. Protokol musí být vložen do zásilky, která bude obsahovat reklamované zboží. Zboží je třeba zabalit tak, aby obal dostatečně bránil poškození reklamovaného výrobku během přepravy (včetně obalu výrobku).<br />\r\n4. Vyřízení reklamace bude do 5 pracovních dnů od přijetí zboží.<br />\r\n\r\n<b><br />\r\nZávěrečná ustanovení</b><br />\r\nTento reklamační řád nabývá účinnost 22. března 2008. <i>Změny reklamačního řádu vyhrazeny</i>.<br />\r\n<br />\r\n\r\n<br />\r\n<br />\r\n<div align="center">\r\n<a title="ochrna" name="ochrna"></a><u><b><span style="font-size: 10pt">OCHRANA OSOBNÍCH ÚDAJŮ</span></b></u><br />\r\n\r\n</div>\r\n<br />\r\nProhlášení o ochraně osobních údajů<br />\r\nMushow s.r.o. neshromažďuje žádná osobní data, která by mohla identifikovat specifickou osobu, kromě případů, kdy osoba sama poskytne Mushow s.r.o. tato data dobrovolně. Takováto data mohou být získána v případě, kdy se osoba dobrovolně zaregistruje za účelem využívání služeb serveru Mushow s.r.o., účastní se průzkumů, účastní se hlasování atd. Jakékoliv osobní informace identifikující konkrétní osobu nebudou předány, ani prodány třetí straně, kromě případů kdy na to uživatel bude upozorněn v době sběru dat.<br />\r\nMushow s.r.o. si vyhrazuje právo provádět analýzy o chování uživatelů na svých internetových stránkách. Mezi tyto analýzy patří např.: měření návštěvnosti, počet uživatelů shlédnuvších reklamní banner a počet kliknutí na jednotlivý banner, tato data jsou k dispozici též jednotlivým zadavatelům reklamy - vždy jako statistický přehled, nikoliv jmenovitě. Uživatelé by také měli vzít na vědomí, že data, která dobrovolně poskytnou do diskuzních fór nebo jiných automaticky generovaných stránek mohou být použita třetí stranou. Takovéto využití osobních informací však nelze kontrolovat a Mushow s.r.o. za toto nemůže nést a neponese žádnou odpovědnost.<br />\r\nUživatelé by si měli být vědomi skutečnosti, že některé informace o uživatelích mohou být automaticky sbírány v průběhu standardních operací našeho serveru (např. IP adresa uživatelova počítače) a také při použití cookies (malé textové soubory, které se ukládají na uživatelově počítači a server podle nich dokáže rozpoznat uživatele, který ho již jednou navštívil a poté zaznamenávat jeho chování a podle toho například přizpůsobit design a obsah nebo lépe zaměřovat reklamní kampaně). Cookies nejsou programy, které by mohly způsobit škodu na uživatelově počítači. Většina prohlížečů nabízí možnost neakceptovat cookies - elekronický obchod však nebude bez povolených cookies fungovat korektně.<br />\r\nNa žádost uživatele podnikne Mushow s.r.o. veškeré finančně přiměřené kroky k odstranění všech osobních dat daného uživatele.<br /><br />\r\n<p style=''margin-bottom: 30px''><i>Toto prohlášení nabývá na platnosti 1.října 2010</i></p>\r\n<br />\r\n</div>', NULL, 0, '2010-11-01 19:08:55', '2010-11-01 19:08:58', 1, 'obchodni-podminky', NULL),
(3, 1, 'Slevový systém', ' ', ' ', 1, 0, '2010-11-11 20:35:31', '2010-11-11 20:35:35', 1, 'slevovy-system', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fpost`
--

CREATE TABLE `fpost` (
  `f_code` int(11) NOT NULL AUTO_INCREMENT,
  `f_father` int(11) DEFAULT NULL,
  `f_user` int(11) DEFAULT NULL,
  `f_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `f_mail` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `f_web` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  `f_text` text COLLATE utf8_czech_ci NOT NULL,
  `f_date` datetime NOT NULL,
  `f_id` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `f_head` int(11) NOT NULL,
  `f_ip` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`f_code`),
  KEY `f_user` (`f_user`),
  KEY `f_father` (`f_father`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=39 ;

--
-- Dumping data for table `fpost`
--

INSERT INTO `fpost` (`f_code`, `f_father`, `f_user`, `f_name`, `f_mail`, `f_web`, `f_text`, `f_date`, `f_id`, `f_head`, `f_ip`) VALUES
(32, NULL, NULL, 'peter', 'p@k.cz', '', '<p>\r\n	heheeeeee</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	hehe</p>\r\n', '2010-08-01 01:09:34', '1', 1, '::1'),
(33, NULL, NULL, 'testik', 'l@l.l', '', '<p>\r\n	luliiiiiiiiiii</p>\r\n<p>\r\n	nnnnnnnnnnnnnnnnnnnnnnnnnnnnn</p>\r\n', '2010-08-01 01:11:54', '2', 2, '::1'),
(34, 33, NULL, 'pett', 'p@k.cz', '', '<p>\r\n	odpoveeeeeeeee<u>eeeeeeee</u>eeeeeed heh</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>heh</strong></p>\r\n', '2010-08-01 01:24:13', '2.1', 2, '::1'),
(35, NULL, NULL, 'pett', 'p@k.cz', '', '<p>\r\n	terekkaaaa</p>\r\n', '2010-09-10 19:45:25', '3', 3, '::1'),
(36, 35, NULL, 'ahoj', 'p@k.cz', '', '<p>\r\n	odpevode na terkaaaaaaa</p>\r\n', '2010-09-10 20:33:43', '3.1', 3, '::1'),
(37, NULL, NULL, 'dsd', 'dsd@ds.ds', '', '<p>\r\n	ds</p>\r\n', '2010-11-14 23:30:22', '4', 4, '::1'),
(38, 37, 1, 'admin', 'ds@ds.cs', '', '<p>\r\n	dsdsd</p>\r\n', '2010-11-14 23:34:22', '4.1', 4, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `ks_enum`
--

CREATE TABLE `ks_enum` (
  `kse_code` int(11) NOT NULL AUTO_INCREMENT,
  `kse_type` int(3) NOT NULL,
  `kse_descr` varchar(80) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`kse_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ks_enum`
--

INSERT INTO `ks_enum` (`kse_code`, `kse_type`, `kse_descr`) VALUES
(1, 1, 'dobírkou'),
(2, 1, 'hotově (při předání)'),
(3, 1, 'předem na účet');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `ori_code` int(11) NOT NULL AUTO_INCREMENT,
  `ori_name` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `ori_head` int(11) NOT NULL,
  `ori_price` int(6) NOT NULL,
  `ori_count` int(3) NOT NULL,
  `ori_url` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `ori_item_row_code` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ori_code`),
  KEY `orderi_ibfk_1` (`ori_head`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=42 ;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`ori_code`, `ori_name`, `ori_head`, `ori_price`, `ori_count`, `ori_url`, `ori_item_row_code`) VALUES
(10, 'Slingshot Fuel 2010 7m2', 4, 30000, 1, 'slingshot/kites/v/slingshot-fuel-2010', 0),
(11, 'Slingshot Fuel 2010 11m2', 4, 32000, 2, 'slingshot/kites/v/slingshot-fuel-2010', 0),
(12, 'Slingshot Fuel 2010 13m2', 4, 33000, 1, 'slingshot/kites/v/slingshot-fuel-2010', 0),
(16, 'Slingshot Fuel 2010 7m2', 6, 30000, 1, 'slingshot/kites/v/slingshot-fuel-2010', 0),
(17, 'Slingshot Fuel 2010 11m2', 6, 32000, 2, 'slingshot/kites/v/slingshot-fuel-2010', 0),
(18, 'Slingshot Fuel 2010 13m2', 6, 33000, 1, 'slingshot/kites/v/slingshot-fuel-2010', 0),
(22, 'Slingshot Fuel 2010 7m2', 8, 30000, 1, 'slingshot/kites/v/slingshot-fuel-2010', 0),
(23, 'Slingshot Fuel 2010 11m2', 8, 32000, 2, 'slingshot/kites/v/slingshot-fuel-2010', 0),
(24, 'Slingshot Fuel 2010 13m2', 8, 33000, 1, 'slingshot/kites/v/slingshot-fuel-2010', 0),
(25, 'Slingshot Fuel 2010 (5m2)', 9, 27000, 1, 'slingshot/kites/v/slingshot-fuel-2010', 2),
(26, 'Slingshot Fuel 2010 (13m2)', 10, 33000, 1, 'slingshot/kites/v/slingshot-fuel-2010', 6),
(27, 'Slingshot Fuel 2010 (5m2)', 11, 27000, 1, 'slingshot/kites/v/slingshot-fuel-2010', 2),
(28, 'Slingshot Fuel 2010 (5m2)', 12, 28000, 2, 'slingshot/kites/v/slingshot-fuel-2010', 2),
(29, 'Slingshot Fuel 2010 (5m2)', 13, 27000, 1, 'slingshot/kites/v/slingshot-fuel-2010', 2),
(30, 'Slingshot Fuel 2010 (13m2)', 14, 33000, 3, 'slingshot/kites/v/slingshot-fuel-2010', 6),
(31, 'Slingshot RPM 2010 (9m)', 15, 28000, 1, 'slingshot/kites/v/slingshot-rpm-2010', 8),
(32, 'Slingshot RPM 2010 (9m)', 16, 28000, 1, 'slingshot/kites/v/slingshot-rpm-2010', 8),
(33, 'Slingshot RPM 2010 (11m)', 17, 28000, 2, 'slingshot/kites/v/slingshot-rpm-2010', 9),
(34, 'Slingshot RPM 2010 (11m)', 18, 28000, 1, 'slingshot/kites/v/slingshot-rpm-2010', 9),
(35, 'Ful Cell England Polished White (7m2)', 19, 30000, 1, 'slingshot/kites/v/ful-cell-england-polished-white', 3),
(36, 'Ful Cell England Polished White (11m2)', 19, 32000, 1, 'slingshot/kites/v/ful-cell-england-polished-white', 5),
(37, 'Ful Cell England Polished White (13m2)', 20, 33000, 1, 'slingshot/kites/v/ful-cell-england-polished-white', 6),
(41, 'test', 21, 1000, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `shop_item`
--

CREATE TABLE `shop_item` (
  `si_code` int(11) NOT NULL AUTO_INCREMENT,
  `si_title` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `si_descr` varchar(500) COLLATE utf8_czech_ci NOT NULL,
  `si_text` text COLLATE utf8_czech_ci,
  `si_type` int(11) NOT NULL,
  `si_brand` int(11) NOT NULL,
  `si_price` int(6) NOT NULL,
  `si_fake_price` int(11) DEFAULT NULL,
  `si_count` int(3) NOT NULL,
  `si_url` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `si_dcreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `si_hot` tinyint(1) NOT NULL DEFAULT '0',
  `si_hot_date` datetime NOT NULL,
  `si_online` tinyint(1) NOT NULL DEFAULT '1',
  `si_onsale` tinyint(1) NOT NULL DEFAULT '0',
  `si_banner` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`si_code`),
  KEY `si_ibfk_1` (`si_type`),
  KEY `si_ibfk_2` (`si_brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `shop_item`
--

INSERT INTO `shop_item` (`si_code`, `si_title`, `si_descr`, `si_text`, `si_type`, `si_brand`, `si_price`, `si_fake_price`, `si_count`, `si_url`, `si_dcreate`, `si_hot`, `si_hot_date`, `si_online`, `si_onsale`, `si_banner`) VALUES
(6, 'Ful Cell England Polished White', 'Slingshot Fuel je legendární kite, který má za sebou již 9 leté období vývoje, tzn že první Fuel byl vyroben v roce 2001. Fuel je považován za zcela nejdokonalejší C kite na trhu vůbec. Mnohé z dokonalých vychytávek Fuelu přebraly i kity od jiných firem. Fuel 2010 navíc letos přichází i s novým řídícim systémem.', '<h2>\r\n	&Uacute;vod</h2>\r\n<p style="text-align: justify; ">\r\n	M&aacute;me tady konec roku 2009 a s n&iacute;m i nov&yacute; Slingshot Fuel 2010. Kite m&aacute; za sebou již děvet roků v&yacute;voje a na jeho vlastnostech je to patřičně zn&aacute;t. Možn&aacute; nevid&iacute;te rozd&iacute;ly mezi přede&scaron;l&yacute;m a aktu&aacute;ln&iacute;m modelem, ale rozdhodně ten rozd&iacute;l poc&iacute;t&iacute;te. Je&scaron;tě než se začtete do podrobn&eacute;ho popisu je třeba se zeptat, zda jste zač&aacute;tečnici, nebo pokročil&iacute; rideři s freestyle ambicema. Pokud zač&iacute;n&aacute;te, nem&aacute; smysl, aby jste v čten&iacute; pokračovali, protože nov&yacute; Fuel je hračka pouze pro velmi zku&scaron;en&eacute; jezdce až experty. A &scaron;el bych je&scaron;tě d&aacute;l, Fuel je kite určen&yacute; pro ridery s vyloženě freestylov&yacute;mi ambicemi a předev&scaron;&iacute;m pro jezdce vyzn&aacute;vaj&iacute;c&iacute; nekompromisn&iacute; wakestyle. Pokud se tedy řad&iacute;te do skupiny unhooked riderů, je v&aacute;m Fuel 2010 &scaron;it&yacute; přimo na m&iacute;ru.</p>\r\n<p>\r\n	<a href="http://localhost:8888/kiteshop/user_files/images/HMS_7147.JPG" rel="shadowbox[img]"><img alt="" height="261" src="http://localhost:8888/kiteshop/user_files/images/nahledVHMS_7147.JPG" width="530" /></a></p>\r\n', 1, 1, 28000, 32000, 0, 'ful-cell-england-polished-white', '0000-00-00 00:00:00', 1, '2010-10-14 22:19:38', 1, 1, 1),
(7, 'Slingshot RPM 2010', 'Slingshot RPM je full de-power Open &quot;C&quot; kite u kterého technici využili své znalosti z konstruování C-kites a kitů hybridních. RPM je tedy kite hybridního stylu s vlastnostmi C kitu a možností nastavení do maximálního výkonu pro sportovní, až závodní jízdu.', '<p>\r\n	<span class="Apple-style-span" style="font-size: 18px; font-weight: bold; ">&Uacute;vod</span></p>\r\n<p style="text-align: justify; ">\r\n	M&aacute;me tady konec roku 2009 a s n&iacute;m i nov&yacute; Slingshot Fuel 2010. Kite m&aacute; za sebou již děvet roků v&yacute;voje a na jeho vlastnostech je to patřičně zn&aacute;t. Možn&aacute; nevid&iacute;te rozd&iacute;ly mezi přede&scaron;l&yacute;m a aktu&aacute;ln&iacute;m modelem, ale rozdhodně ten rozd&iacute;l poc&iacute;t&iacute;te. Je&scaron;tě než se začtete do podrobn&eacute;ho popisu je třeba se zeptat, zda jste zač&aacute;tečnici, nebo pokročil&iacute; rideři s freestyle ambicema. Pokud zač&iacute;n&aacute;te, nem&aacute; smysl, aby jste v čten&iacute; pokračovali, protože nov&yacute; Fuel je hračka pouze pro velmi zku&scaron;en&eacute; jezdce až experty. A &scaron;el bych je&scaron;tě d&aacute;l, Fuel je kite určen&yacute; pro ridery s vyloženě freestylov&yacute;mi ambicemi a předev&scaron;&iacute;m pro jezdce vyzn&aacute;vaj&iacute;c&iacute; nekompromisn&iacute; wakestyle. Pokud se tedy řad&iacute;te do skupiny unhooked riderů, je v&aacute;m Fuel 2010 &scaron;it&yacute; přimo na m&iacute;ru.</p>\r\n<p>\r\n	<a href="http://localhost:8888/kiteshop/user_files/images/HMS_7147.JPG" rel="shadowbox[img]"><img alt="" height="261" src="http://localhost:8888/kiteshop/user_files/images/nahledVHMS_7147.JPG" style="cursor: default; " width="530" /></a></p>\r\n', 1, 1, 27000, NULL, 0, 'slingshot-rpm-2010', '0000-00-00 00:00:00', 1, '2010-10-12 23:55:21', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shop_order`
--

CREATE TABLE `shop_order` (
  `or_code` int(11) NOT NULL AUTO_INCREMENT,
  `or_descr` varchar(500) COLLATE utf8_czech_ci DEFAULT NULL,
  `or_transport` int(11) NOT NULL,
  `or_trans_price` int(4) NOT NULL DEFAULT '0',
  `or_trans_text` varchar(100) COLLATE utf8_czech_ci NOT NULL DEFAULT 'osobní odběr',
  `or_user` int(11) NOT NULL,
  `or_name` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `or_surname` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `or_company` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `or_street` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `or_town` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `or_psc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `or_state` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `or_tel` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `or_ico` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `or_dic` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `or_date` datetime NOT NULL,
  `or_date_due` datetime NOT NULL,
  `or_number` int(11) NOT NULL,
  `or_order_state` int(2) NOT NULL DEFAULT '1',
  `or_fa_name` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `or_fa_surname` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `or_fa_company` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `or_fa_street` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `or_fa_town` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `or_fa_psc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `or_fa_state` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `or_fa_tel` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `or_fa_ico` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `or_fa_dic` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `or_email` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `or_price_sum` float(11,2) NOT NULL,
  `or_pay_type` int(11) NOT NULL DEFAULT '1',
  `or_pay_text` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`or_code`),
  KEY `order_ibfk_1` (`or_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=22 ;

--
-- Dumping data for table `shop_order`
--

INSERT INTO `shop_order` (`or_code`, `or_descr`, `or_transport`, `or_trans_price`, `or_trans_text`, `or_user`, `or_name`, `or_surname`, `or_company`, `or_street`, `or_town`, `or_psc`, `or_state`, `or_tel`, `or_ico`, `or_dic`, `or_date`, `or_date_due`, `or_number`, `or_order_state`, `or_fa_name`, `or_fa_surname`, `or_fa_company`, `or_fa_street`, `or_fa_town`, `or_fa_psc`, `or_fa_state`, `or_fa_tel`, `or_fa_ico`, `or_fa_dic`, `or_email`, `or_price_sum`, `or_pay_type`, `or_pay_text`) VALUES
(4, 'tradaaaa <br /> <br />aa', 1, 0, 'osobní odběr', 1, 'brouk', 'pytlik', '', 'honluu', 'hawaii', '444', 'Česká republika', '4444', '', '', '2010-12-12 00:00:00', '2010-11-24 20:31:37', 10001, 2, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 124000.00, 1, ''),
(6, 'tradaaaa <br /> <br />aa', 1, 0, 'osobní odběr', 1, 'brouk', 'pytlik', '', 'honluu', 'hawaii', '444', 'Česká republika', '4444', '', '', '2010-10-10 21:21:46', '2010-11-24 20:31:37', 10002, 3, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 200000.00, 1, ''),
(8, 'tradaaaa\r<br />\r<br />aa', 1, 0, 'osobní odběr', 1, 'brouk', 'pytlik', '', 'honluu', 'hawaii', '444', 'Česká republika', '4444', '', '', '2010-10-11 10:51:03', '2010-11-24 20:31:37', 10004, 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 117150.00, 1, ''),
(9, '', 1, 0, 'osobní odběr', 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', '2010-11-14 23:41:06', '2010-11-24 20:31:37', 10005, 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 27150.00, 1, ''),
(10, '', 2, 0, 'osobní odběr', 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', '2010-11-15 09:40:51', '2010-11-24 20:31:37', 10006, 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 33000.00, 1, ''),
(11, '', 1, 0, 'osobní odběr', 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', '2011-01-15 00:00:00', '2010-11-24 20:31:37', 10007, 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 27150.00, 3, ''),
(12, '', 2, 0, 'osobní odběr', 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', '2010-12-15 00:00:00', '2010-11-24 20:31:37', 10008, 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 50000.00, 2, ''),
(13, '', 1, 0, 'osobní odběr', 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', '2010-11-15 21:41:49', '2010-11-24 20:31:37', 10009, 3, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 21720.00, 1, ''),
(14, '', 1, 0, 'osobní odběr', 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', '2010-11-12 00:00:00', '2010-11-24 20:31:37', 10010, 3, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 79320.00, 1, ''),
(15, '', 0, 150, 'Česká pošta - obchodní balík', 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', '2010-11-24 20:46:28', '0000-00-00 00:00:00', 10011, 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 28150.00, 1, 'předem na účet'),
(16, '', 0, 155, 'PPL - obchodní balík', 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', '2010-11-24 00:00:00', '2010-12-24 00:00:00', 10012, 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '', 'ds@ds.cs', 28155.00, 1, 'na bobesa'),
(17, '', 0, 150, 'Česká pošta - obchodní balík', 4, 'dsd', 'dsd', '', 'dsd', 'ds', 'ds', 'Česká republika', 'dsd', '', '', '2010-11-26 21:50:21', '2010-11-26 21:50:21', 10013, 1, 'dsd', 'dsd', '', 'dsd', 'ds', 'ds', 'Česká republika', 'dsd', '', '', 'terihei@gmail.com', 56150.00, 1, 'předem na účet'),
(18, '', 0, 0, 'Osobní odběr - na MKB nebo jinde po dohodě', 4, 'dsd', 'dsd', '', 'dsd', 'ds', 'ds', 'Česká republika', 'dsd', '', '', '2010-11-26 22:08:12', '2010-11-26 22:08:12', 10014, 1, 'dsd', 'dsd', '', 'dsd', 'ds', 'ds', 'Česká republika', 'dsd', '', '', 'terihei@gmail.com', 28000.00, 1, 'předem na účet'),
(19, 'dsds', 0, 150, 'Česká pošta - obchodní balík', 4, 'dsd', 'dsd', '', 'dsd', 'ds', 'ds', 'Česká republika', 'dsd', '', '', '2010-11-26 22:09:53', '2010-11-26 22:09:53', 10015, 1, 'dsd', 'dsd', '', 'dsd', 'ds', 'ds', 'Česká republika', 'dsd', '', '', 'terihei@gmail.com', 62150.00, 1, 'předem na účet'),
(20, 'new comment', 0, 150, 'Česká pošta - obchodní balík', 4, 'dd', 'dd', '', 'dd', 'dd', 'dd', 'Česká republika', 'ddd', '', '', '2010-11-27 00:00:00', '2010-11-27 00:00:00', 10016, 1, 'dsd', 'dsd', '', 'dsd', 'ds', 'ds', 'Česká republika', 'dsd', '', '', 'terihei@gmail.com', 33150.00, 1, 'dobírkou'),
(21, '', 0, 152, 'Česká pošta - obchodní balík', 5, 'dsd', 'kj', '', 'j', 'kj', 'jkj', 'Česká republika', 'jk', '', '', '2013-01-28 00:00:00', '2013-01-28 00:00:00', 10017, 1, 'dsd', 'kj', '', 'j', 'kj', 'jkj', 'Česká republika', 'jk', '', '', 's@s.s', 500.00, 1, 'dobírkou');

-- --------------------------------------------------------

--
-- Table structure for table `s_item_brand`
--

CREATE TABLE `s_item_brand` (
  `sib_code` int(11) NOT NULL AUTO_INCREMENT,
  `sib_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `sib_url` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `sib_order` int(2) NOT NULL,
  PRIMARY KEY (`sib_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `s_item_brand`
--

INSERT INTO `s_item_brand` (`sib_code`, `sib_name`, `sib_url`, `sib_order`) VALUES
(1, 'SLINGSHOT', 'slingshot', 1),
(2, 'MUSHOW', 'mushow', 2),
(3, 'OZONE', 'ozone', 5),
(4, 'MYSTIC', 'mystic', 6),
(5, 'SP BOARDING', 'sp-boarding', 7),
(6, 'OAKLEY', 'oakley', 8),
(7, 'INFINITY', 'infinity', 9),
(8, 'GOPRO', 'gopro', 10),
(9, 'APO', 'apo-snowboards', 11),
(10, 'FLOW', 'flow', 12),
(11, 'BERN', 'bern', 13),
(13, 'IKON', 'ikon', 4),
(14, 'HADLOW', 'hadlow-collection', 3),
(15, 'RAPACE', 'rapace', 3);

-- --------------------------------------------------------

--
-- Table structure for table `s_item_row`
--

CREATE TABLE `s_item_row` (
  `sir_code` int(11) NOT NULL AUTO_INCREMENT,
  `sir_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `sir_price` int(6) NOT NULL,
  `sir_count` int(3) NOT NULL,
  `sir_head` int(11) NOT NULL,
  `sir_visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sir_code`),
  KEY `sir_ibfk_1` (`sir_head`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `s_item_row`
--

INSERT INTO `s_item_row` (`sir_code`, `sir_name`, `sir_price`, `sir_count`, `sir_head`, `sir_visible`) VALUES
(2, '5m2', 27000, 96, 6, 1),
(3, '7m2', 30000, 97, 6, 1),
(4, '9m2', 31000, 100, 6, 1),
(5, '11m2', 32000, 95, 6, 1),
(6, '13m2', 33000, 6, 6, 1),
(7, '15ka', 3434343, 8, 6, 0),
(8, '9m', 28000, 0, 7, 1),
(9, '11m', 28000, 1, 7, 1),
(10, '13m', 28000, 4, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_item_type`
--

CREATE TABLE `s_item_type` (
  `sit_code` int(11) NOT NULL AUTO_INCREMENT,
  `sit_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `sit_head` int(11) DEFAULT NULL,
  `sit_brand` int(11) NOT NULL,
  `sit_id` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `sit_url` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`sit_code`),
  KEY `sit_ibfk_1` (`sit_head`),
  KEY `sit_ibfk_2` (`sit_brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=35 ;

--
-- Dumping data for table `s_item_type`
--

INSERT INTO `s_item_type` (`sit_code`, `sit_name`, `sit_head`, `sit_brand`, `sit_id`, `sit_url`) VALUES
(1, 'Kites', NULL, 1, '0001', 'kites'),
(2, 'Boards', NULL, 1, '0002', 'boards'),
(3, 'Waveboards', NULL, 1, '0003', 'waveboards'),
(4, 'Wakeboarding', NULL, 1, '0004', 'wakeboarding'),
(5, 'Boards', 4, 1, '0004.0005', 'wakeboards'),
(6, 'Vázání', 4, 1, '0004.0006', 'vazani'),
(7, 'Zvýhodněné komplety', NULL, 1, '0007', 'zvyhodnene-komplety'),
(8, 'Accesories', NULL, 1, '0008', 'accesories'),
(9, 'Pads + Straps', 8, 1, '0008.0009', 'pads-straps'),
(10, 'Fins', 8, 1, '0008.0010', 'fins'),
(11, 'Bags', NULL, 1, '0011', 'bags'),
(12, 'Oblečení', NULL, 1, '0012', 'obleceni'),
(13, 'Trička', 12, 1, '0012.0013', 'tricka'),
(14, 'Mikiny', 12, 1, '0012.0014', 'Mikiny'),
(15, 'Šortky', 12, 1, '0012.0015', 'sortky'),
(16, 'Čepice', 12, 1, '0012.0016', 'cepice'),
(18, 'Boards', NULL, 2, '0018', 'boards'),
(19, 'SNK Boards', NULL, 2, '0019', 'snk-boards'),
(20, 'Oblečení', NULL, 2, '0020', 'obleceni'),
(22, 'Snowkites', NULL, 3, '0022', 'snowkites'),
(23, 'Trapézy', NULL, 4, '0023', 'trapezy'),
(24, 'Neoprény', NULL, 4, '0024', 'neopreny'),
(25, 'Oblečení', NULL, 4, '0025', 'obleceni'),
(26, 'Trička', 25, 4, '0025.0026', 'tricka'),
(27, 'Mikiny', 25, 4, '0025.0027', 'mikiny'),
(28, 'Šortky', 25, 4, '0025.0028', 'sortky'),
(29, 'Pady', NULL, 5, '0029', 'pady'),
(30, 'Strapy', NULL, 5, '0030', 'strapy'),
(31, 'Handles', NULL, 5, '0031', 'Handles'),
(32, 'Trapézy', NULL, 5, '0032', 'trapezy'),
(33, 'nunu', 1, 1, '0001.0033', 'nunu'),
(34, 'snowkites', NULL, 13, '0034', 'snowkites');

-- --------------------------------------------------------

--
-- Table structure for table `transport`
--

CREATE TABLE `transport` (
  `tr_code` int(11) NOT NULL AUTO_INCREMENT,
  `tr_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `tr_price` int(6) NOT NULL,
  PRIMARY KEY (`tr_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `transport`
--

INSERT INTO `transport` (`tr_code`, `tr_name`, `tr_price`) VALUES
(1, 'Česká pošta - obchodní balík', 150),
(2, 'Osobní odběr - na MKB nebo jinde po dohodě', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_code` int(11) NOT NULL AUTO_INCREMENT,
  `u_nick` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `u_mail` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `u_date_create` datetime NOT NULL,
  `u_passwd` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `u_ip` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `u_main_addr` int(11) DEFAULT NULL,
  `u_activation` varchar(32) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`u_code`),
  UNIQUE KEY `u_nick` (`u_nick`),
  KEY `user_ibfk_1` (`u_main_addr`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_code`, `u_nick`, `u_mail`, `u_date_create`, `u_passwd`, `u_ip`, `u_main_addr`, `u_activation`) VALUES
(1, 'admin', 'ds@ds.cs', '2010-09-14 09:17:19', 'c593f6f22f99a6a24d1191cdf5f40384', '11', 1, NULL),
(4, 'terihei', 'terihei@gmail.com', '0000-00-00 00:00:00', 'f24df5d4ba8c9c4aa176cba6ede91c86', '::1', 2, ''),
(5, 'bambala', 's@s.s', '0000-00-00 00:00:00', 'a2c59a6418be444cabb8e67d89ff2f9c', '::1', 4, '9131d3d7c14debe57cdf4db04b292721'),
(9, 'pedro', 'pett.k@email.cz', '2014-06-01 21:55:06', '421563e9830f22ce83c35e0f343ce9cf', '::1', 12, '58d7733172c2deddafefcb595ae0fba4');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `ua_code` int(11) NOT NULL AUTO_INCREMENT,
  `ua_abbr` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `ua_user` int(11) NOT NULL,
  `ua_name` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `ua_surname` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `ua_company` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `ua_street` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `ua_town` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `ua_psc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `ua_state` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ua_tel` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `ua_ico` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `ua_dic` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`ua_code`),
  KEY `user_address_ibfk_1` (`ua_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`ua_code`, `ua_abbr`, `ua_user`, `ua_name`, `ua_surname`, `ua_company`, `ua_street`, `ua_town`, `ua_psc`, `ua_state`, `ua_tel`, `ua_ico`, `ua_dic`) VALUES
(1, 'main_addr_1', 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', ''),
(2, 'main_addr_4', 4, 'dsd', 'dsd', '', 'dsd', 'ds', 'ds', 'Česká republika', 'dsd', '', ''),
(4, 'main_addr_5', 5, 'dsd', 'kj', '', 'j', 'kj', 'jkj', 'Česká republika', 'jk', '', ''),
(5, 'nova adresa', 1, 'brouk', 'pytlik', '', 'honluu', 'hawaii', '444', 'Česká republika', '4444', '', ''),
(8, 'new', 4, 'new name', 'new surname', '', 'new street', 'new town', '43434', 'Česká republika', 'new tel', '', ''),
(9, 'ss', 4, 'ss', 'ss', '', 'sss', 'sss', 'ss', 'Česká republika', 'ss', '', ''),
(10, 'dd', 4, 'dd', 'dd', '', 'dd', 'dd', 'dd', 'Česká republika', 'ddd', '', ''),
(12, 'main_addr_9', 9, 'pett', 'dsd', '', 'kj', 'kjk', 'jk', 'Česká republika', 'jkjkk', '', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fpost`
--
ALTER TABLE `fpost`
  ADD CONSTRAINT `fpost_ibfk_1` FOREIGN KEY (`f_user`) REFERENCES `user` (`u_code`),
  ADD CONSTRAINT `fpost_ibfk_2` FOREIGN KEY (`f_father`) REFERENCES `fpost` (`f_code`);

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `orderi_ibfk_1` FOREIGN KEY (`ori_head`) REFERENCES `shop_order` (`or_code`) ON DELETE CASCADE;

--
-- Constraints for table `shop_item`
--
ALTER TABLE `shop_item`
  ADD CONSTRAINT `si_ibfk_1` FOREIGN KEY (`si_type`) REFERENCES `s_item_type` (`sit_code`),
  ADD CONSTRAINT `si_ibfk_2` FOREIGN KEY (`si_brand`) REFERENCES `s_item_brand` (`sib_code`);

--
-- Constraints for table `shop_order`
--
ALTER TABLE `shop_order`
  ADD CONSTRAINT `shop_order_ibfk_1` FOREIGN KEY (`or_user`) REFERENCES `user` (`u_code`) ON DELETE CASCADE;

--
-- Constraints for table `s_item_row`
--
ALTER TABLE `s_item_row`
  ADD CONSTRAINT `sir_ibfk_1` FOREIGN KEY (`sir_head`) REFERENCES `shop_item` (`si_code`);

--
-- Constraints for table `s_item_type`
--
ALTER TABLE `s_item_type`
  ADD CONSTRAINT `sit_ibfk_1` FOREIGN KEY (`sit_head`) REFERENCES `s_item_type` (`sit_code`),
  ADD CONSTRAINT `sit_ibfk_2` FOREIGN KEY (`sit_brand`) REFERENCES `s_item_brand` (`sib_code`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`u_main_addr`) REFERENCES `user_address` (`ua_code`) ON DELETE SET NULL;

--
-- Constraints for table `user_address`
--
ALTER TABLE `user_address`
  ADD CONSTRAINT `user_address_ibfk_1` FOREIGN KEY (`ua_user`) REFERENCES `user` (`u_code`) ON DELETE CASCADE;





-- ------------------------------------------------------------------------
-- ---------------------------- UPDATE ------------------------------------
-- ------------------------------------------------------------------------

ALTER TABLE  `user` ADD  `u_unsubscribed` TINYINT( 1 ) NOT NULL DEFAULT  '0';

CREATE TABLE `newsletter` (
  `nl_id` int(10) NOT NULL AUTO_INCREMENT,
  `nl_subject` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `nl_text` text COLLATE utf8_czech_ci,
  `nl_date_create` datetime NOT NULL,
  PRIMARY KEY (`nl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1;

CREATE TABLE `email_history` (
  `esh_id` int(10) NOT NULL AUTO_INCREMENT,
  `esh_address` varchar(200) COLLATE utf8_czech_ci NOT NULL,
  `esh_date_sent` datetime NOT NULL,
  `esh_newsletter` int(10) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`esh_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=16 ;

ALTER TABLE `email_history`
  ADD CONSTRAINT `emailhist_fk_newsletter` FOREIGN KEY (`esh_newsletter`) 
  REFERENCES `newsletter` (`nl_id`) ON DELETE CASCADE;

ALTER TABLE  `newsletter` ADD  `nl_removed` TINYINT( 1 ) NOT NULL DEFAULT  '0';

CREATE TABLE `email_stack` (
  `es_id` int(10) NOT NULL AUTO_INCREMENT,
  `es_address` varchar(200) COLLATE utf8_czech_ci NOT NULL,
  `es_subject` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `es_text` text COLLATE utf8_czech_ci NOT NULL,
  `es_date` datetime NOT NULL,
  `es_newsletter` int(10) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`es_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

ALTER TABLE `email_stack`
  ADD CONSTRAINT `emailstack_fk_newsletter` FOREIGN KEY (`es_newsletter`) 
  REFERENCES `newsletter` (`nl_id`) ON DELETE CASCADE;


