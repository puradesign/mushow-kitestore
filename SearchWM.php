<?php

class SearchWM extends WebModule {
  var $categ = 1;
  var $search = "";

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (isset($_GET["categ"]))
      $this->categ = $_GET["categ"];

    if (isset($_GET["search"]))
      $this->search = addSlashes($_GET["search"]);

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Hledání";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    // pole pro hledánní
    echo "<fieldset class='form'>";
    echo "  <form method='get' action='" . WR_SEARCH . "?categ=" . $this->categ . "'>";
    echo "		<input type='text' name='search' size='25' " . ($this->search != "" ? "value='" . $this->search . "'" : "") . "/>";
    echo "		<input type='hidden' name='categ' size='25' value='" . $this->categ . "'/>";
    echo "		<input type='submit' id='search_button' value='Hledat'/>";
    echo "  </form>";

    $this->printSearchPanel();

    echo "</fieldset>";

    echo "<h3>Výsledky vyhledávání:</h3>";
    $search = $this->search;
    $categ = $this->categ;

    if ($categ == 1) {
      require_once("ShopWM.php");
      $cond = "(si_title LIKE '%$search%' OR si_descr LIKE '%$search%' OR si_text LIKE '%$search%')";

      $wm = new ShopWM(SHOP);
      $wm->mThisUrl = WR_SEARCH . "?categ=1&amp;search=" . $this->search;
      $wm->defineHtmlOutput(true, $cond);
    } elseif ($categ == 2) {
      require_once("BazarWM.php");
      $cond = "(ad_title LIKE '%$search%' OR ad_text LIKE '%$search%')";

      $wm = new BazarWM(BAZAR);
      // $wm->beforeAction();
      $wm->defineHtmlOutput(true, $cond);
    }
  }

  /*
   * Panel pro vyber vysledku hledani
   */
  function printSearchPanel() {
    $actPanel = $this->categ;
    $search = $this->search;

    echo "<div id='search_panel'><p style='float:left;width:45px;margin:4px;'>Hledat v: </p><ul>";

    if ($actPanel == 1)
      $str = "class='in'";
    else
      $str = "";
    echo "<li><a href='" . WR_SEARCH . "?categ=1&amp;search=$search' $str>Shop</a></li>";

    if ($actPanel == 2)
      $str = "class='in'";
    else
      $str = "";
    echo "<li><a href='" . WR_SEARCH . "?categ=2&amp;search=$search' $str>Bazar</a></li>";

    echo "</ul></div>";
  }
}
?>