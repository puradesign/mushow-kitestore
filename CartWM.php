<?php
require_once "StoreUtil.php";

class CartWM extends WebModule {
  var $mAction;
  var $mState = 0;
  var $mLoginWM = null;
  var $mRegisterWM = null;
  var $mAddrWM = null;
  var $mMainAddr = null;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {

    if (isset($_GET["state"]) && is_numeric($_GET["state"]) && $_GET["state"] >= 0 && $_GET["state"] < 5)
      $this->mState = $_GET["state"];
    else
      $this->mState = 0;

    // kontroly
    if ($this->mState > 1 && !isLogged()) {
      $GLOBALS["rv"]->addError("Pro pokračování musíte být přihlášen.");
      $this->mState = 1;

      return false;
    }

    // reakce na zmenu uzivatele pri checkoutu admina
    if (isset($_GET["onchange"]) && $_GET["onchange"] == "user" && isLoggedAdmin()) {
      $query = "SELECT * FROM user WHERE u_code='" . addSlashes($_POST["or_user"]) . "'";
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      // naplneni do kosiku
      if ($row) {
        $_SESSION[SN_CART]->setUser(addSlashes($_POST["or_user"]));
        $_SESSION[SN_CART]->setUserAddress($row["u_main_addr"]);
        $_SESSION[SN_CART]->setAddress(0);

        // nastaveni slevy
        $query = "SELECT * FROM shop_order WHERE or_order_state = 2 AND or_user=" . $row["u_code"];
        $query .= " AND or_date >= DATE_ADD( NOW(), INTERVAL -400 DAY)";
        $result = $GLOBALS["db"]->query($query);
        $lTotal = 0;
        while ($row = $result->fetch_assoc()) {
          $lTotal += $row["or_price_sum"];
        }

        $_SESSION[SN_CART]->setDiscount(getDiscount($lTotal));
      } else {
        $_SESSION[SN_CART]->setDiscount(0);
        $_SESSION[SN_CART]->setUser(0);
        $_SESSION[SN_CART]->setUserAddress(0);
        $_SESSION[SN_CART]->setAddress(0);
      }
    }

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    $lCart = $_SESSION[SN_CART];

    // ------------------------------------------------------------------------ //
    // prepocitani
    if (isset($_POST["recount"])) {

      for ($i = 0; $i < $lCart->getCount(); $i++) {
        $lCount = $_POST["ci_count_" . $lCart->getRowCode($i)];

        if (!is_numeric($lCount) || $lCount < 1)
          $lCount = 1;

        $lCart->setItemCount($i, $lCount);
      }
    }

    // ------------------------------------------------------------------------ //
    // dalsi
    elseif (isset($_POST["next"])) {

      if ($this->mState == 1 && isLogged()) {
        // pridani adresy
        if ($_POST["address"] == -1) {
          $this->getAddrWM()->reactOnActionLow();

          if (!$GLOBALS["rv"]->isOk())
            return true;

          $lCode = $GLOBALS["db"]->insert_id;
          $lCart->setAddress($lCode);
        } else {
          $lCart->setAddress(addSlashes($_POST["address"]));
        }
      } elseif ($this->mState == 2) {
        if (!isset($_POST["transport"])) {
          $GLOBALS["rv"]->addError("Musí být vybrán typ dopravy.");
          return true;
        }

        $lCart->setTransport(addSlashes($_POST["transport"]));
        $lCart->setPayType(addSlashes($_POST["pay_type"]));
        $lCart->setDescr(addSlashes($_POST["or_descr"]));
      }

      $this->mState++;

      if ($this->mState > 1 && !isLogged())
        $this->mState = 1;

      $_POST = array();
      $this->defineElements();
    }

    // ------------------------------------------------------------------------ //
    // zpet
    elseif (isset($_POST["back"])) {
      if ($this->mState == 0)
        return true;

      $this->mState--;

      $this->defineElements();
    }

    // ------------------------------------------------------------------------ //
    // prihlaseni
    elseif (isset($_POST["login"])) {
      $lLoginWM = $this->getLoginWM();
      $_GET["a"] = "login";

      $lLoginWM->reactOnActionLow();

      if (isLogged()) {
        $this->defineElements();

        $_POST["address"] = 0;
      }
    }

    // ------------------------------------------------------------------------ //
    // registrace
    elseif (isset($_POST["register"])) {
      $lRegWM = $this->getRegisterWM();
      $_GET["action"] = "add";

      $lRegWM->reactOnActionLow();

      if ($GLOBALS["rv"]->isOk()) {
        $lRegWM->setForOutput(false);
        $lRegWM->mElements = null;
        $GLOBALS["rv"]->addInfo("Po aktivaci účtu a přihlášení můžete pokračovat v dokončení objednávky.");
      }
    }

    // ------------------------------------------------------------------------ //
    // potvrzení objednávky
    elseif (isset($_POST["confirm_order"])) {
      if (!isLogged()) {
        $GLOBALS["rv"]->addError("Omlouváme se, nepodařilo se dokončit objednávku, protože nejste přihlášen. Přihlaste se a zkuste to prosím znovu.");
        return true;
      }

      if ($lCart->isEmpty()) {
        $GLOBALS["rv"]->addError("Nelze opakovat akci.");
        return true;
      }

      // ulozeni do DB
      $lAddrCode = $lCart->getAddress();
      if ($lAddrCode == 0)
        $lAddrCode = $lCart->getUserAddress();

      // fakturacni udaje
      $query = "SELECT * FROM user LEFT JOIN user_address ON u_main_addr = ua_code ";
      $query .= "WHERE u_code='" . $lCart->getUser() . "'";
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      $fa_company = $row["ua_company"];
      $fa_name = $row["ua_name"];
      $fa_surname = $row["ua_surname"];
      $fa_street = $row["ua_street"];
      $fa_town = $row["ua_town"];
      $fa_psc = $row["ua_psc"];
      $fa_state = $row["ua_state"];
      $fa_tel = $row["ua_tel"];
      $fa_ico = $row["ua_ico"];
      $fa_dic = $row["ua_dic"];
      $email = $row["u_mail"];

      // dodaci adresa
      $query = "SELECT * FROM user_address WHERE ua_code=$lAddrCode";
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      $company = $row["ua_company"];
      $name = $row["ua_name"];
      $surname = $row["ua_surname"];
      $street = $row["ua_street"];
      $town = $row["ua_town"];
      $psc = $row["ua_psc"];
      $state = $row["ua_state"];
      $tel = $row["ua_tel"];
      $ico = $row["ua_ico"];
      $dic = $row["ua_dic"];

      $query = "SELECT MAX(or_number) AS last_num FROM `shop_order`";
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();
      if (!empty($row["last_num"]) && is_numeric($row["last_num"]))
        $lNum = $row["last_num"] + 1;
      else
        $lNum = 1000;

      // doprava
      $query = "SELECT * FROM transport WHERE tr_code=" . $lCart->getTransport();
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();
      $lTransPrice = $row["tr_price"];
      $lTransText = $row["tr_name"];

      // typ platby
      $query = "SELECT * FROM ks_enum WHERE kse_code=" . $lCart->getPayType();
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();
      $lPayType = $row["kse_descr"];

      // ulozeni objednavky
      $query = "INSERT INTO `shop_order` (`or_user`, `or_email`, `or_fa_name`, `or_fa_surname`, `or_fa_company`";
      $query .= ", `or_fa_street`, `or_fa_town`, `or_fa_psc`, `or_fa_state`, `or_fa_tel`, `or_fa_ico`";
      $query .= ", `or_fa_dic`, `or_name`, `or_surname`, `or_company`, `or_street`";
      $query .= ", `or_town`, `or_psc`, `or_state`, `or_tel`, `or_ico`, `or_dic`, `or_descr`";
      $query .= ", `or_trans_text`, `or_trans_price`, `or_pay_text`, `or_date`, `or_date_due`, `or_number`)";
      $query .= " VALUES (" . $lCart->getUser() . ", '$email', '$fa_name', '$fa_surname', '$fa_company'";
      $query .= ", '$fa_street', '$fa_town', '$fa_psc', '$fa_state', '$fa_tel', '$fa_ico', '$fa_dic'";
      $query .= ", '$name', '$surname', '$company'";
      $query .= ", '$street', '$town', '$psc', '$state', '$tel', '$ico', '$dic', ";
      $query .= "'" . alterTextForDB($lCart->getDescr()) . "', '$lTransText', $lTransPrice, '$lPayType', NOW(), NOW(), $lNum)";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error . "\n" . $query);
        return true;
      }

      $lOrderCode = $GLOBALS["db"]->insert_id;
      $lTotal = 0;

      // ulozeni polozek
      for ($i = 0; $i < $lCart->getCount(); $i++) {
        $lItem = $lCart->getItemByOrder($i);
        $lTotal += $lItem[4] * $lItem[2];

        $query = "INSERT INTO order_item (`ori_head`, `ori_name`, `ori_price`, `ori_count`, `ori_url`, `ori_item_row_code`)";
        $query .= " VALUES ($lOrderCode, '" . $lItem[3] . "', " . $lItem[4] . ", " . $lItem[2];
        $query .= ", '" . Str_Replace(WR, "", $lItem[5]) . "', '" . $lItem[1] . "')";
        $result = $GLOBALS["db"]->query($query);

        // odecteni polozek
        $query = "UPDATE s_item_row SET sir_count = sir_count - " . $lItem[2] . " WHERE sir_code=" . $lItem[1];
        $result = $GLOBALS["db"]->query($query);
      }

      $lTotal += $lTransPrice;

      // odecteni slevy
      $lTotal = $lTotal - $lTotal * $lCart->getDiscount() / 100;

      $query = "UPDATE `shop_order` SET or_price_sum = '$lTotal' WHERE or_code=$lOrderCode";
      $result = $GLOBALS["db"]->query($query);

      // vytvoreni mailu
      $lDate = StrFTime("%d.%m.%Y", time());
      $lSubj = "Mushow kitestore - objednávka č. " . $lNum;

      $lPlain = "Vážený zákazníku,\n\n";
      $lPlain .= "zasíláme Vám potvrzení objednávky v internetové prodejně Mushow Kitestore (" . WR . ").\n\n";
      $lPlain .= "Jelikož Váš emailový klient nepodporuje html, nelze zobrazit detail objednávky.";
      $lPlain .= " Detail objednávky naleznete na odkazu uvedeném níže (po přihlášení):\n";
      $lPlain .= WR_ORDER . "/" . $lNum;


      $lHtmlText = "Vážený zákazníku,<br/><br/>";
      $lHtmlText .= "zasíláme Vám potvrzení objednávky v internetové prodejně Mushow Kitestore ";
      $lHtmlText .= "(<a href='" . WR . "'>" . WR . "</a>).<br/><br/>";
      $lHtmlText .= "Následuje zopakování vaší objednávky.<br/><br/>";

      require_once F_ROOT . "StoreOrderViewWM.php";
      $_GET["item"] = $lNum;
      $lOrderWM = new StoreOrderViewWM(OR_VIEW);
      $lOk = $lOrderWM->beforeAction();

      $lHtml = getOrderFact($lNum, $lDate, $lOrderWM->mActRow, true, false, true);

      // maily se neoesilaji pokud kupuje admin
      if (!isLoggedAdmin()) {
        if (sendMail($email, $lSubj, $lPlain, $lHtmlText . $lHtml)) {
          $GLOBALS["rv"]->addInfo("Objednávka byla úspěšně dokončena. Na Vaši emailovou adresu bylo zasláno její potvrzení.");
        }

        // odesleme aji switchovy
        sendMail("pk.kadlec@gmail.com", $lSubj, $lPlain, $lHtml);
        sendMail("obchod@mushow.cz", $lSubj, $lPlain, $lHtml);
      }

      // vycisteni kosiku
      $lDiscount = $_SESSION[SN_CART]->getDiscount();
      $_SESSION[SN_CART] = new CartObject();
      $_SESSION[SN_CART]->setDiscount($lDiscount);

      if (!isLoggedAdmin()) {
        $_SESSION[SN_CART]->setUser($_SESSION[SN_CODE]);
        $_SESSION[SN_CART]->setUserAddress($_SESSION[SN_MAIN_ADDR]);
      }

      $this->mState = 0;

      //$this->ht = $lHtml;
    }

    // ------------------------------------------------------------------------ //
    // smazani
    else {
      for ($i = 0; $i < $lCart->getCount(); $i++) {
        if (isset($_POST["delete_" . $i]))
          $lCart->deleteItem($i);
      }
    }
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    if ($this->mState == 0)
      return "Obsah košíku";
    else
      return "Objednávka";
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $lCart = $_SESSION[SN_CART];
    $lEmptyPost = empty($_POST);

    // kosik
    if ($this->mState == 0 || $this->mState == 3) {
      // kosik je prazdny
      if ($lCart->isEmpty())
        return;

      for ($i = 0; $i < $lCart->getCount(); $i++) {
        if ($lEmptyPost || isset($_POST["back"])) {
          $_POST["ci_count_" . $lCart->getRowCode($i)] = $lCart->getItemCount($i);
        }

        // pocet polozek
        $lEF = new EditInt("ci_count_" . $lCart->getRowCode($i), null, 0, true,
          40, 2, false);
        if ($this->mState == 3)
          $lEF->addFieldAttr("readonly", "readonly");
        $this->addElement($lEF);

        // smazani
        $lEF = new ButtonImg("delete_" . $i, "Smazat", WR_IMG . "delete.png");
        $lEF->addStyle("height", "14px");
        $this->addElement($lEF);
      }

      // refresh
      $lEF = new ButtonImg("recount", "Přepočítat", WR_IMG . "count_button.jpg");
      $lEF->addStyle("height", "27px");
      $this->addElement($lEF);
    }

    /* ------------------------------------------------------------------------*/
    // fakturacni a dodaci udaje
    elseif ($this->mState == 1) {

      // uzivatel neni prihlasen
      if (!isLogged()) {
        $this->getLoginWM();

        $this->mLoginWM->defineElements();

        $this->getRegisterWM();

        if ($this->getRegisterWM()->isForOutput)
          $this->mRegisterWM->defineElements();
      } // vyber adresy
      else {
        if (!isset($_POST["address"]) && $lCart->getAddress() != null)
          $_POST["address"] = $lCart->getAddress();

        if (!isset($_POST["address"]))
          $_POST["address"] = 0;

        $lCond = new WhereCondition();

        // je prihlasen admin
        if (isLoggedAdmin()) {

          // Uzivatel
          $lEF = new EditCodeCombo("or_user", "Uživatel", 120, true, 150,
            "user LEFT JOIN user_address ON u_main_addr = ua_code", "u_code", "ua_surname",
            array("ua_name"));
          $lEF->setOrderBy("ua_surname ASC, ua_name ASC");
          $lEF->addFieldAttr("onChange", "submitOnChange(\"cart_form\", \"user\");");
          $lEF->setForNull(true);
          $lEF->initOptions();
          $this->addElement($lEF);

          if (!isset($_POST["or_user"])) {
            $_POST["or_user"] = $lCart->getUser();
            $lAddrCode = $lCart->getUserAddress();
          }

          // je vybran uzivatel
          $lCond->addCondStr("ua_user=" . $lCart->getUser());
          $lCond->addCondStr("ua_code!=" . $lCart->getUserAddress());
        } // normalni uzivatel
        else {
          $lCond->addCondStr("ua_user=" . $lCart->getUser());
          $lCond->addCondStr("ua_code!=" . $lCart->getUserAddress());
        }

        // Adresa
        $lEF = new EditCodeCombo("address", "Vyberte adresu", 100, false,
          180, "user_address", "ua_code", "ua_abbr");
        $lEF->addFieldAttr("onChange", "submitOnChange(\"cart_form\");");
        $lEF->setCond($lCond);
        $lEF->setGapWidth(0);
        $lEF->setWidthPX(300);
        $lEF->setForNull(false);

        if (!isLoggedAdmin())
          $lEF->addSelect("...Přidat novou adresu...", -1);

        $lEF->addSelect("Stejná jako fakturační", 0);
        $lEF->initOptions();
        $this->addElement($lEF);

        if ($_POST["address"] == -1) {
          $lAddrWM = $this->getAddrWM();
          $lAddrWM->defineElements();
        }
      }
    }

    /* ------------------------------------------------------------------------*/
    // Zpusob dopravy a platby
    elseif ($this->mState == 2) {
      if (!isset($_POST["or_descr"]) && $lCart->getDescr() != null)
        $_POST["or_descr"] = $lCart->getDescr();

      if (!isset($_POST["pay_type"]) && $lCart->getPayType() != null)
        $_POST["pay_type"] = $lCart->getPayType();

      if (!isset($_POST["transport"])) {
        if ($lCart->getTransport() != null)
          $_POST["transport"] = $lCart->getTransport();
        else
          $_POST["transport"] = 1;
      }

      // platba
      $lEF = new EditCodeCombo("pay_type", "Způsob platby", 90, false,
        150, "ks_enum", "kse_code", "kse_descr");
      $lCond = new WhereCondition();
      $lCond->addCondStr("kse_type=1");
      if ($_POST["transport"] == 1)
        $lCond->addCondStr("kse_code!=2");
      elseif ($_POST["transport"] == 2)
        $lCond->addCondStr("kse_code!=1");

      $lEF->setCond($lCond);
      //$lEF->addFieldAttr("disabled", "disabled");
      $lEF->setForNull(false);
      $lEF->initOptions();
      $this->addElement($lEF);

      // Poznamka
      $lEF = new EditText("or_descr", "Poznámka", 90, false,
        35, 500, 3);
      $lEF->addStyle("margin-top", "10px;");
      $this->addElement($lEF);

    }

    // next
    $lEF = new ButtonImg("next", "Další", WR_IMG . "next_button.jpg");
    $lEF->addStyle("height", "27px");
    $this->addElement($lEF);

    if ($this->mState > 0) {
      // back
      $lEF = new ButtonImg("back", "Zpět", WR_IMG . "back_button.jpg");
      $lEF->addStyle("height", "28px");
      $lEF->addStyle("float", "left");
      $this->addElement($lEF);
    }

    if ($this->mState == 3) {
      // back
      $lEF = new ButtonImg("confirm_order", "Odeslat objednávku", WR_IMG . "confirm_order.jpg");
      $lEF->addStyle("height", "28px");
      $this->addElement($lEF);
    }
  }

  /**
   * Definuje tlacitko pro pridani
   */
  function defineHeaderButton() {
    if ($_SESSION[SN_CART]->isEmpty())
      return false;

    $lInStyle = "color:#316a81;font-weight:bold";

    echo "<div id='order_line_cont'>";
    echo "<span style='" . ($this->mState == 0 ? $lInStyle : "") . "'>Obsah košíku</span>";
    echo "<span style='left:160px;" . ($this->mState == 1 ? $lInStyle : "") . "'>Dodací údaje</span>";
    echo "<span style='left:350px;" . ($this->mState == 2 ? $lInStyle : "") . "'>Doprava</span>";
    echo "<span style='left:470px;" . ($this->mState == 3 ? $lInStyle : "") . "'>Potvrzení objednávky</span>";

    $lWidth = "12px";
    if ($this->mState == 1)
      $lWidth = "194px";
    elseif ($this->mState == 2)
      $lWidth = "380px";
    elseif ($this->mState == 3)
      $lWidth = "536px";
    echo "<div id='order_line' style='width:$lWidth'></div>";
    echo "</div>";
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    $lCart = $_SESSION[SN_CART];

    // kosik je prazdny
    if ($lCart->isEmpty()) {
      echo "<div style='padding: 30px 0px 80px 50px;'><p style='font-size:26px;font-weight:bold;color:#555555'><span style='font-size:42px;'>KOŠÍK</span>&nbsp;&nbsp;JE&nbsp;&nbsp;<span style='font-size:32px;'>PRÁZDNÝ...</span></p></div>";
      return true;
    }

    echo "<form method='post' id='cart_form' style='margin:0px;' action='" . WR_CART . "?state=" . $this->mState . "#head'>";

    // ------------------------------------ 0 --------------------------------- //
    //  obsah kosiku
    if ($this->mState == 0) {

      $this->printCart();

      echo "<div id='order_navig'>";
      $this->getFormElement("next")->toHtmlLow();
      $this->getFormElement("recount")->toHtmlLow();

      echo "</div>";
    }

    // ------------------------------------ 1 --------------------------------- //
    // fakturacni a dodaci udaje
    elseif ($this->mState == 1) {

      if (isLoggedAdmin()) {
        $this->getFormElement("or_user")->toHtmlLow();
      }

      // neprhlasen
      if (!isLogged()) {
        echo "<p>Pokud již máte uživatelský účet, prosím přihlaste se.</p>";
        echo "<fieldset class='form'>";
        $this->mLoginWM->printElements();
        echo "<input type='submit' value='" . getRText("util64") . "' name='login' style='position:absolute; top:50px; left:270px;padding:5px;'/>"; // Přihlásit se
        echo "</fieldset>";

        if ($this->mRegisterWM->isForOutput) {
          echo "<p style='margin-top:40px; float:left; display:block;'>Jestliže jste nový zákazník, poskytněte prosím fakturační a nové přihlašovací údaje.</p>";
          printSpamCheck1();
          $this->mRegisterWM->printElements();
          printSpamCheck2();

          echo " <div class='' style='height:100%;width: 100%;'>";
          echo " <label for='conditions'>Souhlasím s obchodními podmínkami </label>";
          echo "(<a href='" . WR_PODMINKY . "' target='_blank'>podrobnosti zde</a>)&nbsp;&nbsp;";
          echo "<input type='checkbox' name='conditions' id='conditions'/></div>";
          ?>
          <div class='' style='height:100%;width: 100%; margin: 5px 0;'>
            <label for='gdpr'>Souhlasím se zpracováním osobních údajů </label>
            (<a href='<?= WR_GDPR ?>' target="_blank">podrobnosti zde</a>)
            <input type='checkbox' name='gdpr' id='gdpr' />
          </div>


          <script>
            function validateRegister(form) {
              if (!form.conditions.checked) {
                alert("Musíte potvrdit, že souhlasíte s obchodními podmínkami.");
                return false;
              }

              if (!form.gdpr.checked) {
                alert("Musíte potvrdit, že souhlasíte se zpracováním osobních údajů.");
                return false;
              }

              return true;
            };
          </script>

          <?php


          echo " <div class='td_left' style='height:100%;width: 120px;'>";
          echo "<input type='submit' value='Registrovat se' name='register' style='padding:5px;'";
          echo "  onclick='return validateRegister(this.form)'/></div>";
          echo "</fieldset>";
        }
      } // prihlasen nebo vybran uzivatel u admina
      elseif ($lCart->getUser() > 0) {
        $lMainAddr = $this->getMainAddrRow();

        echo "<h3>Fakturační údaje:</h3>";
        $this->printAddrTable($lMainAddr, true);

        echo "<h3>Dodací údaje:</h3>";
        $this->getFormElement("address")->toHtmlLow();

        // exitujici adresa
        if ($_POST["address"] > -1) {
          if ($_POST["address"] == 0)
            $this->printAddrTable($lMainAddr, true);
          else {
            $query = "SELECT * FROM user_address WHERE ua_code=" . intval($_POST["address"]);
            $result = $GLOBALS["db"]->query($query);
            $row = $result->fetch_assoc();

            $this->printAddrTable($row, false);
          }
        } // pridani adresy
        else {
          $this->getAddrWM()->printElements();
        }
      }

      echo "<div id='order_navig'>";

      if (isLogged())
        $this->getFormElement("next")->toHtmlLow();

      $this->getFormElement("back")->toHtmlLow();

      echo "</div>";
    }
    // ------------------------------------ 2 --------------------------------- //
    // Zpusob dopravy a platby
    elseif ($this->mState == 2) {
      $query = "SELECT * FROM transport";
      $result = $GLOBALS["db"]->query($query);

      echo "<table class='addr_table'>";
      while ($row = $result->fetch_assoc()) {
        $lCode = $row["tr_code"];

        echo "<tr><td><input type='radio' name='transport' id='transport$lCode'";
        echo " value='$lCode'" . (@$_POST["transport"] == $lCode ? " checked=\"checked\"" : "") . " onchange='submitOnChange(\"cart_form\")'/>";
        echo "</td><td><label for='transport$lCode'>" . $row["tr_name"] . "</label></td>";
        echo "<td>" . ($row["tr_price"] > 0 ? number_format($row["tr_price"], 0, "", " ") . " Kč" : "zdarma");
        echo "</td></tr>";
      }
      echo "</table>";

      $this->getFormElement("pay_type")->toHtmlLow();

      $this->getFormElement("or_descr")->toHtmlLow();

      // navigace
      echo "<div id='order_navig'>";
      if (isLogged())
        $this->getFormElement("next")->toHtmlLow();

      $this->getFormElement("back")->toHtmlLow();

      echo "</div>";
    }

    // ------------------------------------ 3 --------------------------------- //
    // Dokonceni objednavky
    elseif ($this->mState == 3) {
      // obsah kosiku
      $this->printCart(true);

      // fakt. udaje
      echo "<h3 style='float:left;width:530px;margin-top:-20px'>Fakturační údaje:</h3>";
      $this->printAddrTable($this->getMainAddrRow(), true, false);

      echo "<h3>Dodací údaje:</h3>";
      if ($lCart->getAddress() == 0)
        $this->printAddrTable($this->getMainAddrRow(), true, false);
      else {
        $query = "SELECT * FROM user_address WHERE ua_code=" . $lCart->getAddress();
        $result = $GLOBALS["db"]->query($query);
        $row = $result->fetch_assoc();

        $this->printAddrTable($row, false, false);
      }

      // navigace
      echo "<div id='order_navig'>";
      if (isLogged())
        $this->getFormElement("confirm_order")->toHtmlLow();

      $this->getFormElement("back")->toHtmlLow();

      echo "</div>";

      //	echo $this->ht;
    }

    echo "</form>";
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Vraci WM pro login
   */
  function getLoginWM() {
    if ($this->mLoginWM == null) {
      require_once "LoginWM.php";
      $this->mLoginWM = new LoginWM(LOGIN);
    }

    return $this->mLoginWM;
  }

  /**
   * Vraci WM pro registraci
   */
  function getRegisterWM() {
    if ($this->mRegisterWM == null) {
      require_once "RegisterWM.php";
      $this->mRegisterWM = new RegisterWM(REGISTER);
    }

    return $this->mRegisterWM;
  }

  /**
   * Vraci WM pro pridani adresy
   */
  function getAddrWM() {
    if ($this->mAddrWM == null) {
      require_once "AddrEditWM.php";
      $_GET["action"] = "add";
      $this->mAddrWM = new AddrEditWM(ADDR_EDIT);
    }

    return $this->mAddrWM;
  }

  /**
   * Vraci hlavní adresu
   */
  function getMainAddrRow() {
    if ($this->mMainAddr != null)
      return $this->mMainAddr;

    $query = "SELECT * FROM user_address WHERE ua_code=" . $_SESSION[SN_CART]->getUserAddress();
    $result = $GLOBALS["db"]->query($query);
    $this->mMainAddr = $result->fetch_assoc();

    return $this->mMainAddr;
  }

  /**
   * Vytiskne tabulku s obsahem kosiku
   */
  function printCart($aTransport = false) {
    $lCart = $_SESSION[SN_CART];

    echo "<table id='cart_table' class='brw_table'>";
    echo "<thead><tr><th style='-moz-border-radius: 4px 0px 0px 0px; -webkit-border-radius: 4px 0px 0px 0px;'>Název</th>";
    echo "<th>Cena</th><th>Množství</th><th></th><th style='-moz-border-radius: 0px 4px 0px 0px; -webkit-border-radius: 0px 4px 0px 0px;'>Celkem</th></tr></thead>";

    $lTotal = 0;

    // vykresleni polozek
    for ($i = 0; $i < $lCart->getCount(); $i++) {
      $lItem = $lCart->getItemByOrder($i);
      $lTotal += $lItem[4] * $lItem[2];

      echo "<tr>";
      echo "<td style='font-weight:bold'><a href='" . $lItem[5] . "'>" . $lItem[3] . "</a></td>";
      echo "<td style='text-align:right;white-space:nowrap;'>" . number_format($lItem[4], 0, "", " ") . ",- Kč</td>";
      $this->getFormElement("ci_count_" . $lItem[1])->toHtml();
      echo "<td>";
      $this->getFormElement("delete_" . $i)->toHtmlLow();
      echo "</td>";
      echo "<td style='text-align:right;white-space:nowrap;'>" . number_format($lItem[4] * $lItem[2], 0, "", " ") . ",- Kč</td>";
      echo "</tr>\n";
    }

    echo "</table>\n";

    echo "<table class='brw_table' id='cart_total'>";

    if ($aTransport || $lCart->getDiscount() > 0) {
      echo "<tr><td>Mezisoučet: </td><td style='text-align:right; '>" . number_format($lTotal, 2, ".", " ") . " Kč</td></tr>";
    }

    $lStyle = "border-bottom:1px solid #555555; -moz-border-radius: 0px;-webkit-border-radius: 0px;";

    if ($aTransport) {
      $query = "SELECT * FROM transport WHERE tr_code=" . $lCart->getTransport();
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();
      $lTransPrice = $row["tr_price"];

      echo "<tr><td style='$lStyle'>Doprava: </td><td style='$lStyle;text-align:right'>" . number_format($lTransPrice, 2, ".", " ") . ",- Kč</td></tr>";

      $lTotal += $lTransPrice;
    }

    if ($lCart->getDiscount() > 0) {
      $lTotal = $lTotal - $lTotal * $lCart->getDiscount() / 100;
      echo "<tr><td style='$lStyle'>Sleva: </td><td style='$lStyle;text-align:right;'>" . $lCart->getDiscount() . " %</td></tr>";
    }

    $lPriceNoDPH = $lTotal / 1.2;

    echo "<tr><td>DPH celkem: </td><td style='text-align:right;'>" . number_format($lTotal - $lPriceNoDPH, 2, ".", " ") . " Kč</td></tr>";
    echo "<tr><td>Celkem: </td><td style='text-align:right; font-weight:bold'>" . number_format($lTotal, 2, ".", " ") . " Kč</td></tr>";

    echo "</table>";
  }

  /**
   * Vytiskne udaje z adresy v parametru
   */
  function printAddrTable($aAddrRow, $aMain, $aEdit = true) {
    $lEditLink = ($aMain ? WR_PROFILE : WR . "?m=" . ADDR_EDIT . "&amp;action=edit&amp;item=" . $aAddrRow["ua_code"]);

    echo "<table class='addr_table'>";

    if ($aEdit && !isLoggedAdmin())
      echo "<tr><td>(<a href='$lEditLink'>Upravit</a>)</td><td></td></tr>";

    if (!empty($aAddrRow["ua_company"]))
      echo "<tr><td><b>Firma</b></td><td>" . $aAddrRow["ua_company"] . "</td></tr>";

    echo "<tr><td><b>Jméno a příjmení</b></td><td>" . $aAddrRow["ua_name"] . " " . $aAddrRow["ua_surname"] . "</td></tr>";
    echo "<tr><td><b>Adresa</b></td><td>" . $aAddrRow["ua_street"] . ", " . $aAddrRow["ua_town"] . ", " . $aAddrRow["ua_psc"] . "</td></tr>";
    echo "<tr><td><b>Stát</b></td><td>" . $aAddrRow["ua_state"] . "</td></tr>";
    echo "<tr><td><b>Telefon</b></td><td>" . $aAddrRow["ua_tel"] . "</td></tr>";

    if (!empty($aAddrRow["ua_ico"]))
      echo "<tr><td><b>ICO</b></td><td>" . $aAddrRow["ua_ico"] . "</td></tr>";

    if (!empty($aAddrRow["ua_dic"]))
      echo "<tr><td><b>DIC</b></td><td>" . $aAddrRow["ua_dic"] . "</td></tr>";

    echo "</table>";
  }
}

?>