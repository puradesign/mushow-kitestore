<?php

/*
 * Pristupove validace pro editaci alba
 */
function validateArticleEdit() {

  if (!isset($_GET["item"])) {
    $GLOBALS["rv"]->addError(getRText("err11")); // "Oops. Chybí číslo článku. Pokud potize pretrvaji, kontaktujte spravce."
    return false;
  } elseif (!isLogged()) {
    $GLOBALS["rv"]->addError(getRText("err12")); // "Pro úpravu článku je nutné přihlášení."

    require_once F_ROOT . "LoginWM.php";
    $GLOBALS["wm"] = new LoginWM(LOGIN);
    $GLOBALS["wm"]->reactOnActionLow();

    return false;
  } else {
    $query = "SELECT a_user FROM article where a_code = " . $_GET["item"];
    $result = $GLOBALS["db"]->query($query);

    $row = $result->fetch_assoc();
    if (empty($row)) {
      $GLOBALS["rv"]->addError(getRText("err13")); // "Neexistující článek!"
      return false;
    }

    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError(getRText("err14")); // "Nemáte právo editovat tento článek."
      return false;
    }
  }

  return true;
}

/**
 *
 * Vytiskne jednu polozku browseru albumu
 */
function printOneArticleBrw($row, $i = -1) {
  $filename = F_ARTICLES . "article" . $row["a_code"] . ".jpg";
  if (!file_exists($filename))
    $filename = F_IMG . "none.png";

  $imgStr = "<img src='" . getBrowserPath($filename) . "' alt='" . htmlspecialchars($row["a_title"], ENT_QUOTES) . "'/>";
  ;
  $time = strtotime($row["a_date_create"]);
  $date_create = StrFTime("%d/%m/%y", $time);

  echo "<div class='ar_td'>";

  echo "<div class='ar_title_brw'><h2><a href='" . getArtViewUrl($row["a_url"], $row["a_type"]) . "'>" . $row["a_title"] . "</a></h2>";
  echo "<span class='right'>"/*$date_create | "*/ . getSectionName($row["a_type"]) . "</span></div>";

  echo "<div class='ar_text_brw'><p>" . $row["a_description"] . "</p>";
  echo "</div>";
  echo "<div class='ar_pic_brw'><a href='" . getArtViewUrl($row["a_url"], $row["a_type"]) . "'>" . $imgStr . "</a></div>";


  echo "</div>";
}


/**
 * Vytiskne odkaz pro navrat na prehled galerii
 */
function printRefBackToArticles($code = 0, $edit = false) {
  $str = "<span class='content_block'><a href='" . WR_ART;

  if ($code != 0)
    $str .= "?edit=" . $code;

  $str .= "'>" . getRText("util1"); // Zpět na přehled
  if ($edit)
    $str .= " (" . getRText("util2") . ")"; // bez uložení změn

  $str .= "</a></span>";

  echo $str;
}

/**
 * Vraci nazev sekce
 */
function getSectionName($sect) {
  if ($sect == A_NONE)
    return getRText("menu13"); // "Články"
  else
    return getRText("menu1"); // "News"
}

/**
 * Vraci ID polozky v menu pro sekci
 */
function getSectionMenuID($sect) {
  return MENU_NEWS;
}

/**
 * Vrati nice url pro sekci clanku
 */
function getArtSectUrl($sect) {
  $url = "";

  return WR_ART . $url;
}

/**
 * Vrati nice url pro zobrzeni clanku
 */
function getArtViewUrl($urlTitle, $sect = A_NONE) {
  return getArtSectUrl($sect) . "v/" . $urlTitle;
}

?>