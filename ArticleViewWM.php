<?php
require_once "SimpleImage.php";
require_once "ArticleUtil.php";

class ArticleViewWM extends WebModule {
  var $row;

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {
    echo "<link rel='stylesheet' type='text/css' href='" . F_SHADOWBOX . "shadowbox.css' />";
    echo "<script type='text/javascript' src='" . F_SHADOWBOX . "shadowbox.js'></script>";
    echo "<script type='text/javascript'>";
    echo " Shadowbox.init({";
    echo "  players:    [\"img\", \"swf\"]";
    echo " });";
    echo "</script>";
  }

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    $query = "SELECT * FROM article WHERE";
    $query .= " a_url='" . $_GET["aid"] . "'";
    // echo $query;
    $result = $GLOBALS["db"]->query($query);
    $this->row = $result->fetch_assoc();

    if (!$this->row) {
      $GLOBALS["rv"]->addError(getRText("err15") . "."); // "Článek nenalezen"
      $this->setForOutput(false);
      return true;
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    if ($this->row)
      return $this->row["a_title"];
    else
      return getRText("err15"); // "Článek nenalezen"
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {

    echo "<div class='ar_text'>" . $this->row["a_text"] . "</div>";
  }

  /**
   * Vraci popis do description v hlavicce
   */
  function getMetaDescription() {
    if ($this->row)
      return $this->row["a_description"];
  }
}
?>