<?php

class ShopEditWM extends WebModule {
  var $mAction;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Editace - Zboží";
  }

  /**
   * Definuje tlacitko pro pridani
   */
  function defineHeaderButton() {
    echo "<div class='right_top_button add_button'>";
    echo "<a href='" . WR . "?m=" . SI_EDIT . "&amp;action=add'>";
    echo "Nová položka";
    echo "</a></div>";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    $limit = 1000;
    $offset = 0;

    // slozeni podminky
    $whereCond = new WhereCondition();

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $lTable = "shop_item LEFT JOIN s_item_brand ON shop_item.si_brand = s_item_brand.sib_code";
    $lTable .= " LEFT JOIN s_item_type ON shop_item.si_type = s_item_type.sit_code";
    $result = $itemsBrowser->executeQuery($lTable, $whereCond, "si_brand ASC, sit_id ASC, si_title ASC", "");
    if (!$result)
      die(getRText("err9") . $result->error);

    // nebyla nalezena zadna polozka
    if ($itemsBrowser->getCountAll() < 1) {
      echo "<br /><p>Žádné dostupné položky</p>";
    }

    // byla nalezena alespon jedna polozka
    else {
      echo "<table class='brw_table' style='width:520px;'>";
      echo "<thead><tr><th>Název</th><th>Firma</th><th>Typ</th><th></th><th></th></tr></thead>";
      // vykresleni polozek
      while ($row = $result->fetch_assoc()) {
        $lColor = $row["si_online"] == 1 ? "" : "background-color:#777777;color:#eeeeee";

        echo "<tr>";
        echo "<td style='width:100%;$lColor'>" . $row["si_title"] . "</td>";
        echo "<td>" . $row["sib_name"] . "</td>";
        echo "<td>" . $row["sit_name"] . "</td>";
        echo "<td><a href='" . WR . "?m=" . SI_EDIT . "&amp;action=edit&amp;item=" . $row["si_code"] . "'>Editovat</a></td>";
        echo "<td><a href='" . WR . "?m=" . SI_DELETE . "&amp;item=" . $row["si_code"] . "'>Smazat</a></td>";
        echo "</tr>\n";
      }

      echo "</table>\n";
    }
  }
}
?>