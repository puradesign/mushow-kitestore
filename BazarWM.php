<?php

class BazarWM extends WebModule {
  var $mAction;
  var $mEdit = false;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (isset($_GET["edit"]) && !isLogged()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    if (isset($_GET["edit"]))
      $this->mEdit = true;
    else {
      if (empty($_POST)) {
        $_POST["categ"] = isset($_SESSION[SN_B_CATEG]) ? $_SESSION[SN_B_CATEG] : 0;
        $_POST["type"] = isset($_SESSION[SN_B_TYPE]) ? $_SESSION[SN_B_TYPE] : 1;
      }

      if (isset($_POST["categ"]) && isset($_POST["type"])) {
        $_SESSION[SN_B_CATEG] = $_POST["categ"];
        $_SESSION[SN_B_TYPE] = $_POST["type"];
      }
    }

    if (isLogged() && isset($_GET["action"]) && $_GET["action"] == "del" &&
      isset($_GET["item"]) && is_numeric($_GET["item"])) {

      $query = "SELECT ad_user FROM advert WHERE ad_code = " . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      if ($row["ad_user"] != $_SESSION[SN_CODE] && !isLoggedAdmin()) {
        $GLOBALS["rv"]->addError("Nemáte právo smazat tento inzerát.");
        return true;
      }

      $query = "DELETE FROM advert WHERE ad_code = " . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);
      if ($result)
        $GLOBALS["rv"]->addInfo("Inzerát byl úspěšně smazán.");
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    if (isLoggedAdmin())
      return "Bazar - editace";
    else
      return "Bazar" . ($this->mEdit ? " - inzeráty uživatele" : "");
  }

  /**
   * Definuje tlacitko pro pridani
   */
  function defineHeaderButton() {
    echo "<div class='right_top_button add_button'>";
    echo "<a href='" . WR . "?m=" . B_EDIT . "&amp;action=add'>";
    echo "Přidat inzerát";
    echo "</a></div>";
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $whereCond = new WhereCondition();

    // Kategorie
    $lEF = new EditCodeCombo("categ", "Kategorie", 55, false,
      150, "advert_categ", "adc_code", "adc_name");
    $lEF->addSelect("Vše", 0);
    $lEF->setForNull(false);
    $lEF->initOptions();
    $lEF->addFieldAttr("onChange", "this.form.submit();");
    $lEF->setGapWidth(5);
    $this->addElement($lEF);

    // Typ
    $lEF = new EditRadio("type", "Typ", 55, false, false);
    $lEF->addFieldAttr("onChange", "this.form.submit();");
    $lEF->setGapWidth(5);
    $lEF->addRadio("Nabídka", 1);
    $lEF->addRadio("Poptávka", 2);
    $this->addElement($lEF);
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput($aSearch = false, $aSearchCond = "") {
    $limit = $this->mEdit ? 1000 : 20;
    $offset = 0;

    if (isset($_GET["page"]))
      $offset = $_GET["page"] * $limit;

    // slozeni podminky
    $whereCond = new WhereCondition();

    if ($this->mEdit) {
      if (!isLoggedAdmin())
        $whereCond->addCondStr("ad_user = " . $_SESSION[SN_CODE]);
    } elseif (!$aSearch) {
      if ($_POST["type"] != 0)
        $whereCond->addCondStr("ad_type = " . $_POST["type"]);
      if ($_POST["categ"] != 0)
        $whereCond->addCondStr("ad_categ = " . $_POST["categ"]);
    }

    if ($aSearch && $aSearchCond != "")
      $whereCond->addCondStr($aSearchCond);

    $lTable = "advert";
    if (!$this->mEdit)
      $lTable .= " LEFT JOIN advert_categ ON ad_categ = advert_categ.adc_code";

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $result = $itemsBrowser->executeQuery($lTable, $whereCond, "ad_date_create", "DESC");
    if (!$result) {
      $GLOBALS["rv"]->addError(getRText("err9") . $result->error);
      return true;
    }

    // mod editace
    if ($this->mEdit) {

      // nebyla nalezena zadna polozka
      if ($itemsBrowser->getCountAll() < 1) {
        echo "<p>Žádné dostupné položky</p>";
      } else {
        echo "<table class='brw_table'>";
        echo "<thead><th>Název</th><th>Cena</th><th></th><th></th></thead>";

        // vykresleni polozek
        while ($row = $result->fetch_assoc()) {
          echo "<tr>";
          echo "<td style='width:100%;" . ($row["ad_user"] == 1 ? 'background:#b8dcb8' : '') . "'>" . $row["ad_title"] . "</td>";
          echo "<td><a href='" . WR . "?m=" . B_EDIT . "&amp;action=edit&amp;item=" . $row["ad_code"] . "'>Editovat</a></td>";
          echo "<td><a href='" . WR . "?m=" . BAZAR . "&amp;edit&amp;action=del&amp;item=" . $row["ad_code"] . "' ";
          echo "onclick='if(!confirm(\"Opravdu si přejete smazat tento inzerát?\")) return false;'>Smazat</a></td>";
          echo "</tr>\n";
        }

        echo "</table>\n";
      }
    }

    // prehled
    else {

      if (!$aSearch) {
        //echo "<div style='float:left;width:100%;height:100%;'>";
        echo "<fieldset class='form' style='width:300px;margin:-25px 0px 20px 0px;padding:0px'>";
        echo "  <form method='post' id='edit_form' style='margin:0px;' action='" . WR_BAZAR . "'>";

        $this->printElements();

        echo "  </form></fieldset>";
      }

      echo "<div style='float:left;height:100%;width:510px;margin-bottom:30px;'>";

      $ref = WR_BAZAR;
      // vykresleni ovladaci tabulky
      $itemsBrowser->printControlDiv($ref, "inzerátů");

      // nebyla nalezena zadna polozka
      if ($itemsBrowser->getCountAll() < 1) {
        echo "<p>Žádné dostupné položky</p>";
      }

      // vykresleni polozek
      while ($row = $result->fetch_assoc()) {

        $filename = F_BAZAR . "advert_foto_" . $row["ad_code"] . ".jpg";
        if (!file_exists($filename))
          $filename = F_IMG . "none.png";

        $image = new WebImage();
        $image->load($filename);
        $image->setFrameSize(100, 100);
        $imgStr = $image->getHtmlOutput($row["ad_title"]);

        $time = strtotime($row["ad_date_create"]);
        $date = StrFTime("%d/%m/%y", $time);

        echo "<div class='bazar_td'>";
        echo "<h2><a href='" . WR_BAZAR . "?m=" . B_VIEW . "&amp;bid=" . $row["ad_code"] . "'>" . $row["ad_title"] . "</a></h2>";
        echo "<div class='rightup'>" . $date . "</div>";
        echo "<div class='bazar_text'>";
        echo "<p>" . getTextForBrw($row["ad_text"]) . "</p>";
        echo "</div>";
        echo "<a class='pic' href='" . WR_BAZAR . "?m=" . B_VIEW . "&amp;bid=" . $row["ad_code"] . "'>" . $imgStr . "</a>";

        echo "<p class='b_prize'>Cena: " . $row["ad_price"] . "</p>";
        echo "<div class='b_detail'>" . $row["adc_name"] . " - " . ($row["ad_type"] == 1 ? "Nabídka" : "Poptávka");
        echo " | <a href='" . WR_BAZAR . "?m=" . B_VIEW . "&amp;bid=" . $row["ad_code"] . "'>Detail inzerátu &gt;&gt;</a>";
        echo "</div></div>";
      }

      $itemsBrowser->printControlDiv($ref, "inzerátů");

      echo "</div>";
    }
  }
}
?>