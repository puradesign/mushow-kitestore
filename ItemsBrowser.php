<?php
class ItemsBrowser {

  var $limit;
  var $offset;
  var $countAll;

  /*
   * Konstruktor
   */
  function ItemsBrowser($alimit, $aoffset) {
    $this->limit = $alimit;
    $this->offset = $aoffset;
  }

  /*
   * provede query, vrati result a naplni pocet vet
   */
  function executeQuery($table, $whereCond, $orderBy, $orderByDir = "DESC") {
    $sqlStr = "SELECT SQL_CALC_FOUND_ROWS * FROM " . $table;

    if ($whereCond != null && !$whereCond->isEmpty())
      $sqlStr .= " WHERE " . $whereCond;

    if (!empty($orderBy))
      $sqlStr .= " ORDER BY " . $orderBy . " " . $orderByDir;

    if ($this->limit > 0)
      $sqlStr .= " LIMIT " . $this->limit . " OFFSET " . $this->offset;
    // echo $sqlStr;
    $result = $GLOBALS["db"]->query($sqlStr);
    if (!$result)
      return false;

    $rowCount = $GLOBALS["db"]->query(" SELECT FOUND_ROWS() as count")->fetch_row();
    $this->countAll = $rowCount[0];

    return $result;
  }

  /*
   * Vraci pocet vsech vet
   */
  function getCountAll() {
    return $this->countAll;
  }

  /*
   * Vraci pocet vybranych vet pro zobrazeni
   */
  function getCountSelected() {
    if (($this->offset + $this->limit) <= $this->countAll)
      return $this->limit;
    else
      return $this->countAll - $this->offset;
  }


  /*
   * Vytiskne div s ovladacimi prvky browseru
   */
  function printControlDiv($refPage, $name = "xxx") {
    $offset = $this->offset;
    $count_select = $this->getCountSelected();
    $count_all = $this->getCountAll();
    $limit = $this->limit;

    if ($name == "xxx") {
      $name = getRText("util58"); // záznamů
    }

    if (preg_match("/\?/", $refPage) == 0) {
      $firstParam = true;
      $refPage .= "?";
    } else
      $refPage .= "&amp;";


    // pocet stranek
    $pagesCount = ceil($count_all / $limit);

    $actPage = $offset / $limit;

    // ovladaci tabulka
    echo "<div class='paging'>";
    echo "<div style='float:right'>";
    //echo "<p>Zobrazeno ".($offset + 1)." - ".($offset + $count_select)." z celkových ".$count_all." $name</p>";

    if ($count_all > $limit) {
      echo "<p class='page_numbers'>";

      if ($actPage > 0)
        echo "<a href='" . $refPage . "page=" . ($actPage - 1) . "'>&lt;" . getRText("util59") . "</a>"; // předchozí
      else
        echo "<span>&lt;" . getRText("util59") . "</span>"; // předchozí

      // stranek je min nez deset
      if ($pagesCount < 10) {
        for ($page = 0; $page < $pagesCount; $page++) {
          echo "<a href='" . $refPage . "page=" . $page . "' ";
          if ($page == $actPage)
            echo "class='in'";

          echo ">" . ($page + 1) . "</a>";
        }
      }

      // stranek je vice
      else {
        echo "<a href='" . $refPage . "page=0' " . (0 == $actPage ? "class='in'" : "") . ">1</a>";

        $start = $actPage - 3 < 1 ? 1 : $actPage - 3;
        $start = $pagesCount - $actPage < 5 ? $pagesCount - 8 : $start;

        if ($start > 1)
          echo "<span>&nbsp;...&nbsp;</span>";

        for ($page = $start; $page < $start + 7; $page++) {
          echo "<a href='" . $refPage . "page=" . $page . "' ";
          if ($page == $actPage)
            echo "class='in'";

          echo ">" . ($page + 1) . "</a>";
        }

        if ($page < $pagesCount - 1)
          echo "<span>&nbsp;...&nbsp;</span>";

        echo "<a href='" . $refPage . "page=" . ($pagesCount - 1) . "' " . ($pagesCount - 1 == $actPage ? "class='in'" : "") . ">$pagesCount</a>";
      }

      if ($actPage < $pagesCount - 1)
        echo "<a href='" . $refPage . "page=" . ($actPage + 1) . "'>" . getRText("util60") . "&gt;</a>&nbsp;"; // další
      else
        echo "<span>" . getRText("util60") . "&gt;</span>"; // další

      echo "</p>";
    }

    echo "</div></div>";
  }
}
?>