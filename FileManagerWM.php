<?php
require_once "SimpleImage.php";

class FileManagerWM extends WebModule {
  var $path;
  var $pic_preview = array(); // [jmeno souboru, sirka, vyska]

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    //echo $this->path;
    if (!@$_SESSION[SN_LOGGED]) {
      $GLOBALS["rv"]->addError(getRText("err31")); // "Nejste přihlášen"
      $this->setForOutput(false);
      return true;
    }

    $this->path = $_SESSION[SN_HOMEDIR];
    if (isset($_GET["CKEditor"]) && isset($_SESSION[SN_LASTDIR]))
      $this->path = $this->path . $_SESSION[SN_LASTDIR];

    if (isset($_GET["path"]) && !empty($_GET["path"])) {
      if (!preg_match("/\.\./", $_GET["path"]))
        $this->path .= $_GET["path"];
    }

    // je nastavena akce
    if (isset($_GET["a"])) {

      // -----------------------------------------------------------------------------------------//
      // -----------------------------------------------------------------------------------------//
      // vytvoreni adresare
      if ($_GET["a"] == "create_dir") {
        $dir = $_POST["dirName"];
        if (!hasValidChars($dir)) {
          $GLOBALS["rv"]->addError(getRText("err32")); // "Chyba. Neplatný název složky."
          return true;
        }

        $dirPath = $this->path . $dir;
        if (is_dir($dirPath)) {
          $GLOBALS["rv"]->addError(getRText("err33")); // "Chyba. Složka s tímto názvem existuje."
          return true;
        }

        mkdir($dirPath);
      }

      // -----------------------------------------------------------------------------------------//
      // -----------------------------------------------------------------------------------------//
      // pridani obrazku
      elseif ($_GET["a"] == "add_pic") {
        $image = new ImageUploaded($_FILES["picture"]);
        $pic_ok = $image->validate();

        if (!$pic_ok)
          return true;

        $image->loadFile();

        // ulozeni velke verze obrazku
        $image->resizeToMaxWAndH(1000, 3000);
        $name = $image->saveToDir($this->path);

        if ($name == "") {
          $GLOBALS["rv"]->addError(getRText("err34")); // "Obrázek se nepodařilo nahrát."
          return true;
        }

        $infoStr = getRText("fm1") . " <strong>$name</strong>"; // Vložen obrázek

        $big_thumb = @$_POST["tbb"];
        $small_thumb = @$_POST["tbs"];

        if ($big_thumb) {
          // ulozeni vetsiho nahledu
          $image->resizeToWidth(530);
          $nameV = $image->saveWithName($this->path, "nahledV" . $name, false);

          $infoStr .= ", " . getRText("fm2") . " <strong>$nameV</strong>"; // vytvořen obrázek pro náhled na celou šířku článku
        }

        if ($small_thumb) {
          // ulozeni mensiho nahledu
          $image->resizeToWidth(260);
          $nameM = $image->saveWithName($this->path, "nahledM" . $name, false);

          $infoStr .= ", " . getRText("fm3") . " <strong>$nameM</strong>"; // vytvořen obrázek pro náhled na polovinu šířky článku
        }

        $GLOBALS["rv"]->addInfo($infoStr . ".");
      }

      // -----------------------------------------------------------------------------------------//
      // -----------------------------------------------------------------------------------------//
      // otevreni adresare
      elseif ($_GET["a"] == "open_dir") {
        //if ()
        if (!empty($_GET["path"]))
          $this->path .= DIRECTORY_SEPARATOR;
      }

      // -----------------------------------------------------------------------------------------//
      // -----------------------------------------------------------------------------------------//
      // otevreni souboru
      elseif ($_GET["a"] == "open_file") {
        if (!isset($_GET["file"]) || empty($_GET["file"]))
          return true;

        $filename = $this->path . $_GET["file"];
        if (!file_exists($filename))
          return true;

        $image = new WebImage();
        $image->load($filename);

        $this->pic_preview["name"] = $_GET["file"];
        $this->pic_preview["width"] = $image->getWidth();
        $this->pic_preview["height"] = $image->getHeight();

        $image->resizeToMaxWAndH(150, 150);
        $image->setFrameSize(150, 150);

        if (!is_dir($_SESSION[SN_HOMEDIR] . "temp"))
          mkdir($_SESSION[SN_HOMEDIR] . "temp");

        $image->save($_SESSION[SN_HOMEDIR] . "temp" . DIRECTORY_SEPARATOR . "temp.jpg", IMAGETYPE_JPEG);
      }

      // -----------------------------------------------------------------------------------------//
      // -----------------------------------------------------------------------------------------//
      // smazani obrazku
      elseif ($_GET["a"] == "del_pic") {
        $filename = $this->path . $_GET["pic"];
        if (file_exists($filename))
          unlink($filename);
      }
    }

    $_SESSION[SN_LASTDIR] = $this->getRelativePath();
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return getRText("fm4"); // "Správce souborů"
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {

    echo "<div id='left'>";
    echo "  <fieldset class='form'>";
    echo "  <legend>" . getRText("fm5") . "</legend>"; // Vytvořit složku
    echo "  <form method='post' action='filemanager.php?a=create_dir&path=" . $this->getRelativePath() . "'>";
    echo "    <input type='text' name='dirName' maxlength='30' size='30' value=''><br>";
    echo "    <input type='submit' value='" . getRText("fm6") . "'>"; // Vytvořit
    echo "  </form>";
    echo "  </fieldset>";

    echo "  <fieldset class='form'>";
    echo "  <legend>" . getRText("util39") . "</legend>"; // Přidat novou fotografii
    echo "  <form method='post' action='filemanager.php?a=add_pic&path=" . $this->getRelativePath() . "' enctype='multipart/form-data'>";
    echo "    <div><input type='file' name='picture' value='upload'/></div>";
    echo "    <h4>" . getRText("fm7") . ":</h4>"; // Vytvořit automaticky náhled fotky
    echo "    <div><input type='checkbox' name='tbb' id='tbb'/><span><label for='tbb'>";
    echo getRText("fm8") . "</label></span></div>"; // větší rozměr (na celou šířku)
    echo "    <div><input type='checkbox' name='tbs' id='tbs'/><span><label for='tbs'>";
    echo getRText("fm9") . "</label></span></div>"; // menší rozměr (na polovinu šířky)
    echo "    <div><input type='submit' value='" . getRText("util40") . "'></div>"; // Nahrát fotku
    echo "  </form>";
    echo "  </fieldset>";

    echo "</div>";

    echo "<div id='middle' style='overflow:auto;height:450px;'>";

    echo getRText("fm10") . ": /" . $this->getRelativePath(); // Aktuální cesta

    $dir_handle = @opendir($this->path) or die("Unable to open $this->path");

    echo "<ul>";

    $folders = array();
    $files = array();

    // projdeme obsah slozky a zapamatujeme si zvlast soubory a slozky
    while ($file = readdir($dir_handle)) {
      if ($file == "." || $file == "temp")
        continue;

      if ($file == "..") {
        if ($this->isInHomeDir())
          continue;

        echo "<li><a href='filemanager.php?a=open_dir&path=" . $this->getParentDir() . "'>$file</a></li>\n";
        continue;
      }

      if (is_dir($this->path . $file)) {
        $folders[] = $file;
        continue;
      }

      $ext = strtolower(pathinfo($this->path . $file, PATHINFO_EXTENSION));
      if ($ext == "jpeg" || $ext == "jpg" || $ext == "gif" || $ext == "png") {
        $files[] = $file;
      }
    }

    // vypis slozek
    for ($i = 0; $i < count($folders); $i++) {
      echo "<li class='folder'><a href='filemanager.php?a=open_dir&path=" . $this->getRelativePath() . $folders[$i] . "'>$folders[$i]</a></li>\n";
    }

    // vypis osuboru
    for ($i = 0; $i < count($files); $i++) {
      $class = "img";

      if (!empty($this->pic_preview) && $this->pic_preview["name"] == $files[$i])
        $class .= " chosen";

      echo "<li class='$class'>&nbsp;<a href='filemanager.php?a=open_file&path=" . $this->getRelativePath();
      echo "&file=" . $files[$i] . "'>$files[$i]</a></li>\n";
    }

    closedir($dir_handle);

    echo "</ul>";
    echo "</div>";

    echo "<div id='right'>";
    echo "  <fieldset class='form'>";
    echo "  <legend>" . getRText("fm11") . ":</legend>"; // Vybraná fotka

    if (!empty($this->pic_preview)) {
      $image = new WebImage();
      $image->load($_SESSION[SN_HOMEDIR] . "temp" . DIRECTORY_SEPARATOR . "temp.jpg");
      $image->setFrameSize(150, 150);

      echo $image->getHtmlOutput();
      echo "<p><strong>" . getRText("fm12") . ": </strong>" . $this->pic_preview["name"] . "</p>\n"; // Jméno souboru
      echo "<p><strong>" . getRText("fm13") . ": </strong>" . $this->pic_preview["width"] . " x " . $this->pic_preview["height"] . "</p>\n"; // Rozměry

      $imgPath = getBrowserPath($this->path . $this->pic_preview["name"]);
      // echo ($this->path.$this->pic_preview["name"]);
      echo "<a href='' onClick='selectFile(\"$imgPath\");' id='choose'>" . getRText("fm14") . "</a>\n"; // Vybrat
      echo "<a href='filemanager.php?a=del_pic&path=" . $this->getRelativePath() . "&pic=" . $this->pic_preview["name"] . "' id=delete>" . getRText("util23") . "</a>\n"; // Smazat
    } else {
      echo "<span>" . getRText("fm15") . "</span>"; // Nebyla vybrána fotka
    }
    ?>
    </fieldset>
    </div>
    <?php
  }

  function getRelativePath() {
    $str = Str_Replace(F_USER_IMAGES, "", $this->path);

    return $str;
  }

  function isInHomeDir() {
    return $this->path == $_SESSION[SN_HOMEDIR];
  }

  function getParentDir() {
    $relPath = $this->getRelativePath();
    if ($relPath == "")
      return $relPath;

    $s = DIRECTORY_SEPARATOR;
    if (!preg_match("/(.+)\\" . $s . "(.+)\\" . $s . "/", $relPath))
      return "";

    $pattern = "/^(.*)\\" . $s . "[^\\" . $s . "]+\\" . $s . "$/";
    $relPath = preg_replace($pattern, '$1', $relPath);
    return $relPath;
  }
}
?>