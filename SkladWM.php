<?php
require_once 'lib/phpexcel/Classes/PHPExcel.php';

class SkladWM extends WebModule {
  var $mFile = "";

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (!isLoggedAdmin()) {
      $this->setForOutput(false);
      $GLOBALS["rv"]->addError("Musí být přihlášen administrátor.");

      return false;
    }

    $GLOBALS["rv"]->addInfo("Soubor <i>sklad" . date('ymd') . ".xlsx</i> se právě generuje. Za pár vteřin by se měl automaticky stáhnout. ");

    if (isset($_GET["export"])) {
      // Create new PHPExcel object
      $objPHPExcel = new PHPExcel();

      // Set properties
      //echo date('H:i:s') . " Set properties\n";
      $objPHPExcel->getProperties()->setCreator("Petr Kadlec")
        ->setLastModifiedBy("Petr Kadlec")
        ->setTitle("Mushow Kitestore sklad")
        ->setSubject("Mushow Kitestore sklad")
        ->setDescription("Přehled zboží na skladu pro mushow kitestore.");


      // Add some data
      //echo date('H:i:s') . " Add some data\n";

      $lActRow = 1;

      // sirky sloupcu
      $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(9);
      $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(19);
      $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(29);
      $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
      $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
      $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(9);

      //$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(5);

      $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

      // head
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A2", 'Firma')
        ->setCellValue("B2", 'Název zboží')
        ->setCellValue("C2", 'Varianta')
        ->setCellValue("D2", 'Kusů')
        ->setCellValue("E2", 'Cena/kus')
        ->setCellValue("F2", 'Celkem');

      $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()->setARGB('dddddd');

      $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getFont()->setBold(true);
      $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getFont()->setSize(10);

      $styleArray = array(
        'borders' => array(
          'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '000000'),
          ),
        ),
      );

      $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray);

      $lTable = "shop_item LEFT JOIN s_item_type ON shop_item.si_type = s_item_type.sit_code";
      $lTable .= " LEFT JOIN s_item_brand ON si_brand = s_item_brand.sib_code";

      $query = "SELECT * FROM s_item_brand ORDER BY sib_name ASC";
      $result = $GLOBALS["db"]->query($query);

      $lTotal = 0;
      $lTotalCount = 0;
      $lOutlineFirst = "A2";

      // pruchod vsech znacek
      while ($row = $result->fetch_assoc()) {
        $lActRow += 2;

        $query = "SELECT * FROM s_item_row LEFT JOIN shop_item ON sir_head = si_code";
        $query .= " WHERE si_brand = " . $row["sib_code"] . " AND sir_visible = 1 ORDER BY si_type, si_title ASC, sir_code ASC";
        $lItemRes = $GLOBALS["db"]->query($query);

        $lLastItemCode = -1;

        // jednotlive polozky
        while ($lItemRow = $lItemRes->fetch_assoc()) {

          // vykresleni ramecku kolem vse variant jedne polozky
          if ($lLastItemCode != $lItemRow["si_code"]) {
            // vykreslime ramecek
            if ($lLastItemCode > -1) {
              $objPHPExcel->getActiveSheet()->getStyle($lOutlineFirst . ":F" . ($lActRow - 1))->applyFromArray($styleArray);
            }

            // nastavime prvni bunku pro dalsi ramecek
            $lOutlineFirst = "A$lActRow";
          }

          $lItemTotal = 0;

          // cena celkem a pocet celkem
          if ($lItemRow["sir_count"] > 0) {
            $lItemTotal = $lItemRow["sir_count"] * $lItemRow["sir_price"];
            $lTotal += $lItemRow["sir_count"] * $lItemRow["sir_price"];
            $lTotalCount += $lItemRow["sir_count"];
          }

          $objPHPExcel->getActiveSheet()->setCellValue("A$lActRow", $row["sib_name"]);
          $objPHPExcel->getActiveSheet()->setCellValue("B$lActRow", $lItemRow["si_title"]);
          $objPHPExcel->getActiveSheet()->setCellValue("C$lActRow", $lItemRow["sir_name"]);
          $objPHPExcel->getActiveSheet()->setCellValue("D$lActRow", $lItemRow["sir_count"]);
          $objPHPExcel->getActiveSheet()->setCellValue("E$lActRow", $lItemRow["sir_price"]);
          $objPHPExcel->getActiveSheet()->setCellValue("F$lActRow", $lItemTotal);

          $objPHPExcel->getActiveSheet()->getStyle("A$lActRow")->getFont()->getColor()->setARGB("444444");
          $objPHPExcel->getActiveSheet()->getStyle("A$lActRow")->getFont()->setSize(8);

          $objPHPExcel->getActiveSheet()->getStyle("E$lActRow:F$lActRow")->getNumberFormat()
            ->setFormatCode('#,##0');

          $lLastItemCode = $lItemRow["si_code"];

          $lActRow++;
        }

        // posledni ramecek od jedne znacky
        $objPHPExcel->getActiveSheet()->getStyle($lOutlineFirst . ":F" . ($lActRow - 1))->applyFromArray($styleArray);
      }

      $lActRow--;

      // CELKEM
      $objPHPExcel->getActiveSheet()->setCellValue("C$lActRow", "CELKEM");
      $objPHPExcel->getActiveSheet()->setCellValue("D$lActRow", $lTotalCount);
      $objPHPExcel->getActiveSheet()->setCellValue("F$lActRow", $lTotal);

      $objPHPExcel->getActiveSheet()->getStyle("D$lActRow:E$lActRow")->getNumberFormat()
        ->setFormatCode('#,##0');

      // ramecek pro celkem
      $objPHPExcel->getActiveSheet()->getStyle("C$lActRow:F$lActRow")->applyFromArray($styleArray);
      $objPHPExcel->getActiveSheet()->getStyle("C$lActRow:F$lActRow")->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()->setARGB('dddddd');

      $lFileName = 'sklad' . date('ymd') . '.xlsx';

      // Redirect output to a client’s web browser (Excel2007)
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="' . $lFileName . '"');
      header('Cache-Control: max-age=0');

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');
      exit;
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Sklad";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    $lTotal = 0;
    $lTotalCount = 0;

    $query = "SELECT * FROM s_item_row WHERE sir_visible = 1";
    $result = $GLOBALS["db"]->query($query);

    while ($row = $result->fetch_assoc()) {
      if ($row["sir_count"] > 0) {
        $lTotal += $row["sir_count"] * $row["sir_price"];
        $lTotalCount += $row["sir_count"];
      }
    }

    echo "<table><tr><td>Celkem položek: </td><td><span style='font-size:14px;font-weight:bold'>" . $lTotalCount . "</span></td></tr>";
    echo "<tr><td>Hodnota zboží: </td><td><span style='font-size:14px;font-weight:bold'>" . number_format($lTotal, 0, ",", ".") . " Kč</span></td></tr></table>";

    echo "<p><a href='" . WR . "?m=" . SKLAD . "&amp;export' style='font-weight:bold;font-size:14px;' onclick='$(\".info\").show();'>Export do Excelu...</a></p>";

    if ($this->mFile != "")
      echo "<p><a href='" . WR . "temp/" . $this->mFile . "' style='font-weight:bold;font-size:14px;'>" . $this->mFile . "</a></p>";

  }

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {
    echo "<script type='text/javascript'>";
    echo "$(document).ready(function() {\n";

    echo "$('.info').hide();})";
    echo "</script>";
  }
}
?>