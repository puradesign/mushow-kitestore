<?php

class GalleryWM extends WebModule {
  var $edit = false;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    // priznak jestli se jedna o prehled galerii prilaseneho uzivatele
    $this->edit = isset($_SESSION[SN_LOGGED]) && isset($_GET["edit"]) &&
      @$_GET["edit"] == @$_SESSION[SN_CODE];

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {

    if ($this->edit)
      return getRText("menu10") . " - " . getRText("util49"); // Foto a video - Editace
    else
      return getRText("menu10"); // Foto a video
  }

  function defineHeaderButton() {
    if ($this->edit) {
      echo "<div class='right_top_button add_button'>";
      echo "<a href='" . WR . "?m=" . G_ADD . "' style='padding-left:27px'>";
      echo getRText("util50"); // Přidat galerii
      echo "</a></div>";
    }
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput($searchCond = "", $search = false) {
    $edit = $this->edit;

    $limit = 12;
    $offset = 0;

    if (isset($_GET["page"]))
      $offset = $limit * $_GET["page"];

    // slozeni podminky
    $whereCond = new WhereCondition();
    if (isset($_GET["uid"]))
      $whereCond->addCondStr("al_user = " . $_GET["uid"]);

    if (!$edit)
      $whereCond->addCondStr("al_is_public = 1");

    if (!empty($searchCond))
      $whereCond->addCondStr($searchCond);

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $result = $itemsBrowser->executeQuery("album", $whereCond, "al_date_create");
    if (!$result)
      die(getRText("err9") . $result->error);

    // nebyla nalezena zadna polozka
    if ($itemsBrowser->getCountAll() < 1) {
      echo "<br><p>" . getRText("util51") . "</p>"; // Výběru neodpovídají žádné galerie.
    }

    // byla nalezena alespon jedna polozka
    else {

      /* if ($search)
         echo "<p class='searchCount'>Na zadaný dotaz bylo nalezeno ".$itemsBrowser->getCountAll()." odpovídajících galerií.</p>";
   */
      $i = -1;

      // vykresleni nactenych uzivatel na strance
      while ($row = $result->fetch_assoc()) {
        if ($edit)
          printBrwEditPanel($row["al_code"]);

        $i = $i * (-1);

        printOneGalleryBrw($row, $i);
      }

      // vykresleni ovladaci tabulky
      if ($search) {
        $ref = WR . "?m=" . SEARCH . $this->getGETparamsStr(false);
      } else {
        $params = $this->getGETparamsStr(false, false);
        $ref = WR_GAL . ($params == "" ? "" : "?" . $params);
      }
      $itemsBrowser->printControlDiv($ref, getRText("util52")); // "galerií"

      if (!$this->edit && !$search)
        addShareButton();
    }
  }

  /**
   * Pro prepsani - vraci ID polozky v menu, ktera patri k tomuto WM (podle menu konstant)
   */
  function getMenuItemID() {
    if ($this->edit)
      return 0;
    else
      return MENU_GALL;
  }

  /**
   * Vraci nazvy GET parametru, ktere se maji zachovavat - pro prepsani
   */
  function getImportantParamsNames() {
    return array("uid");
  }
}
?>