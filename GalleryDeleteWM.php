<?php
require_once "AlbumUtil.php";

class GalleryDeleteWM extends WebModule {
  var $title = "";

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    // ------------------------ pristupove validace ------------------------------//

    $authorized = validateAlbumEdit();

    if (!$authorized) {
      $this->setForOutput(false);
      return false;
    }

    $query = "SELECT al_code, al_title FROM album WHERE al_code=" . $_GET["aid"];
    $result = $GLOBALS["db"]->query($query);
    $album_row = $result->fetch_assoc();

    $this->title = $album_row["al_title"];

    // ------------------------ zpracovani akce ----------------------------------//

    if (isset($_POST["delete"])) {
      // vymazani vsech fotek
      /*$query = "SELECT * FROM photo WHERE ph_album=".$album_row["al_code"];
      $result = $GLOBALS["db"]->query($query);
      while ($row = $result->fetch_assoc()) {
        deletePhotoRow($row["ph_code"]);
      }*/

      // vymazani slozky alba a podrizenych souboru
      removeDirR(F_ALBUMS . $album_row["al_code"]);

      $query = "DELETE FROM album WHERE `al_code`=" . $album_row["al_code"];
      $GLOBALS["db"]->query($query);

      $_GET["edit"] = $_SESSION[SN_CODE];
      require_once F_ROOT . "GalleryWM.php";
      $GLOBALS["wm"] = new GalleryWM(GALLERY);
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Edit - " . $this->title;
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    printEditPanel(4);

    echo "<div class='form' id='delete'>";
    echo "  <p>" . getRText("util46") . "</p>"; // Opravdu si přejete smazat galerii?
    echo "  <form method='post' action='" . WR . "?m=" . G_DELETE . "&amp;aid=" . $_GET["aid"] . "'>";
    echo "    <input type='hidden' name='delete'>";
    echo "    <input type='submit' class='submit' value='" . getRText("util23") . "'>"; // Smazat
    echo "  </form>";
    echo "</div>";

    printRefBackToGallery($_SESSION[SN_CODE], true);
  }

  /**
   * Zde naplneni vektoru cesty - pro prepsani
   */
  function definePathVect() {
    $GLOBALS["pv"]->addItem(WR . "?m=" . GALLERY . "&amp;uid=" . $_SESSION[SN_CODE], getRText("util47")); // Galerie
    $GLOBALS["pv"]->addItem("", getRText("util6")); // "Smazat galerii"
  }

  /**
   * Pro prepsani - vraci ID polozky v menu, ktera patri k tomuto WM (podle menu konstant)
   */
  function getMenuItemID() {
    return -1;//sMENU_MY_GALL;
  }
}
?>