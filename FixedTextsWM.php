<?php

class FixedTextsWM extends WebModule {
  var $mTextID = NULL;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {

    $this->mID = $_GET["text"];

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    if ($this->mID == "kontakt")
      return "Kontakt";
    elseif ($this->mID == "obchod-podminky")
      return "Obchodní podmínky";
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    if ($this->mID == "kontakt") {
      echo "<h3 style='margin-top:40px'>Mushow s.r.o.</a></h3>";
      echo "<p style='margin-bottom:40px'>Email: <a href='mailto:obchod@mushow.cz'>obchod@mushow.cz</a></p>";
    } elseif ($this->mID == "obchod-podminky") {
      ?>

      <div align="justify">
        <b>1) VŠEOBECNÁ USTANOVENÍ</b><br />
        <b>
        </b>Kupující učiněním závazné objednávky stvrzuje, že akceptuje
        Obchodní podmínky pro dodávku zboží vyhlášené prodávajícím. Vztahy mezi
        kupujícím a prodávajícím se řídí těmito obchodními podmínkami, které
        jsou zároveň pro obě strany závazné.<br />
        <br />
        <b>
          2) OBJEDNÁNÍ ZBOŽÍ A SLUŽEB</b><br />
        <b>
        </b>Nabízíme vám možnost nákupu přímo z Vašeho domova, kde můžete
        nakupovat nonstop. Objednávat si můžete jednoduše a pohodlně pomocí
        nákupního košíku, telefonu, e-mailu, či písemně na naši adrese.<br />

        Předmětem smlouvy je pouze zboží uvedené v kupní smlouvě – objednávce.
        Rozměry, váha, výkony, kapacita a ostatní údaje uvedené na našich
        stránkách, v katalozích, prospektech a jiných tiskovinách jsou údaji
        nezávaznými a vycházejícími z údajů výrobců. V případě nesrovnalostí
        Vás samozřejmě budeme kontaktovat.<br />
        Zavazujeme se, že svým odběratelům budeme dodávat jen zboží v
        perfektním stavu a v souladu se specifikacemi či vlastnostmi obvyklými
        pro daný druh zboží vyhovujících daným normám, předpisům a nařízením
        platným na území České republiky a zároveň řádně vybavené, záručními listy a seznamy pozáručních servisních
        středisek je-li to pro daný druh zboží obvyklé.<br />
        Podmínkou pro naplnění platnosti naší elektronické objednávky je
        vyplnění veškerých požadovaných údajů a náležitostí uvedených v
        objednávkovém formuláři. Objednávka je zároveň návrhem kupní smlouvy,
        kdy samotná kupní smlouva posléze vzniká samotným dodáním zboží. K
        uzavření kupní smlouvy se vyžaduje formální potvrzení objednávky
        prodávajícím. V jednotlivých, zejména cenově náročnějších případech, si
        prodávající vyhrazuje právo k vzniku smlouvy potvrzením objednávky
        nejlépe osobně nebo telefonicky a úhradu finanční zálohy kupujícím.<br />
        V případě, že v průběhu doby, kdy bylo zboží objednáno, došlo k výrazné
        změně kurzu zahraniční měny nebo ke změně ceny či dodávaného sortimentu
        ze strany dodavatele, má naše firma právo objednávku po dohodě s
        kupujícím modifikovat nebo od ní jednostranně s okamžitou platností
        odstoupit. Stejné oprávnění si vyhrazujeme i v případě, kdy výrobce
        přestane dodávat objednaný produkt nebo uvede na trh novou verzi
        produktu popřípadě výrazným způsobem změní cenu produktu.<br />
        <br />
        <b>3) POTVRZENÍ OBJEDNÁVKY</b><br />
        Objednávka je přijata do 24 hodin, potvrzení objednávky vám zašleme
        e-mailem, o odeslání budete taktéž informováni e-mailem. V případě
        nejasností Vás budeme samozřejmě kontaktovat.<br />
        <br />
        <b>4) ZRUŠENÍ OBJEDNÁVKY</b><br />
        Každou objednávku můžete do 24 hodin zrušit a to telefonicky, či
        e-mailem a to bez udání důvodu. Stačí uvést jméno, e-mail a popis
        objednaného zboží či služby.<br />

        <br />
        <b>5) BALNÉ A POŠTOVNÉ</b><br />
        Objednané zboží vám zašleme poštou jako dobírku. V případě, že jste na
        naší rozvozové trase bude o dodání informováni telefonicky nebo
        emailem. Je možno zvolit i jiný způsob dopravy a to dle specifikací
        jednotlivé objednávky. Cena poštovného se může lišit i podle váhy
        objednaného zboží, způsou přepravy (expresní dodánní) nebo na základě
        dodání zboží do zahraničí.<br />
        <br />
        <b>6) DODACÍ LHŮTA</b><br />
        Dodací lhůta je od 2-15 dnů, není-li uvedeno jinak. V případě, že
        některé zboží nebude skladem, budeme Vás neprodleně kontaktovat.<br />
        <br />
        <b>
          7) VÝMĚNA ZBOŽÍ</b><br />
        V případě potřeby Vám nepoužité a nepoškozené zboží vyměníme za jiný
        druh. Zboží stačí zaslat doporučeným balíkem (ne na dobírku) na naši
        adresu. Náklady spojené s vyměňováním nese v plné výši kupující. Na
        výměnu zboží nebude brán zřetel, zhodnotí li prodávající tuto službu
        jako zneužití této obchodní politiky ze strany objednatele.<br />
        <br />

        <b>9) REKLAMACE A ZÁRUKA</b><br />
        Případné reklamace vyřídíme k Vaší spokojenosti individuální dohodou s
        Vámi a v souladu s platným právním řádem. Kupující je povinen zboží po
        jeho převzetí prohlédnout tak, aby zjistil případné vady a poškození.
        Případné vady je kupující povinen neprodleně hlásit naší firmě. Za vady
        vzniklé přepravcem neručíme.<br />
        <br />
        Na veškeré zboží se vztahuje zákonná lhůta 24 měsíců, pokud není uvedeno jinak. Záruka se vztahuje pouze na výrobní
        vady.<br />
        <br />
        <b>Záruka se nevztahuje na:</b><br />
        a) vady vzniklé běžným používáním<br />
        b) nesprávným použitím výrobku<br />
        c) nesprávným skladováním<br />
        <br />

        Tato záruka neplatí jestli ji Mushow s.r.o. zhodnotí jako zneužívání této obch. politiky.<br />
        Záruka neplatí u zboží nakupovaného mimo www.kite-shop.cz.<br />
        <br />
        <b>Postup při reklamaci:</b><br />
        <br />
        1) informujte nás o reklamaci telefonicky, e-mailem, či písemně<br />
        2) zboží zašlete jako doporučený balík (ne na dobírku) na naši adresu<br />
        3) do zásilky uveďte důvod reklamace, vaši adresu<br />
        4) doklad o nabytí reklamovaného zboží v našem obchodě<br />
        <br />

        Vaši reklamaci vyřídíme co nejrychleji, nejpozději do 30 dnů od jejího
        vzniku, tedy převzetí zboží naší firmou. V případě delších reklamací
        Vás budeme neprodleně informovat o stavu reklamace.<br />
        <br />
        <br />
        <div align="center">
          <a title="reklam_rad" name="reklam_rad"></a><u><b><span style="font-size: 10pt">REKLAMAČNÍ ŘÁD</span></b><br />
          </u>
        </div>
        <br />
        Reklamační řád obsahuje informace pro zákazníka při uplatňování reklamace zboží nakoupeného na www.kite-shop.cz<br />
        <br />
        <b>Povinnosti kupujícího</b><br />
        Kupující je povinen dodané zboží při převzetí prohlédnout a bez zbytečného odkladu informovat prodávajícího o
        zjištěných závadách.<br />

        <br />
        <b>Kupující může oprávněnou reklamaci podat jednou z následujících možností:</b><br />
        a) Poštou na adresu provozovatele za použití reklamačního formuláře.<br />
        b) Osobním doručením.<br />
        <br />
        <b>Kupující je povinen uvést:</b><br />
        a) Celé své jméno, adresu bydliště a alespoň jeden funkční tel. kontakt.<br />
        b) Co nejvýstižnější popis závad a jejich projevů.<br />
        c) Doložit doklad o vlastnictví zboží (přikládaná faktura, PPD).<br />
        <br />

        <b>Povinnosti prodávajícího</b><br />
        Prodávající rozhodne o reklamaci nejpozději do 5 pracovních dnů a vyrozumí o tom kupujícího elektronickou poštou,
        pokud se s kupujícím nedohodne jinak. <br />
        Reklamace včetně vady bude vyřízena bez zbytečného odkladu, nejpozději do jednoho měsíce ode dne uplatnění reklamace,
        pokud se prodávající s kupujícím nedohodne jinak.<br />
        <br />
        <b>Reklamace uplatněná v záruční době</b><br />
        Tento reklamační řád byl zpracován dle Občanského zákoníku a vztahuje se na zboží jehož reklamace byla uplatněna v
        záruční době.<br />
        Ke každému zboží je přikládána faktura nebo PPD, který může sloužit zároveň jako záruční, pokud tento není přiložen
        (závislé na výrobci). Převzetí zboží a souhlas se záručními podmínkami stvrzuje zákazník podpisem faktury nebo PPD.
        Pokud není zboží osobně odebráno, rozumí se převzetím zboží okamžik, kdy zboží přebírá od dopravce.<br />
        Pokud odběratel zjistí jakýkoliv rozdíl mezi fakturou, PPD a skutečně dodaným zbožím (v druhu nebo množství) nebo
        neobdržel se zásilkou správně vyplněnou fakturu nebo PPD, je povinen podat ihned (nejpozději do 72 hodin) písemně
        zprávu adresovanou na jméno obchodníka, který vyhotovil fakturu. Pokud tak neučiní, vystavuje se nebezpečí, že mu
        pozdější případná reklamace nebude uznána.<br />
        <br />
        <b>Záruční podmínky</b><br />

        Délka záruky je standardně 24 měsíců (mimo jiné je vyznačena na záručním listě) kde je i výrobní číslo výrobku. Tato
        doba začíná dnem vystavení dokladu o prodeji a prodlužuje se o dobu, po kterou byl výrobek v záruční opravně. V
        případě výměny se záruční doba neprodlužuje. Zákazník dostane nový záruční list, kde bude uvedeno nové i původní
        výrobní číslo. Další případná reklamace se bude uplatňovat na základě faktury nebo PPD. Ke každé položce musí být
        připojen přesný popis závad a četnost výskytu.<br />
        Servisní středisko po vyřízení reklamace vyzve zákazníka k odběru zboží, případně jej zašle na své náklady a riziko
        zpět.<br />
        <br />
        <b>Nárok na uplatnění záruky zaniká v následujících případech:</b><br />
        - poškozením zboží při přepravě (tyto škody je nutno řešit s dopravcem při převzetí),<br />
        - porušením ochranných pečetí a nálepek, pokud na výrobku jsou,<br />
        - neodborným zacházením či obsluhou, použitím, které jsou v rozporu s uživatelskou příručkou, <br />
        - používáním zboží v podmínkách, které neodpovídají svými parametry parametrům uvedeným v dokumentaci,<br />
        - zboží bylo poškozeno živly,<br />

        - zboží bylo poškozeno nadměrným zatěžováním nebo používáním v rozporu s podmínkami uvedenými v dokumentaci,<br />
        - při opakovaném dodání neúplných průvodních dokladů (faktura, PPD atd.) je dodavatel oprávněn účtovat odběrateli
        poplatek 100,- Kč za dohledání těchto dokladů.<br />
        Dodavatel si vyhrazuje právo nahradit vadné a neopravitelné zboží jiným se stejnými parametry.<br />
        <br />
        <br />
        <span style="font-size: 10pt">Reklamace zboží:</span><br />
        <b>Rozhodnete-li se pro vrácení zboží v garanční lhůtě 14 dní, prosíme Vás o dodržení všech níže uvedených
          podmínek:</b><br />
        1. zboží nesmí být použito <br />
        2. zboží nesmí být žádným způsobem poškozeno<br />
        3. zboží musí být v originálním obalu a kompletní (včetně příslušenství, návodu, atd.) <br />

        4. společně s vraceným zbožím je třeba zaslat také veškeré doklady vydané při prodeji<br />
        5. poštovné spojené se zasláním zboží zpět k prodejci hradí kupující, vracené zboží by mělo být při přepravě pojištěno
        <br />
        6. zboží nesmí být zasíláno na dobírku, takováto zásilka nebude přijata.<br />
        <br />
        <b>Pří uplatnění výměny nebo vrácení zboží postupujte takto:<br />
        </b>1. Zákazník nejprve vyplní reklamační protokol, který najde v sekci Obchodní podmínky. Tento protokol obsahuje
        všechny důležité informace, které jsou nezbytné pro bezproblémové vyřízení reklamace. <br />
        2. Vyplněný protokol je třeba vytisknout a opatřit podpisem.<br />
        3. Protokol musí být vložen do zásilky, která bude obsahovat reklamované zboží. Zboží je třeba zabalit tak, aby obal
        dostatečně bránil poškození reklamovaného výrobku během přepravy (včetně obalu výrobku).<br />
        4. Vyřízení reklamace bude do 5 pracovních dnů od přijetí zboží.<br />

        <b><br />
          Závěrečná ustanovení</b><br />
        Tento reklamační řád nabývá účinnost 22. března 2008. <i>Změny reklamačního řádu vyhrazeny</i>.<br />
        <br />

        <br />
        <br />
        <div align="center">
          <a title="ochrana" name="ochrana"></a><u><b><span style="font-size: 10pt">OCHRANA OSOBNÍCH
                ÚDAJŮ</span></b></u><br />

        </div>
        <br />
        Prohlášení o ochraně osobních údajů<br />
        Mushow s.r.o. neshromažďuje žádná osobní data, která by mohla identifikovat specifickou osobu, kromě případů, kdy
        osoba sama poskytne Mushow s.r.o. tato data dobrovolně. Takováto data mohou být získána v případě, kdy se osoba
        dobrovolně zaregistruje za účelem využívání služeb serveru Mushow s.r.o., účastní se průzkumů, účastní se hlasování
        atd. Jakékoliv osobní informace identifikující konkrétní osobu nebudou předány, ani prodány třetí straně, kromě
        případů kdy na to uživatel bude upozorněn v době sběru dat.<br />
        Mushow s.r.o. si vyhrazuje právo provádět analýzy o chování uživatelů na svých internetových stránkách. Mezi tyto
        analýzy patří např.: měření návštěvnosti, počet uživatelů shlédnuvších reklamní banner a počet kliknutí na jednotlivý
        banner, tato data jsou k dispozici též jednotlivým zadavatelům reklamy - vždy jako statistický přehled, nikoliv
        jmenovitě. Uživatelé by také měli vzít na vědomí, že data, která dobrovolně poskytnou do diskuzních fór nebo jiných
        automaticky generovaných stránek mohou být použita třetí stranou. Takovéto využití osobních informací však nelze
        kontrolovat a Mushow s.r.o. za toto nemůže nést a neponese žádnou odpovědnost.<br />
        Uživatelé by si měli být vědomi skutečnosti, že některé informace o uživatelích mohou být automaticky sbírány v
        průběhu standardních operací našeho serveru (např. IP adresa uživatelova počítače) a také při použití cookies (malé
        textové soubory, které se ukládají na uživatelově počítači a server podle nich dokáže rozpoznat uživatele, který ho
        již jednou navštívil a poté zaznamenávat jeho chování a podle toho například přizpůsobit design a obsah nebo lépe
        zaměřovat reklamní kampaně). Cookies nejsou programy, které by mohly způsobit škodu na uživatelově počítači. Většina
        prohlížečů nabízí možnost neakceptovat cookies - elekronický obchod však nebude bez povolených cookies fungovat
        korektně.<br />
        Na žádost uživatele podnikne Mushow s.r.o. veškeré finančně přiměřené kroky k odstranění všech osobních dat daného
        uživatele.<br /><br />
        <p style='margin-bottom: 30px'><i>Toto prohlášení nabývá na platnosti 1.října 2010</i></p>
        <br />
      </div>

      <?php
    }
  }
}
?>