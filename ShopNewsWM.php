<?php

class ShopNewsWM extends WebModule {
  var $mHeader = "NOVINKY | ZBOŽÍ V AKCI";
  var $mThisUrl = WR;
  var $mMode = "head";

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {

    if (isset($_GET["mode"]))
      $this->mMode = $_GET["mode"];

    if ($this->mMode == "onsale") {
      $this->mThisUrl = WR_ONSALE;
      $this->mHeader = "ZBOŽÍ V AKCI";
    } elseif ($this->mMode == "shop-news") {
      $this->mThisUrl = WR_SHOP_NEWS;
      $this->mHeader = "NOVINKY";
    } elseif ($this->mMode == "hot") {
      $this->mThisUrl = WR_HOT_STUFF;
      $this->mHeader = "HOT STUFF";
    }

    if (isset($_POST["order_by"])) {
      $_SESSION[SN_ORDERBY] = $_POST["order_by"];
    } elseif (isset($_SESSION[SN_ORDERBY]))
      $_POST["order_by"] = $_SESSION[SN_ORDERBY];

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return $this->mHeader;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $whereCond = new WhereCondition();

    // Razeni
    $lEF = new EditCombo("order_by", "Řadit dle", 55, false, 150);
    $lEF->addFieldAttr("onChange", "this.form.submit();");
    $lEF->setGapWidth(5);
    $lEF->addSelect("Datum (od nejnovějších)", 1);
    $lEF->addSelect("Názvu", 2);
    $lEF->addSelect("Ceny", 3);
    $this->addElement($lEF);
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput($aSearch = false) {
    $lTable = "shop_item LEFT JOIN s_item_type ON shop_item.si_type = s_item_type.sit_code";
    $lTable .= " LEFT JOIN s_item_brand ON si_brand = s_item_brand.sib_code";

    // uvodni stranka
    if ($this->mMode == "head") {

      $whereCond = new WhereCondition();
      $whereCond->addCondStr("si_online = 1");

      // ---------
      // News
      echo "<div class='shop_brw' style='margin-top:-5px'>";
      echo "<h2 class='h2_head'>NOVINKY</h2>";

      $lBrw = new ItemsBrowser(2, 0);
      $result = $lBrw->executeQuery($lTable, $whereCond, "si_dcreate DESC", "");

      while ($row = $result->fetch_assoc()) {
        printShopItem($row);
      }

      echo "</div><p><a href='" . WR_SHOP_NEWS . "'>Zobrazit všechny novinky...</a></p>";

      // ---------
      // Akcni zbozi
      echo "<div class='shop_brw'>";
      echo "<h2 class='h2_head'>ZBOŽÍ V AKCI</h2>";

      $whereCond = new WhereCondition();
      $whereCond->addCondStr("si_online = 1");
      $whereCond->addCondStr("si_onsale = 1");

      $lBrw = new ItemsBrowser(2, 0);
      $result = $lBrw->executeQuery($lTable, $whereCond, "si_dcreate DESC", "");

      while ($row = $result->fetch_assoc()) {
        printShopItem($row);
      }

      echo "</div><p><a href='" . WR_ONSALE . "'>Zobrazit všechno zboží v akci...</a></p>";

      // ---------
      // Hot stuff
      echo "<div class='shop_brw shop_news'>";
      echo "<h2 class='h2_head'>HOT STUFF</h2>";

      $whereCond = new WhereCondition();
      $whereCond->addCondStr("si_online = 1");
      $whereCond->addCondStr("si_hot = 1");

      $lBrw = new ItemsBrowser(2, 0);
      $result = $lBrw->executeQuery($lTable, $whereCond, "si_hot_date DESC", "");

      while ($row = $result->fetch_assoc()) {
        printShopItem($row);
      }

      echo "</div><p><a href='" . WR_HOT_STUFF . "'>Zobrazit všechno 'hot stuff' zboží...</a></p>";

    }

    /* ------------------------------------------------------------------------*/
    /* ------------------------------------------------------------------------*/
    // ostatni
    else {
      $limit = 10;
      $offset = 0;

      if (isset($_GET["page"]))
        $offset = $_GET["page"] * $limit;

      // slozeni podminky
      $whereCond = new WhereCondition();
      $whereCond->addCondStr("si_online = 1");

      if ($this->mMode == "hot")
        $whereCond->addCondStr("si_hot = 1");
      elseif ($this->mMode == "onsale")
        $whereCond->addCondStr("si_onsale = 1");

      $lOrderBy = "si_dcreate DESC";
      if (isset($_SESSION[SN_ORDERBY])) {
        if ($_POST["order_by"] == 1)
          $lOrderBy = "si_dcreate DESC";
        elseif ($_POST["order_by"] == 2)
          $lOrderBy = "si_title ASC";
        elseif ($_POST["order_by"] == 3)
          $lOrderBy = "si_price ASC";
      }

      // vytvoreni browseru a nacteni polozek pro zborazeni
      $lBrw = new ItemsBrowser($limit, $offset);
      $result = $lBrw->executeQuery($lTable, $whereCond, $lOrderBy, "");
      if (!$result) {
        $GLOBALS["rv"]->addError(getRText("err9") . ":\n" . $result->error);
        return false;
      }


      if ($lBrw->getCountAll() == 0) {
        echo "<p>Nenalezena žádná položka.</p>";
      }
      // byla nalezena alespon jedna polozka
      else {
        echo "<div style='float:left;width:100%;height:100%;'>";
        echo "  <form method='post' id='edit_form' style='margin:0px;' action='" . $this->mThisUrl . "'>";

        $this->printElements();

        echo "  </form></div>";

        echo "<div class='shop_brw'>";

        while ($row = $result->fetch_assoc()) {
          printShopItem($row);
        }

        $lBrw->printControlDiv($this->mThisUrl);

        echo "</div>";
      }
    }

    // nakonec share button pro vsechny
    addShareButton();
  }

  /**
   * Zde naplneni vektoru cesty - pro prepsani
   */
  function definePathVect() {

  }

  /**
   * Vraci aktualni kategorii
   */
  function getActCateg() {
    if (!isset($GLOBALS[GL_CATEGS]))
      return null;

    return $GLOBALS[GL_CATEGS][count($GLOBALS[GL_CATEGS]) - 1]["sit_code"];
  }
}
?>