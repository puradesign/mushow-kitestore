<?php
require_once "AlbumUtil.php";

class GalleryAddWM extends WebModule {

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (!@$_SESSION[SN_LOGGED]) {
      $GLOBALS["rv"]->addError(getRText("err27")); // "Pro vložení galerie je nutné přihlášení."

      require_once "LoginWM.php";
      $GLOBALS["wm"] = new LoginWM(LOGIN);

      return false;
    }

    if (!empty($_POST)) {

      $title = $_POST["title"];
      $description = $_POST["description"];
      //$spot = $_POST["spot"];

      // validace povinnych polozek
      if ($title == '' || $description == '') {
        $GLOBALS["rv"]->addError(getRText("err6")); // "Některá z povinných položek není vyplněna."
        return true;
      }

      $urlTitle = createUrlTitleAlbum($title);

      // vlozeni do DB
      $query = "INSERT INTO album (`al_user`, `al_title`, `al_text`, `al_date_create`, `al_date_update`, `al_url`, `al_ip`)";
      $query .= " VALUES ('" . $_SESSION[SN_CODE] . "', '" . alterTextForDB($title) . "'";
      $query .= ", '" . alterTextForDB($description) . "', NOW(), NOW(), '$urlTitle', '" . getIP() . "')";
      $result = $GLOBALS["db"]->query($query);

      if (!$result)
        die(getRText("err9") . $result->error);

      $GLOBALS["rv"]->addInfo(getRText("err28")); // "Galerie vytvořena."

      // presmerovani na vlozeni fotek
      $_GET["aid"] = $GLOBALS["db"]->insert_id;

      require_once "GAddPicsWM.php";
      $GLOBALS["wm"] = new GAddPicsWM(G_ADD_PICS);
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return getRText("util43"); // "Galerie - vytvoření"
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {

    echo "<fieldset class='form'>";
    echo "<form method='post' action='" . WR . "?m=" . G_ADD . "'>";
    echo "  <div class='td_left'><span class='err'>* </span>" . getRText("util44") . ": </div>"; // Název
    echo "  <div class='td_right'>";
    echo "    <input type='text' name='title' maxlength='50' size='51' value='" . @$_POST["title"] . "'>";
    echo "  </div>";

    echo "  <div class='td_left' ><span class='err'>* </span>" . getRText("util8") . ": </div>"; // Popis
    echo "  <div class='td_right' style='height:100%'>";
    echo "    <textarea name='description' cols='50' rows='6''>";
    echo "      " . @$_POST["description"] . "</textarea>";
    echo "  </div>";

    echo "  <div class='td_left' style='height:100%'>";
    echo "  <input type='submit' class='submit' value='" . getRText("util45") . "'></div>"; // Vytvořit galerii
    echo "  <div class='td_one' style='padding-left: 0px'>";
    echo " <span class='note'><span class='err'>*</span> " . getRText("util20") . "</span>"; // Položky označené hvězdičkou je nutné vyplnit.
    echo "  </div>";

    echo "</form></fieldset>";

  }

  /**
   * Pro prepsani - vraci ID polozky v menu, ktera patri k tomuto WM (podle menu konstant)
   */
  function getMenuItemID() {
    return -1;//MENU_MY_GALL;
  }
}
?>