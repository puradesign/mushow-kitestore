<?php
require_once "const.php";
?>
<html lang='cs' xml:lang='cs' xmlns='http://www.w3.org/1999/xhtml'>

<head>
  <meta http-equiv='content-type' content='text/html; charset=cp1250' />
  <meta http-equiv='content-language' content='cs' />
  <meta name='Author' content='Petr Kadlec' />

  <script type='text/javascript' src='<? echo WR; ?>script/jquery.js'></script>
  <script type='text/javascript' src='<? echo WR; ?>script/jquery.innerfade.js'></script>
  <script type='text/javascript'>
    $(document).ready(function () {
      $('#ks_banner ul').show();
      $('#ks_banner ul').innerfade({
        animationtype: 'fade',
        speed: 1200,
        timeout: 8000,
        type: 'sequence',
        containerheight: '106px'
      });
    });
  </script>
  <title></title>
</head>

<body style='margin:0px;padding: 0px;font-family:Verdana, Geneva, Arial;'>
  <?php
  $_GET["banner_stajla"] = "wg";

  require "bannerForMushow.php"; ?>
</body>

</html>