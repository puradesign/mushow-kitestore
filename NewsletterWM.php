<?php

class NewsletterWM extends WebModule {

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    if (isset($_GET["delete"]) && is_numeric($_GET["delete"]) && isLoggedAdmin()) {
      $query = "UPDATE newsletter SET nl_removed = '1' WHERE `nl_id`=" . $_GET["delete"] . " LIMIT 1";
      $GLOBALS["db"]->query($query);
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Newsletter";
  }

  /**
   * Definuje tlacitko pro pridani
   */
  function defineHeaderButton() {
    echo "<div class='right_top_button add_button'>";
    echo "<a href='" . WR . "?m=" . NS_EDIT . "'>";
    echo "Nový newsletter";
    echo "</a></div>";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    $limit = 100;
    $offset = 0;

    if (isset($_GET["page"]))
      $offset = $_GET["page"] * $limit;

    $whereCond = new WhereCondition();

    $whereCond->addCondStr("nl_removed = 0");

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $result = $itemsBrowser->executeQuery("newsletter", $whereCond, "nl_date_create", "DESC");

    echo "<table class='brw_table' style='width:500px; '>";
    echo "<thead><tr><th>ID</th><th>Předmět</th><th>Datum vytvoření</th><th></th><th></th>";
    echo "</tr></thead>";

    // vykresleni polozek
    while ($row = $result->fetch_assoc()) {
      $date = date("d. m. Y, H:i", strtotime($row["nl_date_create"]));

      echo "<tr>";
      echo "<td><a href='" . WR . "?m=" . NS_EDIT . "&id=" . $row["nl_id"] . "'>" . $row["nl_id"] . "</a></td>";

      echo "<td>" . $row["nl_subject"] . "</td>";
      echo "<td>$date</td>";


      echo "<td><a href='" . WR . "?m=" . NS_EDIT . "&amp;item=" . $row["nl_id"] . "'>Edit</a></td>";
      echo "<td><a href='" . WR . "?m=" . NS_BROWSER . "&amp;delete=" . $row["nl_id"] . "' onclick='if (!confirm(\"Smazat newsletter " . $row["nl_subject"] . "?\")) return false;'>X</a></td>";

      echo "</tr>\n";
    }

    echo "</table>\n";

    $itemsBrowser->printControlDiv(WR . "?m=" . NS_BROWSER);
  }

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {

  }
}
?>