<?php
require_once "SimpleImage.php";
require_once "ItemsBrowser.php";
require_once "ArticleUtil.php";
require_once "AlbumUtil.php";

class NewsWM extends WebModule {

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return getRText("menu1"); // "News"
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput($searchCond = "", $search = false) {

    $limit = 10;
    $offset = 0;

    if (isset($_GET["page"]))
      $offset = $limit * $_GET["page"];

    // slozeni podminky
    $whereCond = new WhereCondition();

    $whereCond->addCondStr("public = 1");

    $str = "(SELECT al_code AS code, al_date_create AS dcreate, al_is_public AS public, ";
    $str .= "'0' AS kind FROM album";
    $str .= " UNION SELECT a_code AS code, a_date_create AS dcreate, a_publish AS public, ";
    $str .= "'1' AS kind FROM article WHERE a_langs % " . $_SESSION[SN_LANG] . " = 0";
    $str .= ") AS news";

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $result = $itemsBrowser->executeQuery($str, $whereCond, "dcreate");
    if (!$result)
      die(getRText("err9") . $result->error);

    // nebyla nalezena zadna polozka
    if ($itemsBrowser->getCountAll() < 1) {
      echo "<br /><p>" . getRText("util65") . "</p>"; // Výběru neodpovídají žádné položky.
    }

    // byla nalezena alespon jedna polozka
    else {
      $i = -1;

      // vykresleni novinek na strance
      while ($row = $result->fetch_assoc()) {
        $code = $row["code"];
        $i = $i * (-1);

        if ($row["kind"]) {
          $artResult = $db->query("SELECT * FROM article WHERE a_code='$code'");
          printOneArticleBrw($artResult->fetch_assoc(), $i);
        } else {
          $artResult = $db->query("SELECT * FROM album WHERE al_code='$code'");
          printOneGalleryBrw($artResult->fetch_assoc(), $i);
        }
      }

      // vykresleni ovladaci tabulky
      $params = $this->getGETparamsStr(false, false);
      $ref = WR . ($params == "" ? "" : "?" . $params);

      $itemsBrowser->printControlDiv($ref, getRText("util66")); // novinek
    }


    addShareButton();
  }

  /**
   * Pro prepsani - vraci ID polozky v menu, ktera patri k tomuto WM (podle menu konstant)
   */
  function getMenuItemID() {
    return MENU_NEWS;
  }
}
?>