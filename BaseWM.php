<?php
require_once "ItemsBrowser.php";

class BaseWM extends WebModule {
  var $useComments;
  var $fatherComment = NULL;

  /**
   * Konstruktor
   */
  function __construct($wmID, $useComments = false) {
    parent::__construct($wmID);

    $this->useComments = $useComments;
  }

  /**
   * Reaguje na akci vyvolanou uzivatelem
   */
  function reactOnActionLow() {
    parent::reactOnActionLow();

    if ($this->useComments)
      $this->reactOnActionComments();
  }

  /**
   * Vykresli obsah
   */
  final function display() {
    parent::display();

    //vlastni obsah
    if ($this->isForOutput && $this->useComments) {
      $this->printComments();
    }
  }

  /**
   * Vraci string GET parametru pro odkazy (napr. komentaru)
   */
  function getGETparamsStr($withPage = true, $firstAmper = true) {
    $str = "";
    $params = $this->getImportantParamsNames();

    foreach ($params as $pom => $name) {
      if (isset($_GET[$name]))
        $str .= ($str == "" && !$firstAmper ? "" : "&amp;") . $name . "=" . $_GET[$name];
    }

    if ($withPage && isset($_GET["page"]))
      $str .= ($str == "" && !$firstAmper ? "" : "&amp;") . "page=" . $_GET["page"];

    return $str;
  }

  /**
   * Vraci nazvy GET parametru, ktere se maji zachovavat - pro prepsani
   */
  function getImportantParamsNames() {
    return array();
  }

  // -------------------------------- Podpora komentaru -----------------------------------------//
  // -------------------------------- ----------------- -----------------------------------------//  

  /**
   * Vraci kod pro vykresleni komentaru - pro prepsani
   */
  function getCodeForComments() {
    return false;
  }

  /**
   * Vraci url clanku pro komentare - pro prepsani
   */
  function getUrlForComments() {
    return WR . "?m=0";
  }

  /**
   * Vykresli komentare
   */
  function printComments() {

    $code = $this->getCodeForComments();
    $type = $this->wmID;

    if ($code == -1)
      return;

    echo "<div class='comments'><a name='comments'></a>";

    $limit = 20;
    $offset = 0;

    if (isset($_GET["page"]))
      $offset = $limit * $_GET["page"];

    // slozeni podminky
    $whereCond = new WhereCondition();
    $whereCond->addCondStr("c_head = $code");
    $whereCond->addCondStr("c_head_type = $type");

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $result = $itemsBrowser->executeQuery("comment", $whereCond, "c_most_father DESC, c_id ASC", "");
    if (!$result)
      die(getRText("err9") . $result->error);

    echo "<br /><div class='add_button'><a href='" . $this->getUrlForComments();
    echo "?caction=add#add' style='font-size:12px;padding-left:23px; padding-right: 0px;'>";
    echo getRText("util94") . "</a></div>"; // Přidat komentář

    // nebyla nalezena zadna polozka
    if ($itemsBrowser->getCountAll() < 1) {
      echo "<br /><p>" . getRText("util96") . "</p>"; // Žádné komentáře.
    }

    // byla nalezena alespon jedna polozka
    else {

      // vykresleni prispevku fora na strance
      while ($row = $result->fetch_assoc()) {
        $indent = preg_match_all("/\./", $row["c_id"], $matches) * 20;

        echo "<div class='cpost' style='margin-left:" . $indent . "px; width: " . (550 - $indent) . "px; " . ($indent == 0 ? "margin-top: 10px;" : "") . "'>";
        echo "<a name='" . $row["c_code"] . "'></a>";
        printPostHeader($row["c_name"], $row["c_web"], $row["c_user"]);

        $date = strtotime($row["c_date"]);
        $date = StrFTime("%d.%m.%Y, %H:%M:%S", $date);
        echo "<div class='rightup'>" . $date . "</div>";
        echo $row["c_text"];
        echo "<a href='" . $this->getUrlForComments() . "?post=" . $row["c_code"];
        echo "&amp;caction=reply#" . $row["c_id"] . "' class='post_link'>" . getRText("util38") . "</a>"; // Odpovědět

        printCommentDeleteLink($row["c_code"], $row["c_user"], $this->getUrlForComments(), $type);

        echo "</div>";
      }

      // vykresleni ovladaci tabulky
      //$ref = ($type == A_VIEW ? WR_ART : WR_GAL)."?m=".$type.$this->getGETparamsStr(false);
      $itemsBrowser->printControlDiv($this->getUrlForComments(), getRText("util95")); // komentářů
    }

    echo "</div>";

    if (isset($_GET["caction"]) && $_GET["caction"] != "delete") {
      $this->printCommentForm();
    }
  }

  /**
   * Vykresli formular pro pridani komentare
   */
  function printCommentForm() {

    echo "<div style='float:left'>";
    echo "<a name='" . ($_GET['caction'] == "reply" ? $this->fatherComment["c_id"] : "add") . "'></a>";

    if (isset($_GET["post"])) {

      echo "<h3>" . getRText("util97") . ":</h3>"; // Odpověď na komentář
      echo "<div class='cpost'>";
      printPostHeader($this->fatherComment["c_name"], $this->fatherComment["c_web"],
        $this->fatherComment["c_user"]);

      $date = strtotime($this->fatherComment["c_date"]);
      $date = StrFTime("%d.%m.%Y, %H:%M:%S", $date);
      echo "<div class='rightup'>" . $date . "</div>";
      echo "<p>" . $this->fatherComment["c_text"] . "</p>";

      echo "</div>";
    } else {
      echo "<h3>" . getRText("util98") . ":</h3>"; // Nový komentář
    }

    echo "</div>";

    $wmLink = $this->getUrlForComments() . (isset($_GET["post"]) ? "?caction=reply&amp;post=" . $_GET["post"] : "?caction=add");

    echo "<div class='form'>";
    echo "	<form method='post' action='" . $wmLink . "'>";
    echo printSpamCheck1();
    echo "		<div class='td_left'><span class='err'>* </span>" . getRText("util32") . ": </div>"; // Jméno
    echo "		<div class='td_right'>";
    echo "			<input type='text' name='name' maxlength='50' size='30' value='" . @$_POST["name"] . "' onBlur='reactOnBlurField(this)'/>";
    echo "		</div>";

    echo "		<div class='td_left'><span class='err'>* </span>E-mail: </div>";
    echo "		<div class='td_right'>";
    echo "			<input type='text' name='mail' maxlength='50' size='30' value='" . @$_POST["mail"] . "'/>";
    echo "		</div>";
    echo printSpamCheck2();
    echo "		<div class='td_left'>www: </div>";
    echo "		<div class='td_right'>";
    echo "			<input type='text' name='web' maxlength='200' size='51' value='" . @$_POST["web"] . "'/>";
    echo "		</div>";

    echo "		<div class='td_one'>";
    echo "			<p>" . getRText("util33") . ":</p>"; // Text příspěvku
    echo "			<textarea class='ckeditor' id='comment_text' name='text' cols='2' rows='2'>" . @$_POST["text"] . "</textarea>";
    echo "		</div>";

    //if (!isLogged()) addCaptcha();

    echo "		<div class='td_left'><input type='submit' value='" . getRText("util34") . "'/></div>"; // Uložit příspěvek
    echo "	</form>";
    echo "</div>";
  }

  /**
   * Reaguje na akci vyvolanou uzivatelem - podpora komentaru
   */
  function reactOnActionComments() {
    if (!isset($_GET["caction"]))
      return true;

    if ($_GET["caction"] == "delete") {
      if (!isLogged()) {
        $GLOBALS["rv"]->addError(getRText("err35")); // "Mazat vlastní komentáře lze pouze po přihlášení."
        return true;
      }

      $query = "SELECT c_code, c_user, c_id, c_head, c_head_type FROM comment WHERE c_code =" . $_GET["post"];
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      if (!$row) {
        $GLOBALS["rv"]->addError(getRText("err36")); // "Nebyl nalezen komentář pro smazání."
        return true;
      }

      if (isLoggedAdmin()) {
        $query = "DELETE FROM comment WHERE c_head = " . $row["c_head"] . " AND c_head_type = " . $row["c_head_type"] . " AND c_id LIKE '" . $row["c_id"] . "%' ORDER BY c_id DESC";
        $result = $GLOBALS["db"]->query($query);

        $GLOBALS["rv"]->addInfo(getRText("err37")); // "Komentář byl smazán."
      } elseif (!empty($row["c_user"]) && isLoggedUser($row["c_user"])) {
        $query = "SELECT c_code FROM comment WHERE c_father = " . $_GET["post"] . " LIMIT 1";
        $result = $GLOBALS["db"]->query($query);
        $sonRow = $result->fetch_assoc();

        if ($sonRow) {
          $GLOBALS["rv"]->addError(getRText("err38")); // "Komentář nemůže být smazán, protože už na něj někdo reagoval."
          return true;
        }

        $query = "DELETE FROM comment WHERE c_code = " . $_GET["post"] . " LIMIT 1";
        $result = $GLOBALS["db"]->query($query);

        $GLOBALS["rv"]->addInfo(getRText("err37")); // "Komentář byl smazán."
      }

      return true;
    }

    // pokud se jedna o odpoved na existujici prispevek
    if (isset($_GET["post"])) {
      $query = "SELECT * FROM comment WHERE c_code = " . $_GET["post"];
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      // pokud nebyla nalezena zadna veta
      if (!$row) {
        $GLOBALS["rv"]->addError(getRText("err16")); // "Nebyl nalezen příspěvek pro reakci, pravděpodobně byla ručně upravena URL stránky. Zkuste to prosím znovu."
        unset($_GET["caction"]);
        unset($_GET["post"]);
        return true;
      } else {
        $this->fatherComment = $row;
      }
    }

    // pokud je uzivatel prihlasen, vyplni se nektere hodnoty
    if (@$_SESSION["logged"] && empty($_POST)) {
      $_POST["name"] = $_SESSION["login"];
      $_POST["mail"] = $_SESSION["mail"];

      $_POST["email1"] = "hehe" . $_POST["name"] . "haha";
      return true;
    }

    // zpracovani formulare
    if (!empty($_POST)) {
      // spam kontrola
      if (!isLogged() && !doSpamCheck("name", false))
        return true;

      $name = $_POST["name"];
      $mail = $_POST["mail"];
      $web = $_POST["web"];
      $text = $_POST["text"];

      // validace povinnych polozek
      if ($name == "" || $mail == "" || $text == "") {
        $GLOBALS["rv"]->addError(getRText("err6")); // "Některá z povinných položek není vyplněna."
        return true;
      }

      // tvar emailove adresy
      if (!preg_match("/^.+@.+\.[a-zA-Z]+$/", $mail)) {
        $GLOBALS["rv"]->addError(getRText("err17")); // "Emailová adresa má neplatný tvar."
        return true;
      }

      // tvar webove adresy
      if ($web != "" && !preg_match("/^http:\/\/.+$/", $web)) {
        $web = "http://" . $web;
      }

      $itemCode = $this->getCodeForComments();

      // vypocet id komentare
      if (empty($this->fatherComment)) {
        $query = "SELECT max(c_most_father) AS lastHead FROM comment WHERE c_father IS NULL";
        $query .= " AND c_head = $itemCode AND c_head_type = $this->wmID";
        $result = $GLOBALS["db"]->query($query);
        $row = $result->fetch_assoc();

        $ID = $row["lastHead"] + 1;
        $most_father = $ID;
      } else {
        $query = "SELECT c_id FROM comment WHERE c_father = " . addSlashes($_GET["post"]);
        $query .= " AND c_head = $itemCode AND c_head_type = $this->wmID ORDER BY c_id DESC LIMIT 1";
        $result = $GLOBALS["db"]->query($query);
        $row = $result->fetch_assoc();

        $ID = $this->fatherComment["c_id"] . ".";
        if (!$row)
          $ID .= "1";
        else
          $ID .= (preg_replace("/.*\.(\d*)$/", '$1', $row["c_id"]) + 1);

        $most_father = $this->fatherComment["c_most_father"];
      }

      // vlozeni do DB
      $query = "INSERT INTO comment (`c_name`, `c_mail`, `c_web`, `c_text`, `c_date`, `c_user`, ";
      $query .= "`c_father`, `c_id`, `c_most_father`, `c_head`, `c_head_type`, `c_ip`)";
      $query .= " VALUES ('" . alterTextForDB($name) . "', '" . alterTextForDB($mail) . "'";
      $query .= ", '" . alterTextForDB($web) . "'";
      $query .= ", '" . addSlashes($text) . "', NOW()";
      $query .= ", " . (@$_SESSION["logged"] ? ("'" . $_SESSION["code"] . "'") : "NULL");
      $query .= ", " . (isset($_GET["post"]) ? "'" . $_GET["post"] . "'" : "NULL");
      $query .= ", '" . $ID . "', " . $most_father;
      $query .= ", '" . $this->getCodeForComments() . "', '" . $this->wmID . "', '" . getIP() . "')";
      $result = $GLOBALS["db"]->query($query);

      if (!$result)
        die(getRText("err9") . $result->error);

      //$GLOBALS["rv"]->addInfo("Příspěvek vložen.");

      // uklidit po pridavani komentu
      unset($_GET["caction"]);
      unset($_GET["post"]);

    }

    return true;
  }
}
?>