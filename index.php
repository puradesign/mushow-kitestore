<?php
require_once "TextsObject.php"; // pred startem protoze je drzen v sesne
require_once "CartObject.php";
session_start();
require_once "connect.php";
require_once "UtilFunctions.php";
require_once "const.php";
require_once "WebModule.php";
require_once "BaseWM.php";
require_once "SimpleImage.php";
require_once "ItemsBrowser.php";
require_once "StoreUtil.php";

require_once F_HTML . "PresentElement.php";
require_once F_HTML . "EditField.php";
require_once F_HTML . "EditFile.php";
require_once F_HTML . "EditText.php";
require_once F_HTML . "EditCKText.php";
require_once F_HTML . "EditBool.php";
require_once F_HTML . "EditCombo.php";
require_once F_HTML . "EditCodeCombo.php";
require_once F_HTML . "EditRadio.php";
require_once F_HTML . "EditInt.php";
require_once F_HTML . "ButtonImg.php";



function beforeOutput() {
  // vytvoreni vektoru vysledku akce
  $GLOBALS["rv"] = new ResultVect();

  $GLOBALS["pv"] = new PathVect();
  // $GLOBALS["rv"]->addInfo("session id: ".session_id());

  if (@$_SESSION["logged_now"] == "yes" && isLogged()) {
    $GLOBALS["rv"]->addInfo("Přihlášení bylo úspěšné.");
    $_SESSION["logged_now"] = "no";
  }
  if (@$_SESSION["logged_out"] == "yes" && !isLogged()) {
    $GLOBALS["rv"]->addInfo("Byl jste odhlášen.");
    $_SESSION["logged_out"] = "no";
  }

  // inicializace kosiku
  if (!isset($_SESSION[SN_CART]))
    $_SESSION[SN_CART] = new CartObject();

  if (!isset($_SESSION[SN_TEXTS])) {
    $_SESSION[SN_TEXTS] = new TextsObject();
    $_SESSION[SN_TEXTS]->initTexts(LANG_CODE_CS); // vzdy cestina
  }

  if (isset($_GET['m']) && $_GET['m'] == "error")
    $_GET['m'] = -1;

  $module = NEWS;

  if (isset($_GET['m'])) {
    $module = (int) $_GET['m'];

    if ($module == LOGIN && file_exists(F_ROOT . "LoginWM.php")) {
      require_once F_ROOT . "LoginWM.php";
      $GLOBALS["wm"] = new LoginWM(LOGIN);
    } elseif ($module == LOGOUT && file_exists(F_ROOT . "LogoutWM.php")) {
      require_once F_ROOT . "LogoutWM.php";
      $GLOBALS["wm"] = new LogoutWM(LOGOUT);
    } elseif ($module == FORUM && file_exists(F_ROOT . "ForumWM.php")) {
      require_once F_ROOT . "ForumWM.php";
      $GLOBALS["wm"] = new ForumWM(FORUM);
    } elseif ($module == F_ADD_POST && file_exists(F_ROOT . "FAddPostWM.php")) {
      require_once F_ROOT . "FAddPostWM.php";
      $GLOBALS["wm"] = new FAddPostWM(F_ADD_POST);
    } elseif ($module == S_CATEG && file_exists(F_ROOT . "ShopCategWM.php")) {
      require_once F_ROOT . "ShopCategWM.php";
      $GLOBALS["wm"] = new ShopCategWM(S_CATEG);
    } elseif ($module == S_CATEG_EDIT && file_exists(F_ROOT . "ShopCategEditWM.php")) {
      require_once F_ROOT . "ShopCategEditWM.php";
      $GLOBALS["wm"] = new ShopCategEditWM(S_CATEG_EDIT);
    } elseif ($module == S_CATEG_DEL && file_exists(F_ROOT . "ShopCategDeleteWM.php")) {
      require_once F_ROOT . "ShopCategDeleteWM.php";
      $GLOBALS["wm"] = new ShopCategDeleteWM(S_CATEG_DEL);
    } elseif ($module == SHOP && file_exists(F_ROOT . "ShopWM.php")) {
      require_once F_ROOT . "ShopWM.php";
      $GLOBALS["wm"] = new ShopWM(SHOP);
    } elseif ($module == SHOP_EDIT && file_exists(F_ROOT . "ShopEditWM.php")) {
      require_once F_ROOT . "ShopEditWM.php";
      $GLOBALS["wm"] = new ShopEditWM(SHOP_EDIT);
    } elseif ($module == SI_EDIT && file_exists(F_ROOT . "ShopItemEditWM.php")) {
      require_once F_ROOT . "ShopItemEditWM.php";
      $GLOBALS["wm"] = new ShopItemEditWM(SI_EDIT);
    } elseif ($module == SI_DELETE && file_exists(F_ROOT . "ShopItemDeleteWM.php")) {
      require_once F_ROOT . "ShopItemDeleteWM.php";
      $GLOBALS["wm"] = new ShopItemDeleteWM(SI_DELETE);
    } elseif ($module == SI_VIEW && file_exists(F_ROOT . "ShopItemViewWM.php")) {
      require_once F_ROOT . "ShopItemViewWM.php";
      $GLOBALS["wm"] = new ShopItemViewWM(SI_VIEW);
    } elseif ($module == REGISTER || $module == PROF_EDIT) {
      require_once F_ROOT . "RegisterWM.php";
      $_GET["action"] = $module == REGISTER ? "add" : "edit";
      $GLOBALS["wm"] = new RegisterWM(REGISTER);
    } elseif ($module == ADDRS) {
      require_once F_ROOT . "AddrsWM.php";
      $GLOBALS["wm"] = new AddrsWM(ADDRS);
    } elseif ($module == ADDR_EDIT) {
      require_once F_ROOT . "AddrEditWM.php";
      $GLOBALS["wm"] = new AddrEditWM(ADDR_EDIT);
    } elseif ($module == CART) {
      require_once F_ROOT . "CartWM.php";
      $GLOBALS["wm"] = new CartWM(CART);
    } elseif ($module == BAZAR) {
      require_once F_ROOT . "BazarWM.php";
      $GLOBALS["wm"] = new BazarWM(BAZAR);
    } elseif ($module == B_EDIT) {
      require_once F_ROOT . "BazarEditWM.php";
      $GLOBALS["wm"] = new BazarEditWM(B_EDIT);
    } elseif ($module == B_VIEW) {
      require_once F_ROOT . "BazarViewWM.php";
      $GLOBALS["wm"] = new BazarViewWM(B_VIEW);
    } elseif ($module == FIX_TEXT) {
      require_once F_ROOT . "FixedTextsWM.php";
      $GLOBALS["wm"] = new FixedTextsWM(FIX_TEXT);
    } elseif ($module == ORDERS) {
      require_once F_ROOT . "StoreOrderWM.php";
      $GLOBALS["wm"] = new StoreOrderWM(ORDERS);
    } elseif ($module == OR_VIEW) {
      require_once F_ROOT . "StoreOrderViewWM.php";
      $GLOBALS["wm"] = new StoreOrderViewWM(OR_VIEW);
    } elseif ($module == SHOP_NEWS) {
      require_once F_ROOT . "ShopNewsWM.php";
      $GLOBALS["wm"] = new ShopNewsWM(SHOP_NEWS);
    } elseif ($module == A_EDIT) {
      require_once F_ROOT . "ArticleEditWM.php";
      $GLOBALS["wm"] = new ArticleEditWM(A_EDIT);
    } elseif ($module == A_VIEW) {
      require_once F_ROOT . "ArticleViewWM.php";
      $GLOBALS["wm"] = new ArticleViewWM(A_VIEW);
    } elseif ($module == SKLAD) {
      require_once F_ROOT . "SkladWM.php";
      $GLOBALS["wm"] = new SkladWM(SKLAD);
    } elseif ($module == SEARCH) {
      require_once F_ROOT . "SearchWM.php";
      $GLOBALS["wm"] = new SearchWM(SEARCH);
    } elseif ($module == OR_EDIT) {
      require_once F_ROOT . "StoreOrderEditWM.php";
      $GLOBALS["wm"] = new StoreOrderEditWM(OR_EDIT);
    } elseif ($module == USERS) {
      require_once F_ROOT . "UsersWM.php";
      $GLOBALS["wm"] = new UsersWM(OR_EDIT);
    } elseif ($module == NS_BROWSER) {
      require_once F_ROOT . "NewsletterWM.php";
      $GLOBALS["wm"] = new NewsletterWM(NS_BROWSER);
    } elseif ($module == NS_EDIT) {
      require_once F_ROOT . "NewsletterEditWM.php";
      $GLOBALS["wm"] = new NewsletterEditWM(NS_EDIT);
    } elseif ($module == NS_VIEW) {
      require_once F_ROOT . "NewsletterViewWM.php";
      $GLOBALS["wm"] = new NewsletterViewWM(NS_VIEW);
    }
  } else {
    require_once F_ROOT . "ShopNewsWM.php";
    $GLOBALS["wm"] = new ShopNewsWM(SHOP_NEWS);
  }

  if (!isset($GLOBALS["wm"]))
    $GLOBALS["wm"] = new WebModule(0);

  if ($GLOBALS["wm"]->wmID != LOGIN && $GLOBALS["wm"]->wmID != LOGOUT && @$_GET['m'] > -1)
    $_SESSION[SN_LASTWM] = $_SERVER["REQUEST_URI"];

  // reakce na akci webmodulem
  $GLOBALS["wm"]->reactOnActionLow();

  if ($GLOBALS["wm"]->wmID != LOGIN && $GLOBALS["wm"]->wmID != LOGOUT && @$_GET['m'] > -1)
    $_SESSION[SN_LASTWM] = $_SERVER["REQUEST_URI"];
}

beforeOutput();
?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='cs' xml:lang='cs' xmlns='http://www.w3.org/1999/xhtml'>

<head>
  <meta http-equiv='content-type' content='text/html; charset=utf-8' />
  <meta http-equiv='content-language' content='cs' />

  <meta name='Copyright' content='Copyright 2010, http://www.mushow.cz/' />
  <meta name='description' content='<?php echo htmlspecialchars($GLOBALS["wm"]->getMetaDescription(), ENT_QUOTES); ?>' />
  <meta name="keywords"
    content="kite obchod, Kiteboarding, Snowkiting, Wakeboarding, slingshot, mushow, ozone, kites, boards, e-shop, obchod, " />
  <link rel='stylesheet' href='<? echo WR; ?>css/main.css' />
  <link rel='stylesheet' href='<? echo WR; ?>lib/tooltip/jquery.tooltip.css' />
  <link rel="shortcut icon" href='http://mushow.cz/img/mushow.ico' />
  <link rel="stylesheet" href="<? echo WR; ?>lib/nivoslider/nivo-slider.css" type="text/css" media="screen" />

  <script type='text/javascript' src='<? echo WR; ?>script/jquery2.js'></script>
  <script type='text/javascript' src='<? echo WR; ?>script/jquery.innerfade.js'></script>
  <!--   <script type='text/javascript' src='<? echo WR; ?>lib/tooltip/jquery.tooltip.min.js'></script> -->
  <script src="<? echo WR; ?>lib/nivoslider/jquery.nivo.slider.pack.js" type="text/javascript"></script>
  <script type='text/javascript' src='<? echo WR; ?>script/main.js'></script>
  <?php
  $GLOBALS["wm"]->addScripts();
  ?>
  <!--[if IE]>
   <link rel='stylesheet' href='<? echo WR; ?>css/IEmain.css' />
 <![endif]-->

  <title><?php echo !isset($_GET["m"]) ? "" : $GLOBALS["wm"]->getHeader() . " | ";
  echo "Mushow Kite Store | mushow.cz/kitestore";

  ?>
  </title>
</head>

<body>
  <div id='bg_head'></div>
  <div id='wwidth'>
    <div id='wrapper'>

      <div class='header'>
        <?php require 'header.php' ?>
      </div>

      <div id='wr_body'>

        <div class='left_panel'>
          <?php require "left_panel.php"; ?>
        </div>

        <div id='wr_path'>

        </div>

        <div id='content'>
          <div class="middle_panel">
            <?php $GLOBALS["wm"]->display(); ?>
            <div class='mp_bottom'></div>
          </div>
        </div>

        <div class='right_panel'>
          <?php require "right_panel.php"; ?>
        </div>

      </div>
    </div>
    <div id='bottom'>
      <?php require "bottom.php"; ?>
    </div>
  </div>
</body>

</html>