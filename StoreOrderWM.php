<?php
require_once 'lib/phpexcel/Classes/PHPExcel.php';

class StoreOrderWM extends WebModule {
  var $mAction;
  var $mFile;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (!isLogged()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    if (isset($_GET["delete"]) && is_numeric($_GET["delete"]) && isLoggedAdmin()) {
      $query = "DELETE FROM shop_order WHERE `or_code`=" . $_GET["delete"] . " LIMIT 1";
      $GLOBALS["db"]->query($query);
    }

    if (isset($_GET["export"])) {
      $this->exportOrders(@$_GET["year"], @$_GET["months"]);
    }

    // -------
    // user filter
    if (!isset($_GET["user"])) {
      if (isset($_SESSION["orders_user"]))
        $_GET["user"] = $_SESSION["orders_user"];
    } else {
      if (@$_GET["user"] == '0') {
        unset($_GET["user"]);
        unset($_SESSION["orders_user"]);
      } else {
        $_SESSION["orders_user"] = $_GET["user"];
      }
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Objednávky";
  }

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {

    // Uzivatel
    $lEF = new EditCodeCombo("user", "Objednávky uživatele", 120, false, 250,
      "user LEFT JOIN user_address ON u_main_addr = ua_code", "u_code", "ua_surname",
      array("ua_name", "u_mail"));
    $lEF->setOrderBy("ua_surname ASC, ua_name ASC");
    $lEF->addFieldAttr("onChange", "$(this).closest(\"form\").submit();");
    $lEF->addFieldAttr("class", "chosen");
    $lEF->initOptions("Vše");

    $this->addElement($lEF);

  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    $limit = 100;
    $offset = 0;

    if (isset($_GET["page"]))
      $offset = $_GET["page"] * $limit;

    // EXPORT do XLS
    //echo "<p id='orders_export'><a href='".WR_ORDER."?export' style='font-weight:bold;font-size:14px;' onclick='$(\"#orders_export\").html(\"Soubor <i>sklad".date('ymd').".xlsx</i> se právě generuje. Za pár vteřin by se měl automaticky stáhnout.\");'>Export do Excelu...</a></p>";

    echo "<form action='" . WR_ORDER . "?export' id='export_form' method='GET' style=''>";
    echo "<label>Tržby:</label>";
    echo "<div class='form-container'><select name='year' id='export-years' class='chosen' style='width: 70px;'>";
    echo $this->getSelectYears();
    echo "</select></div>";
    echo "<input type='hidden' name='export' value='1'/>";
    echo "<div class='form-container'>
                <select name='months[]' class='chosen' multiple style='width: 150px;' id='export-months'
                    data-placeholder='Vybrat měsíce...'>";
    $months = array('1' => 'leden', '2' => 'únor', '3' => 'březen', '4' => 'duben', '5' => 'květen', '6' => 'červen',
      '7' => 'červenec', '8' => 'srpen', '9' => 'září', '10' => 'říjen', '11' => 'listopad', '12' => 'prosinec');
    foreach ($months as $num => $name)
      echo "<option value='$num'>$name</option>";
    echo "</select></div>";

    echo "<input type='submit' value='Export do XLS'>";
    echo "</form>";


    if ($this->mFile != "")
      echo "<p><a href='" . WR . "temp/" . $this->mFile . "' style='font-weight:bold;font-size:14px;'>" . $this->mFile . "</a></p>";



    // slozeni podminky
    $whereCond = new WhereCondition();

    if (isLoggedAdmin()) {
      if (isset($_GET["user"])) {
        $whereCond->addCondStr("or_user = " . addslashes($_GET["user"]));
      }

      echo "<form action='" . WR_ORDER . "' id='edit_form' method='GET' style='margin: 10px 0; float: left; z-index: 10; position: relative'>";
      $this->printElements();
      echo "</form>";
    } else {
      $whereCond->addCondStr("or_user = " . $_SESSION[SN_CODE]);
    }

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $result = $itemsBrowser->executeQuery("shop_order", $whereCond, "or_number", "DESC");
    if (!$result) {
      $GLOBALS["rv"]->addError(getRText("err9") . ":\n" . $result->error);
      return false;
    }

    echo "<table class='brw_table' style='width:500px; '>";
    echo "<thead><tr><th>Číslo</th><th>Datum</th><th>Cena celkem</th><th>Stav</th><th></th>";
    if (isLoggedAdmin())
      echo "<th></th><th></th>";
    echo "</tr></thead>";

    // vykresleni polozek
    while ($row = $result->fetch_assoc()) {
      $lDate = StrFTime("%d.%m.%Y", strtotime($row["or_date"]));

      echo "<tr>";
      echo "<td><a href='" . WR_ORDER . "/" . $row["or_number"] . "'>" . $row["or_number"] . "</a></td>";
      echo "<td>$lDate</td>";
      echo "<td style='text-align:right'>" . number_format($row["or_price_sum"], 2, ".", " ") . ",- Kč</td>";

      $lColor = $row["or_order_state"] == 1 ? "#2F6A8B" : ($row["or_order_state"] == 2 ? "#20991C" : "#aaa");
      echo "<td><span class='badge' style='background-color:$lColor'>" . getOrderStateStr($row["or_order_state"]) . "</span></td>";

      echo "<td><a href='" . WR_ORDER . "/" . $row["or_number"] . "'>Zobrazit</a></td>";

      if (isLoggedAdmin()) {
        echo "<td><a href='" . WR . "?m=" . OR_EDIT . "&amp;item=" . $row["or_code"] . "'>Edit</a></td>";

        if ($row["or_order_state"] == 3)
          echo "<td><a href='" . WR_ORDER . "?delete=" . $row["or_code"] . "' onclick='if (!confirm(\"Fakt chceš smazat objednávku číslo " . $row["or_number"] . "?\")) return false;'>X</a></td>";
        else
          echo "<td></td>";
      }

      echo "</tr>\n";
    }

    echo "</table>\n";

    $itemsBrowser->printControlDiv(WR_ORDER);
  }

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {
    echo "<link rel='stylesheet' type='text/css' href='" . WR . "script/chosen/chosen.min.css' />";
    echo "<script type='text/javascript' src='" . WR . "script/chosen/chosen.jquery.min.js'></script>";

    ?>
    <script type='text/javascript'>
      $(document).ready(function () {
        $('.chosen').chosen();

        $('#export-years').chosen().change(function () {
          if ($('#export-years').val() == '0')
            $('#export-months').parent().hide();
          else
            $('#export-months').parent().show();
        });
      });
    </script>
    <?php
  }

  /**
   * Prida potrebne skripty modulu
   */
  function getSelectYears() {
    $html = "<option value='0'>vše</option>";
    $nowyear = date('Y');

    for ($year = 2010; $year < $nowyear; $year++) {
      $html .= "<option value='$year'>$year</option>";
    }

    $html .= "<option value='$nowyear' selected='selected'>$nowyear</option>";

    return $html;
  }

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function exportOrders($year, $months) {
    if (!isLoggedAdmin()) {
      $this->setForOutput(false);
      $GLOBALS["rv"]->addError("Musí být přihlášen administrátor.");

      return false;
    }

    $GLOBALS["rv"]->addInfo("Soubor <i>orders" . date('ymd') . ".xlsx</i> se právě generuje. Za pár vteřin by se měl automaticky stáhnout. ");

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set properties
    //echo date('H:i:s') . " Set properties\n";
    $objPHPExcel->getProperties()->setCreator("Petr Kadlec")
      ->setLastModifiedBy("Petr Kadlec")
      ->setTitle("Mushow Kitestore tržby")
      ->setSubject("Mushow Kitestore tržby")
      ->setDescription("Přehled tržeb Mushow Kitestore.");


    // Add some data
    //echo date('H:i:s') . " Add some data\n";

    $lActRow = 1;

    // sirky sloupcu
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(3);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);

    //$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(5);

    $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

    // head
    $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue("A2", 'Faktura')
      ->setCellValue("B2", 'Datum')
      ->setCellValue("C2", 'DPH základ')
      ->setCellValue("D2", 'DPH')
      ->setCellValue("E2", 'Celkem')
      ->setCellValue("G2", 'Položka faktury')
      ->setCellValue("H2", 'Počet')
      ->setCellValue("I2", 'Cena/kus');

    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getFill()
      ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
      ->getStartColor()->setARGB('dddddd');

    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getFont()->setSize(10);

    $styleArray = array(
      'borders' => array(
        'outline' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => '000000'),
        ),
      ),
    );

    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($styleArray);

    $lTotal = 0;
    $lTotalCount = 0;

    $query = "SELECT * FROM shop_order WHERE or_order_state != 3";
    if ($year > 0) {
      // some months selected
      if (!empty($months)) {
        $subqueries = array();
        foreach ($months as $month) {
          $month = str_pad($month, 2, "0", STR_PAD_LEFT);
          $subqueries[] = "(or_date >= '$year-$month-01' AND or_date <= '$year-$month-31')";
        }

        $query .= " AND " . implode(" OR ", $subqueries);
      } // year only
      else {
        $query .= " AND or_date >= '$year-01-01' AND or_date <= '$year-12-31'";
      }
    }

    $query .= " ORDER BY or_date ASC";
    $result = $GLOBALS["db"]->query($query);
    //echo $query; exit;

    $lastCode = -1;
    $lActRow = 3;

    // jednotlive polozky
    while ($row = $result->fetch_assoc()) {

      $total = $row["or_price_sum"];

      $lNoDPH = $total / 1.2;
      $lDPH = $total - $lNoDPH;

      $objPHPExcel->getActiveSheet()->setCellValue("A$lActRow", $row["or_number"]);
      $objPHPExcel->getActiveSheet()->setCellValue("B$lActRow", date('d. m. Y', strtotime($row["or_date"])));
      $objPHPExcel->getActiveSheet()->setCellValue("C$lActRow", $lNoDPH);
      $objPHPExcel->getActiveSheet()->setCellValue("D$lActRow", $lDPH);
      $objPHPExcel->getActiveSheet()->setCellValue("E$lActRow", $total);

      $objPHPExcel->getActiveSheet()->getStyle("A$lActRow")->getFont()->getColor()->setARGB("444444");
      //$objPHPExcel->getActiveSheet()->getStyle("A$lActRow")->getFont()->setSize(8);

      $objPHPExcel->getActiveSheet()->getStyle("C$lActRow:E$lActRow")->getNumberFormat()
        ->setFormatCode('#,##0');

      // vypis polozek faktury
      $queryItems = "SELECT * FROM order_item WHERE ori_head = '" . $row["or_code"] . "'";
      $resultItems = $GLOBALS["db"]->query($queryItems);

      while ($rowItem = $resultItems->fetch_assoc()) {
        $objPHPExcel->getActiveSheet()->setCellValue("G$lActRow", $rowItem["ori_name"]);
        $objPHPExcel->getActiveSheet()->setCellValue("H$lActRow", $rowItem["ori_count"]);
        $objPHPExcel->getActiveSheet()->setCellValue("I$lActRow", $rowItem["ori_price"]);

        $lActRow++;
      }
    }

    if (!empty($result)) {
      $lNoDPH = $lTotal / 1.2;
      $lDPH = $lTotal - $lNoDPH;

      // CELKEM
      $objPHPExcel->getActiveSheet()->setCellValue("A$lActRow", "CELKEM");
      $objPHPExcel->getActiveSheet()->setCellValue("C$lActRow", "=SUM(C3:C" . ($lActRow - 1) . ")");
      $objPHPExcel->getActiveSheet()->setCellValue("D$lActRow", "=SUM(D3:D" . ($lActRow - 1) . ")");
      $objPHPExcel->getActiveSheet()->setCellValue("E$lActRow", "=SUM(E3:E" . ($lActRow - 1) . ")");

      $objPHPExcel->getActiveSheet()->getStyle("B$lActRow:E$lActRow")->getNumberFormat()
        ->setFormatCode('#,##0');

      // ramecek pro celkem
      $objPHPExcel->getActiveSheet()->getStyle("A$lActRow:E$lActRow")->applyFromArray($styleArray);
      $objPHPExcel->getActiveSheet()->getStyle("A$lActRow:E$lActRow")->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()->setARGB('dddddd');
    }

    $lFileName = 'orders' . date('ymd') . '.xlsx';

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $lFileName . '"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
  }
}
?>