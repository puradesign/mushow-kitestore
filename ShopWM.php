<?php

class ShopWM extends WebModule {
  var $mHeader = "Kite Store";
  var $mThisUrl = WR;
  var $mHotStuff = false;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {

    // akcni nabidka
    if (!isset($_GET["brand"]) && !isset($_GET['categ'])) {
      $this->mHotStuff = true;
      $this->mHeader = "AKČNÍ ZBOŽÍ";
    }

    // znacka
    if (isset($_GET["brand"])) {
      $query = "SELECT * FROM s_item_brand WHERE sib_code=" . $_GET["brand"];
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      $GLOBALS[GL_BRAND] = $row;
      $this->mHeader = alterHtmlTextToPlain($row["sib_name"]);
      $this->mThisUrl .= $row["sib_url"] . "/";
    }

    // kategorie
    if (isset($_GET['categ'])) {
      $this->mThisUrl .= $_GET['categ'];
      $GLOBALS[GL_CATEGS] = array();
      $categs = explode("/", $_GET['categ']);

      for ($i = 0; $i < count($categs); $i++) {
        $query = "SELECT * FROM s_item_type WHERE sit_url='" . addSlashes($categs[$i]) . "'";
        $query .= " AND sit_brand=" . $_GET["brand"];
        $result = $GLOBALS["db"]->query($query);
        $row = $result->fetch_assoc();

        if (!$row) {
          $GLOBALS["rv"]->addError("Nepodařilo se nalézt kategorii.");
          return false;
        }

        $GLOBALS[GL_CATEGS][] = $row;
        $this->mHeader = $row["sit_name"];
      }
    }

    if (isset($_POST["order_by"])) {
      $_SESSION[SN_ORDERBY] = addSlashes($_POST["order_by"]);
    } elseif (isset($_SESSION[SN_ORDERBY]))
      $_POST["order_by"] = $_SESSION[SN_ORDERBY];

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return $this->mHeader;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $whereCond = new WhereCondition();

    // Razeni
    $lEF = new EditCombo("order_by", "Řadit dle", 55, false, 150);
    $lEF->addFieldAttr("onChange", "this.form.submit();");
    $lEF->setGapWidth(5);
    $lEF->addSelect("Datum (od nejnovějších)", 1);
    $lEF->addSelect("Názvu", 2);
    $lEF->addSelect("Ceny", 3);
    $this->addElement($lEF);
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput($aSearch = false, $aSearchCond = "") {
    $limit = 10;
    $offset = 0;

    if (isset($_GET["page"]))
      $offset = $_GET["page"] * $limit;

    // slozeni podminky
    $whereCond = new WhereCondition();
    $whereCond->addCondStr("si_online = 1");

    if ($aSearch && $aSearchCond != "")
      $whereCond->addCondStr($aSearchCond);

    if ($this->mHotStuff)
      $whereCond->addCondStr("si_hot = 1");

    if (isset($GLOBALS[GL_BRAND]))
      $whereCond->addCondStr("si_brand = " . $GLOBALS[GL_BRAND]["sib_code"]);

    if (isset($GLOBALS[GL_CATEGS])) {
      $lCategsStr = $this->getCategsStrForCond($this->getActCateg());
      $whereCond->addCondStr("si_type IN ($lCategsStr)");
    }

    $lOrderBy = "si_dcreate DESC";
    if (isset($_SESSION[SN_ORDERBY])) {
      if ($_POST["order_by"] == 1)
        $lOrderBy = "si_dcreate DESC";
      elseif ($_POST["order_by"] == 2)
        $lOrderBy = "si_title ASC";
      elseif ($_POST["order_by"] == 3)
        $lOrderBy = "si_price ASC";
    }

    if ($this->mHotStuff)
      $lOrderBy = "si_hot_date DESC";

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $lBrw = new ItemsBrowser($limit, $offset);
    $lTable = "shop_item LEFT JOIN s_item_type ON shop_item.si_type = s_item_type.sit_code";
    $lTable .= " LEFT JOIN s_item_brand ON si_brand = s_item_brand.sib_code";
    $result = $lBrw->executeQuery($lTable, $whereCond, $lOrderBy, "");
    if (!$result) {
      $GLOBALS["rv"]->addError(getRText("err9") . ":\n" . $result->error);
      return false;
    }


    if ($lBrw->getCountAll() == 0) {
      echo "<p style='margin: 40px 5px 290px 5px;'>Nenalezena žádná položka.</p>";
    }
    // byla nalezena alespon jedna polozka
    else {
      if (!$this->mHotStuff) {
        echo "<div style='float:left;width:100%;height:100%;'>";
        echo "  <form method='post' id='edit_form' style='margin:0px;' action='" . $this->mThisUrl . "'>";

        $this->printElements();

        echo "  </form></div>";
      }

      echo "<div class='shop_brw'>";

      if ($lBrw->getCountAll() > $limit) {
        echo "<div style='margin-top:-30px'>";
        $lBrw->printControlDiv($this->mThisUrl);
        echo "</div>";
      }

      while ($row = $result->fetch_assoc()) {
        printShopItem($row);
      }

      $lBrw->printControlDiv($this->mThisUrl);

      echo "</div>";
    }

    //addShareButton();
  }

  /**
   * Zde naplneni vektoru cesty - pro prepsani
   */
  function definePathVect() {
    if (isset($GLOBALS[GL_BRAND])) {
      $lBrandRow = $GLOBALS[GL_BRAND];
      $lUrl = WR . $lBrandRow["sib_url"];

      $GLOBALS["pv"]->addItem($lUrl, $lBrandRow["sib_name"]);

      if (isset($GLOBALS[GL_CATEGS])) {
        $lCategs = $GLOBALS[GL_CATEGS];

        for ($i = 0; $i < count($lCategs); $i++) {
          $lUrl .= "/" . $lCategs[$i]["sit_url"];
          $GLOBALS["pv"]->addItem($lUrl, $lCategs[$i]["sit_name"]);
        }
      }
    }
  }

  /**
   * Vraci retezec pro dotaz na kategorie
   */
  function getCategsStrForCond($aHeadCateg) {
    $str = (string) $aHeadCateg;

    $query = "SELECT * FROM s_item_type WHERE sit_head = $aHeadCateg";
    $result = $GLOBALS["db"]->query($query);

    while ($row = $result->fetch_assoc())
      $str .= ", " . $this->getCategsStrForCond($row["sit_code"]);

    return $str;
  }

  /**
   * Vraci aktualni kategorii
   */
  function getActCateg() {
    if (!isset($GLOBALS[GL_CATEGS]))
      return null;

    return $GLOBALS[GL_CATEGS][count($GLOBALS[GL_CATEGS]) - 1]["sit_code"];
  }
}
?>