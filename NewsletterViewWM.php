<?php
require_once "SimpleImage.php";
require_once "ArticleUtil.php";

class NewsletterViewWM extends WebModule {
  var $action;
  var $mHeader = "Přidat newsletter";
  var $mActRow;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    // unsubscribe
    if (@$_GET['action'] == 'unsubscribe') {
      if (empty($_GET['key']) || empty($_GET['email']))
        return false;

      $email = alterTextForDB($_GET['email']);
      $sql = "SELECT * FROM user WHERE u_mail = '$email'";
      $result = $GLOBALS["db"]->query($sql);
      $userRow = $result->fetch_assoc();

      if (!$userRow)
        return false;

      $key = sha1($userRow['u_date_create'] . $email . "solenicko" . $userRow['u_code']);
      if ($key == $_GET['key']) {
        $sql = "UPDATE user SET u_unsubscribed = '1' WHERE u_code = " . $userRow['u_code'];
        $result = $GLOBALS["db"]->query($sql);

        $GLOBALS["rv"]->addInfo("Příjem novinek na email $email byl zrušen.");
      }
    }

    // odeslani newsletteru
    elseif (@$_GET['action'] == 'sendall') {
      $query = "SELECT * FROM email_stack ORDER BY es_date ASC LIMIT 200";
      $result = $GLOBALS["db"]->query($query);

      $sentCount = 0;

      // get users
      while ($row = $result->fetch_assoc()) {
        $address = $row["es_address"];
        $subject = $row["es_subject"];
        $text = $row["es_text"];

        // send email
        sendNewsletter($address, $subject, $text);

        $sentCount++;

        // delete from stack
        $query = "DELETE FROM email_stack WHERE es_id ='" . $row["es_id"] . "'";
        $GLOBALS["db"]->query($query);

        // insert to history
        $query = "INSERT INTO email_history (esh_address, esh_newsletter,esh_date_sent) VALUES
               		 ('$address', '" . $row["es_newsletter"] . "', '" . date('Y-m-d H:i:s') . "')";
        $GLOBALS["db"]->query($query);
      }

      echo "Sent $sentCount emails";
      exit;
    }

    // view newsletter
    elseif (!empty($_GET["id"])) {
      $query = "SELECT * FROM newsletter WHERE nl_id=" . intval($_GET["id"]) . " AND nl_removed = 0";
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      if (!$row) {
        header('location: ' . WR . '?m=error');
      }

      echo $this->getNewsletterHtml($row['nl_text'], $row['nl_id']);
      exit;
    }
  }

  /**
   * Zobrazeni emailu
   */
  function getNewsletterHtml($html, $id) {
    return "<html>
    		<head>
    			<meta http-equiv='content-type' content='text/html; charset=utf-8' />
    			<meta http-equiv='content-language' content='cs' />
    			<style type='text/css'>
    				#outlook a {padding:0;}
    				body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
    				.ExternalClass {width:100%;}
    				.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    				#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
    				
    				img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; max-width: 600px}
					a img {border:none;}
					.image_fix {display:block;}
					p {margin: 1em 0;}
					table td {border-collapse: collapse;}
					p { line-height: 21px; font-size: 14px;}
					h2 { font-size: 17px; color: #00b0a1; text-align: left; text-decoration: underline}
					h3 { font-size: 17px; color: #7c1212; text-align: left; text-decoration: underline}
					h4 { font-size: 15px; margin: 0 0 10px 0;color: #555; text-align: left; text-decoration: none}
					
					a { color: #00b0a1; text-decoration: underline; }
    			</style>
    		</head>
            <body style=\"font-family:Helvetica, sans-serif; font-size:14px; line-height: 18px;padding: 0;  
    				background:url('" . WR . "img/newsletter_bg.png');color:#333; margin: 0;\">
    			<div style='float: none; width: 600px; padding: 5px 20px 20px 20px; background: #fff; margin: 0 auto;min-height: 400px'>
    				
    				<h1 style='display:block; font-size: 0; width: 600px; height:74px; padding:0; margin:5px 0;
    							background: url(\"" . WR . "img/newsletter_top.jpg\") no-repeat'>
    					Mushow newsletter</h1>
                    
                    $html
                    
                    <div style='width: 600px; margin: 45px 0; height: 30px '>
                    	<a href='http://mushow.cz' style='display: block; width:170px; margin:0; padding: 10px; 
                    		border-radius: 10px; background: #00b0a1; color: #fff; text-align: center; float: left;
                    		text-decoration: none; font-weight: 600; font-size: 15px'>Mushow News</a>
                    		
                    	<a href='http://mushow.cz/kitestore' style='display: block; width:170px; margin:0 0 0 30px; padding: 10px 0; 
                    		border-radius: 10px; background: #7c1212; color: #fff; text-align: center; float: left;
                    		text-decoration: none; font-weight: 600; font-size: 15px'>Mushow e-shop</a>
                    		
                    	<a href='http://www.facebook.com/pages/Mushow-community/361467200304' 
                    		style='display: block; width:170px; margin:0 0 0 30px; padding: 10px 0; 
                    		border-radius: 10px; background: #185ea0; color: #fff; text-align: center; float: left;
                    		text-decoration: none; font-weight: 600; font-size: 15px'>Mushow facebook</a>
                    </div>
                    
                    <div style='display:block; font-size: 0; width: 600px; height:34px; padding:0; margin:5px 0;
    							background: url(\"" . WR . "img/newsletter_foot.jpg\") no-repeat'>
    					Mushow newsletter</div>
                </div>
            </body> 
    	</html>";
  }

  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Odhlášení z newsletteru";
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {

  }
}
?>