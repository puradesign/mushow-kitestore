<?php

class FAddPostWM extends WebModule {
  var $father = NULL;

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {
    echo "<script type='text/javascript' src='" . F_CKEDITOR . "ckeditor.js'></script>\n";
    echo "<script type='text/javascript' src='" . WR_SCRIPT . "ckeditor_conf_plain.js'></script>\n";
    echo "<script type='text/javascript' src='" . WR_SCRIPT . "ckeditor_conf_forum.js'></script>\n";
    echo "<script type='text/javascript'>\n";
    echo "  CKEDITOR.config.customConfig = '" . WR_SCRIPT . "ckeditor_conf_";
    echo (isset($_GET["post"]) || !isLoggedAdmin() ? "plain" : "forum") . "plain.js';\n";
    echo "  CKEDITOR.replace(text);\n";
    echo "</script>";
  }

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {

    if (isset($_GET["post"])) {
      $query = "SELECT * FROM fpost WHERE f_code = " . $_GET["post"];
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      // pokud nebyla nalezena zadna veta
      if (!$row) {
        $GLOBALS["rv"]->addError(getRText("err16")); // "Nebyl nalezen příspěvek pro reakci, pravděpodobně byla ručně upravena URL stránky. Zkuste to prosím znovu."

        // presmerovani na forum
        require_once "ForumWM.php";
        $GLOBALS["wm"] = new ForumWM(FORUM);
        $GLOBALS["wm"]->reactOnActionLow();

        return false;
      } else {
        $this->father = $row;
      }
    }

    // pokud je uzivatel prihlasen, vyplni se nektere hodnoty
    if (isLogged() && empty($_POST)) {
      $_POST["name"] = $_SESSION[SN_NICK];
      $_POST["mail"] = $_SESSION[SN_MAIL];

      $_POST["email1"] = "hehe" . $_POST["name"] . "haha";
      return true;
    }

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    // ochrana znovuodeslani formulare
    if (isset($_SESSION["form_sent"])) {
      // presmerovani na forum
      require_once "ForumWM.php";
      $GLOBALS["wm"] = new ForumWM(FORUM);

      $GLOBALS["wm"]->reactOnActionLow();

      return false;
    }

    // spam kontrola
    if (!doSpamCheck("name"))
      return true;

    $name = $_POST["name"];
    $mail = $_POST["mail"];
    $web = $_POST["web"];
    $text = $_POST["text"];

    // validace povinnych polozek
    if ($name == "" || $mail == "" || $text == "") {
      $GLOBALS["rv"]->addError(getRText("err6")); // "Některá z povinných položek není vyplněna."
      return true;
    }

    // tvar emailove adresy
    if (!preg_match("/^.+@.+\.[a-zA-Z]+$/", $mail)) {
      $GLOBALS["rv"]->addError(getRText("err17")); // "Emailová adresa má neplatný tvar."
      return true;
    }

    // tvar webove adresy
    if ($web != "" && !preg_match("/^http:\/\/.+$/", $web)) {
      $web = "http://" . $web;
    }

    if (empty($this->father)) {
      $query = "SELECT max(f_head) AS lastHead FROM fpost WHERE f_father IS NULL";
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      $ID = $row["lastHead"] + 1;
      $head = $ID;
    } else {
      $query = "SELECT f_id FROM fpost WHERE f_father = " . $_GET["post"] . " ORDER BY f_id DESC LIMIT 1";
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      $ID = $this->father["f_id"] . ".";
      if (!$row)
        $ID .= "1";
      else
        $ID .= (preg_replace("/.*\.(\d*)$/", '$1', $row["f_id"]) + 1);

      $head = $this->father["f_head"];
    }

    // vlozeni do DB
    $query = "INSERT INTO fpost (`f_name`, `f_mail`, `f_web`, `f_text`, `f_date`, `f_user`, `f_father`, `f_id`, `f_head`, `f_ip`)";
    $query .= " VALUES ('" . alterTextForDB($name) . "', '" . alterTextForDB($mail) . "'";
    $query .= ", '" . alterTextForDB($web) . "'";
    $query .= ", '" . addSlashes($text) . "', NOW()";
    $query .= ", " . (isLogged() ? ("'" . $_SESSION[SN_CODE] . "'") : "NULL");
    $query .= ", " . (isset($_GET["post"]) ? "'" . $_GET["post"] . "'" : "NULL");
    $query .= ", '" . $ID . "', " . $head . ", '" . getIP() . "')";
    $result = $GLOBALS["db"]->query($query);

    if (!$result)
      die(getRText("err9") . $result->error);

    $GLOBALS["rv"]->addInfo((getRText("err18")) . "."); // "Příspěvek vložen

    // presmerovani na forum
    require_once "ForumWM.php";
    $GLOBALS["wm"] = new ForumWM(FORUM);

    $GLOBALS["wm"]->reactOnActionLow();

    $_SESSION["form_sent"] = true;

    return false;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return getRText("util30"); // "Přidat příspěvek"
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {

    // Jméno
    $lEF = new EditField("name", getRText("util32"), 100, true,
      200, 50);
    $lEF->addFieldAttr("onBlur", "reactOnBlurField(this)");
    $this->addElement($lEF);

    // E-mail
    $lEF = new EditField("mail", "E-mail", 100, true,
      200, 50);
    $this->addElement($lEF);

    // Web
    $lEF = new EditField("web", "Web", 100, false,
      300, 200);
    $this->addElement($lEF);

    // Text příspěvku
    $lEF = new EditCKText("text", getRText("util33"), true);
    $this->addElement($lEF);
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    if (isset($_GET["post"])) {

      echo "<p>" . getRText("util31") . ":</p>"; // Odpověď na příspěvek
      echo "<div class='fpost'>";
      echo "<div><span style='font-size: 14px; font-weight:bold'>" . $this->father["f_name"] . "</span>";
      echo "</div>\n";

      $date = strtotime($this->father["f_date"]);
      $date = StrFTime("%d.%m.%Y, %H:%M:%S", $date);
      echo "<div class='rightup'>" . $date . "</div>";
      echo "<p>" . $this->father["f_text"] . "</p>";

      echo "</div>";
    }

    echo "<form method='post' action='" . WR_FOR . "?m=" . F_ADD_POST . (isset($_GET["post"]) ? "&amp;post=" . $_GET["post"] : "") . "'>";
    echo "<fieldset class='form'>";
    printSpamCheck1();

    $this->printElements();


    printSpamCheck2();


    echo "<div class='td_left'><input type='submit' value='" . getRText("util34") . "'/></div>"; // Uložit příspěvek

    echo " </fieldset></form>";

    echo "<p><a href='" . WR_FOR . "'>" . getRText("util35") . "</a></p>"; // Zpět na fórum

    unset($_SESSION["form_sent"]);
  }

  /**
   * Zde naplneni vektoru cesty - pro prepsani
   */
  function definePathVect() {
    $GLOBALS["pv"]->addItem(WR_FOR, getRText("menu12")); // "Fórum"
    $GLOBALS["pv"]->addItem("", getRText("util30")); // "Přidat příspěvek"
  }

  /**
   * Pro prepsani - vraci ID polozky v menu, ktera patri k tomuto WM (podle menu konstant)
   */
  function getMenuItemID() {
    return MENU_FORUM;
  }
}
?>