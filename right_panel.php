<?php
?>
<div class='right_box' id='cart_box'>
  <?php
  // kosik je prazdny
  if ($_SESSION[SN_CART]->isEmpty()) {
    echo "<p style='margin:45px 0px 0px 10px;'>Košík je prázdný.</p>";
  } else {
    echo "<p style='position:absolute; top:50px; left: 48px;'>";
    echo "<b style='font-size:12px;'>" . $_SESSION[SN_CART]->getTotalCount() . "</b> položek v koši</p>";
    echo "<p style='position:absolute; top:70px; left: 8px;'>Celkem: <b style='font-size:12px;'>";
    echo number_format($_SESSION[SN_CART]->getCashTotal(), 0, "", ".") . " Kč</b></p>";
    echo "<p style='position:absolute; bottom:10px; left: 8px;'><a href='" . WR_CART . "'>Obsah košíku/Objednat</a></p>";
  }

  if (isLogged()) {
    echo "<p style='position:absolute; top:86px; left: 8px;'>";
    echo "Sleva: <span style='font-weight:bold;font-size:12px;'>" . $_SESSION[SN_CART]->getDiscount() . "%</span></p>";
  }
  ?>
</div>

<?php
if (isLoggedAdmin()) {
  ?>

  <div class='right_box'>
    <div class='box_content'>
      <div class='box_head'>
        <h3 style=''>ADMIN</h3>
        <ul class='right_menu'>
          <?php
          echo "<li><a href='" . WRS . "?m=" . S_CATEG . "'>Kategorie</a></li>";
          echo "<li><a href='" . WRS . "?m=" . SHOP_EDIT . "'>Zboží</a></li>";
          echo "<li><a href='" . WR_BAZAR . "?edit'>Bazar</a></li>";
          echo "<li><a href='" . WR_ORDER . "'>Objednávky</a></li>";
          echo "<li><a href='" . WRS . "?m=" . SKLAD . "'>Sklad</a></li>";
          echo "<li><a href='" . WRS . "users'>Uživatelé</a></li>";
          echo "<li><a href='" . WRS . "?m=" . NS_BROWSER . "'>Newsletter</a></li>";

          echo "<li style='margin-top:8px;'><a href='" . WR . "?m=5&amp;action=edit&amp;item=1'>Kontakt</a></li>";
          echo "<li><a href='" . WR . "?m=5&amp;action=edit&amp;item=2'>Obch. podmínky</a></li>";
          echo "<li><a href='" . WR . "?m=5&amp;action=edit&amp;item=3'>Slevy</a></li>";
          echo "<li><a href='" . WR . "?m=5&amp;action=edit&amp;item=4'>GDPR podmínky</a></li>";

          echo "<li style='margin-top:15px;'><a href='" . WR . "?m=" . LOGOUT . "'>" . getRText("menu15") . "</a></li>"; // Odhlásit se
          ?>
        </ul>

      </div>
    </div>
    <div class='box_tail'></div>
  </div>

  <?php
}

// panel s on sale a hot stuff
echo "<div id='goods_banner' class='right_box'>";
$lTable = "shop_item LEFT JOIN s_item_type ON shop_item.si_type = s_item_type.sit_code";
$lTable .= " LEFT JOIN s_item_brand ON si_brand = s_item_brand.sib_code";

$query = "SELECT * FROM $lTable WHERE si_online = 1 AND si_onsale=1 LIMIT 4";
$result = $GLOBALS["db"]->query($query);

echo "<ul id='action_goods' style='margin-left:7px; margin-top:18px;height:130px;width:130px'>";

while ($row = $result->fetch_assoc()) {

  $lUrl = WR . $row["sib_url"] . "/" . $row["sit_url"] . "/v/" . $row["si_url"];
  $lImgUrl = getBrowserPath(F_SHOP_ITEMS . "headfoto" . $row["si_code"] . ".jpg");

  echo "<li><a href='$lUrl'><img src='$lImgUrl' height='130px' alt='" . $row["si_title"] . "' style='' title='" . $row["si_title"] . "'/></a>";
  echo "<div style='position:absolute;left:0px;bottom:5px;height:13px;width:130px;background-color:#0e2937; padding-top:2px;text-align:center'>";
  echo "<a href='$lUrl' style='font-size:10px;padding:2px;color:#dddddd'>" . $row["si_title"] . "</a></div></li>";
}

echo "</ul>";

$query = "SELECT * FROM $lTable WHERE si_online = 1 AND si_hot=1 LIMIT 3";
$result = $GLOBALS["db"]->query($query);

echo "<ul id='hot_stuff' style='margin-left:7px;margin-top:8px;height:130px;width:130px'>";

while ($row = $result->fetch_assoc()) {

  $lUrl = WR . $row["sib_url"] . "/" . $row["sit_url"] . "/v/" . $row["si_url"];
  $lImgUrl = getBrowserPath(F_SHOP_ITEMS . "headfoto" . $row["si_code"] . ".jpg");

  echo "<li><a href='$lUrl'><img src='$lImgUrl' height='130px' alt='" . $row["si_title"] . "' style='' title='" . $row["si_title"] . "'/></a>";
  echo "<div style='position:absolute;left:0px;bottom:5px;height:13px;width:130px;background-color:#0e2937; padding-top:2px;text-align:center'>";
  echo "<a href='$lUrl' style='font-size:10px;padding:2px;color:#dddddd'>" . $row["si_title"] . "</a></div></li>";
}

echo "</ul>";

echo "</div>";

echo "<div class='right_box'>";

echo "<a href='" . WR . "slevovy-system' title='slevový systém'>";
echo "<img src='" . WR_IMG . "discounts.jpg' alt='slevový systém'/></a>";

echo "</div>";


if (!isLogged()) {
  ?>

  <div class='right_box login_box'>
    <div class='box_content'>
      <div class='box_head'>
        <h3 style='margin-top: -22px;font-size:20px'>LOGIN</h3>
        <?php
        echo "<form method='post' action='" . WR . "login?a=login'>";
        echo "	<p class='note'>Přihlašovací jméno:</p>";
        echo "	<input type='text' name='username' maxlength='30' style='width:119px;'/><br/>";
        echo "	<p class='note'>Heslo:</p>";
        echo "	<input type='password' name='password' maxlength='32' style='width:119px;'/><br/>";
        echo "	<input type='submit' value='Přihlásit se'/>";
        echo "</form>";

        echo "<p class='note'>Nemáte vytvořený účet?</p>";
        echo "<p><a href='" . WR . "registrace'>Registrovat se.</a></p>";
        echo "<p><a href='" . WR . "login?a=send_pswd'>Zapomněli jste heslo?</a></p>";
        ?>

      </div>
    </div>
    <div class='box_tail'></div>
  </div>

  <?php
} elseif (!isLoggedAdmin()) {
  ?>

  <div class='right_box'>
    <div class='box_content'>
      <div class='box_head'>
        <h3 style='margin-left : -10px; margin-right: -10px'>USER MENU</h3>
        <?php
        echo "<p class='note' style='margin:-9px 0px 5px -5px;'>Přihlášen: <span style='font-weight:bold;font-size:11px;'>" . $_SESSION[SN_NICK] . "</span></p>";
        echo "<ul class='right_menu'>";

        echo "<li><a href='" . WR_PROFILE . "'>Editace údajů</a></li>";
        echo "<li><a href='" . WR . "?m=" . ADDRS . "'>Dodací adresy</a></li>";
        echo "<li><a href='" . WR_BAZAR . "?edit'>Moje inzeráty</a></li>";
        echo "<li><a href='" . WR_ORDER . "'>Moje objednávky</a></li>";

        echo "<li style='margin-top:15px;'><a href='" . WR . "?m=" . LOGOUT . "'>" . getRText("menu15") . "</a></li>"; // Odhlásit se
        ?>
        </ul>

      </div>
    </div>
    <div class='box_tail'></div>
  </div>

  <?php
}
//require "bannerForMushow.php"; TEST
?>



<div class='right_box'>
  <div class='box_content'>
    <div class='box_head'>
      <h3 style='margin-left:-7px'>PARTNERS</h3>
      <a href="http://www.mushow.cz"><img src="<?php echo WR . "img/banner/mushow.png"; ?>"
          style="width: 125px; margin-bottom:5px; padding:0px;" alt="mushow kiteboarding" /></a>
      <a href="http://www.slingshotsports.com/"><img src="<?php echo WR . "img/banner/slingshot.png"; ?>"
          style="width: 125px; margin-bottom:5px; padding:0px;" alt="slingshot" /></a>
      <a href="http://www.mushow.cz/news/oakley/"><img src="<?php echo WR . "img/banner/oakley.jpg"; ?>"
          style="width: 125px; margin-bottom:5px; padding:0px;" alt="oakley" /></a>
      <a href="http://www.redbull.cz/"><img src="<?php echo WR . "img/banner/redbull.jpg"; ?>"
          style="width: 125px; margin-bottom:5px; padding:0px;" alt="red bull" /></a>

      <a href="http://www.toplist.cz/toplist/?kat=6&amp;search=kiteboarding&amp;a=s" target="_top"><img
          src="http://toplist.cz/count.asp?id=435512&amp;logo=mc" border="0" alt="TOPlist" width="1" height="1" /></a>
    </div>
  </div>
  <div class='box_tail'></div>
</div>