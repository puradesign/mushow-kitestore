<?php

class BazarEditWM extends WebModule {
  var $mAction;
  var $mHeader = "Přidat inzerát";
  var $mActRow;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {

    if (!isset($_GET["action"]))
      $this->mAction = "add";
    else
      $this->mAction = $_GET["action"];

    if (($this->mAction != "add" && $this->mAction != "edit") ||
      ($this->mAction == "edit" && (!isset($_GET["item"]) || !is_numeric($_GET["item"])))) {
      $GLOBALS["rv"]->addError("Chyba. Pravděpodobně byla ručně upravena url stránky.");
      $this->setForOutput(false);
      return false;
    }

    // kontroly
    if (!isLogged()) {
      $GLOBALS["rv"]->addError("Pro přidání/editaci inzerátu musíte být přihlášen.");
      $this->setForOutput(false);

      require_once "LoginWM.php";
      $GLOBALS["wm"] = new LoginWM(LOGIN);
      $GLOBALS["wm"]->reactOnActionLow();

      return false;
    }

    // editace
    if ($this->mAction == "edit") {
      $query = "SELECT * FROM advert WHERE ad_code=" . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);

      $row = $result->fetch_assoc();

      if ($row["ad_user"] != $_SESSION[SN_CODE]) {
        $GLOBALS["rv"]->addError("Nemáte právo editovat tento inzerát.");
        $this->setForOutput(false);
        return false;
      }

      $this->mActRow = $row;
      $this->mHeader = alterHtmlTextToPlain($row["ad_title"]) . " - editace";

      if (empty($_POST)) {
        $_POST["ad_title"] = alterHtmlTextToPlain($row["ad_title"]);
        $_POST["ad_name"] = alterHtmlTextToPlain($row["ad_name"]);
        $_POST["ad_email"] = alterHtmlTextToPlain($row["ad_email"]);
        $_POST["ad_text"] = alterHtmlTextToPlain($row["ad_text"]);
        $_POST["ad_price"] = alterHtmlTextToPlain($row["ad_price"]);
        $_POST["ad_phone"] = alterHtmlTextToPlain($row["ad_phone"]);
        $_POST["ad_type"] = $row["ad_type"];
        $_POST["ad_categ"] = $row["ad_categ"];
      }
    } elseif ($this->mAction == "add" && empty($_POST)) {
      $query = "SELECT * FROM user LEFT JOIN user_address ON u_main_addr = user_address.ua_code WHERE u_code=" . $_SESSION[SN_CODE];
      $result = $GLOBALS["db"]->query($query);

      $row = $result->fetch_assoc();

      $_POST["ad_name"] = alterHtmlTextToPlain($row["ua_name"]) . " " . alterHtmlTextToPlain($row["ua_surname"]);
      $_POST["ad_email"] = alterHtmlTextToPlain($row["u_mail"]);
      $_POST["ad_phone"] = alterHtmlTextToPlain($row["ua_tel"]);
      $_POST["ad_type"] = 1;
    }

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    $title = alterTextForDB($_POST["ad_title"]);
    $name = alterTextForDB($_POST["ad_name"]);
    $email = alterTextForDB($_POST["ad_email"]);
    $text = alterTextForDB($_POST["ad_text"]);
    $price = alterTextForDB($_POST["ad_price"]);
    $phone = alterTextForDB($_POST["ad_phone"]);
    $type = $_POST["ad_type"];
    $categ = $_POST["ad_categ"];

    $image = new ImageUploaded($_FILES["ad_foto"]);

    $lCond = new WhereCondition();

    // vlozeni
    if ($this->mAction == "add") {

      // vlozeni do DB
      $query = "INSERT INTO advert (`ad_title`, `ad_categ`, `ad_type`, `ad_text`, ";
      $query .= "`ad_user`, `ad_name`, `ad_email`, `ad_phone`, `ad_price`, `ad_date_create`, `ad_ip`)";
      $query .= " VALUES ('$title', '$categ', '$type', '$text'";
      $query .= ", " . (@$_SESSION[SN_LOGGED] ? ("'" . $_SESSION[SN_CODE] . "'") : "NULL");
      $query .= ", '$name', '$email', '$phone', '$price'";
      $query .= ", NOW(), '" . getIP() . "')";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError('OOps, neco se stalo. Pokud potize pretrvaji, kontaktujte spravce:' . $result->error);
        return true;
      }

      $id = $GLOBALS["db"]->insert_id;

      if (isset($image)) {
        $image->loadFile();

        // ulozeni obrazku
        $image->cropToSize(100, 100);
        $image->save(F_BAZAR . "advert_foto_" . $id . ".jpg", IMAGETYPE_JPEG);
      }

      $GLOBALS["rv"]->addInfo("Inzerát úspěšně vložen.");

      require_once "BazarWM.php";
      $GLOBALS["wm"] = new BazarWM(BAZAR);
      $_GET["edit"] = "yes";
      $GLOBALS["wm"]->reactOnActionLow();

      return false;
    }

    // editace
    elseif ($this->mAction == "edit") {

      // update v DB
      $query = "UPDATE advert SET `ad_title` = '$title'";
      $query .= ", `ad_text` = '$text'";
      $query .= ", `ad_categ` = '$categ'";
      $query .= ", `ad_type` = '$type'";
      $query .= ", `ad_name` = '$name'";
      $query .= ", `ad_email` = '$email'";
      $query .= ", `ad_phone` = '$phone'";
      $query .= ", `ad_price` = '$price'";
      $query .= " WHERE `ad_code` = " . $this->mActRow["ad_code"] . " LIMIT 1";

      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError($query . 'OOps, neco se stalo. Pokud potize pretrvaji, kontaktujte spravce:' . $result->error);
        return true;
      }

      if (!empty($_FILES["ad_foto"]["name"])) {
        $image->loadFile();

        // ulozeni obrazku
        $image->cropToSize(100, 100);
        $image->save(F_BAZAR . "advert_foto_" . $this->mActRow["ad_code"] . ".jpg", IMAGETYPE_JPEG);
      }

      $GLOBALS["rv"]->addInfo("Inzerát úspěšně upraven.");
      unset($_POST);
      require_once "BazarWM.php";
      $GLOBALS["wm"] = new BazarWM(BAZAR);
      $_GET["edit"] = "yes";

      $GLOBALS["wm"]->reactOnActionLow();

      return true;
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {

    return $this->mHeader;
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $lPromptWidth = 120;
    $lInputWidth = 200;
    $whereCond = new WhereCondition();
    $lIsEdit = $this->mAction == "edit";

    $lEF = new PresentElement("<fieldset class='form'><legend>Předmět inzerátu</legend>");
    $this->addElement($lEF);

    // Nazev
    $lEF = new EditField("ad_title", "Název", $lPromptWidth, true, $lInputWidth, 40);
    $this->addElement($lEF);

    // Kategorie
    $lEF = new EditCodeCombo("ad_categ", "Kategorie", 120, true,
      $lInputWidth, "advert_categ", "adc_code", "adc_name");
    $lEF->initOptions();
    $this->addElement($lEF);

    // Typ
    $lEF = new EditRadio("ad_type", "Typ", $lPromptWidth, true, false);
    $lEF->addRadio("Nabídka", 1);
    $lEF->addRadio("Poptávka", 2);
    $this->addElement($lEF);

    // Foto
    $lEF = new EditFile("ad_foto", "Foto", $lPromptWidth, $this->mAction == "add");
    $this->addElement($lEF);

    // Cena
    $lEF = new EditField("ad_price", "Cena", $lPromptWidth, true, 100, 20);
    $this->addElement($lEF);

    // Popis
    $lEF = new EditText("ad_text", "Popis", $lPromptWidth, true, 46, 1000, 5);
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset><fieldset class='form'><legend>Kontakt</legend>");
    $this->addElement($lEF);

    // Jmeno
    $lEF = new EditField("ad_name", "Jméno", $lPromptWidth, true, $lInputWidth, 150);
    $this->addElement($lEF);

    // Email
    $lEF = new EditField("ad_email", "Email", $lPromptWidth, true, $lInputWidth, 150);
    $this->addElement($lEF);

    // Telefon
    $lEF = new EditField("ad_phone", "Telefon", $lPromptWidth, false, 100, 20);
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset>");
    $this->addElement($lEF);
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    //	 echo "<p>Přidání/editace inzerátu.</p>";
    echo "  <form method='post' id='edit_form' action='" . WR . "?m=" . B_EDIT . "&amp;action=" . $this->mAction;
    echo ($this->mAction != "add" ? "&amp;item=" . $_GET["item"] : "") . "' enctype='multipart/form-data'>";

    $this->printElements();

    echo "  <fieldset class='form' style='padding-top:5px; padding-bottom:3px'><div class='td_left' style='height:100%;width: 80px;'>";
    echo "  <input type='submit' class='submit' value='Uložit'/></div>";
    echo "<p>(<span style='color:#551310;font-weight:bold;font-size:14px;'>*</span>Položky označené hvězdičkou jsou povinné.)</p>";
    echo "  </fieldset></form>";

    //    echo "<p>(<span style='color:#551310;font-weight:bold;font-size:14px;'>*</span>Položky označené hvězdičkou jsou povinné.)</p>";

    echo "<p><a href='" . WR_BAZAR . "?edit'>Zpět na přehled inzerátů</a></p>";
  }
}
?>