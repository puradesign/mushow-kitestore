<?php
require_once F_HTML . "ItemRow.php";
require_once "StoreUtil.php";

class StoreOrderEditWM extends WebModule {
  var $mHeader = "Editace objednávky";
  var $mActRow;
  var $mItemRows = array();
  var $mRealTotal = 0;

  /* -------------------------- BEFORE ACTION -------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    // kontroly
    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    // ----------------------- Akce EDITACE ----------------------------------//
    // -------------------------------------------------------==--------------//
    $query = "SELECT * FROM shop_order WHERE or_code=" . $_GET["item"];
    $result = $GLOBALS["db"]->query($query);

    $row = $result->fetch_assoc();
    $this->mActRow = $row;
    $this->mHeader = "Objednávka č." . $row["or_number"];

    if (empty($_POST)) {
      $_POST["or_user"] = $row["or_user"];
      $_POST["or_number"] = $row["or_number"];
      $_POST["or_order_state"] = $row["or_order_state"];
      $_POST["or_email"] = $row["or_email"];
      $_POST["or_fa_name"] = $row["or_fa_name"];
      $_POST["or_fa_surname"] = $row["or_fa_surname"];
      $_POST["or_fa_company"] = $row["or_fa_company"];
      $_POST["or_fa_street"] = $row["or_fa_street"];
      $_POST["or_fa_town"] = $row["or_fa_town"];
      $_POST["or_fa_psc"] = $row["or_fa_psc"];
      $_POST["or_fa_state"] = $row["or_fa_state"];
      $_POST["or_fa_tel"] = $row["or_fa_tel"];
      $_POST["or_fa_ico"] = $row["or_fa_ico"];
      $_POST["or_fa_dic"] = $row["or_fa_dic"];

      $_POST["or_name"] = $row["or_name"];
      $_POST["or_surname"] = $row["or_surname"];
      $_POST["or_company"] = $row["or_company"];
      $_POST["or_street"] = $row["or_street"];
      $_POST["or_town"] = $row["or_town"];
      $_POST["or_psc"] = $row["or_psc"];
      $_POST["or_state"] = $row["or_state"];
      $_POST["or_tel"] = $row["or_tel"];
      $_POST["or_ico"] = $row["or_ico"];
      $_POST["or_dic"] = $row["or_dic"];

      $_POST["or_trans_price"] = $row["or_trans_price"];
      $_POST["or_trans_text"] = $row["or_trans_text"];
      $_POST["or_pay_text"] = $row["or_pay_text"];
      $_POST["or_price_sum"] = $row["or_price_sum"];

      $_POST["or_descr"] = $row["or_descr"];

      $date = strtotime($row["or_date"]);
      $date = StrFTime("%d/%m/%Y", $date);
      $_POST["or_date"] = $date;
      $date = strtotime($row["or_date_due"]);
      $date = StrFTime("%d/%m/%Y", $date);
      $_POST["or_date_due"] = $date;

      $query = "SELECT * FROM order_item WHERE ori_head='" . $_GET["item"] . "'";
      $result = $GLOBALS["db"]->query($query);

      // naplneni moznosti
      $lOrder = 0;
      $this->mItemRows = array();
      while ($lItemRow = $result->fetch_assoc()) {
        $_POST["ori_name" . $lOrder] = alterHtmlTextToPlain($lItemRow["ori_name"]);
        $_POST["ori_price" . $lOrder] = $lItemRow["ori_price"];
        $_POST["ori_count" . $lOrder] = $lItemRow["ori_count"];

        $this->mItemRows[$lOrder] = new ItemRow($lOrder, $lItemRow["ori_code"]);

        $lOrder++;
      }

      $_POST["items_count"] = $lOrder;

      $this->computeTotals(true);
    } // vytvoreni objektu radku
    else {
      $query = "SELECT * FROM order_item WHERE ori_head=" . intval($_GET["item"]);
      $result = $GLOBALS["db"]->query($query);

      // nacteme nejdriv vety z db
      $this->mItemRows = array();
      $lOrder = 0;
      while ($lItemRow = $result->fetch_assoc()) {
        $this->mItemRows[$lOrder] = new ItemRow($lOrder, $lItemRow["ori_code"]);

        if (!isset($_POST["ori_name" . $lOrder]))
          $this->mItemRows[$lOrder]->setForDelete();

        $lOrder++;
      }

      // pripojime nove pridane
      for ($i = $lOrder; $i < $_POST["items_count"]; $i++) {
        $this->mItemRows[$i] = new ItemRow($i);

        if (!isset($_POST["ori_name" . $i]))
          $this->mItemRows[$i]->setForDelete();
      }
    }

    // smazani radku fa
    if (isset($_GET["onchange"]) && $_GET["onchange"] == "delete_row") {
      $lCount = 0;

      // pocet nesmazanych vet
      for ($i = 0; $i < $_POST["items_count"]; $i++) {
        if (isset($_POST["ori_name" . $i]))
          $lCount++;
      }

      // smazeme jen pokud je vic polozek jeste
      if ($lCount > 0)
        $this->mItemRows[$_GET["del_order"]]->setForDelete();
      else
        $GLOBALS["rv"]->addError("Nelze smazat, na faktuře musí být alespoň jedna položka.");
    }

    // vytvoreni radku fa
    if (isset($_GET["onchange"]) && $_GET["onchange"] == "add_row") {
      $lOrder = count($this->mItemRows);

      $this->mItemRows[$lOrder] = new ItemRow($lOrder, null);
      $_POST["ori_name$lOrder"] = "";
      $_POST["ori_price$lOrder"] = 0;
      $_POST["ori_count$lOrder"] = 1;
    } // zmena uzivatele
    elseif (isset($_GET["onchange"]) && $_GET["onchange"] == "user" && $_POST["or_user"] > 0) {
      $query = "SELECT * FROM user LEFT JOIN user_address ON u_main_addr = ua_code";
      $query .= " WHERE u_code=" . $_POST["or_user"];
      $result = $GLOBALS["db"]->query($query);

      $row = $result->fetch_assoc();

      // naplneni udaju
      $_POST["or_email"] = $row["u_mail"];
      $_POST["or_fa_name"] = $row["ua_name"];
      $_POST["or_fa_surname"] = $row["ua_surname"];
      $_POST["or_fa_company"] = $row["ua_company"];
      $_POST["or_fa_street"] = $row["ua_street"];
      $_POST["or_fa_town"] = $row["ua_town"];
      $_POST["or_fa_psc"] = $row["ua_psc"];
      $_POST["or_fa_state"] = $row["ua_state"];
      $_POST["or_fa_tel"] = $row["ua_tel"];
      $_POST["or_fa_ico"] = $row["ua_ico"];
      $_POST["or_fa_dic"] = $row["ua_dic"];

      $_POST["or_name"] = $row["ua_name"];
      $_POST["or_surname"] = $row["ua_surname"];
      $_POST["or_company"] = $row["ua_company"];
      $_POST["or_street"] = $row["ua_street"];
      $_POST["or_town"] = $row["ua_town"];
      $_POST["or_psc"] = $row["ua_psc"];
      $_POST["or_state"] = $row["ua_state"];
      $_POST["or_tel"] = $row["ua_tel"];
      $_POST["or_ico"] = $row["ua_ico"];
      $_POST["or_dic"] = $row["ua_dic"];
    }

    $_POST["items_count"] = count($this->mItemRows);
    if (isset($_GET["onchange"]))
      $this->computeTotals();

    return true;
  }

  /* -------------------------- PROCESS ACTION ------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    $or_user = alterTextForDB($_POST["or_user"]);
    $or_number = alterTextForDB($_POST["or_number"]);
    //$or_order_state = alterTextForDB($_POST["or_order_state"]);
    $or_email = alterTextForDB($_POST["or_email"]);
    $note = alterTextForDB($_POST["or_descr"]);
    $or_fa_name = alterTextForDB($_POST["or_fa_name"]);
    $or_fa_surname = alterTextForDB($_POST["or_fa_surname"]);
    $or_fa_company = alterTextForDB($_POST["or_fa_company"]);
    $or_fa_street = alterTextForDB($_POST["or_fa_street"]);
    $or_fa_town = alterTextForDB($_POST["or_fa_town"]);
    $or_fa_psc = alterTextForDB($_POST["or_fa_psc"]);
    $or_fa_state = alterTextForDB($_POST["or_fa_state"]);
    $or_fa_tel = alterTextForDB($_POST["or_fa_tel"]);
    $or_fa_ico = alterTextForDB($_POST["or_fa_ico"]);
    $or_fa_dic = alterTextForDB($_POST["or_fa_dic"]);

    $or_name = alterTextForDB($_POST["or_name"]);
    $or_surname = alterTextForDB($_POST["or_surname"]);
    $or_company = alterTextForDB($_POST["or_company"]);
    $or_street = alterTextForDB($_POST["or_street"]);
    $or_town = alterTextForDB($_POST["or_town"]);
    $or_psc = alterTextForDB($_POST["or_psc"]);
    $or_state = alterTextForDB($_POST["or_state"]);
    $or_tel = alterTextForDB($_POST["or_tel"]);
    $or_ico = alterTextForDB($_POST["or_ico"]);
    $or_dic = alterTextForDB($_POST["or_dic"]);

    $or_trans_text = alterTextForDB($_POST["or_trans_text"]);
    $or_trans_price = alterTextForDB($_POST["or_trans_price"]);
    $or_pay_text = alterTextForDB($_POST["or_pay_text"]);
    $or_price_sum = alterTextForDB($_POST["or_price_sum"]);

    $or_date = preg_replace("/^(.*)\/(.*)\/(.*)$/", '$3-$2-$1 00:00:00', $_POST["or_date"]);
    $or_date_due = preg_replace("/^(.*)\/(.*)\/(.*)$/", '$3-$2-$1 00:00:00', $_POST["or_date_due"]);

    $lCond = new WhereCondition();

    // ----------------------------------- EDITACE -------------==--------------//
    // potvrzeni zeditovanych hodnot

    // update v DB
    $query = "UPDATE shop_order SET";
    $query .= " `or_user` = $or_user, `or_email` = '$or_email', ";
    $query .= "`or_fa_name` = '$or_fa_name', `or_fa_surname` = '$or_fa_surname', `or_fa_company` = '$or_fa_company', ";
    $query .= "`or_fa_street` = '$or_fa_street', `or_fa_town` = '$or_fa_town', `or_fa_psc` = '$or_fa_psc', ";
    $query .= "`or_fa_state` = '$or_fa_state', `or_fa_tel` = '$or_fa_tel', `or_fa_ico` = '$or_fa_ico', ";
    $query .= "`or_fa_dic` = '$or_fa_dic', `or_name` = '$or_name', `or_surname` = '$or_surname', ";
    $query .= "`or_company` = '$or_company', `or_street` = '$or_street', `or_town` = '$or_town', ";
    $query .= "`or_psc` = '$or_psc', `or_state` = '$or_state', `or_tel` = '$or_tel', ";
    $query .= "`or_ico` = '$or_ico', `or_dic` = '$or_dic', or_number = '$or_number', ";
    $query .= "`or_trans_price` = '$or_trans_price', ";
    $query .= "`or_trans_text` = '$or_trans_text', ";
    $query .= "`or_pay_text` = '$or_pay_text', ";
    $query .= "`or_price_sum` = '$or_price_sum', ";
    $query .= "`or_date` = '$or_date', ";
    $query .= "`or_date_due` = '$or_date_due', ";
    $query .= "`or_descr` = '$note' ";

    $query .= " WHERE `or_code` = " . $this->mActRow["or_code"];
    $result = $GLOBALS["db"]->query($query);

    if (!$result) {
      $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
      return true;
    }

    // pridani/update/delete variant
    for ($i = 0; $i < count($this->mItemRows); $i++) {
      $lOneRow = $this->mItemRows[$i];

      // nove pridana a zaroven smazana
      if ($lOneRow->getRowCode() == null && $lOneRow->isForDelete())
        continue;

      // update existujici vety
      elseif ($lOneRow->getRowCode() != null && !$lOneRow->isForDelete()) {
        $lRowName = alterTextForDB($_POST["ori_name" . $i]);
        $lRowPrice = alterTextForDB($_POST["ori_price" . $i]);
        $lRowCount = alterTextForDB($_POST["ori_count" . $i]);

        $query = "UPDATE order_item SET `ori_name` = '$lRowName'";
        $query .= ", `ori_price` = $lRowPrice, `ori_count` = $lRowCount";
        $query .= " WHERE `ori_code` = " . $lOneRow->getRowCode();
        $result = $GLOBALS["db"]->query($query);

        if (!$result)
          $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
      } // pridani nove vety
      elseif ($lOneRow->getRowCode() == null && !$lOneRow->isForDelete()) {
        $lRowName = $_POST["ori_name" . $i];
        $lRowPrice = $_POST["ori_price" . $i];
        $lRowCount = $_POST["ori_count" . $i];

        // vlozeni do DB
        $query = "INSERT INTO order_item (`ori_name`, `ori_price`, `ori_count`, `ori_head`)";
        $query .= " VALUES ('" . alterTextForDB($lRowName) . "', ";
        $query .= "$lRowPrice, $lRowCount, " . $this->mActRow["or_code"] . ")";
        $result = $GLOBALS["db"]->query($query);

        if (!$result)
          $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
      } // smazani vety
      elseif ($lOneRow->getRowCode() != null && $lOneRow->isForDelete()) {

        // skryti v DB
        $query = "DELETE FROM order_item ";
        $query .= " WHERE `ori_code` = " . $lOneRow->getRowCode();
        $result = $GLOBALS["db"]->query($query);

        if (!$result)
          $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
      }

    }

    $GLOBALS["rv"]->addInfo("Faktura " . $this->mActRow["or_number"] . " upravena.");

    // presmerovani na prehled zbozi
    require_once "StoreOrderWM.php";
    $GLOBALS["wm"] = new StoreOrderWM(0);
    $GLOBALS["wm"]->reactOnActionLow();

    return true;
  }

  /* ------------------------------- HEADER ---------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    $str = $this->mHeader;

    return $str;
  }


  /* -------------------------- DEFINE ELEMENTS  ----------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $lPromptWidth = 120;
    $lInputWidth = 200;
    $lInputWidth2 = 120;
    $whereCond = new WhereCondition();

    $lEF = new PresentElement("<fieldset class='form'>");
    $this->addElement($lEF);

    // Uzivatel
    $lEF = new EditCodeCombo("or_user", "Uživatel", 120, true, 150,
      "user LEFT JOIN user_address ON u_main_addr = ua_code", "u_code", "ua_surname",
      array("ua_name"));
    $lEF->setOrderBy("ua_surname ASC, ua_name ASC");
    $lEF->addFieldAttr("onChange", "submitOnChange(\"edit_form\", \"user\");");
    $lEF->addFieldAttr("class", "chosen");
    $lEF->initOptions();
    $this->addElement($lEF);

    // cislo
    $lEF = new EditField("or_number", "Číslo faktury", $lPromptWidth, true, 50, 6);
    $this->addElement($lEF);

    // Email
    $lEF = new EditField("or_email", "Email", $lPromptWidth, true, $lInputWidth, 50);
    $this->addElement($lEF);

    // Datum vzstaveni
    $lEF = new EditField("or_date", "Datum vystavení", $lPromptWidth, true, 70, 10);
    $lEF->setSuffix("<span class='note'>(DD/MM/YYYY)</span>");
    $this->addElement($lEF);

    // Datum splatnosti
    $lEF = new EditField("or_date_due", "Datum splatnosti", $lPromptWidth, true, 70, 10);
    $lEF->setSuffix("<span class='note'>(DD/MM/YYYY)</span>");
    $this->addElement($lEF);

    $lEF = new EditText("or_descr", "Poznámka", $lPromptWidth, false, 30, 255, 3);
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset>");
    $this->addElement($lEF);

    // editovat fakt udaje
    $lEF = new EditBool("edit_fact", "Editovat fakturační údaje", $lPromptWidth);
    $lEF->addFieldAttr("onchange", "alterStructure(this.checked, \"fact_info\");");
    $this->addElement($lEF);

    $lEF = new PresentElement("<fieldset class='form' id='fact_info'><h3>Fakturační údaje</h3>");
    $this->addElement($lEF);

    // Firma
    $lEF = new EditField("or_fa_company", "Firma", $lPromptWidth, false, $lInputWidth2, 50);
    $this->addElement($lEF);

    // Jmeno
    $lEF = new EditField("or_fa_name", "Jméno", $lPromptWidth, true, $lInputWidth2, 30);
    $this->addElement($lEF);

    // Prijmeni
    $lEF = new EditField("or_fa_surname", "Příjmení", $lPromptWidth, true, $lInputWidth2, 30);
    $this->addElement($lEF);

    // Ulice
    $lEF = new EditField("or_fa_street", "Ulice a č.p.", $lPromptWidth, true, $lInputWidth, 100);
    $this->addElement($lEF);

    // Mesto
    $lEF = new EditField("or_fa_town", " Město", $lPromptWidth, true, $lInputWidth2, 100);
    $this->addElement($lEF);

    // PSC
    $lEF = new EditField("or_fa_psc", "PSČ", $lPromptWidth, false, 50, 10);
    $this->addElement($lEF);

    // Stat
    $lEF = new EditCombo("or_fa_state", "Stát", $lPromptWidth, false, 200);
    fillStatesCombo($lEF);
    $this->addElement($lEF);

    // Telefon
    $lEF = new EditField("or_fa_tel", " Telefon", $lPromptWidth, true, $lInputWidth2, 20);
    $this->addElement($lEF);

    // ICO
    $lEF = new EditField("or_fa_ico", "IČO", $lPromptWidth, false, $lInputWidth2, 20);
    $this->addElement($lEF);

    // DIC
    $lEF = new EditField("or_fa_dic", "DIČ", $lPromptWidth, false, $lInputWidth2, 20);
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset>");
    $this->addElement($lEF);

    // editovat dodaci udaje
    $lEF = new EditBool("edit_dod", "Editovat dodací údaje", $lPromptWidth);
    $lEF->addFieldAttr("onchange", "alterStructure(this.checked, \"dod_info\");");
    $this->addElement($lEF);

    $lEF = new PresentElement("<fieldset class='form' id='dod_info'><h3>Dodací údaje</h3>");
    $this->addElement($lEF);

    // Firma
    $lEF = new EditField("or_company", "Firma", $lPromptWidth, false, $lInputWidth2, 50);
    $this->addElement($lEF);

    // Jmeno
    $lEF = new EditField("or_name", "Jméno", $lPromptWidth, false, $lInputWidth2, 30);
    $this->addElement($lEF);

    // Prijmeni
    $lEF = new EditField("or_surname", "Příjmení", $lPromptWidth, false, $lInputWidth2, 30);
    $this->addElement($lEF);

    // Ulice
    $lEF = new EditField("or_street", "Ulice a č.p.", $lPromptWidth, false, $lInputWidth, 100);
    $this->addElement($lEF);

    // Mesto
    $lEF = new EditField("or_town", " Město", $lPromptWidth, false, $lInputWidth2, 100);
    $this->addElement($lEF);

    // PSC
    $lEF = new EditField("or_psc", "PSČ", $lPromptWidth, false, 50, 10);
    $this->addElement($lEF);

    // Stat
    $lEF = new EditCombo("or_state", "Stát", $lPromptWidth, false, 200);
    fillStatesCombo($lEF);
    $this->addElement($lEF);

    // Telefon
    $lEF = new EditField("or_tel", " Telefon", $lPromptWidth, false, $lInputWidth2, 20);
    $this->addElement($lEF);

    // ICO
    $lEF = new EditField("or_ico", "IČO", $lPromptWidth, false, $lInputWidth2, 50);
    $this->addElement($lEF);

    // DIC
    $lEF = new EditField("or_dic", "DIČ", $lPromptWidth, false, $lInputWidth2, 50);
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset><fieldset class='form'>");
    $this->addElement($lEF);

    // Varianty
    $lEF = new EditInt("items_count", "Počet variant", $lPromptWidth, false,
      30, 2, false);
    $lEF->setHidden();
    $this->addElement($lEF);

    // vypis polozek
    $lRowCount = count($this->mItemRows);
    if ($lRowCount > 0) {
      $lEF = new PresentElement("<h3>Položky</h3>");
      $this->addElement($lEF);

      for ($i = 0; $i < $lRowCount; $i++) {
        // smazane se preskoci
        if (!$this->mItemRows[$i]->isForShow())
          continue;

        // Nazev
        $lEF = new EditField("ori_name" . $i, "Název", 60, true, 100, 50);
        $lEF->setGapWidth(0);
        $lEF->setWidthPX(200);
        $this->addElement($lEF);

        // Cena
        $lEF = new EditInt("ori_price" . $i, "Cena", 60, true, 70, 10, true);
        $lEF->setGapWidth(0);
        $lEF->setWidthPX(160);
        $lEF->addFieldAttr('class', 'edit_itemprice');
        $lEF->addFieldAttr('itemorder', $i);
        $lEF->setSuffix("Kč");
        $this->addElement($lEF);

        // Kusu
        $lEF = new EditInt("ori_count" . $i, "Kusů", 60, true, 30, 3, false);
        $lEF->setGapWidth(0);
        $lEF->addFieldAttr('class', 'edit_itemcount');
        $lEF->setWidthPX(140);
        $lDeleteLink = "&nbsp;&nbsp;<a href='' style='padding:3px;background-color:#dddddd;' onclick='";
        $lDeleteLink .= "if(confirm(\"Fakt chceš smazat tuhle položku?\"))";
        $lDeleteLink .= "submitOnChange(\"edit_form\", \"delete_row&amp;del_order=$i\");return false;'";
        $lDeleteLink .= "><b>&nbsp;x&nbsp;</b></a>";
        $lEF->setSuffix($lDeleteLink);
        $this->addElement($lEF);
      }
    }
    // nova polozka
    $lEF = new PresentElement("<a href='' onclick='submitOnChange(\"edit_form\", \"add_row\");return false;'>Přidat položku</a>");
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset><fieldset class='form'>");
    $this->addElement($lEF);

    // doprava text
    $lEF = new EditField("or_trans_text", "Doprava", $lPromptWidth, true, $lInputWidth2, 50);
    $lEF->setWidthPX(300);
    $this->addElement($lEF);

    // doprava cena
    $lEF = new EditInt("or_trans_price", "", 0, true, 70, 10, true);
    $lEF->setWidthPX(180);
    $lEF->setSuffix("Kč");
    $this->addElement($lEF);

    // platba
    $lEF = new EditField("or_pay_text", "Typ platby", $lPromptWidth, true, $lInputWidth2, 50);
    $this->addElement($lEF);

    $lStr = "<p>Celková cena (před slevou): <b>" . number_format($this->mRealTotal, 2, ",", " ") . "</b></p>";
    //$lStr .= "<p>Sleva: <b>".number_format($_POST["discount"], 2, ",", "")." %</b></p>";
    $lEF = new PresentElement("</fieldset><fieldset class='form'>$lStr");
    $this->addElement($lEF);

    // sleva
    $lEF = new EditInt("discount", "Sleva", $lPromptWidth, false, 50, 6, true);
    //$lEF->setHidden();
    $lEF->setSuffix("%");
    $this->addElement($lEF);

    // Cena
    $lEF = new EditInt("or_price_sum", "Celková cena", $lPromptWidth, true,
      70, 10, true);
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset><fieldset class='form'>");
    $this->addElement($lEF);

    // pridani noveho radku
    /*if ($this->mAction == "edit") {
      $lEF = new PresentElement("<a href='' onclick='submitOnChange(\"edit_form\", \"add_row\");return false;'><b>+</b>&nbsp;Přidat novou variantu..</a><div class='td_line'></div>");
      $this->addElement($lEF);
    }*/
  }

  /* -------------------------------- HTML ----------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {

    echo "  <form method='post' id='edit_form' action='" . WR . "?m=" . OR_EDIT . "&amp;item=" . $this->mActRow["or_code"] . "'>";

    $this->printElements();

    echo "  <div style='height:100%;width: 100%;'>";
    echo "  <input type='submit' class='submit' value='Uložit'/>";
    echo "  <input type='submit' class='submit' value='Přepočítat' onclick='submitOnChange(\"edit_form\", \"recount\")'/>";
    echo "  </div>";

    echo "  </fieldset></form>";
  }

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {
    echo "<link rel='stylesheet' type='text/css' href='" . WR . "script/chosen/chosen.min.css' />";
    echo "<script type='text/javascript' src='" . WR . "script/chosen/chosen.jquery.min.js'></script>";

    ?>

    <script type='text/javascript'>
      $(document).ready(function () {

        $('.chosen').chosen();

        $('#edit_fact').change();
        $('#edit_dod').change();

        $('.edit_itemprice, .edit_itemcount, #discount, #or_trans_price, #or_price_sum').change(function () {
          total = 0;
          total = total + parseInt($('#or_trans_price').attr('value'), 10);
          $('.edit_itemprice').each(function () {
            order = $(this).attr('itemorder');
            total = total + $('#ori_count' + order).attr('value') * $(this).attr('value');
          });

          if ($(this).attr('id') == 'or_price_sum') {
            discount = ((total - $('#or_price_sum').attr('value')) / total) * 100;
            $('#discount').attr('value', discount.toFixed(2));
          } else {
            total -= total * ($('#discount').attr('value')) / 100;
            $('#or_price_sum').attr('value', total.toFixed(2));
          }
        });
      });</script>
    <?php
  }

  /**
   * Prepocita celkovou castku bez slevy a slevu
   */
  function computeTotals($aCountDisc = false) {
    $lItemsSum = 0;
    $lSum = 0;

    // spocteni celkove ceny polozek
    for ($i = 0; $i < count($this->mItemRows); $i++) {
      $lOneRow = $this->mItemRows[$i];

      if (!$lOneRow->isForDelete()) {
        $lRowPrice = alterTextForDB($_POST["ori_price" . $i]);
        $lRowCount = alterTextForDB($_POST["ori_count" . $i]);

        $lItemsSum += $lRowPrice * $lRowCount;
      }
    }

    $lItemsSum += $_POST["or_trans_price"];
    $this->mRealTotal = $lItemsSum;

    // pocitat slevu
    if ($aCountDisc) {
      $_POST["discount"] = round(($lItemsSum - $_POST["or_price_sum"]) * 100 / $lItemsSum, 2);
    } // jinak se nastavi celkova cena podle slevy atd.
    else {
      $_POST["or_price_sum"] = round($lItemsSum - $lItemsSum * $_POST["discount"] / 100, 2);
    }
  }
}

?>