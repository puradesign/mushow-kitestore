<?php

class ShopItemDeleteWM extends WebModule {
  var $mHeader = "Kategorie";
  var $mActRow;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    // kontroly
    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    if (!isset($_GET["item"]) || !is_numeric($_GET["item"])) {
      $GLOBALS["rv"]->addError("Chyba. Pravděpodobně byla ručně upravena url stránky.");
      $this->setForOutput(false);
      return false;
    }

    $query = "SELECT * FROM shop_item WHERE si_code='" . $_GET["item"] . "'";
    $result = $GLOBALS["db"]->query($query);

    $row = $result->fetch_assoc();
    $this->mActRow = $row;
    $this->mHeader = alterHtmlTextToPlain($row["si_title"]);

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    $query = "DELETE FROM s_item_row WHERE `sir_head`=" . $this->mActRow["si_code"];
    $result = $GLOBALS["db"]->query($query);

    $query = "DELETE FROM shop_item WHERE `si_code`=" . $this->mActRow["si_code"];
    $result = $GLOBALS["db"]->query($query);

    if (!$result) {
      $GLOBALS["rv"]->addError("Položku se nepodařilo smaazat. Chyba SQL: " . $result->error);
      return true;
    }

    // presmerovani na prehled kategorii
    require_once "ShopEditWM.php";
    $GLOBALS["wm"] = new ShopEditWM(SHOP_EDIT);
    $GLOBALS["wm"]->reactOnActionLow();

    return false;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    $str = $this->mHeader . " - Smazat";

    return $str;
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {

    echo "  <p>Opravdu smazat tuto položku?</p>";
    echo "  <form method='post' action='" . WR . "?m=" . SI_DELETE . "&amp;item=" . $_GET["item"] . "'>";
    echo "  <fieldset class='form' id='delete'>";
    echo "    <input type='hidden' name='delete'/>";
    echo "    <input type='submit' class='submit' value='" . getRText("util23") . "'/>"; // Smazat
    echo "  </fieldset></form>";
  }
}
?>