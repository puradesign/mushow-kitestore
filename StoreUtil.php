<?php

/**
 * Vytvori URL pro sekci
 */
function getStoreSectionUrl($aCode, $aRow = null, $aBrandUrl = null) {
  $db = $GLOBALS["db"];

  if ($aRow == null) {
    $lQuery = "SELECT * FROM s_item_type WHERE sit_code = $aCode";
    $lResult = $db->query(($lQuery));
    $aRow = $lResult->fetch_assoc();
  }

  if ($aBrandUrl == null) {
    $lQuery = "SELECT * FROM s_item_brand WHERE sib_code = " . $aRow["sit_brand"];
    $lResult = $db->query(($lQuery));
    $lBrandRow = $lResult->fetch_assoc();

    $aBrandUrl = $lBrandRow["sib_url"];
  }

  $lUrl = $aRow["sit_url"];

  $head = $aRow["sit_head"];
  while ($head != null) {
    $lQuery = "SELECT sit_head, sit_url FROM s_item_type WHERE sit_code = $head";
    $lResult = $db->query(($lQuery));
    $lHeadRow = $lResult->fetch_assoc();

    if (!lHeadRow)
      break;

    $lUrl = $lHeadRow["sit_url"] . "/" . $lUrl;
    $head = $lHeadRow["sit_head"];
  }

  $lUrl = WR . $aBrandUrl . "/" + $lUrl;
  return $lUrl;
}

/**
 * Vykresli polozky menu jeden sekce a pripadne podsekci
 */
function getMenuItemsCateg($aParentUrl, $aCondStr, $aLevel = 0) {
  $db = $GLOBALS["db"];
  $lQuery = "SELECT * FROM s_item_type WHERE $aCondStr";
  $lQuery .= " ORDER BY sit_id ASC";
  $lTypesRes = $db->query(($lQuery));

  $lItemsStr = "";
  while ($lTypeRow = $lTypesRes->fetch_assoc()) {
    $lIsActive = isset($GLOBALS[GL_CATEGS]) && isset($GLOBALS[GL_CATEGS][$aLevel]) &&
      $GLOBALS[GL_CATEGS][$aLevel]["sit_code"] == $lTypeRow["sit_code"];
    //   echo $aLevel." paaause ".$GLOBALS[GL_CATEGS][$aLevel]["sit_code"]."vs".$lTypeRow["sit_code"]."<br>";
    $lUrl = $aParentUrl . "/" . $lTypeRow["sit_url"];

    $lItemsStr .= "<li" . ($lIsActive ? " class='in'" : "") . ">";
    $lItemsStr .= "<a href='" . $lUrl . "'>";
    $lItemsStr .= $lTypeRow["sit_name"] . "</a></li>";

    if ($lIsActive) {
      $lItemsStr .= getMenuItemsCateg($lUrl, "sit_head = " . $lTypeRow["sit_code"], $aLevel + 1);
    }
  }

  if ($lItemsStr != "") {
    $lItemsStr = "<li class='submenu$aLevel'><ul>" . $lItemsStr . "</ul></li>";
  }

  return $lItemsStr;
}

/**
 * Vytiskne jednu polozku shopu
 */
function printShopItem($aRow) {
  $db = $GLOBALS["db"];
  $lUrl = WR . $aRow["sib_url"] . "/" . $aRow["sit_url"] . "/v/" . $aRow["si_url"];
  $lImgUrl = getBrowserPath(F_SHOP_ITEMS . "headfoto" . $aRow["si_code"] . ".jpg");

  echo "<div class='shop_item'>";

  echo "<h2><a href='" . $lUrl . "'>" . $aRow["si_title"] . "</a></h2>";

  echo "<div class='si_foto'><a href='" . $lUrl . "'>";
  echo "<img src='" . $lImgUrl . "' alt='" . $aRow["si_title"] . "'/>";
  echo "</a><div><span style='background-color:#6c954e;padding:2px'>S</span><span style='color:#6c954e'>kladem</span></div>";

  if ($aRow["si_hot"] == 1)/*yell:F6FF00*/
    echo "<div><span style='background-color:#FFFC66;padding:2px'>H</span><span style='color:#FFFC66'>ot stuff</span></div>";

  if ($aRow["si_onsale"] == 1)/*yell:CC60CC*/
    echo "<div><span style='background-color:#CC60CC;padding:2px'>A</span><span style='color:#CC60CC'>kce</span></div>";

  echo "</div>";

  echo "<div class='si_descr'><p>" . $aRow["si_descr"] . "</p></div>";

  echo "<div class='si_price'>";
  $lOnSale = $aRow["si_fake_price"] != null && $aRow["si_fake_price"] > 0;
  if ($lOnSale) {
    echo "Původní cena:";
    echo "<span style='padding:5px 4px;text-decoration:line-through; font-size:13px;color:#222222'>";
    echo number_format($aRow["si_fake_price"], 0, "", ".") . "&nbsp;Kč</span>";
  }
  echo "</div>";

  echo "<div class='si_price'>" . ($lOnSale ? "Nová cena" : "Cena") . ":";
  echo "<span>&nbsp;" . number_format($aRow["si_price"], 0, "", ".") . "&nbsp;Kč</span>";
  echo "</div>";

  echo "<div class='si_order'><a href='$lUrl' title='Detail produktu, objednat'>Detail produktu, objednat</a></div>"; // <img src='".WR_IMG."order_butt.jpg' alt='objednat'/><img src='".WR_IMG."order_butt_hover.jpg' class='img_hover' alt='objednat'/>
  echo "</div>";
}

/**
 * Vrací řetězec s html fakturou
 */
function getOrderFact($aNum, $aDate, $rowOrder, $aForEmail = false, $aForPrint = false,
  $aPredbezna = false, $aForPdf = false, $aFixed = false) {
  $db = $GLOBALS["db"];
  // fakturacni udaje
  $fa_company = $rowOrder["or_fa_company"];
  $fa_name = $rowOrder["or_fa_name"];
  $fa_surname = $rowOrder["or_fa_surname"];
  $fa_street = $rowOrder["or_fa_street"];
  $fa_town = $rowOrder["or_fa_town"];
  $fa_psc = $rowOrder["or_fa_psc"];
  $fa_state = $rowOrder["or_fa_state"];
  $fa_tel = $rowOrder["or_fa_tel"];
  $fa_ico = $rowOrder["or_fa_ico"];
  $fa_dic = $rowOrder["or_fa_dic"];
  $email = $rowOrder["or_email"];

  // dodaci udaje
  $company = $rowOrder["or_company"];
  $name = $rowOrder["or_name"];
  $surname = $rowOrder["or_surname"];
  $street = $rowOrder["or_street"];
  $town = $rowOrder["or_town"];
  $psc = $rowOrder["or_psc"];
  $state = $rowOrder["or_state"];
  $tel = $rowOrder["or_tel"];
  $ico = $rowOrder["or_ico"];
  $dic = $rowOrder["or_dic"];
  $priceSum = $rowOrder["or_price_sum"];

  $lStyle = !$aForEmail ? "width:530px; margin:5px 5px 5px -10px;" : "width:800px; margin:5px;";
  //$lStyle .= "font-family: 'Helvetica', 'Arial', sans-serif";
  $lWidthHalf = !$aForEmail ? "width:260px; " : "width:395px;";
  $lWidth = !$aForEmail ? "width:530px; " : "width:800px;";

  $html = "<table class='wrap_table' style='float:left;" . ($aForPrint ? "height: 1180px;" : "") . "border: 2px solid #444444; $lStyle'>\n";

  $html .= "<tr><td><p style='font-size:18px;margin:0px;padding:0px;font-weight:bold; text-align: left;'>";
  $html .= $aPredbezna ? "Předběžná faktura ($aNum)" : "Faktura - " . ($aFixed ? "opravný " : "") . "daňový doklad ($aNum)</p></td>";
  $html .= "<td style='text-align:right; padding:0px 20px 0px 0px'>$aDate</td></tr>\n";

  $html .= "<tr><td style='vertical-align:middle;'><table style='$lWidthHalf margin:2px;border: 1px solid #444444'>
		<tr><td><b>Dodavatel</b></td></tr>
		<tr><td><span style='font-weight:bold; font-size:16px'>Mushow s.r.o.</span></td></tr>
		<tr><td><b>23.Dubna č.1</b></td></tr>
		<tr><td><b>69201 Pavlov</b></td></tr>
		<tr><td style='height:12px;'> </td></tr>
		<tr><td>IČO: 29247896</td></tr><tr><td>DIČ: CZ29247896</td></tr>
		<tr><td>e-mail: obchod@mushow.cz</td></tr>
		<tr><td>web: www.mushow.cz</td></tr>
		<tr><td style='padding-top:10px;'>Číslo účtu: <b style='font-size:14px;'>43-7969740207/0100</b> (Komerční banka)</td></tr>
		</table></td>
		<td style='text-align:right; '><a href='" . WR . "'>
			<img src='" . WR_IMG . ($aForEmail ? "logo_mail.jpg" : "logo_mail.png") . "' height='130px' style='margin: 0 50px 0 0'/>
		</a></td></tr>";

  $html .= "<tr><td><b>Odběratel: </b><br /><table style='$lWidthHalf margin:2px;border: 1px solid #444444'>\n";
  $html .= "<tr><td colspan='2'><b>Fakturační údaje</b></td></tr>\n";
  $html .= "<tr><td>Firma:</td><td>$fa_company</td></tr>\n";
  $html .= "<tr><td>Jméno:</td><td>$fa_name $fa_surname</td></tr>\n";
  $html .= "<tr><td>Adresa:</td><td>$fa_street</td></tr>\n";
  $html .= "<tr><td>Město:</td><td>$fa_town</td></tr>\n";
  $html .= "<tr><td>Stát:</td><td>$fa_state</td></tr>\n";
  $html .= "<tr><td>PSČ:</td><td>$fa_psc</td></tr>\n";
  $html .= "<tr><td>Telefon:</td><td>$fa_tel</td></tr>\n";
  $html .= "<tr><td>e-mail:</td><td>$email</td></tr>\n";
  $html .= "<tr><td>IČO:</td><td>$fa_ico</td></tr>\n";
  $html .= "<tr><td>DIČ:</td><td>$fa_dic</td></tr>\n";
  $html .= "</table></td>";

  $html .= "<td><br /><table style='$lWidthHalf margin:2px; border: 1px solid #444444'>";
  $html .= "<tr><td><b>Dodací údaje</b></td><td></td></tr>\n";
  $html .= "<tr><td>Firma:</td><td>$company</td></tr>\n";
  $html .= "<tr><td>Jméno:</td><td>$name $surname</td></tr>\n";
  $html .= "<tr><td>Adresa:</td><td>$street</td></tr>\n";
  $html .= "<tr><td>Město:</td><td>$town</td></tr>\n";
  $html .= "<tr><td>Stát:</td><td>$state</td></tr>\n";
  $html .= "<tr><td>PSČ:</td><td>$psc</td></tr>\n";
  $html .= "<tr><td>Telefon:</td><td>$tel</td></tr>\n";
  $html .= "<tr><td>IČO:</td><td>$ico</td></tr>\n";
  $html .= "<tr><td>DIČ:</td><td>$dic</td></tr>\n";
  $html .= "<tr><td>&nbsp;</td><td></td></tr>\n";
  $html .= "</table></td></tr>";

  // doprava
  $lTransPrice = $rowOrder["or_trans_price"];
  $lTransText = $rowOrder["or_trans_text"];

  // doprava
  $html .= "<tr><td colspan='2'><table style='$lWidth margin:2px; border: 1px solid #444444'>\n";
  $html .= "<tr><td>Doprava</td><td><b>$lTransText</b></td>\n";
  $html .= "<td>" . number_format($lTransPrice, 2, ",", " ") . " Kč</td></tr>\n";

  // typ platby
  $html .= "<tr><td>Způsob platby</td><td><b>" . $rowOrder["or_pay_text"] . "</b></td>\n";
  $html .= "<td></td></tr>\n";

  $lDueDate = StrFTime("%d.%m.%Y", strtotime($rowOrder["or_date_due"]));

  // datumy
  $html .= "<tr><td>Datum vystavení</td><td><b>$aDate</b></td>\n";
  $html .= "<td></td></tr>\n";
  $html .= "<tr><td style='width:34%'>Datum skutečného zdanitelného plnění</td><td><b>$aDate</b></td>\n";
  $html .= "<td></td></tr>\n";
  $html .= "<tr><td>Datum splatnosti</td><td><b>$lDueDate</b></td>\n";
  $html .= "<td></td></tr>\n";
  $html .= "</table></td></tr>";

  // DPH
  $dph = getDPH(strtotime($aDate));
  $dphKoef = ($dph / 100) + 1;

  // polozky
  $html .= "<tr><td colspan='2'><h3 style='padding: 10px 0px 0px 5px; margin:3px;'>Objednané zboží</h3>\n";
  $html .= "<table class='order_items' style='$lWidth margin:2px; border: 1px solid #444444; border-collapse:collapse;'>\n";
  $html .= "<thead><tr><th>Název zboží</th><th>Cena/ks</th><th style='width:20px'>Ks</th><th>DPH %</th><th>Bez DPH</th><th>DPH</th><th>Celkem</th></tr></thead>\n";

  $lTotal = 0;

  $query = "SELECT * FROM order_item WHERE ori_head=" . $rowOrder["or_code"];
  $result = $db->query(($query));

  // vypis polozek
  while ($row = $result->fetch_assoc()) {
    $lItemTotal = $row["ori_count"] * $row["ori_price"];
    $lTotal += $lItemTotal;

    $lBorder = "border: 1px solid #444444;padding:5px;";

    $html .= "<tr><td style='$lBorder'><a href='" . WR . $row["ori_url"] . "' style='color:black'>" . $row["ori_name"] . "</a></td>\n";
    $html .= "<td style='$lBorder text-align:right;white-space:nowrap;'>" . number_format($row["ori_price"] / $dphKoef, 2, ",", " ") . " Kč</td>\n";
    $html .= "<td style='$lBorder text-align:right'>" . $row["ori_count"] . "</td>\n";
    $html .= "<td style='$lBorder text-align:right'>$dph</td>\n";
    $html .= "<td style='$lBorder text-align:right;white-space:nowrap;'>" . number_format($lItemTotal / $dphKoef, 2, ",", " ") . " Kč</td>\n";
    $html .= "<td style='$lBorder text-align:right;white-space:nowrap;'>" . number_format($lItemTotal - $lItemTotal / $dphKoef, 2, ",", " ") . " Kč</td>\n";
    $html .= "<td style='$lBorder text-align:right;white-space:nowrap;'>" . number_format($lItemTotal, 2, ",", " ") . " Kč</td></tr>\n";
  }

  $html .= "</table></td></tr>\n";

  $lStyle = "style='text-align:right'";

  $html .= "<tr><td colspan='2'>";
  $html .= "<table style='$lWidth margin-top:10px;border-top: 2px solid #444444;border-bottom: 1px solid #444444;padding:2px;'>\n";
  $lHeadStyle = "style='border-bottom:1px solid black'";
  $html .= "<thead><tr><th $lHeadStyle></th><th $lHeadStyle>DPH %</th><th $lHeadStyle>Základ</th><th $lHeadStyle>DPH</th><th $lHeadStyle>Celkem</th></tr></thead>";
  $html .= "<tr><td>Základní sazba: </td><td>$dph %</td><td $lStyle>" . number_format($lTotal / $dphKoef, 2, ",", " ") . " Kč</td>";
  $html .= "<td $lStyle>" . number_format($lTotal - $lTotal / $dphKoef, 2, ",", " ") . " Kč</td><td $lStyle>" . number_format($lTotal, 2, ",", " ") . " Kč</td></tr>\n";

  // doprava
  if ($lTransPrice > 0) {
    $html .= "<tr><td>Doprava: </td><td>$dph %</td><td $lStyle>" . number_format($lTransPrice / 1.2, 2, ",", " ") . " Kč</td>";
    $html .= "<td $lStyle>" . number_format($lTransPrice - $lTransPrice / $dphKoef, 2, ",", " ") . " Kč</td><td $lStyle>" . number_format($lTransPrice, 2, ",", " ") . " Kč</td></tr>\n";
  }

  $lTotal += $lTransPrice;

  // sleva
  if ($priceSum != $lTotal) {
    $html .= "<tr><td>Sleva: </td><td>" . number_format(($lTotal - $priceSum) * 100 / $lTotal, 2, ",", " ") . " %</td><td></td><td></td><td></td></tr>";
  }

  $html .= "<tr><td>CELKEM: </td><td>$dph %</td><td $lStyle>" . number_format($priceSum / $dphKoef, 2, ",", " ") . " Kč</td>";
  $html .= "<td $lStyle>" . number_format($priceSum - $priceSum / $dphKoef, 2, ",", " ") . " Kč</td><td $lStyle>" . number_format($priceSum, 2, ",", " ") . " Kč</td></tr>\n";

  $html .= "</table></td></tr>";



  $html .= "<tr><td colspan='2'>
				<table style='width: 100%;'>
				  <tr><td>" .
    ($aForPdf || $aForEmail || $aForPrint ? "<img src='" . WR_IMG . "stamp.jpg' style='width: 200px;'/>" : "") .
    "</td>
		            <td style='width:100%;' align='right'>
		            	<table style='margin: 25px 0px 5px; width:300px; border: 3px solid black;padding:2px;font-size:14px; '>
		            	  <tr><td>Celkem k úhradě: </td>
							<td style='text-align:right; font-weight:bold'>" . number_format($priceSum, 2, ",", " ") . " Kč</td>
						  </tr>
		            	</table>
		            </td>
				</table>
			</td></tr>

			<tr><td colspan='2'>
			<table style='$lWidth margin-top:10px;border-top: 1px solid #444444;padding:2px;'>\n";

  $lHeadStyle = "style='border-bottom:1px solid black'";
  $html .= "<thead><tr><th>Rekapitulace DPH v Kč</th><th $lHeadStyle>Základ v Kč</th><th $lHeadStyle>Sazba</th><th $lHeadStyle>DPH v Kč</th><th $lHeadStyle>Celkem s DPH v Kč</th></tr></thead>";
  $html .= "<tr><td></td><td>" . number_format($priceSum / $dphKoef, 2, ",", " ") . "</td><td $lStyle>$dph %</td>";
  $html .= "<td $lStyle>" . number_format($priceSum - $priceSum / $dphKoef, 2, ",", " ") . "</td><td $lStyle>" . number_format($priceSum, 2, ",", " ") . " Kč</td></tr>\n";
  $html .= "</table></td></tr>";

  // poznamka
  //$html .= "<tr><td colspan='2'><table style='width:530px;margin:2px; border: 1px solid #444444'>";
  //$html .= "<tr><td><b>Poznámka: </b></td><td>".$rowOrder["or_descr"]."</td></tr>";
  //$html .= "</table></td></tr>";
  if (!$aForPdf)
    $html .= "<tr><td colspan='2' style='height:100%'></td></tr>";
  $html .= "</table>";

  // podpis
  /*$html .= "<table style='width:100%'><tr><td align='right'>
           <img src='".WR_IMG."logo_mail.jpg' height='60px'/>
     </td></tr></table>";*/

  return $html;
}

/**
 * Vrací řetězec s html fakturou do zahranici
 */
function getOrderFactEn($aNum, $aDate, $rowOrder, $aForEmail = false, $aForPrint = false,
  $aPredb = false, $aForPdf = false) {

  $db = $GLOBALS["db"];

  // fakturacni udaje
  $fa_company = $rowOrder["or_fa_company"];
  $fa_name = $rowOrder["or_fa_name"];
  $fa_surname = $rowOrder["or_fa_surname"];
  $fa_street = $rowOrder["or_fa_street"];
  $fa_town = $rowOrder["or_fa_town"];
  $fa_psc = $rowOrder["or_fa_psc"];
  $fa_state = $rowOrder["or_fa_state"];
  $fa_tel = $rowOrder["or_fa_tel"];
  $fa_ico = $rowOrder["or_fa_ico"];
  $fa_dic = $rowOrder["or_fa_dic"];
  $email = $rowOrder["or_email"];

  // dodaci udaje
  $company = $rowOrder["or_company"];
  $name = $rowOrder["or_name"];
  $surname = $rowOrder["or_surname"];
  $street = $rowOrder["or_street"];
  $town = $rowOrder["or_town"];
  $psc = $rowOrder["or_psc"];
  $state = $rowOrder["or_state"];
  $tel = $rowOrder["or_tel"];
  $ico = $rowOrder["or_ico"];
  $dic = $rowOrder["or_dic"];
  $priceSum = $rowOrder["or_price_sum"];

  $lStyle = !$aForEmail ? "width:530px; margin:5px 5px 5px -10px;" : "width:800px; margin:5px;";
  $lWidthHalf = !$aForEmail ? "width:260px; " : "width:395px;";
  $lWidth = !$aForEmail ? "width:530px; " : "width:800px;";

  $html = "<table class='wrap_table' style='float:left;" . ($aForPrint ? "height: 1180px;" : "") . "border: 2px solid #444444; $lStyle'>\n";

  $html .= "<tr><td><p style='font-size:18px;margin:0px;padding:0px;font-weight:bold'>INVOICE No. $aNum</p></td>";
  $html .= "<td style='text-align:right; padding:0px 20px 0px 0px'>$aDate</td></tr>\n";

  $html .= "<tr><td style='vertical-align:middle;'><table style='$lWidthHalf margin:2px;border: 1px solid #444444'>\n";
  $html .= "<tr><td><b>Supplier</b></td></tr>\n";
  $html .= "<tr><td><span style='font-weight:bold; font-size:16px'>Mushow s.r.o.</span></td></tr>";
  $html .= "<tr><td><b>23.Dubna č.1</b></td></tr>\n";
  $html .= "<tr><td><b>69201 Pavlov</b></td></tr>\n";
  $html .= "<tr><td style='height:12px;'> </td></tr>";

  $html .= "<tr><td>Identif. number: 29247896</td></tr>";
  $html .= "<tr><td>Tax identity: CZ29247896</td></tr>";
  $html .= "<tr><td style='height:12px;'> </td></tr>";
  $html .= "<tr><td>E-mail: obchod@mushow.cz</td></tr>";
  $html .= "<tr><td>Web: www.mushow.cz</td></tr>";
  $html .= "<tr><td>Mobile-phone: </td></tr>";

  $html .= "<tr><td style='padding-top:10px;'>Issuing Bank: KB bank</td></tr>";
  $html .= "<tr><td>SWIFT: KOMBCZPP</td></tr>";
  $html .= "<tr><td>IBAN:  CZ8001000000437969740207</td></tr>";
  $html .= "<tr><td>Account No.: 43-7969740207</td></tr>";
  $html .= "<tr><td>Bank code: 0100</td></tr>";
  $html .= "</table></td>\n";
  $html .= "<td style='text-align:right; '><a href='" . WR . "'>
		<img src='" . WR_IMG . ($aForEmail ? "logo_mail.jpg" : "logo_mail.png") . "' style='height:180px; margin: 0 50px 0 0'/></a></td></tr>\n";

  $html .= "<tr><td><b>Customer: </b><br /><table style='$lWidthHalf margin:2px;border: 1px solid #444444'>\n";
  if (!empty($fa_company))
    $html .= "<tr><td>Company:</td><td>$fa_company</td></tr>\n";
  $html .= "<tr><td>Name:</td><td>$fa_name $fa_surname</td></tr>\n";
  $html .= "<tr><td>Street:</td><td>$fa_street</td></tr>\n";
  $html .= "<tr><td>Town:</td><td>$fa_town</td></tr>\n";
  $html .= "<tr><td>Postal code:</td><td>$fa_psc</td></tr>\n";
  $html .= "<tr><td>State:</td><td>$fa_state</td></tr>\n";
  $html .= "<tr><td>Phone num.:</td><td>$fa_tel</td></tr>\n";
  $html .= "<tr><td>E-mail:</td><td>$email</td></tr>\n";
  if (!empty($fa_company))
    $html .= "<tr><td>Identif. number:</td><td>$fa_ico</td></tr>\n";
  if (!empty($fa_company))
    $html .= "<tr><td>Tax identity:</td><td>$fa_dic</td></tr>\n";
  $html .= "</table></td>";

  $html .= "<td><br /><table style='$lWidthHalf margin:2px; border: 1px solid #444444'>";
  $html .= "<tr><td><b>Reciever: </b></td><td></td></tr>\n";
  if (!empty($company))
    $html .= "<tr><td>Company:</td><td>$company</td></tr>\n";
  $html .= "<tr><td>Name:</td><td>$name $surname</td></tr>\n";
  $html .= "<tr><td>Street:</td><td>$street</td></tr>\n";
  $html .= "<tr><td>Town:</td><td>$town</td></tr>\n";
  $html .= "<tr><td>Postal code:</td><td>$psc</td></tr>\n";
  $html .= "<tr><td>State:</td><td>$state</td></tr>\n";
  $html .= "<tr><td>Phone num.:</td><td>$tel</td></tr>\n";
  if (!empty($company))
    $html .= "<tr><td>Identif. number:</td><td>$ico</td></tr>\n";
  if (!empty($company))
    $html .= "<tr><td>Tax identity:</td><td>$dic</td></tr>\n";
  $html .= "</table></td></tr>";

  // doprava
  $lTransPrice = $rowOrder["or_trans_price"];
  $lTransText = $rowOrder["or_trans_text"];

  // doprava
  $html .= "<tr><td colspan='2'><table style='$lWidth margin:2px; border: 1px solid #444444'>\n";
  $html .= "<tr><td>Transport</td><td><b>$lTransText</b></td>\n";
  $html .= "<td>" . number_format($lTransPrice, 2, ",", " ") . " EUR</td></tr>\n";

  // typ platby
  $html .= "<tr><td>Payment</td><td><b>" . $rowOrder["or_pay_text"] . "</b></td>\n";
  $html .= "<td></td></tr>\n";

  $lDueDate = StrFTime("%d.%m.%Y", strtotime($rowOrder["or_date_due"]));

  // datumy
  $html .= "<tr><td>Invoice Date</td><td><b>$aDate</b></td>\n";
  $html .= "<td></td></tr>\n";
  $html .= "<tr><td>Due Date</td><td><b>$lDueDate</b></td>\n";
  $html .= "<td></td></tr>\n";
  $html .= "</table></td></tr>";

  // DPH
  $dph = getDPH(strtotime($aDate));
  $dphKoef = ($dph / 100) + 1;

  // polozky
  $html .= "<tr><td colspan='2'><h3 style='padding: 10px 0px 0px 5px; margin:3px;'>Items</h3>\n";
  $html .= "<table class='order_items' style='$lWidth margin:2px; border: 1px solid #444444; border-collapse:collapse;'>\n";
  $html .= "<thead><tr><th>Description</th><th>Unit price</th><th style='width:20px'>Q'ty</th><th>Total</th></tr></thead>\n";

  $lTotal = 0;

  $query = "SELECT * FROM order_item WHERE ori_head=" . $rowOrder["or_code"];
  $result = $db->query(($query));

  // vypis polozek
  while ($row = $result->fetch_assoc()) {
    $lItemTotal = $row["ori_count"] * $row["ori_price"];
    $lTotal += $lItemTotal;

    $lBorder = "border: 1px solid #444;padding:5px;";

    $html .= "<tr><td style='$lBorder'><a href='" . WR . $row["ori_url"] . "' style='color:black'>" . $row["ori_name"] . "</a></td>\n";
    $html .= "<td style='$lBorder text-align:right;white-space:nowrap;'>" . number_format($row["ori_price"], 2, ",", " ") . " EUR</td>\n";
    $html .= "<td style='$lBorder text-align:right'>" . $row["ori_count"] . "</td>\n";
    $html .= "<td style='$lBorder text-align:right;white-space:nowrap;'>" . number_format($lItemTotal, 2, ",", " ") . " EUR</td></tr>\n";
  }

  if ($lTransPrice > 0) {
    $html .= "<tr><td style='$lBorder'>Shipping</a></td>\n";
    $html .= "<td style='$lBorder text-align:right;white-space:nowrap;'>" . number_format($lTransPrice, 2, ",", " ") . " EUR</td>\n";
    $html .= "<td style='$lBorder text-align:right'>1</td>\n";
    $html .= "<td style='$lBorder text-align:right;white-space:nowrap;'>" . number_format($lTransPrice, 2, ",", " ") . " EUR</td></tr>\n";
  }

  $html .= "</table></td></tr>\n";

  $lTotal += $lTransPrice;


  $html .= "<tr><td colspan='2'>
				<table style='width: 100%;'>
				  <tr><td>" .
    ($aForPdf ? "<img src='" . WR_IMG . "stamp.jpg' style='width: 200px;'/>" : "") .
    "</td>
		            <td style='width:100%;' align='right'>
		            	<table style='margin: 25px 0px 5px; width:300px; border: 3px solid black;padding:2px;font-size:14px; '>
		            	  <tr><td>TOTAL DUE: </td>
							<td style='text-align:right; font-weight:bold'>" . number_format($priceSum, 2, ",", " ") . " EUR</td>
						  </tr>
		            	</table>
		            </td>
				</table>
			</td></tr>

			</table></td></tr>";

  // poznamka
  //$html .= "<tr><td colspan='2'><table style='width:530px;margin:2px; border: 1px solid #444444'>";
  //$html .= "<tr><td><b>Poznámka: </b></td><td>".$rowOrder["or_descr"]."</td></tr>";
  //$html .= "</table></td></tr>";
  if (!$aForPdf)
    $html .= "<tr><td colspan='2' style='height:100%'></td></tr>";
  $html .= "</table>";

  return $html;
}

/**
 * Vraci stav objednavkz dle kodu
 */
function getOrderStateStr($aCode) {
  if ($aCode == 1)
    return "čeká na vyřízení";
  elseif ($aCode == 2)
    return "vyřízená";
  elseif ($aCode == 3)
    return "zrušená";
  else
    return "je to v kopru";
}

/**
 * Vraci slevu v procentech
 */
function getDiscount($aTotal) {

  if ($aTotal < 10000)
    return 0;
  if ($aTotal >= 10000 && $aTotal <= 20000)
    return 5;
  elseif ($aTotal > 20000 && $aTotal <= 35000)
    return 10;
  elseif ($aTotal > 35000 && $aTotal <= 60000)
    return 15;
  elseif ($aTotal > 60000)
    return 20;

  return 0;
}

/**
 * Vraci procenta DPH podle data
 */
function getDPH($date) {
  if ($date > strtotime("1 January 2013"))
    return 21;

  return 20;
}

/**
 * Naplni combo pro staty
 */
function fillStatesCombo($aEF) {
  $aEF->addSelect("Česká republika", "Česká republika");
  $aEF->addSelect("Slovensko", "Slovensko");
  $aEF->addSelect("Austria", "Austria");
  $aEF->addSelect("Cuba", "Cuba");
  $aEF->addSelect("Curaco", "Curaco");
  $aEF->addSelect("Cyprus", "Cyprus");
  $aEF->addSelect("Czech Republic", "Czech republic");
  $aEF->addSelect("Denmark", "Denmark");
  $aEF->addSelect("Djibouti", "Djibouti");
  $aEF->addSelect("Dominica", "Dominica");
  $aEF->addSelect("Dominican Republic", "Dominican Republic");
  $aEF->addSelect("East Timor", "East Timor");
  $aEF->addSelect("Ecuador", "Ecuador");
  $aEF->addSelect("Egypt", "Egypt");
  $aEF->addSelect("El Salvador", "El Salvador");
  $aEF->addSelect("Equatorial Guinea", "Equatorial Guinea");
  $aEF->addSelect("Eritrea", "Eritrea");
  $aEF->addSelect("Estonia", "Estonia");
  $aEF->addSelect("Ethiopia", "Ethiopia");
  $aEF->addSelect("Falkland Islands", "Falkland Islands");
  $aEF->addSelect("Faroe Islands", "Faroe Islands");
  $aEF->addSelect("Fiji", "Fiji");
  $aEF->addSelect("Finland", "Finland");
  $aEF->addSelect("France", "France");
  $aEF->addSelect("French Guiana", "French Guiana");
  $aEF->addSelect("French Polynesia", "French Polynesia");
  $aEF->addSelect("French Southern Ter", "French Southern Ter");
  $aEF->addSelect("Gabon", "Gabon");
  $aEF->addSelect("Gambia", "Gambia");
  $aEF->addSelect("Georgia", "Georgia");
  $aEF->addSelect("Germany", "Germany");
  $aEF->addSelect("Ghana", "Ghana");
  $aEF->addSelect("Gibraltar", "Gibraltar");
  $aEF->addSelect("Great Britain", "Great Britain");
  $aEF->addSelect("Greece", "Greece");
  $aEF->addSelect("Greenland", "Greenland");
  $aEF->addSelect("Grenada", "Grenada");
  $aEF->addSelect("Guadeloupe", "Guadeloupe");
  $aEF->addSelect("Guam", "Guam");
  $aEF->addSelect("Guatemala", "Guatemala");
  $aEF->addSelect("Guinea", "Guinea");
  $aEF->addSelect("Guyana", "Guyana");
  $aEF->addSelect("Haiti", "Haiti");
  $aEF->addSelect("Hawaii", "Hawaii");
  $aEF->addSelect("Honduras", "Honduras");
  $aEF->addSelect("Hong Kong", "Hong Kong");
  $aEF->addSelect("Hungary", "Hungary");
  $aEF->addSelect("Iceland", "Iceland");
  $aEF->addSelect("India", "India");
  $aEF->addSelect("Indonesia", "Indonesia");
  $aEF->addSelect("Iran", "Iran");
  $aEF->addSelect("Iraq", "Iraq");
  $aEF->addSelect("Ireland", "Ireland");
  $aEF->addSelect("Isle of Man", "Isle of Man");
  $aEF->addSelect("Israel", "Israel");
  $aEF->addSelect("Italy", "Italy");
  $aEF->addSelect("Jamaica", "Jamaica");
  $aEF->addSelect("Japan", "Japan");
  $aEF->addSelect("Jordan", "Jordan");
  $aEF->addSelect("Kazakhstan", "Kazakhstan");
  $aEF->addSelect("Kenya", "Kenya");
  $aEF->addSelect("Kiribati", "Kiribati");
  $aEF->addSelect("Korea North", "Korea North");
  $aEF->addSelect("Korea Sout", "Korea Sout");
  $aEF->addSelect("Kuwait", "Kuwait");
  $aEF->addSelect("Kyrgyzstan", "Kyrgyzstan");
  $aEF->addSelect("Laos", "Laos");
  $aEF->addSelect("Latvia", "Latvia");
  $aEF->addSelect("Lebanon", "Lebanon");
  $aEF->addSelect("Lesotho", "Lesotho");
  $aEF->addSelect("Liberia", "Liberia");
  $aEF->addSelect("Libya", "Libya");
  $aEF->addSelect("Liechtenstein", "Liechtenstein");
  $aEF->addSelect("Lithuania", "Lithuania");
  $aEF->addSelect("Luxembourg", "Luxembourg");
  $aEF->addSelect("Macau", "Macau");
  $aEF->addSelect("Macedonia", "Macedonia");
  $aEF->addSelect("Madagascar", "Madagascar");
  $aEF->addSelect("Malaysia", "Malaysia");
  $aEF->addSelect("Malawi", "Malawi");
  $aEF->addSelect("Maldives", "Maldives");
  $aEF->addSelect("Mali", "Mali");
  $aEF->addSelect("Malta", "Malta");
  $aEF->addSelect("Marshall Islands", "Marshall Islands");
  $aEF->addSelect("Martinique", "Martinique");
  $aEF->addSelect("Mauritania", "Mauritania");
  $aEF->addSelect("Mauritius", "Mauritius");
  $aEF->addSelect("Mayotte", "Mayotte");
  $aEF->addSelect("Mexico", "Mexico");
  $aEF->addSelect("Midway Islands", "Midway Islands");
  $aEF->addSelect("Moldova", "Moldova");
  $aEF->addSelect("Monaco", "Monaco");
  $aEF->addSelect("Mongolia", "Mongolia");
  $aEF->addSelect("Montserrat", "Montserrat");
  $aEF->addSelect("Morocco", "Morocco");
  $aEF->addSelect("Mozambique", "Mozambique");
  $aEF->addSelect("Myanmar", "Myanmar");
  $aEF->addSelect("Nambia", "Nambia");
  $aEF->addSelect("Nauru", "Nauru");
  $aEF->addSelect("Nepal", "Nepal");
  $aEF->addSelect("Netherland Antilles", "Netherland Antilles");
  $aEF->addSelect("Netherlands", "Netherlands");
  $aEF->addSelect("Nevis", "Nevis");
  $aEF->addSelect("New Caledonia", "New Caledonia");
  $aEF->addSelect("New Zealand", "New Zealand");
  $aEF->addSelect("Nicaragua", "Nicaragua");
  $aEF->addSelect("Niger", "Niger");
  $aEF->addSelect("Nigeria", "Nigeria");
  $aEF->addSelect("Niue", "Niue");
  $aEF->addSelect("Norfolk Island", "Norfolk Island");
  $aEF->addSelect("Norway", "Norway");
  $aEF->addSelect("Oman", "Oman");
  $aEF->addSelect("Pakistan", "Pakistan");
  $aEF->addSelect("Palau Island", "Palau Island");
  $aEF->addSelect("Palestine", "Palestine");
  $aEF->addSelect("Panama", "Panama");
  $aEF->addSelect("Papua New Guinea", "Papua New Guinea");
  $aEF->addSelect("Paraguay", "Paraguay");
  $aEF->addSelect("Peru", "Peru");
  $aEF->addSelect("Phillipines", "Phillipines");
  $aEF->addSelect("Pitcairn Island", "Pitcairn Island");
  $aEF->addSelect("Poland", "Poland");
  $aEF->addSelect("Portugal", "Portugal");
  $aEF->addSelect("Puerto Rico", "Puerto Rico");
  $aEF->addSelect("Qatar", "Qatar");
  $aEF->addSelect("Republic of Montenegro", "Republic of Montenegro");
  $aEF->addSelect("Republic of Serbia", "Republic of Serbia");
  $aEF->addSelect("Reunion", "Reunion");
  $aEF->addSelect("Romania", "Romania");
  $aEF->addSelect("Russia", "Russia");
  $aEF->addSelect("Rwanda", "Rwanda");
  $aEF->addSelect("St Barthelemy", "St Barthelemy");
  $aEF->addSelect("St Eustatius", "St Eustatius");
  $aEF->addSelect("St Helena", "St Helena");
  $aEF->addSelect("St Kitts-Nevis", "St Kitts-Nevis");
  $aEF->addSelect("St Lucia", "St Lucia");
  $aEF->addSelect("St Maarten", "St Maarten");
  $aEF->addSelect("St Pierre &amp; Miquelon", "St Pierre &amp; Miquelon");
  $aEF->addSelect("St Vincent &amp; Grenadines", "St Vincent &amp; Grenadines");
  $aEF->addSelect("Saipan", "Saipan");
  $aEF->addSelect("Samoa", "Samoa");
  $aEF->addSelect("Samoa American", "Samoa American");
  $aEF->addSelect("San Marino", "San Marino");
  $aEF->addSelect("Sao Tome &amp; Principe", "Sao Tome &amp; Principe");
  $aEF->addSelect("Saudi Arabia", "Saudi Arabia");
  $aEF->addSelect("Senegal", "Senegal");
  $aEF->addSelect("Seychelles", "Seychelles");
  $aEF->addSelect("Sierra Leone", "Sierra Leone");
  $aEF->addSelect("Singapore", "Singapore");
  $aEF->addSelect("Slovakia", "Slovakia");
  $aEF->addSelect("Slovenia", "Slovenia");
  $aEF->addSelect("Slovakia", "Slovensko");
  $aEF->addSelect("Solomon Islands", "Solomon Islands");
  $aEF->addSelect("Somalia", "Somalia");
  $aEF->addSelect("South Africa", "South Africa");
  $aEF->addSelect("Spain", "Spain");
  $aEF->addSelect("Sri Lanka", "Sri Lanka");
  $aEF->addSelect("Sudan", "Sudan");
  $aEF->addSelect("Suriname", "Suriname");
  $aEF->addSelect("Swaziland", "Swaziland");
  $aEF->addSelect("Sweden", "Sweden");
  $aEF->addSelect("Switzerland", "Switzerland");
  $aEF->addSelect("Syria", "Syria");
  $aEF->addSelect("Tahiti", "Tahiti");
  $aEF->addSelect("Taiwan", "Taiwan");
  $aEF->addSelect("Tajikistan", "Tajikistan");
  $aEF->addSelect("Tanzania", "Tanzania");
  $aEF->addSelect("Thailand", "Thailand");
  $aEF->addSelect("Togo", "Togo");
  $aEF->addSelect("Tokelau", "Tokelau");
  $aEF->addSelect("Tonga", "Tonga");
  $aEF->addSelect("Trinidad &amp; Tobago", "Trinidad &amp; Tobago");
  $aEF->addSelect("Tunisia", "Tunisia");
  $aEF->addSelect("Turkey", "Turkey");
  $aEF->addSelect("Turkmenistan", "Turkmenistan");
  $aEF->addSelect("Turks &amp; Caicos Is", "Turks &amp; Caicos Is");
  $aEF->addSelect("Tuvalu", "Tuvalu");
  $aEF->addSelect("Uganda", "Uganda");
  $aEF->addSelect("Ukraine", "Ukraine");
  $aEF->addSelect("United Arab Erimates", "United Arab Erimates");
  $aEF->addSelect("United Kingdom", "United Kingdom");
  $aEF->addSelect("United States of America", "United States of America");
  $aEF->addSelect("Uraguay", "Uraguay");
  $aEF->addSelect("Uzbekistan", "Uzbekistan");
  $aEF->addSelect("Vanuatu", "Vanuatu");
  $aEF->addSelect("Vatican City State", "Vatican City State");
  $aEF->addSelect("Venezuela", "Venezuela");
  $aEF->addSelect("Vietnam", "Vietnam");
  $aEF->addSelect("Virgin Islands (Brit)", "Virgin Islands (Brit)");
  $aEF->addSelect("Virgin Islands (USA)", "Virgin Islands (USA)");
  $aEF->addSelect("Wake Island", "Wake Island");
  $aEF->addSelect("Wallis &amp; Futana Is", "Wallis &amp; Futana Is");
  $aEF->addSelect("Yemen", "Yemen");
  $aEF->addSelect("Zaire", "Zaire");
  $aEF->addSelect("Zambia", "Zambia");
  $aEF->addSelect("Zimbabwe", "Zimbabwe");
}

?>