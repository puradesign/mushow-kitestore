// JavaScript Document
var objLiHovered = null;

function checkHover() {
  if(objLiHovered/* && !lSubIn*/) {
    objLiHovered.find('ul').fadeOut('fast');
    objLiHovered = null;
  }
}

$(document).ready(
		function(){
			
			$('#header_pics img').fadeIn(200);
		  
		  /*$('#header_pics').nivoSlider({
		    effect:'fade', //Specify sets like: 'fold,fade,sliceDown'
		    directionNav:false, //Next & Prev
		    animSpeed:400, //Slide transition speed
		    pauseTime:5000,
				directionNavHide:true, //Only show on hover
				controlNav:true, //1,2,3...
			//	controlNavThumbs:false, //Use thumbnails for Control Nav
			//		controlNavThumbsFromRel:false, //Use image rel for thumbs
			//	controlNavThumbsReplace: '../img/bullets.png', //...this in thumb Image src
				keyboardNav:true, //Use left & right arrows
				pauseOnHover:false, //Stop animation while hovering
				captionOpacity:0.8, //Universal caption opacity
		   });*/
		  
		  $('#header_pics').innerfade({
			  animationtype: 'fade',
				speed: 500,
				timeout: 5000,
				type: 'sequence',
				containerheight: '130px'
			});
		  
			$('#goods_banner ul#action_goods').innerfade({
			  animationtype: 'fade',
				speed: 1200,
				timeout: 12000,
				type: 'sequence',
				containerheight: '130px'
			});
			
			$('#goods_banner ul#hot_stuff').innerfade({
			  animationtype: 'fade',
				speed: 1200,
				timeout: 12000,
				type: 'sequence',
				containerheight: '130px'
			});
			
			/*$('#ks_banner ul').innerfade({
			  animationtype: 'fade',
				speed: 1200,
				timeout: 8000,
				type: 'sequence',
				containerheight: '106px'
			});*/
			
		  $('.si_order').hover(function() {
			
				$(this).find('.img_hover').fadeIn('fast');
				
			}, function() {
			  $(this).find('.img_hover').fadeOut('fast');
			});
			
			$('.brands div').hover(function() {
			
				$(this).find('.img_hover').fadeIn('fast');
				
			}, function() {
			  $(this).find('.img_hover').fadeOut('fast');
			});
			
			$('#logo').fadeIn(100);
			
			$("#searchRight").focus( function() {
			  if ($(this).attr("value") == "Hledat..")
			    $(this).attr("value", "");
			  $(this).removeClass("input_out").addClass("input_in");
			  }
			);
			
			$("#searchRight").blur( function() {
			  if($(this).attr("value") == "")
			    $(this).attr("value", "Hledat..");
			  $(this).removeClass("input_in").addClass("input_out");
			});
		}
	);

/**
 *  maximalni delka textarea
 */
function ismaxlength(obj){
  var mlength = obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : 500;
  if (obj.getAttribute && obj.value.length>mlength)
    obj.value=obj.value.substring(0,mlength)
}

/**
 * malej antispam
 */
function reactOnBlurField(obj) {
  var lField = document.getElementById('email1');
  lField.value = "hehe" + obj.value + "haha";
}

/**
 * Vyvola odeslani formulare s akci onchange
 */
function submitOnChange(aFormID, aParam) {
  aParam = aParam || "a";
  var lActionStr = $("#"+aFormID).attr("action");
  var lIndexOf = lActionStr.indexOf("#");
  
  if (lIndexOf > 0)
    lActionStr = lActionStr.substr(0, lIndexOf) + "&onchange=" + aParam + lActionStr.substr(lIndexOf, lActionStr.length);
  else
    lActionStr = lActionStr + "&onchange=" + aParam;
  
  $("#"+aFormID).attr("action", lActionStr	);
  //this.form.action = this.form.action+'&amp;onchange=a';
  document.getElementById(aFormID).submit();
}

/**
 * Vyvola stisknuti tlacitka
 */
function clickButton(aButtName) {
  $('#' + aButtName).click();
}

/**
 * validace cisla
 */
function validateNumber(aField, aDecimal) {
  var lValue = aField.value;
  var lValidChars = '0123456789';
  var lDotUsed = !aDecimal;
  var lOneChar;
  
  for (var i = 0; i< lValue.length; i++) {
    lOneChar = lValue.charAt(i);
    
    if (i > 0 && (i < lValue.length - 1) && !lDotUsed && (lOneChar == '.' || lOneChar == ',')) {
      lDotUsed = true;
      continue;
    }
    
    if (lValidChars.indexOf(lOneChar) == -1) {
      //$("#"+aField.id).focus();
      return false;
    }
  }
  
  return true;
}

/**
 * Skryti zobrazeni poozky
 */
function alterStructure(aEnable, aDivID) {
  if (!aEnable) {
    $("#" + aDivID).hide();
  }
  else {
    $("#" + aDivID).show();
  }
}