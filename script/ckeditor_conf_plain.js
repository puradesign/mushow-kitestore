// JavaScript Document
CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	 config.language = 'cs';
	 //config.uiColor = '#323232';
	 //config.resize_enabled = false;
	 config.resize_maxWidth = 520;
	 config.resize_minWidth = 520;
	 config.width = 520;
	 config.resize_minHeight = 520;
	 config.resize_minHeight = 150;
	 config.height = 150;
	 config.toolbar =
[
    ['Bold','Italic','Underline'],['Link','Unlink']
];
   //config.toolbar = 'Full';
   
   config.startupOutlineBlocks = false;
   config.skin = 'office2003';
};
