// JavaScript Document
CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	 config.language = 'cs';
	 //config.uiColor = '#323232';
	 config.resize_enabled = false;
	 //config.resize_maxWidth = 530;
	 //config.resize_minWidth = 530;
	 config.width = 530;
	 config.toolbar =
[
    ['Source','-','NewPage','-'], ['Cut','Copy','Paste','PasteText','PasteFromWord'],
    ['Undo','Redo','-','Find','Replace'], 
    ['Image','Flash', 'Table','HorizontalRule','SpecialChar'], '/',['SelectAll','RemoveFormat'],
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    '/',['Link','Unlink'],
    ['Styles','Format','FontSize'],
    ['TextColor','BGColor'],
    ['ShowBlocks','-','About']
];
   //config.toolbar = 'Full';
	 config.forcePasteAsPlainText = true;
   config.startupOutlineBlocks = false;
 //  config.skin = 'office2003';
   
   config.filebrowserBrowseUrl = 'filemanager.php';
};
