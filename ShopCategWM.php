<?php

class ShopCategWM extends WebModule {
  var $mAction;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Kategorie";
  }

  /**
   * Definuje tlacitko pro pridani
   */
  function defineHeaderButton() {
    echo "<div class='right_top_button add_button'>";
    echo "<a href='" . WR . "?m=" . S_CATEG_EDIT . "&amp;action=add'>";
    echo "Nová kategorie";
    echo "</a></div>";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    $limit = 100;
    $offset = 0;

    // slozeni podminky
    $whereCond = new WhereCondition();

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $result = $itemsBrowser->executeQuery("s_item_type LEFT JOIN s_item_brand ON s_item_type.sit_brand = s_item_brand.sib_code", $whereCond, "sit_brand ASC, sit_id ASC", "");
    if (!$result)
      die(getRText("err9") . $result->error);

    // nebyla nalezena zadna polozka
    if ($itemsBrowser->getCountAll() < 1) {
      if (!$search)
        echo "<br /><p>Žádné dostupné položky</p>";
      /* else
         echo "<br /><p class='searchCount'>Nebyly nalezeny žádné příspěvky.</p>";*/
    }

    // byla nalezena alespon jedna polozka
    else {
      echo "<table class='brw_table'>";
      echo "<thead><th>Kategorie</th><th>Firma</th><th></th><th></th></thead>";
      // vykresleni polozek
      while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td style='width:160px'>" . $row["sit_name"] . "</td>";
        echo "<td>" . $row["sib_name"] . "</td>";
        echo "<td><a href='" . WR . "?m=" . S_CATEG_EDIT . "&amp;action=edit&amp;item=" . $row["sit_code"] . "'>Editovat</a></td>";
        echo "<td><a href='" . WR . "?m=" . S_CATEG_DEL . "&amp;item=" . $row["sit_code"] . "'>Smazat</a></td>";
        echo "</tr>\n";
      }

      echo "</table>\n";
    }
  }
}
?>