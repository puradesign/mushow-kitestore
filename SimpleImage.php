<?php
/*
 * File: SimpleImage.php
 * Author: Simon Jarvis
 * Copyright: 2006 Simon Jarvis
 * Date: 08/11/06
 * Link: http://www.white-hat-web-design.co.uk/articles/php-image-resizing.php
 * 
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation; either version 2 
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details: 
 * http://www.gnu.org/licenses/gpl.html
 *
 */

class SimpleImage {

  var $image;
  var $image_type;
  var $filename;

  function load($filename) {
    if (!file_exists($filename))
      return false;

    $image_info = getimagesize($filename);
    $this->filename = $filename;
    $this->image_type = $image_info[2];
    //echo "before loading image: ".number_format(memory_get_usage()) . "<br />\n";
    if ($this->image_type == IMAGETYPE_JPEG) {
      $this->image = imagecreatefromjpeg($filename);
    } elseif ($this->image_type == IMAGETYPE_GIF) {
      $this->image = imagecreatefromgif($filename);
    } elseif ($this->image_type == IMAGETYPE_PNG) {
      $this->image = imagecreatefrompng($filename);
    } else
      return false;

    //   echo "after loading image: ".number_format(memory_get_usage()) . "<br />\n";
    return true;
  }

  function save($filename, $image_type = 0, $compression = 75, $permissions = null) {
    $created = false;

    if (empty($image_type))
      $image_type = $this->image_type;

    if ($image_type == IMAGETYPE_JPEG) {
      $created = imagejpeg($this->image, $filename, $compression);
    } elseif ($image_type == IMAGETYPE_GIF) {
      $created = imagegif($this->image, $filename);
    } elseif ($image_type == IMAGETYPE_PNG) {
      $created = imagepng($this->image, $filename);
    }

    if ($permissions != null) {
      chmod($filename, $permissions);
    }

    return $created;
  }
  function output($image_type = 0) {
    if (empty($image_type))
      $image_type = $this->image_type;

    if ($image_type == IMAGETYPE_JPEG) {
      imagejpeg($this->image);
    } elseif ($image_type == IMAGETYPE_GIF) {
      imagegif($this->image);
    } elseif ($image_type == IMAGETYPE_PNG) {
      imagepng($this->image);
    }
  }
  function getWidth() {
    return imagesx($this->image);
  }
  function getHeight() {
    return imagesy($this->image);
  }
  function resizeToHeight($height) {
    $ratio = $height / $this->getHeight();
    $width = $this->getWidth() * $ratio;
    $this->resize($width, $height);
  }
  function resizeToWidth($width) {
    $ratio = $width / $this->getWidth();
    $height = $this->getheight() * $ratio;
    $this->resize($width, $height);
  }
  function scale($scale) {
    $width = $this->getWidth() * $scale / 100;
    $height = $this->getheight() * $scale / 100;
    $this->resize($width, $height);
  }
  function resize($width, $height) {
    $new_image = imagecreatetruecolor($width, $height);
    imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
    $this->image = $new_image;
  }

  function resizeToMaxWAndH($maxWidth, $maxHeight, $enlarge = false) {
    if (!$enlarge && $maxWidth >= $this->getWidth() && $maxHeight >= $this->getHeight())
      return;

    $ratio = $maxWidth / $maxHeight;
    $imageRatio = $this->getWidth() / $this->getHeight();

    if ($ratio > $imageRatio)
      $this->resizeToHeight($maxHeight);
    else
      $this->resizeToWidth($maxWidth);
  }

  function cropToSize($width, $height) {
    $destImage = imagecreatetruecolor($width, $height);

    $ratio = $width / $height;
    $imageRatio = $this->getWidth() / $this->getHeight();

    if ($ratio < $imageRatio) {
      $this->resizeToHeight($height);
      $left = ($this->getWidth() - $width) / 2;
      $top = 0;
    } else {
      $this->resizeToWidth($width);
      $left = 0;
      $top = ($this->getHeight() - $height) / 2;
    }

    imagecopy($destImage, $this->image, 0, 0, $left, $top, $this->getWidth(), $this->getHeight());

    $this->image = $destImage;

  }
}

class WebImage extends SimpleImage {
  var $frameWidth = 130;
  var $frameHeight = 130;

  function setFrameSize($width, $height) {
    $this->frameWidth = $width;
    $this->frameHeight = $height;
  }

  /**
   * Vrati rozmery HTML obrazku tak aby zabiral danou sirku(frameWidth) a vysku(frameHeight)
   */
  function getHtmlOutput($alt = "") {
    $resized = false;
    // pokud je obrazek vetsi nez ramecek, musime upravit rozmery
    if ($this->frameWidth < $this->getWidth()) {
      $this->resizeToWidth($this->frameWidth);
      $resized = true;
    }

    if ($this->frameHeight < $this->getHeight()) {
      $this->resizeToHeight($this->frameHeight);
      $resized = true;
    }

    $margin_horiz = ($this->frameWidth - $this->getWidth()) / 2;
    $margin_vertical = ($this->frameHeight - $this->getHeight()) / 2;
    $imgStr = "<img src='" . getBrowserPath($this->filename) . "' alt='" . $alt . "' ";
    // pokud jsme menili rozmery musime zadat nove
    if ($resized)
      $imgStr .= " height='" . $this->getHeight() . "' width='" . $this->getWidth() . "' ";

    $imgStr .= "style='padding:" . $margin_vertical . "px " . $margin_horiz . "px " . $margin_vertical . "px " . $margin_horiz . "px;'/>";

    return $imgStr;
  }

  /**
   * Vrati rozmery HTML obrazku s odkazem tak aby zabiral danou sirku(frameWidth) a vysku(frameHeight)
   */
  function getLinkHtmlOutput($linkParamsStr, $alt = "") {
    $resized = false;
    // pokud je obrazek vetsi nez ramecek, musime upravit rozmery
    if ($this->frameWidth < $this->getWidth()) {
      $this->resizeToWidth($this->frameWidth);
      $resized = true;
    }

    if ($this->frameHeight < $this->getHeight()) {
      $this->resizeToHeight($this->frameHeight);
      $resized = true;
    }

    $margin_horiz = ($this->frameWidth - $this->getWidth()) / 2;
    $margin_vertical = ($this->frameHeight - $this->getHeight()) / 2;
    $imgStr = "<a $linkParamsStr style='margin:" . $margin_vertical . "px " . $margin_horiz . "px ";
    $imgStr .= $margin_vertical . "px " . $margin_horiz . "px;'>";
    $imgStr .= "<img src='" . getBrowserPath($this->filename) . "' alt='" . $alt . "' ";
    // pokud jsme menili rozmery musime zadat nove
    if ($resized)
      $imgStr .= " height='" . $this->getHeight() . "' width='" . $this->getWidth() . "' ";

    $imgStr .= "/></a>";

    return $imgStr;
  }
}

class ImageUploaded extends SimpleImage {
  var $uploadedFileArray;

  function ImageUploaded($aFileArray) {
    $this->uploadedFileArray = $aFileArray;
  }

  function validate($isMandatory = true) {
    if (!$isMandatory && $this->uploadedFileArray["name"] == "")
      return true;

    if ($isMandatory && $this->uploadedFileArray["name"] == "") {
      $GLOBALS["rv"]->addError("Nebyla vybrána fotka pro nahrání na server.");
      return false;
    } elseif ($this->uploadedFileArray["error"] > 0) {
      if ($this->uploadedFileArray["error"] == 1) {
        $str = "Fotka je příliš veliká, maximální velikost fotky pro nahrání je 5MB.";
      } else {
        $str = "Chyba při nahrávání fotky na server. Zkontrolujte správnost paramterů fotky (velikost, formát) a zkuste to znovu.";
        $str .= " Pokud potíže přetrvají, kontaktujte prosím administrátora.";
      }

      $GLOBALS["rv"]->addError($str);
      return false;
    } elseif (($this->uploadedFileArray["type"] != "image/jpeg") &&
      ($this->uploadedFileArray["type"] != "image/pjpeg") &&
      ($this->uploadedFileArray["type"] != "image/png") &&
      ($this->uploadedFileArray["type"] != "image/gif")) {

      $GLOBALS["rv"]->addError("Nesprávný formát fotky (povolené formáty jsou .jpg, .jpeg, .png, .gif).");
      return false;
    }

    return true;
  }

  function loadFile() {
    $this->load($this->uploadedFileArray["tmp_name"]);
  }

  /**
   * Slozi cestu a prida odpovidajici extension souboru
   * vraci jmeno ulozeneho souboru   
   */
  function saveWithName($dirName, $name, $addExt = true) {
    // vytvoreni adresare
    if (!is_dir($dirName))
      mkdir($dirName);

    if ($addExt)
      $name = $name . "." . strtolower(pathinfo($this->uploadedFileArray["name"], PATHINFO_EXTENSION));

    $created = $this->save($dirName . DIRECTORY_SEPARATOR . $name);

    if ($created)
      return $name;
    else
      return "";
  }

  /**
   * Ulozi obrazek do specifickeho adresare
   * a vytvori unikatni jmeno v tomto adresari   
   */
  function saveToDir($dirName) {
    // vytvoreni adresare
    if (!is_dir($dirName))
      mkdir($dirName);

    $filename = $this->uploadedFileArray["name"];

    if (file_exists($dirName . DIRECTORY_SEPARATOR . $filename)) {
      $i = 0;
      while (file_exists($dirName . DIRECTORY_SEPARATOR . $i . $filename))
        $i++;

      $filename = $i . $filename;
    }

    $created = $this->save($dirName . DIRECTORY_SEPARATOR . $filename);

    if ($created)
      return $filename;
    else
      return "";
  }
}
?>