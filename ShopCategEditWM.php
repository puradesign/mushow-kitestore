<?php

class ShopCategEditWM extends WebModule {
  var $mAction;
  var $mHeader = "Kategorie";
  var $mActRow;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    // kontroly
    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    if (!isset($_GET["action"]))
      $this->mAction = "add";
    else
      $this->mAction = $_GET["action"];

    if (($this->mAction != "add" && $this->mAction != "edit") ||
      ($this->mAction == "edit" && (!isset($_GET["item"]) || !is_numeric($_GET["item"])))) {
      $GLOBALS["rv"]->addError("Chyba. Pravděpodobně byla ručně upravena url stránky.");
      $this->setForOutput(false);
      return false;
    }

    if ($this->mAction == "edit") {
      $query = "SELECT * FROM s_item_type WHERE sit_code=" . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);

      $row = $result->fetch_assoc();
      $this->mActRow = $row;
      $this->mHeader = alterHtmlTextToPlain($row["sit_name"]);

      if (empty($_POST)) {
        $_POST["sit_name"] = $this->mHeader;
        $_POST["sit_head"] = $row["sit_head"] != null ? $row["sit_head"] : 0;
      }

      $_POST["sit_brand"] = $row["sit_brand"];
    }

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    $name = $_POST["sit_name"];
    $head = $_POST["sit_head"] != 0 ? $_POST["sit_head"] : "NULL";
    $brand = $_POST["sit_brand"];

    $lCond = new WhereCondition();

    // vlozeni
    if ($this->mAction == "add") {

      $lCond->addCondStr("sit_brand = $brand");
      $url = createUrlTitle($name, "sit_url", "s_item_type", $lCond);

      // vlozeni do DB
      $query = "INSERT INTO s_item_type (`sit_name`, `sit_brand`, `sit_head`, `sit_url`)";
      $query .= " VALUES ('" . alterTextForDB($name) . "', '$brand'";
      $query .= ", $head, '$url')";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      $lCode = $GLOBALS["db"]->insert_id;

      // parent
      if ($head != "NULL") {
        $query = "SELECT sit_id FROM s_item_type WHERE sit_code = $head";
        $result = $GLOBALS["db"]->query($query);
        $row = $result->fetch_assoc();

        $lID = $row["sit_id"] . "." . $this->getCodeForID($lCode);
      } else
        $lID = $this->getCodeForID($lCode);

      $query = "UPDATE s_item_type SET sit_id = '$lID' WHERE sit_code = '$lCode' LIMIT 1";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      // presmerovani na prehled kategorii
      require_once "ShopCategWM.php";
      $GLOBALS["wm"] = new ShopCategWM(S_CATEG);
      $GLOBALS["wm"]->reactOnActionLow();

      return false;
    }

    // editace
    elseif ($this->mAction == "edit") {

      if ($this->mActRow["sit_name"] != $name) {
        $lCond->addCondStr("sit_brand = $brand AND sit_code != " . $this->mActRow["sit_code"]);
        $url = createUrlTitle($name, "sit_url", "s_item_type", $lCond);
      } else
        $url = $this->mActRow["sit_url"];

      // zmenila se nadrazena kategorie
      $lHeadChanged = ($head == "NULL" && $this->mActRow["sit_head"] != null) ||
      ($head != "NULL" && $head != $this->mActRow["sit_head"]);
      $lID = $this->mActRow["sit_id"];
      if ($lHeadChanged) {
        if ($head == "NULL")
          $lID = $this->getCodeForID($this->mActRow["sit_code"]);
        else {
          $query = "SELECT sit_id FROM s_item_type WHERE sit_code = '$head'";
          $result = $GLOBALS["db"]->query($query);
          $row = $result->fetch_assoc();

          $lID = $row["sit_id"] . "." . $this->getCodeForID($this->mActRow["sit_code"]);
          //$GLOBALS["rv"]->addInfo($head."ee".$lID);
        }
      }

      // update v DB
      $query = "UPDATE s_item_type SET `sit_name` = '$name'";
      $query .= ", `sit_head` = $head, `sit_url` = '$url', `sit_id` = '$lID'";
      $query .= " WHERE `sit_code` = " . $this->mActRow["sit_code"];
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      // zmenila se hlavicka - aktualizace podrizenych
      if ($lHeadChanged) {
        $query = "SELECT * FROM s_item_type WHERE sit_id LIKE '" . $this->mActRow["sit_id"] . "%'";
        $query .= "ORDER BY sit_id";
        $result = $GLOBALS["db"]->query($query);

        while ($row = $result->fetch_assoc()) {
          $str = Str_Replace(".", "\\.", $this->mActRow["sit_id"]);
          $lNewID = preg_replace("/^$str(.+)$/", $lID . '$1', $row["sit_id"]);

          $queryUpdate = "UPDATE s_item_type SET sit_id = '$lNewID' WHERE sit_code = " . $row["sit_code"];
          $resultUpdate = $GLOBALS["db"]->query($queryUpdate);
        }
      }

      // presmerovani na prehled kategorii
      require_once "ShopCategWM.php";
      $GLOBALS["wm"] = new ShopCategWM(S_CATEG);
      $GLOBALS["wm"]->reactOnActionLow();

      return false;
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    $str = $this->mHeader . " - ";

    if ($this->mAction == "add")
      $str .= "Přidat";
    elseif ($this->mAction == "edit")
      $str .= "Editace";

    return $str;
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $whereCond = new WhereCondition();

    // Nazev
    $lEF = new EditField("sit_name", "Název", 120, true,
      150, 20);
    $this->addElement($lEF);

    // Znacka
    $lEF = new EditCodeCombo("sit_brand", "Značka", 120, true,
      150, "s_item_brand", "sib_code", "sib_name");
    if ($this->mAction == "edit")
      $lEF->addFieldAttr("disabled", "disabled");
    $lEF->addFieldAttr("onChange", "submitOnChange(\"edit_form\");");
    $lEF->initOptions();
    $this->addElement($lEF);

    // Nadrazena kategorie
    $lEF = new EditCodeCombo("sit_head", "Nadřazená kategorie", 120, false,
      150, "s_item_type", "sit_code", "sit_name");
    $lEF->setAttrForStructure("sit_id");
    if ($this->mAction == "edit")
      $whereCond->addCondStr("sit_id NOT LIKE '" . $this->mActRow["sit_id"] . "%'");

    if (isset($_POST["sit_brand"]) && $_POST["sit_brand"] > 0) {
      $whereCond->addCondStr("sit_brand = " . $_POST["sit_brand"]);
    } else
      $lEF->addFieldAttr("disabled", "disabled");

    $lEF->setCond($whereCond);
    $lEF->setOrderBy("sit_id ASC");
    $lEF->initOptions();
    $this->addElement($lEF);
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {

    echo "  <form method='post' id='edit_form' action='" . WR . "?m=" . S_CATEG_EDIT . "&amp;action=" . $this->mAction;
    echo ($this->mAction != "add" ? "&amp;item=" . $_GET["item"] : "") . "'>";
    echo "<fieldset class='form'>";

    $this->printElements();

    echo "  <div class='td_left' style='height:100%;width: 100px;'>";
    echo "  <input type='submit' class='submit' value='Potvrdit'/></div>";

    echo "  </fieldset></form>";
  }

  /**
   * Vraci upraveny kod pro ID
   */
  function getCodeForID($aCode) {
    $str = $aCode;

    while ($aCode < 1000) {
      $str = "0" . $str;

      $aCode *= 10;
    }

    return $str;
  }
}
?>