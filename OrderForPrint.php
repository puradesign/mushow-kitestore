<?php
session_start();
require_once "connect.php";
require_once "UtilFunctions.php";
require_once "const.php";
require_once "StoreUtil.php";
require_once "WebModule.php";
require_once "BaseWM.php";
?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='cs' xml:lang='cs' xmlns='http://www.w3.org/1999/xhtml'>

<head>
  <meta http-equiv='content-type' content='text/html; charset=utf-8' />
  <meta http-equiv='content-language' content='cs' />
  <meta name='Author' content='Petr Kadlec' />
  <meta name='Copyright' content='Copyright 2010, http://www.mushow.cz/' />
  <link rel="shortcut icon" href='<? echo WR; ?>img/logo.gif' />
  <link rel='stylesheet' href='<? echo WR; ?>css/main.css' />
  <?php
  // vytvoreni vektoru vysledku akce
  $GLOBALS["rv"] = new ResultVect();

  require_once F_ROOT . "StoreOrderViewWM.php";
  $GLOBALS["wm"] = new StoreOrderViewWM(OR_VIEW);

  $lOk = $GLOBALS["wm"]->beforeAction();
  if (!$lOk)
    die($GLOBALS["rv"]->printErrors());

  $row = $GLOBALS["wm"]->mActRow;

  echo "<title>Objednávka č. " . $row["or_number"] . "</title>";
  echo "</head>";

  echo "<body style='background: white; color:black; padding:5px; width:800px'>";

  $lDate = StrFTime("%d.%m.%Y", strtotime($row["or_date"]));

  if (isset($_GET["fact_stajla"]) && $_GET["fact_stajla"] == "en")
    echo getOrderFactEn($row["or_number"], $lDate, $row, true, true);
  else
    echo getOrderFact($row["or_number"], $lDate, $row, true, true);


  echo "</body></html>";
  ?>