<?php

class LogoutWM extends WebModule {

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    unset($_SESSION[SN_CODE]);
    unset($_SESSION[SN_ADMIN]);
    $_SESSION[SN_LOGGED] = false;

    $_SESSION[SN_CART]->setDiscount(0);

    // id uzivatele do kosiku
    $_SESSION[SN_CART]->setUser(0);
    $_SESSION[SN_CART]->setUserAddress(0);

    $GLOBALS["rv"]->addInfo("Succesfully logged out.");

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Logging out...";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
  }
}
?>