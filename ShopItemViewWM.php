<?php

class ShopItemViewWM extends WebModule {
  var $mRow;
  var $mThisUrl;

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {
    echo "<link rel='stylesheet' type='text/css' href='" . F_SHADOWBOX . "shadowbox.css' />";
    echo "<script type='text/javascript' src='" . F_SHADOWBOX . "shadowbox.js'></script>";
    echo "<script type='text/javascript'>";
    echo " Shadowbox.init({";
    echo "  players:    [\"img\", \"swf\"]";
    echo " });";
    echo "</script>";
  }

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {

    // test emailu
    /*ip = $_SERVER['REMOTE_ADDR'];
    if ($ip === '109.81.210.68') {
      echo "Jsem tam<br/><br/>";
      $lNum = "00001";
      $lSubj = "Mushow kitestore - objednávka č. " . $lNum;

      $lPlain = "Vážený zákazníku,\n\n";
      $lPlain .= "zasíláme Vám potvrzení objednávky v internetové prodejně Mushow Kitestore (" . WR . ").\n\n";
      $lPlain .= "Jelikož Váš emailový klient nepodporuje html, nelze zobrazit detail objednávky.";
      $lPlain .= " Detail objednávky naleznete na odkazu uvedeném níže (po přihlášení):\n";
      $lPlain .= WR_ORDER . "/" . $lNum;


      $lHtmlText = "Vážený zákazníku,<br/><br/>";
      $lHtmlText .= "zasíláme Vám potvrzení objednávky v internetové prodejně Mushow Kitestore ";
      $lHtmlText .= "(<a href='" . WR . "'>" . WR . "</a>).<br/><br/>";
      $lHtmlText .= "Následuje zopakování vaší objednávky.<br/><br/>";

      $result = false;
      try {
        $result = sendMail("pk.kadlec@gmail.com", $lSubj, $lPlain, $lHtmlText);
      } catch (Exception $e) {
        echo $e->getMessage()."\n";
      }
      if ($result) {
        echo "Email odeslan!<br/>";
      } else {
        echo "Email neodeslan!<br/>";
      }

      die;
    }*/

    // znacka
    $query = "SELECT * FROM s_item_brand WHERE sib_code=" . $_GET["brand"];
    $result = $GLOBALS["db"]->query($query);
    $row = $result->fetch_assoc();

    $GLOBALS[GL_BRAND] = $row;

    // nacteni polozky
    $query = "SELECT * FROM shop_item WHERE si_url='" . $_GET["item"] . "'";
    $result = $GLOBALS["db"]->query($query);
    $this->mRow = $result->fetch_assoc();

    if (!$this->mRow) {
      $GLOBALS["rv"]->addError("Položka nenalezena.");
      $this->setForOutput(false);
      return false;
    }

    // naplneni udaju o kategorii
    $lCategCode = $this->mRow["si_type"];
    $lCategs = array();
    $lCategsUrl = "";

    while ($lCategCode != null) {
      $query = "SELECT * FROM s_item_type WHERE sit_code = $lCategCode";
      $result = $GLOBALS["db"]->query($query);
      $lCategRow = $result->fetch_assoc();

      if ($lCategRow) {
        $lCategs[] = $lCategRow;
        $lCategCode = $lCategRow["sit_head"];
        $lCategsUrl = $lCategRow["sit_url"] . "/" . $lCategsUrl;
      } else
        $lCategCode = null;
    }


    $this->mThisUrl = WR . $GLOBALS[GL_BRAND]["sib_url"] . "/" . $lCategsUrl . "v/" . $this->mRow["si_url"];
    $GLOBALS[GL_CATEGS] = array_reverse($lCategs);

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {

    // projdeme vsechny varinaty
    $query = "SELECT * FROM s_item_row WHERE sir_head=" . $this->mRow["si_code"] . " AND sir_visible=1";
    $result = $GLOBALS["db"]->query($query);

    while ($row = $result->fetch_assoc()) {
      //$GLOBALS["rv"]->addInfo("pruchod ".$row["sir_name"].", ".$row["sir_code"]);
      // pridani do kosiku
      if (isset($_POST["buy_item_" . $row["sir_code"]])) {
        $lName = $this->mRow["si_title"] . " (" . $row["sir_name"] . ")";
        $_SESSION[SN_CART]->addItem($this->mRow["si_code"], $row["sir_code"], $lName,
          $row["sir_price"], $this->mThisUrl);

        $GLOBALS["rv"]->addInfo("Položka $lName byla přidána do košíku. Můžete přejít na <a href='" . WR_CART . "'><b>obsah košíku</b></a> a nebo pokračovat v nákupu.");

        return true;
      }
    }
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    if ($this->mRow)
      return $this->mRow["si_title"];
    else
      return "Položka nenalezena";
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  /*function defineElements() {
    $whereCond = new WhereCondition();
   
    // Velikost
    $lEF = new EditCodeCombo("item_size", "Velikost", 60, false, 0,
                             "s_item_row", "sir_code", "sir_name");
    $lEF->setGapWidth(5);
    $lEF->setWidthPX(150);
   
    $whereCond->addCondStr("sir_head=".$this->mRow["si_code"]." AND sir_visible=1");
    $lEF->setCond($whereCond);
    $lEF->initOptions();
    $this->addElement($lEF);
  }*/


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    $lImgUrl = getBrowserPath(F_SHOP_ITEMS . "headfoto" . $this->mRow["si_code"] . ".jpg");

    echo "<div class='si_view_head'><img src='" . $lImgUrl . "' alt='" . $this->mRow["si_title"] . "' width='150'/>";

    /*echo "<div class='si_mark'>";
    if ($this->mRow["si_hot"] == 1)
      echo "<span style='background-color:#FFFC66;padding:2px'>H</span><span style='color:#FFFC66;margin-top:2px'>ot stuff</span>&nbsp;";
    
    if ($this->mRow["si_onsale"] == 1)
      echo "<span style='background-color:#CC60CC;padding:2px'>A</span><span style='color:#CC60CC;margin-top:2px'>kce</span>&nbsp;";
    echo "</div>";*/

    echo "<p>" . $this->mRow["si_descr"] . "</p>";

    echo "<form method='post' style='margin:0px;' action='" . $this->mThisUrl . "'>";
    echo "<table class='si_view_order'>";
    echo "<thead><tr><th>Velikost</th><th>Cena</th><th>Dostupnost</th><th></th></tr></thead>";

    $query = "SELECT * FROM s_item_row WHERE sir_head=" . $this->mRow["si_code"] . " AND sir_visible=1";
    $result = $GLOBALS["db"]->query($query);

    $lOddRow = 1;
    while ($row = $result->fetch_assoc()) {
      echo "<tr " . ($lOddRow == 1 ? "class='row_odd'" : "") . ">";
      echo "<td style='width:110px;'>" . $row["sir_name"] . "</td>";

      echo "<td>" . number_format($row["sir_price"], 0, "", ".") . "&nbsp;Kč</td>";

      echo "<td>" . ($row["sir_count"] > 0 ? "<span style='color:#6c954e'>skladem</span>" :
        "<span style='color:#540c0c'>vyprodáno</span>") . "</td>";

      echo "<td style='width:30px'>";
      if ($row["sir_count"] > 0) {
        $lButtID = "buy_item_" . $row["sir_code"];
        echo "<input type='submit' style='visibility:hidden; height: 0px; width:0px; float:none; display:none' ";
        echo "title='Koupit' value='Koupit' id='$lButtID' name='$lButtID'/>";
        echo "<a href='' onclick='clickButton(\"$lButtID\"); return false;'><img src='" . WR_IMG . "buy.png' alt='Koupit'/></a>";
      }
      echo "</td></tr>";

      $lOddRow = $lOddRow * (-1);
    }

    echo "</table></form>";

    echo "</div>";
    echo "<div class='si_view_text'>" . $this->mRow["si_text"] . "</div>";

    addShareButton();

    //printRefBackToArticles();
  }

  /**
   * Zde naplneni vektoru cesty - pro prepsani
   */
  function definePathVect() {
    $lBrandRow = $GLOBALS[GL_BRAND];
    $lUrl = WR . $lBrandRow["sib_url"];

    $GLOBALS["pv"]->addItem($lUrl, $lBrandRow["sib_name"]);

    if (isset($GLOBALS[GL_CATEGS])) {
      $lCategs = $GLOBALS[GL_CATEGS];

      for ($i = 0; $i < count($lCategs); $i++) {
        $lUrl .= "/" . $lCategs[$i]["sit_url"];
        $GLOBALS["pv"]->addItem($lUrl, $lCategs[$i]["sit_name"]);
      }
    }

    $GLOBALS["pv"]->addItem("", $this->mRow["si_title"]);
  }

  /**
   * Vraci popis do description v hlavicce
   */
  function getMetaDescription() {
    if ($this->mRow)
      return $this->mRow["si_descr"];
  }
}
?>