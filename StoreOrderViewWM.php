<?php
require_once "./lib/mpdf/mpdf.php";

class StoreOrderViewWM extends WebModule {
  var $mActRow;
  
  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (!isLogged()) {
      $GLOBALS["rv"]->addError("Pro prohlížení objednávky se musíte přihlásit.");
      $this->setForOutput(false);
      return false;
    }
    
    if (!isset($_GET["item"]) || empty($_GET["item"]) || !is_numeric($_GET["item"])) {
      $GLOBALS["rv"]->addError("Není definováno číslo objednávky.");
      $this->setForOutput(false);
      return false;
    }
    
		$query = "SELECT * FROM shop_order WHERE or_number=".addSlashes($_GET["item"]);
		$result = $GLOBALS["db"]->query($query);
			
		$row = $result->fetch_assoc();
		$this->mActRow = $row;
		
		if (!isLoggedAdmin()) {
			if ($row["or_user"] != $_SESSION[SN_CODE]) {
				$GLOBALS["rv"]->addError("Nemáte právo prohlížet tuto objednávku.");
				$this->setForOutput(false);
				return false;
			}
		}
		
		if (isLoggedAdmin()) {
		  if (empty($_POST)) {
		    $_POST["state"] = $this->mActRow["or_order_state"];
		    $_POST["price"] = $this->mActRow["or_price_sum"];
		    //$_POST["inform"] = "1";
		  }
		}
		
		if (isset($_GET["export"])) {
      $this->createPdf($_GET["export"], $_GET["fix"]);
      $this->setForOutput(false);
			return false;
    }
    
    return true;
  }
  
  /* -------------------------- PROCESS ACTION ------------------------------*/
  /* ------------------------------------------------------------------------*/
  
  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    if (!isLoggedAdmin())
      return false;
   
    $descr = alterTextForDB($_POST["descr"]);
		$state = alterTextForDB($_POST["state"]);
		$price = alterTextForDB($_POST["price"]);
		$inform = isset($_POST["inform"]) ? true : false;
		
		$lNum = $this->mActRow["or_number"];
		
		// update v databazi
		if ($state != $this->mActRow["or_order_state"] || $price != $this->mActRow["or_price_sum"]) {
			$query = "UPDATE shop_order SET or_order_state = '$state', or_price_sum = '$price' WHERE or_code=".$this->mActRow["or_code"];
			$result = $GLOBALS["db"]->query($query);
			
			$this->mActRow["or_price_sum"] = $price;
		}
		
		// pricteni odecteneho zbozi
		if ($state != $this->mActRow["or_order_state"] && $state == 3) {
		  $query = "SELECT * FROM order_item WHERE ori_head=".$this->mActRow["or_code"];
		  $result = $GLOBALS["db"]->query($query);
		  
		  // pres vsechny polozky objednavky
		  while ($row = $result->fetch_assoc()) {
		    if ($row["ori_item_row_code"] < 1)
		      continue;
		    
		    $query = "UPDATE s_item_row SET sir_count = sir_count + ".$row["ori_count"];
		    $query .= " WHERE sir_code=".$row["ori_item_row_code"];
		    $updateRes = $GLOBALS["db"]->query($query);
		  }
		  
		  $this->getFormElement("state")->addFieldAttr("disabled", "disabled");
		}
		
		// odeslani mailu
		if ($inform) {
			$lSubj = "Změna stavu objednávky č.".$lNum;
			
			$lPlain = "Pěkný den, \n\n";
			$lPlain .= "stav Vaší objednávky č.$lNum byl změněn, její ";
			$lPlain .= "nový stav je - ".getOrderStateStr($state)."\n\n";
			$lPlain .= "Podrobnosti o této objednávce naleznete na níže uvedeném odkazu (po přihlášení):\n";
			$lPlain .= WR_ORDER."/$lNum";
			
			if (!empty($_POST["descr"]))
				$lPlain .= "\n\nPoznámka ke změně: ".$_POST["descr"];
			
			$lHtml = "<p>Pěkný den, <br/><br/>";
			$lHtml .= "stav Vaší objednávky č.$lNum byl změněn, její ";
			$lHtml .= "nový stav je - <b>".getOrderStateStr($state)."</b><br/><br/>";
			$lHtml .= "Podrobnosti o této objednávce naleznete na níže uvedeném odkazu (po přihlášení):\n";
			$lHtml .= "<a href='".WR_ORDER."/$lNum'>".WR_ORDER."/$lNum</a></p>";
			
			if (!empty($_POST["descr"]))
				$lHtml .= "<p>Poznámka ke změně: ".$descr."</p>";
		  
		  $lHtml .= "<p>Faktura objednávky: </p>";
		  
		  $lDate = StrFTime("%d.%m.%Y", strtotime($this->mActRow["or_date"]));
		  $lHtml .= getOrderFact($this->mActRow["or_number"], $lDate, $this->mActRow, true);
			
			if (sendMail($this->mActRow["or_email"], $lSubj, $lPlain, $lHtml)) {
				$GLOBALS["rv"]->addInfo("Email úspěšně odeslán.");
			} 
			else {
			 $GLOBALS["rv"]->addError("Nepodařilo se odeslat email.");
			}
		}
  }
  
  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu
   */
  function getHeader() {
    return "Objednávka č.".$this->mActRow["or_number"];
  }
  
  /* -------------------------- DEFINE ELEMENTS  ----------------------------*/
  /* ------------------------------------------------------------------------*/
  
  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    if (isLoggedAdmin()) {
      $lPrompt = 100;
      
      // Stav
			$lEF = new EditCombo("state", "Změnit stav na", $lPrompt, false, 130);
			$lEF->addSelect("Čeká na vyřízení", "1");
			$lEF->addSelect("Vyřízená", "2");
			$lEF->addSelect("Zrušená", "3");
			
			if ($this->mActRow["or_order_state"] == 3)
			  $lEF->addFieldAttr("disabled", "disabled");
			  
			$this->addElement($lEF);
			
			// Cena
			$lEF = new EditInt("price", "Celková cena", $lPrompt, true,
												 70, 10, true);
			$this->addElement($lEF);
    
      // Poznamka
			$lEF = new EditText("descr", "Poznámka", $lPrompt, false,
												 35, 500, 3);
			$this->addElement($lEF);
			
			// Informovat zákazníka e-mailem
			$lEF = new EditBool("inform", "Informovat o změně e-mailem", $lPrompt);
			$this->addElement($lEF);
    }
  }
  
  
  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  
  
  /**
   * Definovani vlastniho obsahu
   */     
  function defineHtmlOutput() {
    echo "<div style='float:left;width:530px;margin:10px 0px 20px 0px;'>";
    echo "<p>Náhled faktury k tisku - <a style='font-size:14px;font-weight:bold' href='".WR_ORDER."/".$this->mActRow["or_number"]."?export=pdf' target='_blank'>";
    echo "PDF</a>";
    if (isLoggedAdmin()) {
      echo "&nbsp;(<a href='".WR_ORDER."/".$this->mActRow["or_number"]."?export=pdf&fix=1' target='_blank'>";
	    echo "opravná PDF</a>)";
	  }
    echo "<span style='font-size:14px;font-weight:bold'> | </span>";
    echo "<a style='font-size:14px;font-weight:bold' href='' onclick='javascript:void window.open(\"".WR_ORDER."/tisk/".$this->mActRow["or_number"]."\", \"win2\", \"status=no,toolbar=no,scrollbars=yes,titlebar=yes,menubar=yes,resizable=yes,width=840px,height=1200px,directories=no,location=no\");'>";
    echo "HTML</a></p>";
    
    if (isLoggedAdmin()) {
			echo "<p>Náhled k tisku faktury do zahraničí -";
			echo "<a style='font-size:14px;font-weight:bold' href='".WR_ORDER."/".$this->mActRow["or_number"]."?export=enpdf' target='_blank'>";
			echo "PDF</a><span style='font-size:14px;font-weight:bold'> | </span>";
			echo "<a style='font-size:14px;font-weight:bold' href='' onclick='javascript:void window.open(\"".WR_ORDER."/tisk/".$this->mActRow["or_number"]."?fact_stajla=en\", \"win2\", \"status=no,toolbar=no,scrollbars=yes,titlebar=yes,menubar=yes,resizable=yes,width=840px,height=1200px,directories=no,location=no\");'>";
			echo "HTML</a></p>";
    }
    
    echo "<p>Stav objednávky:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>";
    echo getOrderStateStr($this->mActRow["or_order_state"])."</b></p>";
    
    if (isLoggedAdmin()) {
      echo "<fieldset class='form' style='width:420px;padding:0px'>";
			echo "  <form method='post' id='edit_form' style='margin:0px;' action='".WR_ORDER."/".$this->mActRow["or_number"]."?action'>";
		 
			$this->printElements();
		  
      echo "  <input type='submit' class='submit' value='Potvrdit' style='margin: 5px 5px 5px 120px;'/>";
		  
			echo "  </form></fieldset>";
    }
    
    echo "</div>";
  
    $lDate = StrFTime("%d.%m.%Y", strtotime($this->mActRow["or_date"]));
    
    echo getOrderFact($this->mActRow["or_number"], $lDate, $this->mActRow);
  }
  
  /**
   * Zde naplneni vektoru cesty - pro prepsani
   */
  function definePathVect() {
    $GLOBALS["pv"]->addItem(WR_ORDER, "Objednávky");
    $GLOBALS["pv"]->addItem("", $this->getHeader());
  }
  
  /**
   *
   */
  function createPdf($export = "pdf", $fix = "false") {
    $row = $this->mActRow;
    
    $str = "<head><style>";
    $str .= "td{font-size: 8pt;}";
    $str .= "</style></head>";
    $str .= "<body style='background: white; color:black; padding:5px; width:800px; font-size: 10pt'>";
  
		$lDate = StrFTime("%d.%m.%Y", strtotime($row["or_date"]));
		
		if ($export == "enpdf")
  		$str .=  getOrderFactEn($row["or_number"], $lDate, $row, true, false, false, true);
  	else
      $str .=  getOrderFact($row["or_number"], $lDate, $row, true, false, false, true, $fix);
  
    $str .=  "</body>";
    
    $mpdf = new mPDF();
    $mpdf->WriteHTML($str);
    $mpdf->Output($row["or_number"].".pdf", 'I');
  }
}
?>
