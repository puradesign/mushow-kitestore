<?php

class BazarViewWM extends WebModule {
  var $mRow;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {

    $query = "SELECT * FROM advert  LEFT JOIN advert_categ ON ad_categ = advert_categ.adc_code ";
    $query .= "WHERE ad_code=" . $_GET["bid"];
    $result = $GLOBALS["db"]->query($query);
    $this->mRow = $result->fetch_assoc();

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    if ($this->mRow)
      return $this->mRow["ad_title"];
    else
      return "Položka nenalezena";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    $filename = F_BAZAR . "advert_foto_" . $this->mRow["ad_code"] . ".jpg";
    if (!file_exists($filename))
      $filename = F_IMG . "none.png";

    $image = new WebImage();
    $image->load($filename);
    $image->setFrameSize(100, 100);

    $imgStr = $image->getHtmlOutput($this->mRow["ad_title"]);

    echo "<div class='ad_pic'>" . $imgStr . "</div>";

    echo "<div class='ad_text'><p>" . $this->mRow["ad_text"] . "</p>";
    echo "<p><strong>Cena: " . $this->mRow["ad_price"] . "</strong></p>";
    echo "</div>";

    /* if (!empty($this->mRow["ad_user"])) {
        $query = "SELECT u_code, u_nick FROM user WHERE u_code=".$this->mRow["ad_user"];
        $result = mysql_query($query);
        
        $user_row = $result->fetch_assoc();
        
        echo "<div class='text_block'>Vložil: ";
        echo "<a href='".WR_RID."?m=".PROFILE."&amp;id=".$user_row["u_code"]."'>".$user_row["u_nick"]."</a>";
        $date = strtotime($this->mRow["ad_date_create"]);
        $date = StrFTime("%d.%m.%Y", $date);
        echo ",&nbsp;".$date;
        echo "</div>";
      }*/

    echo "<div class='ad_contact' style='margin-top: 20px;'>";
    echo "<h2>Kontakt:</h2>";
    echo "<p><strong>Jméno: </strong>" . $this->mRow["ad_name"] . "</p>";
    echo "<p><strong>Email: </strong>" . $this->mRow["ad_email"] . "</p>";
    if ($this->mRow["ad_phone"])
      echo "<p><strong>Telefon: </strong>" . $this->mRow["ad_phone"] . "</p>";
    echo "</div>";

    echo "<div style='float:left'><p><a href='" . WR_BAZAR . "'>Zpět na přehled</a></p></div>";
  }

  /**
   * Zde naplneni vektoru cesty - pro prepsani
   */
  function definePathVect() {
    $GLOBALS["pv"]->addItem(WR_BAZAR, "Bazar");
    $GLOBALS["pv"]->addItem("", $this->getHeader());
  }

  /**
   * Pro prepsani - vraci ID polozky v menu, ktera patri k tomuto WM (podle menu konstant)
   */
  function getMenuItemID() {
    return MENU_BAZAR;
  }

  /**
   * Vraci popis do description v hlavicce
   */
  function getMetaDescription() {
    if ($this->mRow)
      return $this->mRow["ad_text"];
  }
}
?>