-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 10, 2010 at 09:13 PM
-- Server version: 5.1.37
-- PHP Version: 5.2.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `ankite`
--

-- --------------------------------------------------------

--
-- Table structure for table `fpost`
--

CREATE TABLE `fpost` (
  `f_code` int(11) NOT NULL AUTO_INCREMENT,
  `f_father` int(11) DEFAULT NULL,
  `f_user` int(11) DEFAULT NULL,
  `f_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `f_mail` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `f_web` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  `f_text` text COLLATE utf8_czech_ci NOT NULL,
  `f_date` datetime NOT NULL,
  `f_id` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `f_head` int(11) NOT NULL,
  `f_ip` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`f_code`),
  KEY `f_user` (`f_user`),
  KEY `f_father` (`f_father`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=37 ;

--
-- Dumping data for table `fpost`
--

INSERT INTO `fpost` VALUES(32, NULL, NULL, 'peter', 'p@k.cz', '', '<p>\r\n	heheeeeee</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	hehe</p>\r\n', '2010-08-01 01:09:34', '1', 1, '::1');
INSERT INTO `fpost` VALUES(33, NULL, NULL, 'testik', 'l@l.l', '', '<p>\r\n	luliiiiiiiiiii</p>\r\n<p>\r\n	nnnnnnnnnnnnnnnnnnnnnnnnnnnnn</p>\r\n', '2010-08-01 01:11:54', '2', 2, '::1');
INSERT INTO `fpost` VALUES(34, 33, NULL, 'pett', 'p@k.cz', '', '<p>\r\n	odpoveeeeeeeee<u>eeeeeeee</u>eeeeeed heh</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>heh</strong></p>\r\n', '2010-08-01 01:24:13', '2.1', 2, '::1');
INSERT INTO `fpost` VALUES(35, NULL, NULL, 'pett', 'p@k.cz', '', '<p>\r\n	terekkaaaa</p>\r\n', '2010-09-10 19:45:25', '3', 3, '::1');
INSERT INTO `fpost` VALUES(36, 35, NULL, 'ahoj', 'p@k.cz', '', '<p>\r\n	odpevode na terkaaaaaaa</p>\r\n', '2010-09-10 20:33:43', '3.1', 3, '::1');


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_code` int(11) NOT NULL AUTO_INCREMENT,
  `u_nick` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `u_mail` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `u_date_create` datetime NOT NULL,
  `u_passwd` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `u_ip` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`u_code`),
  UNIQUE KEY `u_nick` (`u_nick`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` VALUES(1, 'admin', 'ds', NOW(), 'c593f6f22f99a6a24d1191cdf5f40384', '11');

--
-- Table structure for table `user`
--

CREATE TABLE `user_address` (
  `ua_code` int(11) NOT NULL AUTO_INCREMENT,
  `ua_abbr` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `ua_user` int(11) NOT NULL,
  `u_main_addr` tinyint(1) NOT NULL DEFAULT '1',
  
  `ua_name` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `ua_surname` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `ua_company` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ua_street` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `ua_town` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `ua_psc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `ua_state` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ua_tel` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `ua_cellphone` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `ua_ico` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `ua_dic` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  
  PRIMARY KEY (`ua_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=0 ;


-- --------------------------------------------------------

--
-- Table structure for table `shop_item`
--

CREATE TABLE `shop_item` (
  `si_code` int(11) NOT NULL AUTO_INCREMENT,
  `si_title` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `si_descr` varchar(500) COLLATE utf8_czech_ci NOT NULL,
  `si_text` text COLLATE utf8_czech_ci,
  `si_type` int(11) NOT NULL,
  `si_brand` int(11) NOT NULL,
  `si_price` int(6) NOT NULL,
  `si_count` int(3) NOT NULL,
  
  PRIMARY KEY (`si_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=0 ;

--
-- Table structure for table `s_item_type`
--

CREATE TABLE `s_item_type` (
  `sit_code` int(11) NOT NULL AUTO_INCREMENT,
  `sit_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `sit_head` int(11),
  `sit_brand` int(11) NOT NULL,
  `sit_id` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  
  PRIMARY KEY (`sit_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=0 ;

--
-- Table structure for table `s_item_brand`
--

CREATE TABLE `s_item_brand` (
  `sib_code` int(11) NOT NULL AUTO_INCREMENT,
  `sib_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  
  PRIMARY KEY (`sib_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=0 ;

--
-- Table structure for table `s_item_brand`
--

CREATE TABLE `s_item_row` (
  `sir_code` int(11) NOT NULL AUTO_INCREMENT,
  `sir_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `sir_price` int(6) NOT NULL,
  `sit_count` int(3) NOT NULL,
  `sir_head` int(11) NOT NULL,
  
  PRIMARY KEY (`sir_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=0 ;

- -------------------------------

--
-- Table structure for table `transport`
--

CREATE TABLE `transport` (
  `tr_code` int(11) NOT NULL AUTO_INCREMENT,
  `tr_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `tr_price` int(6) NOT NULL,
  
  PRIMARY KEY (`tr_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=0 ;

- -------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `or_code` int(11) NOT NULL AUTO_INCREMENT,
  `or_descr` varchar(500) COLLATE utf8_czech_ci,
  `or_transport` int(11) NOT NULL,
  `or_user` int(11) NOT NULL,
  
  `or_name` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `or_surname` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `or_company` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `or_street` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `or_town` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `or_psc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `or_state` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `or_tel` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `or_ico` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `or_dic` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  
  PRIMARY KEY (`or_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=0 ;

--
-- Table structure for table `order`
--

CREATE TABLE `order_item` (
  `ori_code` int(11) NOT NULL AUTO_INCREMENT,
  `ori_name` varchar(50) COLLATE utf8_czech_ci,
  `ori_head` int(11) NOT NULL,
  
  `ori_price` int(6) NOT NULL,
  `ori_count` int(3) NOT NULL,
  `ori_url` varchar(150) COLLATE utf8_czech_ci,
  
  PRIMARY KEY (`ori_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=0 ;


ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`or_user`) REFERENCES `user` (`u_code`) ON DELETE CASCADE;

ALTER TABLE `order_item`
  ADD CONSTRAINT `orderi_ibfk_1` FOREIGN KEY (`ori_head`) REFERENCES `order` (`or_code`) ON DELETE CASCADE;



-- --------------------------------------------------------

-- BAZAR

-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 06, 2010 at 02:35 PM
-- Server version: 5.1.37
-- PHP Version: 5.2.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `snowkiti`
--

-- --------------------------------------------------------

--
-- Table structure for table `advert`
--

CREATE TABLE `advert` (
  `ad_code` int(11) NOT NULL AUTO_INCREMENT,
  `ad_user` int(11) DEFAULT NULL,
  `ad_title` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ad_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ad_email` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ad_text` text COLLATE utf8_czech_ci NOT NULL,
  `ad_phone` varchar(15) COLLATE utf8_czech_ci DEFAULT NULL,
  `ad_prize` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `ad_type` int(11) NOT NULL DEFAULT '0',
  `ad_categ` int(11) NOT NULL DEFAULT '0',
  `ad_place` int(11) NOT NULL DEFAULT '0',
  `ad_date_create` datetime NOT NULL,
  `ad_ip` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`ad_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1;

--
-- Table structure for table `advert_categ`
--

CREATE TABLE `advert_categ` (
  `adc_code` int(11) NOT NULL AUTO_INCREMENT,
  `adc_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`adc_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1;


-- --------------------------------------------------------

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fpost`
--
ALTER TABLE `fpost`
  ADD CONSTRAINT `fpost_ibfk_1` FOREIGN KEY (`f_user`) REFERENCES `user` (`u_code`),
  ADD CONSTRAINT `fpost_ibfk_2` FOREIGN KEY (`f_father`) REFERENCES `fpost` (`f_code`);

ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`u_main_addr`) REFERENCES `user_address` (`ua_code`) ON DELETE SET NULL;

ALTER TABLE `user_address`
  ADD CONSTRAINT `user_address_ibfk_1` FOREIGN KEY (`ua_user`) REFERENCES `user` (`u_code`) ON DELETE CASCADE;

ALTER TABLE `shop_item`
  ADD CONSTRAINT `si_ibfk_1` FOREIGN KEY (`si_type`) REFERENCES `s_item_type` (`sit_code`),
  ADD CONSTRAINT `si_ibfk_2` FOREIGN KEY (`si_brand`) REFERENCES `s_item_brand` (`sib_code`);

ALTER TABLE `s_item_type`
  ADD CONSTRAINT `sit_ibfk_1` FOREIGN KEY (`sit_head`) REFERENCES `s_item_type` (`sit_code`),
  ADD CONSTRAINT `sit_ibfk_2` FOREIGN KEY (`sit_brand`) REFERENCES `s_item_brand` (`sib_code`);

ALTER TABLE `s_item_row`
  ADD CONSTRAINT `sir_ibfk_1` FOREIGN KEY (`sir_head`) REFERENCES `shop_item` (`si_code`);
