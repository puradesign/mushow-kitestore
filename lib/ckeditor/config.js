/*
Copyright (c) 2003-2009, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	 // Define changes to default configuration here. For example:
	 config.language = 'cs';
	 //config.uiColor = '#323232';
	 //config.resize_enabled = false;
	 config.resize_maxWidth = 550;
	 config.resize_minWidth = 550;
	 config.width = 550;
	 config.toolbar = 'Full';
	 config.toolbar_Full =
[
    ['Source','-','NewPage','Preview','-'],
    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
    '/',
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['Link','Unlink'],
    ['Image','Flash', 'Table','HorizontalRule','SpecialChar'],
    '/',
    ['Styles','Format','Font','FontSize'],
    ['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks','-','About']
];
   
   config.startupOutlineBlocks = false;
};
