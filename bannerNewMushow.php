<?php
require_once "const.php";
?>
<html lang='cs' xml:lang='cs' xmlns='http://www.w3.org/1999/xhtml'>

<head>
  <meta http-equiv='content-type' content='text/html; charset=utf-8' />
  <meta http-equiv='content-language' content='cs' />
  <meta name='Author' content='Petr Kadlec' />

  <script type='text/javascript' src='<? echo WR; ?>script/jquery.js'></script>
  <script type='text/javascript' src='<? echo WR; ?>script/jquery.innerfade.js'></script>
  <script type='text/javascript'>
    $(document).ready(function () {
      $('#ks_banner ul').show();
      $('#ks_banner ul').innerfade({
        animationtype: 'fade',
        speed: 1200,
        timeout: 8000,
        type: 'sequence',
        containerheight: '106px'
      });
    });
  </script>
  <style type="text/css">
    img {
      border: none;
    }

    * {
      margin: 0px;
      padding: 0px;
    }
  </style>
  <title></title>
</head>

<body style='margin:0px;padding: 0px;font-family:Verdana, Geneva, Arial;'>

  <?php
  $lPath = "./";

  // pripojeni do db
  $ks_db = mysqli_connect('sql5.web4u.cz', 'mushow', 'nfyr2520', 'mushow_store');
  if (mysqli_connect_errno($ks_db)) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

  $ks_db->query("SET NAMES utf8");


  require_once $lPath . "const.php";
  require_once $lPath . "UtilFunctions.php";

  $lStyle = "width:172px;float:left; height:181px;margin:0px;";
  $lStyle .= "background-image: url(\"" . WR_IMG . "ks_banner.png\")";

  echo "<div id='ks_banner' style='$lStyle'>";

  $lTable = "shop_item LEFT JOIN s_item_type ON shop_item.si_type = s_item_type.sit_code";
  $lTable .= " LEFT JOIN s_item_brand ON si_brand = s_item_brand.sib_code";

  $query = "SELECT * FROM $lTable WHERE si_online = 1 AND si_banner=1 LIMIT 6";
  $result = $ks_db->query($query, $ks_db);

  if (!$result)
    echo "<p style='display:none'>" . $ks_db->error . "</p>";

  echo "<ul style='margin:35px 0px 0px 0px;height:146px;display:none;width:172px;padding:0px;list-style-type:none;letter-spacing:-1px;'>";

  // projdou se vsechny polozky
  while ($row = $result->fetch_assoc()) {

    $lUrl = WR . $row["sib_url"] . "/" . $row["sit_url"] . "/v/" . $row["si_url"];
    $lImgUrl = getBrowserPath(F_SHOP_ITEMS . "headfoto" . $row["si_code"] . ".jpg");
    $title = UTF8ToEntities($row["si_title"]);

    echo "<li style='padding:0px'><a href='$lUrl' target='_parent'><img src='$lImgUrl' height='106px' alt='" . $title . "' style='margin: 0px 37px' title='" . $title . "'/></a>";
    echo "<p style='text-align:center; margin-top:4px;margin-bottom:0px'><a href='$lUrl' target='_parent' style='font-size:11px;color:#270101; font-weight:bold'>" . $title . "</a></p>";

    // puvodni cena
    $lOnSale = $row["si_fake_price"] != null && $row["si_fake_price"] > 0;

    //	echo "<div style='margin-top:".($lOnSale ? "4px" : "14px").";color:black;text-align:left;font-size:9px;font-family:Verdana, Geneva, Arial;width:112px;margin-left:-3px;'>";
    //echo "<div style='color:black;text-align:left;font-size:9px;font-family:Verdana, Geneva, Arial;width:172px;margin-left:-3px;'>";
  
    $lCenaPrompt = "cena";
    echo "<p style='margin:2px 0px 0px 0px;color:black;text-align:left;font-size:9px;font-family:Verdana, Geneva, Arial;width:172px;text-align:center'>";
    if ($lOnSale) {
      echo "<span style='text-decoration:line-through;'>" . number_format($row["si_fake_price"], 0, "", " ") . " Kč</span>&nbsp;&nbsp;";
    }

    echo "<span style='font-size:12px;font-weight:bold'>" . number_format($row["si_price"], 0, "", " ") . " Kč</span></p>";

    echo "</li>";
  }

  echo "</ul>";
  ?>

</body>

</html>