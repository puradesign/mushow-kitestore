<?php
require_once "TextsObject.php";
session_start();
require_once "const.php";
require_once "UtilFunctions.php";
require_once "WebModule.php";
require_once "FileManagerWM.php";

$GLOBALS["rv"] = new ResultVect();
$GLOBALS["wm"] = new FileManagerWM(0);
$GLOBALS["pv"] = new PathVect();

if (!isLoggedAdmin())
  die("Nepovoleny pristup!");

$wm->reactOnActionLow();

?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='cs' xml:lang='cs' xmlns='http://www.w3.org/1999/xhtml'>

<head>
  <meta http-equiv='content-type' content='text/html; charset=utf-8' />
  <meta http-equiv='content-language' content='cs' />
  <meta http-equiv="cache-control" content="no-cache">

  <meta name='Author' content='Petr Kadlec' />
  <meta name='Copyright' content='Copyright 2009, Petr Kadlec' />
  <meta name='description' content='File manager' />

  <link rel='stylesheet' href='css/fm.css' />
  <script type='text/javascript' charset='utf-8' src='script/fm.js'></script>

  <title><?php $wm->getHeader(); ?></title>
</head>

<body>
  <div id='wrapper'>
    <?php $wm->display(); ?>
  </div>
</body>

</html>