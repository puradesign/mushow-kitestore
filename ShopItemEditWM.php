<?php
require_once F_HTML . "ItemRow.php";

class ShopItemEditWM extends WebModule {
  var $mAction;
  var $mHeader = "Zboží";
  var $mActRow;
  var $mItemRows = array();

  /* -------------------------- BEFORE ACTION -------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    // kontroly
    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    if (!isset($_GET["action"]))
      $this->mAction = "add";
    else
      $this->mAction = $_GET["action"];

    if (($this->mAction != "add" && $this->mAction != "edit") ||
      ($this->mAction == "edit" && (!isset($_GET["item"]) || !is_numeric($_GET["item"])))) {
      $GLOBALS["rv"]->addError("Chyba. Pravděpodobně byla ručně upravena url stránky.");
      $this->setForOutput(false);
      return false;
    }

    // ----------------------- Akce PRIDAT - naplneni-------------------------//
    // -------------------------------------------------------==--------------//
    // naplneni radku pro akci pridat
    if ($this->mAction == "add") {
      $this->mItemRows = array();

      if (empty($_POST)) {
        $_POST["items_count"] = 1;

        $this->mItemRows[] = new ItemRow(0);
      } else {
        for ($i = 0; $i < $_POST["items_count"]; $i++)
          $this->mItemRows[] = new ItemRow($i);
      }
    }

    // ----------------------- Akce EDITACE ----------------------------------//
    // -------------------------------------------------------==--------------//
    // pridani radku
    if (isset($_GET["onchange"]) && $_GET["onchange"] == "add_row") {
      $_POST["items_count"]++;
      $_POST["sir_name" . ($_POST["items_count"] - 1)] = "";
      $_POST["sir_count" . ($_POST["items_count"] - 1)] = "0";
    }

    if ($this->mAction == "edit") {
      $query = "SELECT * FROM shop_item WHERE si_code=" . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);

      $row = $result->fetch_assoc();
      $this->mActRow = $row;
      $this->mHeader = alterHtmlTextToPlain($row["si_title"]);

      if (empty($_POST)) {
        $_POST["si_title"] = $this->mHeader;
        $_POST["si_descr"] = alterHtmlTextToPlain($row["si_descr"]);
        $_POST["si_brand"] = $row["si_brand"];
        $_POST["si_type"] = $row["si_type"];
        $_POST["si_price"] = $row["si_price"];
        $_POST["si_fake_price"] = $row["si_fake_price"];
        $_POST["si_text"] = $row["si_text"];

        if ($row["si_hot"] == 1)
          $_POST["si_hot"] = "1";

        if ($row["si_online"] == 1)
          $_POST["si_online"] = "1";

        if ($row["si_onsale"] == 1)
          $_POST["si_onsale"] = "1";

        if ($row["si_banner"] == 1)
          $_POST["si_banner"] = "1";

        $query = "SELECT * FROM s_item_row WHERE sir_head=" . $_GET["item"] . " AND sir_visible=1";
        $result = $GLOBALS["db"]->query($query);

        // naplneni moznosti
        $lOrder = 0;
        $this->mItemRows = array();
        while ($lItemRow = $result->fetch_assoc()) {
          $_POST["sir_name" . $lOrder] = alterHtmlTextToPlain($lItemRow["sir_name"]);
          $_POST["sir_price" . $lOrder] = $lItemRow["sir_price"];
          $_POST["sir_count" . $lOrder] = $lItemRow["sir_count"];

          $this->mItemRows[$lOrder] = new ItemRow($lOrder, $lItemRow["sir_code"]);

          $lOrder++;
        }

        $_POST["items_count"] = $lOrder;
      }

      // vytvoreni objektu radku
      else {
        $query = "SELECT * FROM s_item_row WHERE sir_head=" . $_GET["item"] . " AND sir_visible=1";
        $result = $GLOBALS["db"]->query($query);

        // nacteme nejdriv vety z db
        $this->mItemRows = array();
        $lOrder = 0;
        while ($lItemRow = $result->fetch_assoc()) {
          $this->mItemRows[$lOrder] = new ItemRow($lOrder, $lItemRow["sir_code"]);

          if (!isset($_POST["sir_name" . $lOrder]))
            $this->mItemRows[$lOrder]->setForDelete();

          $lOrder++;
        }

        // pripojime nove pridane
        for ($i = $lOrder; $i < $_POST["items_count"]; $i++) {
          $this->mItemRows[$i] = new ItemRow($i);

          if (!isset($_POST["sir_name" . $i]))
            $this->mItemRows[$i]->setForDelete();
        }
      }

      // smazani radku
      if (isset($_GET["onchange"]) && $_GET["onchange"] == "delete_row") {
        $this->mItemRows[$_GET["del_order"]]->setForDelete();
      }
    }

    return true;
  }

  /* -------------------------- PROCESS ACTION ------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    $name = $_POST["si_title"];
    $descr = $_POST["si_descr"];
    $brand = $_POST["si_brand"];
    $type = $_POST["si_type"];
    $price = $_POST["si_price"];
    $fake_price = $_POST["si_fake_price"] == "" ? "NULL" : $_POST["si_fake_price"];
    $text = $_POST["si_text"];
    $hot = isset($_POST["si_hot"]) ? 1 : 0;
    $online = isset($_POST["si_online"]) ? 1 : 0;
    $onsale = isset($_POST["si_onsale"]) ? 1 : 0;
    $banner = isset($_POST["si_banner"]) ? 1 : 0;

    // upraveni odkazu na obrazky aby se otviraly v shadowboxu
    $text = addShadowboxToLinks($text);
    $text = addSlashes($text);

    $lCond = new WhereCondition();

    // -------------------------------------------------------==--------------//
    // vlozeni
    if ($this->mAction == "add") {

      //$lCond->addCondStr("sit_brand = $brand");
      $url = createUrlTitle($name, "si_url", "shop_item");

      // vlozeni do DB
      $query = "INSERT INTO shop_item (`si_title`, `si_descr`, `si_brand`, `si_type`, `si_price`, `si_fake_price`, `si_text`, `si_url`, `si_hot`, `si_hot_date`, `si_online`, `si_onsale`, `si_banner`)";
      $query .= " VALUES ('" . alterTextForDB($name) . "', ";
      $query .= "'" . alterTextForDB($descr) . "', ";
      $query .= "$brand, $type, $price, $fake_price, '$text', '$url', $hot, NOW(), $online, $onsale, $banner)";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      $lItemCode = $GLOBALS["db"]->insert_id;

      // ulozeni obrazku
      $image = new ImageUploaded($_FILES["si_foto"]);
      $image->loadFile();

      $image->cropToSize(170, 170);
      $pic_name = $image->save(F_SHOP_ITEMS . "headfoto" . $lItemCode . ".jpg", IMAGETYPE_JPEG);

      // ulozeni radku
      $lRowsCount = (int) $_POST["items_count"];
      for ($i = 0; $i < $lRowsCount; $i++) {
        $lRowName = $_POST["sir_name" . $i];
        $lRowPrice = $_POST["sir_price" . $i];
        $lRowCount = $_POST["sir_count" . $i];

        // vlozeni do DB
        $query = "INSERT INTO s_item_row (`sir_name`, `sir_price`, `sir_count`, `sir_head`)";
        $query .= " VALUES ('" . alterTextForDB($lRowName) . "', ";
        $query .= "$lRowPrice, $lRowCount, $lItemCode)";
        $result = $GLOBALS["db"]->query($query);

        if (!$result) {
          $GLOBALS["rv"]->addError("Chyba v SQL při přidávání řádku:" . $result->error);
        }
      }

      // presmerovani na prehled zbozi
      require_once "ShopEditWM.php";
      $GLOBALS["wm"] = new ShopEditWM(SHOP_EDIT);
      $GLOBALS["wm"]->reactOnActionLow();

      return false;
    }

    // ----------------------------------- EDITACE -------------==--------------//
    // potvrzeni zeditovanych hodnot
    elseif ($this->mAction == "edit") {
      // vytvoreni url pokud se zmenil titulek
      $lUrl = null;
      if ($name != $this->mActRow["si_title"]) {
        $lCond->addCondStr("si_code != " . $this->mActRow["si_code"]);
        $lUrl = createUrlTitle($name, "si_url", "shop_item", $lCond);
      }

      // update v DB
      $query = "UPDATE shop_item SET `si_title` = '" . alterTextForDB($name) . "'";
      $query .= ", `si_descr` = '" . alterTextForDB($descr) . "'";
      $query .= ", `si_brand` = $brand, `si_type` = $type, `si_price` = $price, `si_fake_price` = $fake_price";
      $query .= ", `si_text` = '$text'";
      $query .= ", `si_online` = '$online'";
      $query .= ", `si_onsale` = '$onsale'";
      $query .= ", `si_hot` = '$hot'";
      $query .= ", `si_banner` = '$banner'";

      if ($this->mActRow["si_hot"] == 0 && $hot == 1)
        $query .= ", `si_hot_date` = NOW()";

      if (isset($lUrl) && $lUrl != null)
        $query .= ", `si_url` = '$lUrl'";

      $query .= " WHERE `si_code` = " . $this->mActRow["si_code"];
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      // ulozeni obrazku
      if ($_FILES["si_foto"]["name"] != "") {
        $image = new ImageUploaded($_FILES["si_foto"]);
        $image->loadFile();

        $image->cropToSize(170, 170);
        $pic_name = $image->save(F_SHOP_ITEMS . "headfoto" . $this->mActRow["si_code"] . ".jpg", IMAGETYPE_JPEG);
      }

      // pridani/update/delete variant
      for ($i = 0; $i < count($this->mItemRows); $i++) {
        $lOneRow = $this->mItemRows[$i];

        // nove pridana a zaroven smazana
        if ($lOneRow->getRowCode() == null && $lOneRow->isForDelete())
          continue;

        // update existujici vety
        elseif ($lOneRow->getRowCode() != null && !$lOneRow->isForDelete()) {
          $lRowName = $_POST["sir_name" . $i];
          $lRowPrice = $_POST["sir_price" . $i];
          $lRowCount = $_POST["sir_count" . $i];

          $query = "UPDATE s_item_row SET `sir_name` = '" . alterTextForDB($lRowName) . "'";
          $query .= ", `sir_price` = $lRowPrice, `sir_count` = $lRowCount";
          $query .= " WHERE `sir_code` = " . $lOneRow->getRowCode();
          $result = $GLOBALS["db"]->query($query);

          if (!$result)
            $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        }

        // pridani nove vety
        elseif ($lOneRow->getRowCode() == null && !$lOneRow->isForDelete()) {
          $lRowName = $_POST["sir_name" . $i];
          $lRowPrice = $_POST["sir_price" . $i];
          $lRowCount = $_POST["sir_count" . $i];

          // vlozeni do DB
          $query = "INSERT INTO s_item_row (`sir_name`, `sir_price`, `sir_count`, `sir_head`)";
          $query .= " VALUES ('" . alterTextForDB($lRowName) . "', ";
          $query .= "$lRowPrice, $lRowCount, " . $this->mActRow["si_code"] . ")";
          $result = $GLOBALS["db"]->query($query);

          if (!$result)
            $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        }

        // smazani vety
        elseif ($lOneRow->getRowCode() != null && $lOneRow->isForDelete()) {

          // skryti v DB
          $query = "UPDATE s_item_row SET `sir_visible` = 0";
          $query .= " WHERE `sir_code` = " . $lOneRow->getRowCode();
          $result = $GLOBALS["db"]->query($query);

          if (!$result)
            $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        }

      }

      // presmerovani na prehled zbozi
      require_once "ShopEditWM.php";
      $GLOBALS["wm"] = new ShopEditWM(SHOP_EDIT);
      $GLOBALS["wm"]->reactOnActionLow();

      return false;
    }

    return true;
  }

  /* ------------------------------- HEADER ---------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    $str = $this->mHeader . " - ";

    if ($this->mAction == "add")
      $str .= "Přidat";
    elseif ($this->mAction == "edit")
      $str .= "Editace";

    return $str;
  }


  /* -------------------------- DEFINE ELEMENTS  ----------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $whereCond = new WhereCondition();
    $lOffset = 100;

    // Nazev
    $lEF = new EditField("si_title", "Název", $lOffset, true,
      330, 50);
    $this->addElement($lEF);

    // Popis
    $lEF = new EditText("si_descr", "Popis", $lOffset, true,
      46, 330, 5);
    $this->addElement($lEF);

    // Znacka
    $lEF = new EditCodeCombo("si_brand", "Značka", $lOffset, true,
      150, "s_item_brand", "sib_code", "sib_name");
    $lEF->addFieldAttr("onChange", "submitOnChange(\"edit_form\");");
    $lEF->initOptions();
    $this->addElement($lEF);

    // Kategorie
    $lEF = new EditCodeCombo("si_type", "Kategorie", $lOffset, true,
      150, "s_item_type", "sit_code", "sit_name");
    $lEF->setAttrForStructure("sit_id");

    if (isset($_POST["si_brand"]) && $_POST["si_brand"] > 0) {
      $whereCond->addCondStr("sit_brand = " . $_POST["si_brand"]);
    } else
      $lEF->addFieldAttr("disabled", "disabled");

    $lEF->setCond($whereCond);
    $lEF->setOrderBy("sit_id ASC");
    $lEF->initOptions();
    $this->addElement($lEF);

    // Foto
    $lEF = new EditFile("si_foto", "Head foto", $lOffset, $this->mAction == "add");
    $this->addElement($lEF);

    // Cena
    $lEF = new EditInt("si_price", "Cena", $lOffset, true,
      50, 6, false);
    $lEF->setSuffix("<span class='note'>(orientační, bude zobrazena v přehledu)</span>");
    $this->addElement($lEF);

    // Puvodni cena
    $lEF = new EditInt("si_fake_price", "Původní cena", $lOffset, false,
      50, 6, false);
    $this->addElement($lEF);

    // Hot stuff
    $lEF = new EditBool("si_hot", "Hot stuff", $lOffset);
    $this->addElement($lEF);

    // Akce
    $lEF = new EditBool("si_onsale", "Akce", $lOffset);
    $this->addElement($lEF);

    // banner
    $lEF = new EditBool("si_banner", "Mushow banner", $lOffset);
    $this->addElement($lEF);

    // online
    $lEF = new EditBool("si_online", "Online", $lOffset);
    $this->addElement($lEF);

    // Text
    $lEF = new EditCKText("si_text", "Text", true);
    $this->addElement($lEF);

    // Varianty
    $lEF = new EditInt("items_count", "Počet variant", $lOffset, false,
      30, 2, false);
    if ($this->mAction == "add") {
      $lButtStr = "<input type='button' value='>' onclick='if (validateNumber(document.getElementById(\"items_count\"),false)) {submitOnChange(\"edit_form\");}return false;'/>";
      $lEF->setSuffix($lButtStr);
    } else
      $lEF->setHidden();
    $this->addElement($lEF);

    // vypis moznosti
    $lRowCount = count($this->mItemRows);
    if ($lRowCount > 0) {
      $lEF = new PresentElement("<div class='td_line'></div><h3>Varianty, velikosti</h3>");
      $this->addElement($lEF);

      for ($i = 0; $i < $lRowCount; $i++) {
        // smazane se preskoci
        if (!$this->mItemRows[$i]->isForShow())
          continue;

        // Nazev
        $lEF = new EditField("sir_name" . $i, "Název", 60, true, 100, 50);
        $lEF->setGapWidth(0);
        $lEF->setWidthPX(200);
        $this->addElement($lEF);

        // Cena
        $lEF = new EditInt("sir_price" . $i, "Cena", 60, true, 70, 10, true);
        $lEF->setGapWidth(0);
        $lEF->setWidthPX(160);
        $lEF->setSuffix("Kč");
        $this->addElement($lEF);

        // Kusu
        $lEF = new EditInt("sir_count" . $i, "Kusů", 60, true, 30, 3, false);
        $lEF->setGapWidth(0);
        $lEF->setWidthPX(140);
        if ($this->mAction == "edit") {
          $lDeleteLink = "&nbsp;&nbsp;<a href='' style='padding:3px;background-color:#dddddd;' onclick='";
          $lDeleteLink .= "if(confirm(\"Fakt chceš smazat tuhle variantu?\"))";
          $lDeleteLink .= "submitOnChange(\"edit_form\", \"delete_row&amp;del_order=$i\");return false;'";
          $lDeleteLink .= "><b>&nbsp;x&nbsp;</b></a>";
          $lEF->setSuffix($lDeleteLink);
        }
        $this->addElement($lEF);
      }
    }

    // pridani noveho radku
    if ($this->mAction == "edit") {
      $lEF = new PresentElement("<a href='' onclick='submitOnChange(\"edit_form\", \"add_row\");return false;'><b>+</b>&nbsp;Přidat novou variantu..</a><div class='td_line'></div>");
      $this->addElement($lEF);
    }
  }

  /* -------------------------------- HTML ----------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {

    echo "  <form method='post' id='edit_form' action='" . WR . "?m=" . SI_EDIT . "&amp;action=" . $this->mAction;
    echo ($this->mAction == "edit" ? "&amp;item=" . $_GET["item"] : "") . "' enctype='multipart/form-data'>";
    echo "  <fieldset class='form'>";

    $this->printElements();

    echo "  <div class='td_left' style='height:100%;width: 50px;'>";
    echo "  <input type='submit' class='submit' value='Uložit'/></div>";

    echo "  </fieldset></form>";
  }

  /* ---------------------------------- SCRIPTS -----------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {
    echo "<script type='text/javascript' src='" . F_CKEDITOR . "ckeditor.js'></script>\n";
    echo "<script type='text/javascript' src='" . WR_SCRIPT . "ckeditor_conf.js'></script>\n";
    echo "<script type='text/javascript'>\n";
    ?>
    $(document).ready(function() {
    CKEDITOR.config.customConfig = '<?php echo WR_SCRIPT; ?>ckeditor_conf.js';
    CKEDITOR.replace('si_text');
    });</script>
    <?php
  }
}
?>