<?php
require_once "ItemsBrowser.php";

class WebModule {
  var $isForOutput = true;
  var $wmID;
  var $mElements = null;

  /**
   * Konstruktor
   */
  function WebModule($wmID) {
    $this->wmID = $wmID;
  }

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   * Vraci se priznak, jestli se zustava v tomto webmodulu i pro vykresleni
   */
  function beforeAction() {
    $GLOBALS["rv"]->addError("Requested page was not found.");

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {

    return true;
  }

  /**
   * Reaguje na akci vyvolanou uzivatelem
   */
  function reactOnActionLow() {
    $GLOBALS["pv"]->clear();

    // odstrani lomitka pridane magic_quotes_gpc
    undoSlashes();

    $lIsForProcess = !empty($_POST);

    // volano pred kazdou akci
    $lOk = $this->beforeAction();
    if (!$this->isForOutput)
      return false;

    $this->mElements = array();
    $this->defineElements();

    // zpracovani vykonne akce
    if ($lIsForProcess && $lOk && $GLOBALS["rv"]->isOk() && !isset($_GET["onchange"])) {

      // validace elementu
      if (!$this->validateElements()) {
        $GLOBALS["rv"]->addError("Některá z povinných položek formuláře není vyplněna, nebo má nesprávný tvar.");
      } else {
        $lOk = $this->processAction() && $lOk;
      }
    }

    if ($lOk && $this->isForOutput)
      $this->definePathVect();
  }

  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Error";
  }

  function defineHeaderButton() {
  }

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
  }

  /**
   * Vytiskne pripadne zpravy z rvu
   */
  final function printResults() {
    // vypis pripadnych chyb ci info
    @$GLOBALS["rv"]->printHtmlOutput();
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
  }

  /**
   * Vykresli obsah
   */
  function display() {
    // hlavicka
    echo "<h1>" . $this->getHeader() . "</h1>\n";

    // pripadne buttonky v hlavicce
    if ($this->isForOutput)
      $this->defineHeaderButton();

    // chyby a info
    $this->printResults();

    //vlastni obsah
    if ($this->isForOutput) {
      $this->defineHtmlOutput();
    }
  }

  /**
   * nastaveni priznaku k zobrazeni vlastniho obsahu
   */
  function setForOutput($value) {
    $this->isForOutput = $value;
  }

  /**
   * Zde naplneni vektoru cesty - pro prepsani
   */
  function definePathVect() {
    $GLOBALS["pv"]->addItem("", $this->getHeader());
  }

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {
  }

  /**
   * Pro prepsani - vraci ID polozky v menu, ktera patri k tomuto WM (podle menu konstant)
   */
  function getMenuItemID() {
    return MENU_NONE;
  }

  /**
   * Vraci string GET parametru pro odkazy (napr. komentaru)
   */
  function getGETparamsStr($withPage = true, $firstAmper = true) {
    $str = "";
    $params = $this->getImportantParamsNames();

    foreach ($params as $pom => $name) {
      if (isset($_GET[$name]))
        $str .= ($str == "" && !$firstAmper ? "" : "&amp;") . $name . "=" . $_GET[$name];
    }

    if ($withPage && isset($_GET["page"]))
      $str .= ($str == "" && !$firstAmper ? "" : "&amp;") . "page=" . $_GET["page"];

    return $str;
  }

  /**
   * Vraci nazvy GET parametru, ktere se maji zachovavat - pro prepsani
   */
  function getImportantParamsNames() {
    return array();
  }

  /**
   * Vraci popis do description v hlavicce
   */
  function getMetaDescription() {
    $str = "Mushow Kite Store - vybavení na kitesurfing, snowkiting a wakeboarding od nejznámějších značek jako Slingshot, Mushow, Ozone, Oakley a dalších";

    return $str;
  }

  /**
   * Prida element
   */
  function addElement($aElem) {
    if ($aElem == null || !($aElem instanceof PresentElement))
      return false;

    if ($this->mElements == null)
      $this->mElements = array();

    $this->mElements[] = $aElem;
  }

  /**
   * Vypise elementy
   */
  function printElements() {
    if ($this->mElements == null || empty($this->mElements))
      return false;

    for ($i = 0; $i < count($this->mElements); $i++) {
      $this->mElements[$i]->toHtmlLow();
    }
  }

  /**
   * Validace elementu
   */
  function validateElements() {
    if ($this->mElements == null || empty($this->mElements))
      return true;

    $valid = true;

    for ($i = 0; $i < count($this->mElements); $i++) {
      $lItem = $this->mElements[$i];

      if ($lItem instanceof EditField && !$lItem->validateField())
        $valid = false;
    }

    return $valid;
  }

  /**
   * Vrati element
   */
  function getFormElement($aName) {
    if ($this->mElements == null || empty($this->mElements))
      return null;

    for ($i = 0; $i < count($this->mElements); $i++) {
      $lItem = $this->mElements[$i]->getElement($aName);

      if ($lItem != null)
        return $lItem;
    }

    return null;
  }
}
?>