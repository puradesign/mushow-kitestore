<?php

function alterTextForDB($text) {
  if (empty($text))
    return $text;

  $text = trim($text);

  // prevod znaku pro html entity
  $text = htmlspecialchars($text);

  $text = Str_Replace("\n", "<br />", $text);

  $text = addslashes($text);

  return $text;
}

function alterHtmlTextToPlain($text) {
  if (empty($text))
    return $text;

  $text = Str_Replace("<br />", "\n", $text);
  $text = Str_Replace("<br>", "\n", $text);

  // prevod znaku pro html entity
  $text = htmlspecialchars_decode($text);

  return $text;
}

function removeDirR($dirname) {
  if (is_dir($dirname))
    $dir_handle = opendir($dirname);
  else
    return false;

  if (!$dir_handle)
    return false;

  while ($file = readdir($dir_handle)) {
    if ($file != "." && $file != "..") {
      if (!is_dir($dirname . "/" . $file))
        unlink($dirname . "/" . $file);
      else
        removeDirR($dirname . '/' . $file);
    }
  }

  closedir($dir_handle);
  rmdir($dirname);
  return true;
}

function getBrowserPath($text) {
  if (empty($text))
    return $text;

  $text = Str_Replace(F_ROOT, "", $text);
  $text = Str_Replace("\\", "/", $text);

  return WR_PLAIN . $text;
}

function getArticlePath($aid, $uid) {
  return F_RIDERS . $uid . DIRECTORY_SEPARATOR . F_ARTICLES . $aid . DIRECTORY_SEPARATOR;
}

function hasValidChars($str) {
  return preg_match("/^[0-9a-zA-Z_\-]+$/", $str);
}

/**
 * Anti spam polozka 1
 */
function printSpamCheck1() {
  ?>
  <div class='mandatory'>
    <input type='text' name='name1' maxlength='50' size='25' onblur="this.value='default'" />
  </div>
  <?php
}

/**
 * Anti spam polozka 2
 */
function printSpamCheck2() {
  echo "<div class='mandatory'>";
  echo "<input type='text' name='email1' id='email1' maxlength='50' size='25' ";
  echo "value='" . @$_POST["email1"] . "' onblur=\"this.value=''\"/>";
  echo "</div>";
}

/**
 * Anti spam captcha improved
 */
function addCaptcha() {
  require_once 'lib/captcha/captcha.class.php';
  require_once 'lib/captcha/captcha_http.class.php';

  // vytvorit objekt
  $captcha = new CaptchaHTTP("captcha.seznam.cz", 80);

  // vytvorit novou captchu
  try {
    $hash = $captcha->create();
  } catch (Exception $e) {
    echo "<p><b>Captcha wasn't created</b></p>";
    echo "<pre>" . $e->__toString() . "</pre>";
    exit;
  }

  $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $first = $chars[mt_rand(0, strlen($chars) - 1)];
  $sec = $chars[mt_rand(0, strlen($chars) - 1)];

  $twohash = md5($first . $sec);

  echo "<div class='td_left' style='width:180px'>";
  echo "  <span class='cap_part' style=''>$first</span>";
  echo "	<span class='cap_part' style='right:-185px; top:35px;'>$sec</span>";
  echo "</div>";
  echo "<div class='td_right' style='width: 300px; height:100%'>";
  echo "	<img src='" . $captcha->getImage($hash) . "' style='z-index: 9;' alt='test'/>";
  echo "</div>";

  echo "<input type='hidden' name='imgh' value='$hash'/>";
  echo "<input type='hidden' name='twohash' value='$twohash'/>";
  echo "<div class='td_left' style='width:180px'><span class='err'>* </span>" . getRText("util99") . ": </div>"; // Opište prosím kód z obrázku
  echo "<div class='td_right' style='width: 300px;'>";
  echo "	<input type='text' id='code' name='code' size='8'/>";
  echo "</div>";
}

/**
 * Kontrola spamu
 */
function doSpamCheck($inputName, $checkCaptcha = false) {
  $valid = true;

  // my check
  if ($_POST["name1"] != "") {
    spamRecognized($_POST[$inputName] . " filled empty.");
    $valid = false;
  }

  if ($_POST[$inputName] != "" && $_POST["email1"] != "hehe" . $_POST[$inputName] . "haha") {
    $valid = false;
    spamRecognized($_POST[$inputName] . " doesnt use JS");
  }

  if (!$valid) {
    $GLOBALS["rv"]->addError(getRText("err39")); // "Formulář nebyl zpracován, protože vás systém považuje za spamovacího robota, pokud potíže přetrvají obraťte prosím na správce těchto stránek."
    return false;
  }

  // captcha check
  if ($checkCaptcha) {
    require_once 'lib/captcha/captcha.class.php';
    require_once 'lib/captcha/captcha_http.class.php';

    $code = $_POST['code'];

    // kontrola delky kodu
    if (strlen($code) != 7) {
      $GLOBALS["rv"]->addError(getRText("err40")); // "Nebyl správně opsán kód z obrázku, zkuste to prosím znovu."
      spamRecognized($_POST[$inputName] . " short captcha code.");
      return false;
    }

    // kontrola prvniho a posledniho pismene(nepatri do captchy)
    $twohash = md5($code[0] . $code[6]);
    if ($twohash != $_POST["twohash"]) {
      $GLOBALS["rv"]->addError(getRText("err40")); // "Nebyl správně opsán kód z obrázku, zkuste to prosím znovu."
      spamRecognized($_POST[$inputName] . " different two captcha chars.");
      return false;
    }

    $code = substr($code, 1, 5);

    // vytvorit objekt
    $captcha = new CaptchaHTTP("captcha.seznam.cz", 80);

    // vytvorit novou captchu
    try {
      $valid = $captcha->check($_POST['imgh'], $code);
    } catch (Exception $e) {
      $GLOBALS["rv"]->addError("Captcha couldn't been verified: " . '<pre>' . $e->__toString() . '</pre>');
      spamRecognized($_POST[$inputName] . " captcha cant be verified.");
      return false;
    }

    if (!$valid) {
      $GLOBALS["rv"]->addError(getRText("err40")); // "Nebyl správně opsán kód z obrázku, zkuste to prosím znovu."
      spamRecognized($_POST[$inputName] . " bad captcha code.");
      return false;
    }
  }

  return $valid;
}

/**
 * Vraci prvnich $length znaku retezce, bez prech.na novy radek a nasledovany tremi teckami
 */
function getFirstChars($text, $length) {
  $text = Str_Replace("<br />", " ", $text);
  $text = Str_Replace("<br>", " ", $text);
  mb_internal_encoding("UTF-8");

  if (mb_strlen($text) < $length)
    return $text;
  else
    return mb_substr($text, 0, $length) . "...";
}

function getUserIdent($u_code) {
  $db = $GLOBALS["db"];

  $query = "SELECT u_nick FROM user WHERE u_code =" . $u_code;
  $result = $GLOBALS["db"]->query($query);
  $row = $result->fetch_assoc();

  if (!$row)
    return "";
  else
    return $row["u_nick"];
}

function addShareButton() {

  echo "<div class='add_this'>";
  echo "  <script src='http://connect.facebook.net/cs_CZ/all.js#xfbml=1'></script><fb:like show-faces='false'></fb:like>";
  echo "</div>";
  /*
    echo "<div class='add_this'>";
    echo '<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=terihei"><img src="'.WR_IMG.'share.jpg" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=terihei"></script>';
    echo "</div>";*/

}


/**
 * Upravi hodnoty v $_POST v zavislosti na konfiguraci
 */
function undoSlashes() {
  if (!(function_exists('get_magic_quotes_gpc') && 1 == get_magic_quotes_gpc()))
    return;

  if (empty($_POST))
    return;

  $mqs = strtolower(ini_get('magic_quotes_sybase'));

  $arr = $_POST;
  foreach ($arr as $key => $val) {
    if (!is_string($val))
      continue;

    if (empty($mqs) || 'off' == $mqs) {
      $_POST[$key] = stripslashes($val);
    } else {
      $_POST[$key] = str_replace("''", "'", $val);
    }
  }
}

/**
 * Odkaz pro smazani prispevku
 */
function printDeleteLink($code, $userCode) {
  if (!isLogged())
    return;

  if ($_SESSION[SN_CODE] == 1 || isLoggedUser($userCode)) {
    echo " | <a href='" . WR_FOR . "?post=" . $code . "&amp;delete' ";
    echo "onClick='return confirm(\"" . getRText("util68") . "\")'>"; // Opravdu si přejete smazat příspěvek?
    echo getRText("util23") . "</a>"; // Smazat
  }
}

/**
 * Odkaz pro smazani prispevku
 */
function printCommentDeleteLink($code, $userCode, $refStr, $type) {
  if (!isLogged())
    return;

  if ($_SESSION[SN_CODE] == 1 || isLoggedUser($userCode)) {
    echo " | <a href='" . $refStr . "?post=" . $code . "&amp;caction=delete' ";
    echo "onClick='return confirm(\"" . getRText("util68") . "\")'>"; // Opravdu si přejete smazat příspěvek?
    echo getRText("util23") . "</a>"; // Smazat
  }
}

/**
 * Je uzivatel prihlasen?
 */
function isLogged() {
  return @$_SESSION[SN_LOGGED] && isset($_SESSION[SN_CODE]);
}

/**
 * Je specificky uzivatel prihlasen?
 */
function isLoggedUser($code) {
  return isLogged() && $_SESSION[SN_CODE] == $code;
}

/**
 * Je prihlasen admin?
 */
function isLoggedAdmin() {
  return isLogged() && $_SESSION[SN_ADMIN];
}

/**
 * Prevedeni na UTF-8
 */
function getUTF($s) {
  // detect UTF-8
  if (preg_match('#[\x80-\x{1FF}\x{2000}-\x{3FFF}]#u', $s))
    return $s;

  // detect WINDOWS-1250
  if (preg_match('#[\x7F-\x9F\xBC]#', $s))
    return iconv('WINDOWS-1250', 'UTF-8', $s);

  // assume ISO-8859-2
  return iconv('ISO-8859-2', 'UTF-8', $s);
}

/**
 * Zapis rozpoznaneho spamu do souboru
 */
function spamRecognized($message) {
  $content = "";
  $fn = "antispam.txt";

  if (file_exists($fn)) {
    $fp = fopen($fn, "r");
    if ($fp) {
      $content = fread($fp, filesize("antispam.txt")) . "\n";
      fclose($fp);
    }
  }

  $fp = fopen("antispam.txt", "w");
  if ($fp) {
    fwrite($fp, $content . StrFTime("%d/%m/%Y %H:%M:%S", Time()) . " - " . $message);
    fclose($fp);
  }
}

/**
 * Zapis rozpoznaneho spamu do souboru
 */
function getIP() {
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  //to check ip is pass from proxy
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    $ip = $_SERVER['REMOTE_ADDR'];
  }

  return $ip;
}

/**
 * Hlasovani do ankety
 */
function doPollVote() {
  $poliCode = $_POST["poll_item"];

  if (isset($_COOKIE["poll" . $_GET["poll"]])) {
    $GLOBALS["rv"]->addError("Už jste hlasovali v této anketě.");
  } else {

    $query = "UPDATE poll_item SET poli_votes = poli_votes + 1 WHERE poli_code = $poliCode";
    $GLOBALS["db"]->query($query);

    setcookie("poll" . $_GET["poll"], $poliCode);
    $_COOKIE["poll" . $_GET["poll"]] = $poliCode;
  }

  unset($_POST);
}

/**
 * Vraci vek
 */
function getAge($dob, $tdate) {
  $age = 0;
  while (strtotime('-' . $age . ' year') > strtotime($dob)) {
    ++$age;
  }
  return $age - 1;
}

/**
 * Zmena jazyku
 */
function changeLanguage($langStr = "en") {

  if ($langStr == LANG_EN) {
    $_SESSION[SN_LANG] = 2;
    //  setlocale(LC_ALL, 'en_US');
  } elseif ($langStr == LANG_CS) {
    $_SESSION[SN_LANG] = 3;
    //setlocale(LC_ALL, 'cs_CS');
  } elseif ($langStr == LANG_FR) {
    $_SESSION[SN_LANG] = 5;
    // setlocale(LC_ALL, 'fr_FR');
  }
  // $GLOBALS["rv"]->addInfo( "lang changed to ".$langStr." - ".$_SESSION[SN_LANG] );
  // pokud neni vytvoren
  if (!isset($_SESSION[SN_TEXTS]))
    $_SESSION[SN_TEXTS] = new TextsObject();

  $_SESSION[SN_TEXTS]->initTexts($_SESSION[SN_LANG]);

  // do cookies
  $_COOKIE["lang"] = $langStr;
  setcookie("lang", $langStr, time() + 500000000, "/");
}

/**
 * Zmena jazyku
 */
function getContentLanguage() {
  switch ($_SESSION[SN_LANG]) {
    case 2:
      return LANG_EN;
    case 3:
      return LANG_CS;
    case 5:
      return LANG_FR;
  }
}

/**
 * Vrat text
 */
function getRText($id) {

  // pokud neni vytvoren
  if (!isset($_SESSION[SN_TEXTS])) {
    $_SESSION[SN_TEXTS] = new TextsObject();
    $_SESSION[SN_TEXTS]->initTexts($_SESSION[SN_LANG]);
  }

  return $_SESSION[SN_TEXTS]->getText($id);
}

/**
 * Prevede text na text vhodny do url
 */
function createUrlTitle($title, $urlAttr, $aTable, $cond = "") {

  // prevod na UTF-8
  $title = getUTF($title);

  $title = strtolower($title);

  // odstraneni diakritiky
  $tbl = array("\xc3\xa1" => "a", "\xc3\xa4" => "a", "\xc4\x8d" => "c", "\xc4\x8f" => "d", "\xc3\xa9" => "e", "\xc4\x9b" => "e", "\xc3\xad" => "i", "\xc4\xbe" => "l", "\xc4\xba" => "l", "\xc5\x88" => "n", "\xc3\xb3" => "o", "\xc3\xb6" => "o", "\xc5\x91" => "o", "\xc3\xb4" => "o", "\xc5\x99" => "r", "\xc5\x95" => "r", "\xc5\xa1" => "s", "\xc5\xa5" => "t", "\xc3\xba" => "u", "\xc5\xaf" => "u", "\xc3\xbc" => "u", "\xc5\xb1" => "u", "\xc3\xbd" => "y", "\xc5\xbe" => "z", "\xc3\x81" => "A", "\xc3\x84" => "A", "\xc4\x8c" => "C", "\xc4\x8e" => "D", "\xc3\x89" => "E", "\xc4\x9a" => "E", "\xc3\x8d" => "I", "\xc4\xbd" => "L", "\xc4\xb9" => "L", "\xc5\x87" => "N", "\xc3\x93" => "O", "\xc3\x96" => "O", "\xc5\x90" => "O", "\xc3\x94" => "O", "\xc5\x98" => "R", "\xc5\x94" => "R", "\xc5\xa0" => "S", "\xc5\xa4" => "T", "\xc3\x9a" => "U", "\xc5\xae" => "U", "\xc3\x9c" => "U", "\xc5\xb0" => "U", "\xc3\x9d" => "Y", "\xc5\xbd" => "Z");
  $title = strtr($title, $tbl);

  // odstraneni nezadoucich znaku mezer atd.
  $title = preg_replace("/[^a-zA-Z0-9]+/", '-', $title);

  // melo by zrusit pomlcky na zacatku
  $title = preg_replace("/^[-]+(.*)$/", '$1', $title);

  if (strlen($title) > 49) {
    $title = substr($title, 0, 49);

    // melo by zrusit pomlcky na konci
    $title = preg_replace("/^(.*)[-]+$/", '$1', $title);
  }

  // kontrola jestli takova url uz neexistuje
  $num = 0;
  $newTitle = $title;
  $lCondStr = $cond != "" ? $cond->getTotalCond() : "";
  while (true) {
    $query = "SELECT $urlAttr FROM $aTable WHERE $urlAttr = '$newTitle'" . ($cond != "" ? " AND " . $cond->getTotalCond() : "");
    $res = $GLOBALS["db"]->query($query);

    if (!$res->fetch_assoc())
      break;

    $num++;
    $newTitle = $title . "-" . $num;
  }

  return $newTitle;
}

/**
 * Odesle multipart email
 */
function sendMailOld($aEmail, $aSubj, $aPlain, $aHtml, $aCss = "") {
  # -=-=-=- MIME BOUNDARY 

  $mime_boundary = "----Mushow Kitestore----" . md5(time());

  # -=-=-=- MAIL HEADERS 

  $to = $aEmail;
  $subject = $aSubj;

  $headers = "From: Mushow Kitestore <obchod@mushow.cz>\n";
  $headers .= "Reply-To: Mushow Kitestore <obchod@mushow.cz>\n";
  $headers .= "MIME-Version: 1.0\n";
  $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";

  # -=-=-=- TEXT EMAIL PART 

  $message = "--$mime_boundary\n";
  $message .= "Content-Type: text/plain; charset=UTF-8\n";
  $message .= "Content-Transfer-Encoding: 8bit\n\n";

  $message .= $aPlain . "\n\n";

  $message .= "Mushow s.r.o.\n" . WR . "\nobchod@mushow.cz\n";

  # -=-=-=- HTML EMAIL PART 

  $message .= "--$mime_boundary\n";
  $message .= "Content-Type: text/html; charset=UTF-8\n";
  $message .= "Content-Transfer-Encoding: 8bit\n\n";

  $message .= "<html>\n";
  $message .= "<head>" . $aCss . "</head>\n";
  $message .= "<body style=\"font-family:Verdana, Verdana, Geneva, sans-serif; font-size:12px; width:700px;color:black;\">\n";
  $message .= $aHtml;

  $message .= "<p style='margin:20px 0px 40px 0px;float:left;width:500px'><b>Mushow s.r.o.</b><br/>";
  $message .= "<a href='" . WR . "'>" . WR . "</a><br/>";
  $message .= "<a href='mailto:obchod@mushow.cz'>obchod@mushow.cz</a></p><br/>";

  $message .= "</body>\n";
  $message .= "</html>\n";

  # -=-=-=- FINAL BOUNDARY 

  $message .= "--$mime_boundary--\n\n";

  # -=-=-=- SEND MAIL 

  return @mail($to, mime_header_encode($subject), $message, $headers);
}

/**
 * Odesle multipart email
 */
function sendMail($email, $subj, $plain, $html, $css = "") {

  require_once "lib/PHPMailer/class.phpmailer.php";

  $mail = new PHPMailer;
  /*$mail->isSMTP();
  $mail->Host = 'smtp.e-petice.cz';
  $mail->Username = 'noreply@e-petice.cz';                            // SMTP username
  $mail->Password = '20NoRep13';
  $mail->SMTPSecure = 'ssl';  */

  $mail->CharSet = 'UTF-8';

  $mail->From = "obchod@mushow.cz";
  $mail->FromName = "Mushow Kitestore";
  $mail->Sender = "obchod@mushow.cz";
  $mail->addReplyTo("obchod@mushow.cz");
  $mail->addAddress($email);

  $mail->WordWrap = 50;

  $mail->Subject = $subj;
  $mail->isHTML(true);

  $message = "<html>\n";
  $message .= "<head>";
  $message .= "<meta http-equiv='content-type' content='text/html; charset=utf-8' />";
  $message .= "<meta http-equiv='content-language' content='cs' />";
  $message .= $css . "</head>\n";
  $message .= "<body style=\"font-family:Helvetica, sans-serif; font-size:12px; float:none; margin: 20px; width:700px;color:black;\">\n";
  $message .= $html;
  $message .= "<p>&nbsp;</p><p><b>Mushow s.r.o.</b><br/>";
  $message .= "<a href='" . WR . "'>" . WR . "</a><br/>";
  $message .= "<a href='mailto:obchod@mushow.cz'>obchod@mushow.cz</a></p><br/>";
  $message .= "</body>\n";
  $message .= "</html>\n";

  $mail->Body = $message;

  return $mail->send();
}

/**
 * Odesle multipart email
 */
function sendNewsletter($email, $subj, $html) {

  require_once "lib/PHPMailer/class.phpmailer.php";

  $mail = new PHPMailer;
  /*$mail->isSMTP();
  $mail->Host = 'smtp.e-petice.cz';
  $mail->Username = 'noreply@e-petice.cz';                            // SMTP username
  $mail->Password = '20NoRep13';
  $mail->SMTPSecure = 'ssl';  */

  $mail->CharSet = 'UTF-8';

  $mail->From = "obchod@mushow.cz";
  $mail->FromName = "Mushow Kitestore";
  $mail->Sender = "obchod@mushow.cz";
  $mail->addReplyTo("obchod@mushow.cz");
  $mail->addAddress($email);

  $mail->WordWrap = 50;

  $mail->Subject = $subj;
  $mail->isHTML(true);

  $message = "
    	<html>
    		<head>
    			<meta http-equiv='content-type' content='text/html; charset=utf-8' />
    			<meta http-equiv='content-language' content='cs' />
    		</head>
            <body style=\"font-family:Helvetica, sans-serif; font-size:12px; float:none;  
    				background:url('" . WR . "img/newsleter_bg.png');width:700px;color:#333;\">
                $html
            </body> 
    	</html>";

  $mail->Body = $message;

  return $mail->send();
}

/** 
 * Zakódování e-mailové hlavičky podle RFC 2047
 */
function mime_header_encode($text, $encoding = "utf-8") {
  return "=?UTF-8?B?" . base64_encode($text) . "?=";
}

/**
 * Upravi text pro browser a zkrati
 */
function getTextForBrw($text, $aMaxLength = 200) {
  $text = Str_Replace("<br />", " ", $text);
  $text = Str_Replace("<br>", " ", $text);

  if (mb_strlen($text) < $aMaxLength)
    return $text;
  else
    return mb_substr($text, 0, $aMaxLength) . "...";
}

/**
 * Prida odkazum na obrazky shadowbox
 */
function addShadowboxToLinks($text) {
  //$GLOBALS["rv"]->addInfo($text);
  $text = preg_replace("/(<a href[^>]+\.jpg[^>]+)>/", '$1 rel="shadowbox[img]">', $text);
  $text = preg_replace("/(<a href[^>]+\.JPG[^>]+)>/", '$1 rel="shadowbox[img]">', $text);

  $text = preg_replace("/(<a href[^>]+\.png[^>]+)>/", '$1 rel="shadowbox[img]">', $text);
  $text = preg_replace("/(<a href[^>]+\.PNG[^>]+)>/", '$1 rel="shadowbox[img]">', $text);

  $text = preg_replace("/(<a href[^>]+\.gif[^>]+)>/", '$1 rel="shadowbox[img]">', $text);
  $text = preg_replace("/(<a href[^>]+\.GIF[^>]+)>/", '$1 rel="shadowbox[img]">', $text);

  $text = preg_replace("/(<a href[^>]+\.bmp[^>]+)>/", '$1 rel="shadowbox[img]">', $text);
  $text = preg_replace("/(<a href[^>]+\.BMP[^>]+)>/", '$1 rel="shadowbox[img]">', $text);

  // odstraneni duplikace
  $text = preg_replace('/rel="shadowbox\[img\]"[^>]+rel="shadowbox\[img\]"/', 'rel="shadowbox[img]"', $text);

  // fixnuti linku
  $text = preg_replace('/(="user_files\/riders)/', '="' . WR . 'user_files/riders', $text);

  //$GLOBALS["rv"]->addInfo($text);
  return $text;
}

/**
 * Prevod na entity
 */
function UTF8ToEntities($string) {
  /* note: apply htmlspecialchars if desired /before/ applying this function
   /* Only do the slow convert if there are 8-bit characters */
  /* avoid using 0xA0 (\240) in ereg ranges. RH73 does not like that */
  if (!ereg("[\200-\237]", $string) and !ereg("[\241-\377]", $string))
    return $string;

  // reject too-short sequences
  $string = preg_replace("/[\302-\375]([\001-\177])/", "&#65533;\\1", $string);
  $string = preg_replace("/[\340-\375].([\001-\177])/", "&#65533;\\1", $string);
  $string = preg_replace("/[\360-\375]..([\001-\177])/", "&#65533;\\1", $string);
  $string = preg_replace("/[\370-\375]...([\001-\177])/", "&#65533;\\1", $string);
  $string = preg_replace("/[\374-\375]....([\001-\177])/", "&#65533;\\1", $string);

  // reject illegal bytes & sequences
  // 2-byte characters in ASCII range
  $string = preg_replace("/[\300-\301]./", "&#65533;", $string);
  // 4-byte illegal codepoints (RFC 3629)
  $string = preg_replace("/\364[\220-\277]../", "&#65533;", $string);
  // 4-byte illegal codepoints (RFC 3629)
  $string = preg_replace("/[\365-\367].../", "&#65533;", $string);
  // 5-byte illegal codepoints (RFC 3629)
  $string = preg_replace("/[\370-\373]..../", "&#65533;", $string);
  // 6-byte illegal codepoints (RFC 3629)
  $string = preg_replace("/[\374-\375]...../", "&#65533;", $string);
  // undefined bytes
  $string = preg_replace("/[\376-\377]/", "&#65533;", $string);

  // reject consecutive start-bytes
  $string = preg_replace("/[\302-\364]{2,}/", "&#65533;", $string);

  // decode four byte unicode characters
  $string = preg_replace(
    "/([\360-\364])([\200-\277])([\200-\277])([\200-\277])/e",
    "'&#'.((ord('\\1')&7)<<18 | (ord('\\2')&63)<<12 |" .
    " (ord('\\3')&63)<<6 | (ord('\\4')&63)).';'",
    $string);

  // decode three byte unicode characters
  $string = preg_replace("/([\340-\357])([\200-\277])([\200-\277])/e",
    "'&#'.((ord('\\1')&15)<<12 | (ord('\\2')&63)<<6 | (ord('\\3')&63)).';'",
    $string);

  // decode two byte unicode characters
  $string = preg_replace("/([\300-\337])([\200-\277])/e",
    "'&#'.((ord('\\1')&31)<<6 | (ord('\\2')&63)).';'",
    $string);

  // reject leftover continuation bytes
  $string = preg_replace("/[\200-\277]/", "&#65533;", $string);

  return $string;
}


//*****************************************************************************//
//*****************************************************************************//
//*****************************************************************************//

/**
 * Trida pro skladani SQL podminek
 */
class WhereCondition {
  var $vect = "";

  function addCondStr($str) {
    if (empty($str))
      return;

    if (empty($this->vect))
      $this->vect = $str;
    else
      $this->vect .= " AND " . $str;
  }

  function addCondOrStr($str) {
    if (empty($str))
      return;

    if (empty($this->vect))
      $this->vect = $str;
    else
      $this->vect .= " OR " . $str;
  }

  function addWhereCond($whereCond) {
    if (!($whereCond instanceof WhereCondition))
      return;

    if ($whereCond->isEmpty())
      return;

    if (empty($this->vect))
      $this->vect = $whereCond->getTotalCond();
    else
      $this->vect = "(" . $this->vect . ") AND (" . $whereCond->getTotalCond() . ")";
  }

  function addWhereCondOr($whereCond) {
    if (!($whereCond instanceof WhereCondition))
      return;

    if ($whereCond->isEmpty())
      return;

    if (empty($this->vect))
      $this->vect = $whereCond->getTotalCond();
    else
      $this->vect = "(" . $this->vect . ") OR (" . $whereCond->getTotalCond() . ")";
  }

  function getTotalCond() {
    return $this->vect;
  }

  function __toString() {
    return $this->vect;
  }

  function isEmpty() {
    return empty($this->vect);
  }
}


/**
 * Trida pro ulozeni chybovych a info zprav a jejich nasledne vypsani
 */
class ResultVect {
  var $errorVect = array();
  var $infoVect = array();

  function addError($err) {
    $this->errorVect[] = $err;
  }

  function addInfo($info) {
    $this->infoVect[] = $info;
  }

  function isEmpty() {
    return (empty($this->errorVect) && empty($this->infoVect));
  }

  function isOk() {
    return empty($this->errorVect);
  }

  function printErrors() {
    $array = $this->errorVect;

    if (!empty($array)) {
      echo "<div class='error'>";

      for ($i = 0; $i < count($array); $i++)
        echo "<p>" . $array[$i] . "</p>";

      echo "</div>";
    }
  }

  function printInfo() {
    $array = $this->infoVect;

    if (!empty($array)) {

      echo "<div class='info'>";

      for ($i = 0; $i < count($array); $i++)
        echo "<p>" . $array[$i] . "</p>";

      echo "</div>";
    }
  }

  function printHtmlOutput() {
    if (!$this->isEmpty()) {
      $this->printErrors();
      $this->printInfo();
    }
  }
}

class PathVect {
  var $vect = array();

  function PathVect() {
    $this->addItem(WR, "<strong>Home</strong>");
  }

  function addItem($ref, $text) {
    $item = array("ref" => $ref, "text" => $text);
    $this->vect[] = $item;
  }

  function clear() {
    $this->vect = array();
    $this->addItem(WR, "<strong>Home</strong>");
  }

  function toHtml() {

    $array = $this->vect;

    if (!empty($array)) {

      echo "<div id='path'>";

      for ($i = 0; $i < count($array); $i++) {
        if (empty($array[$i]["ref"]))
          echo "<span>" . $array[$i]["text"] . "</span>";
        else
          echo "<a href='" . $array[$i]["ref"] . "'>" . $array[$i]["text"] . "</a>";

        /* if (($i + 1) < count($array))
           echo " \ ";*/
      }

      echo "</div>";
    }
  }
}
?>