<?php

class AddrsWM extends WebModule {
  var $mAction;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (!isLogged()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    if (isset($_GET["action"]) && $_GET["action"] == "del" && isset($_GET["item"]) &&
      is_numeric($_GET["item"])) {
      $query = "SELECT u_main_addr FROM user WHERE u_code = " . $_SESSION[SN_CODE];
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      if ($row["u_main_addr"] == $_GET["item"])
        return true;

      $query = "SELECT ua_user FROM user_address WHERE ua_code = " . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      if ($row["ua_user"] != $_SESSION[SN_CODE]) {
        $GLOBALS["rv"]->addError("Nemáte právo smazat tuto adresu.");
        return true;
      }

      $query = "DELETE FROM user_address WHERE ua_code = " . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return "Dodací adresy";
  }

  /**
   * Definuje tlacitko pro pridani
   */
  function defineHeaderButton() {
    echo "<div class='right_top_button add_button'>";
    echo "<a href='" . WR . "?m=" . ADDR_EDIT . "&amp;action=add'>";
    echo "Přidat adresu";
    echo "</a></div>";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    $limit = 100;
    $offset = 0;

    // slozeni podminky
    $whereCond = new WhereCondition();
    $whereCond->addCondStr("ua_user = " . $_SESSION[SN_CODE]);
    $whereCond->addCondStr("ua_abbr != 'main_addr_" . $_SESSION[SN_CODE] . "'");

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $result = $itemsBrowser->executeQuery("user_address", $whereCond, "ua_abbr", "");
    if (!$result)
      die(getRText("err9") . $result->error);

    echo "<table class='brw_table'>";
    echo "<thead><th>Název</th><th></th><th></th></thead>";

    echo "<tr><td>Hlavní (fakturační adresa)</td>";
    echo "<td colspan='2'><a href='" . WR . "edit-profile'>Editovat</a></td></tr>";

    // vykresleni polozek
    while ($row = $result->fetch_assoc()) {
      echo "<tr>";
      echo "<td style='width:100%'>" . $row["ua_abbr"] . "</td>";
      echo "<td><a href='" . WR . "?m=" . ADDR_EDIT . "&amp;action=edit&amp;item=" . $row["ua_code"] . "'>Editovat</a></td>";
      echo "<td><a href='" . WR . "?m=" . ADDRS . "&amp;action=del&amp;item=" . $row["ua_code"] . "' ";
      echo "onclick='if(!confirm(\"Opravdu si přejete smazat tuto adresu?\")) return false;'>Smazat</a></td>";
      echo "</tr>\n";
    }

    echo "</table>\n";
  }
}
?>