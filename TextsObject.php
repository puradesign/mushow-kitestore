<?php
/**
 * Trida pro praci s objekty
 */
class TextsObject {
  var $mTextsArray = array();

  /**
   * Inicializace textu
   */
  function initTexts($lang = 2) {
    $this->mTextsArray = array();

    $this->initTextsCS();
  }

  /**
   * Vrat text
   */
  function getText($id) {

    return htmlspecialchars($this->mTextsArray[$id]);
  }

  /**
   * Inicializace ceskych textu
   */
  private function initTextsCS() {

    // menu
    $this->mTextsArray["menu1"] = "News";
    $this->mTextsArray["menu2"] = "Úvod";
    $this->mTextsArray["menu3"] = "Popis spotu";
    $this->mTextsArray["menu4"] = "Kiteschool";
    $this->mTextsArray["menu5"] = "Test centrum a půjčovna";
    $this->mTextsArray["menu6"] = "Kite repair";
    $this->mTextsArray["menu7"] = "Ubytování";
    $this->mTextsArray["menu8"] = "Doprava a letenky";
    $this->mTextsArray["menu9"] = "Trips";
    $this->mTextsArray["menu10"] = "Foto a video";
    $this->mTextsArray["menu11"] = "Kontakty";
    $this->mTextsArray["menu12"] = "Fórum";

    $this->mTextsArray["menu13"] = "Články";
    $this->mTextsArray["menu14"] = "Foto & video";
    $this->mTextsArray["menu15"] = "Odhlásit se";

    // util
    $this->mTextsArray["util1"] = "Zpět na přehled";
    $this->mTextsArray["util2"] = "bez uložení změn";
    $this->mTextsArray["util3"] = "Editace galerie";
    $this->mTextsArray["util4"] = "Přidat fotky";
    $this->mTextsArray["util5"] = "Upravit fotky";
    $this->mTextsArray["util6"] = "Smazat galerii";
    $this->mTextsArray["util7"] = "Přidáno";
    $this->mTextsArray["util8"] = "Popis";
    $this->mTextsArray["util9"] = "Nadpis";
    $this->mTextsArray["util10"] = "Titulní obrázek";
    $this->mTextsArray["util11"] = "Jazyk";
    $this->mTextsArray["util12"] = "Angličtina";
    $this->mTextsArray["util13"] = "Čeština";
    $this->mTextsArray["util14"] = "Francouzština";
    $this->mTextsArray["util15"] = "Text článku:";
    $this->mTextsArray["util16"] = "Zveřejnit";
    $this->mTextsArray["util17"] = "v sekci";
    $this->mTextsArray["util18"] = "prosím vyberte";
    $this->mTextsArray["util19"] = "Uložit článek";
    $this->mTextsArray["util20"] = "Položky označené hvězdičkou je nutné vyplnit.";
    $this->mTextsArray["util21"] = "Přidat článek";
    $this->mTextsArray["util22"] = "Opravdu si přejete smazat článek?";
    $this->mTextsArray["util23"] = "Smazat";
    $this->mTextsArray["util24"] = "Články editace";
    $this->mTextsArray["util25"] = "Nejsou dostupné žádné položky k zobrazení";
    $this->mTextsArray["util26"] = "článků";
    $this->mTextsArray["util27"] = "Editace článku";
    $this->mTextsArray["util28"] = "Smazat článek";
    $this->mTextsArray["util29"] = "Vytvoření článku";
    $this->mTextsArray["util30"] = "Přidat příspěvek";
    $this->mTextsArray["util31"] = "Odpověď na příspěvek";
    $this->mTextsArray["util32"] = "Jméno";
    $this->mTextsArray["util33"] = "Text příspěvku";
    $this->mTextsArray["util34"] = "Uložit příspěvek";
    $this->mTextsArray["util35"] = "Zpět na fórum";
    $this->mTextsArray["util36"] = "Nový příspěvek";
    $this->mTextsArray["util37"] = "příspěvků";
    $this->mTextsArray["util38"] = "Odpovědět";
    $this->mTextsArray["util39"] = "Přidat novou fotografii";
    $this->mTextsArray["util40"] = "Nahrát fotku";
    $this->mTextsArray["util41"] = "Přidat odkaz na video";
    $this->mTextsArray["util42"] = "Vložit odkaz";
    $this->mTextsArray["util43"] = "Galerie - vytvoření";
    $this->mTextsArray["util44"] = "Název";
    $this->mTextsArray["util45"] = "Vytvořit galerii";
    $this->mTextsArray["util46"] = "Opravdu si přejete smazat galerii?";
    $this->mTextsArray["util47"] = "Galerie";
    $this->mTextsArray["util48"] = "Uložit galerii";
    $this->mTextsArray["util49"] = "Editace";
    $this->mTextsArray["util50"] = "Přidat galerii";
    $this->mTextsArray["util51"] = "Výběru neodpovídají žádné galerie.";
    $this->mTextsArray["util52"] = "galerií";
    $this->mTextsArray["util53"] = "Odstranit fotku";
    $this->mTextsArray["util54"] = "Toto je hlavní fotka galerie";
    $this->mTextsArray["util55"] = "Uložit změny";
    $this->mTextsArray["util56"] = "Editace fotek";
    $this->mTextsArray["util57"] = "Centrum kiteboardingu v Ag. Nikolaos v Řecku";
    $this->mTextsArray["util58"] = "záznamů";
    $this->mTextsArray["util59"] = "předchozí";
    $this->mTextsArray["util60"] = "další";
    $this->mTextsArray["util61"] = "Špatné uživ. jméno nebo heslo.";
    $this->mTextsArray["util62"] = "Uživatelské jméno";
    $this->mTextsArray["util63"] = "Heslo";
    $this->mTextsArray["util64"] = "Přihlásit se";
    $this->mTextsArray["util65"] = "Výběru neodpovídají žádné položky.";
    $this->mTextsArray["util66"] = "novinek";
    $this->mTextsArray["util67"] = "Vloženo";
    $this->mTextsArray["util68"] = "Opravdu si přejete smazat příspěvek?";
    $this->mTextsArray["util69"] = "Předběžná objednávka";
    $this->mTextsArray["util70"] = "Příjmení";
    $this->mTextsArray["util71"] = "Telefon";
    $this->mTextsArray["util72"] = "Email";
    $this->mTextsArray["util73"] = "Datum příjezdu";
    $this->mTextsArray["util74"] = "Datum odjezdu";
    $this->mTextsArray["util75"] = "bez ubytování";
    $this->mTextsArray["util76"] = "dvoulůžkový";
    $this->mTextsArray["util77"] = "třílůžkový";
    $this->mTextsArray["util78"] = "dvoulůžkový (větší)";
    $this->mTextsArray["util79"] = "apartmán pro 4 osoby";
    $this->mTextsArray["util80"] = "Transfer z a na letiště";
    $this->mTextsArray["util81"] = "Počet osob";
    $this->mTextsArray["util82"] = "Čas příletu";
    $this->mTextsArray["util83"] = "Čas odletu";
    $this->mTextsArray["util84"] = "Transfer z a na spot";
    $this->mTextsArray["util85"] = "Datum začátku kurzu";
    $this->mTextsArray["util86"] = "Kurz Speed - počet osob";
    $this->mTextsArray["util87"] = "Kurz Komplet - počet osob";
    $this->mTextsArray["util88"] = "Individuální kurz - počet osob";
    $this->mTextsArray["util89"] = "Freestyle kurz - počet osob";
    $this->mTextsArray["util90"] = "Odeslat předběžnou objednávku";
    $this->mTextsArray["util91"] = "Předběžná objednávka úspěšně odeslána. V nejbližších dnech budete kontaktování pro zjištění podrobností a potvrzení objednávky.";
    $this->mTextsArray["util92"] = "Nepodařilo se odelsat objednávku. Zkuste to prosím znovu, nebo nás kontaktujte na adrese";
    $this->mTextsArray["util93"] = "Poznámka";
    $this->mTextsArray["util94"] = "Přidat komentář";
    $this->mTextsArray["util95"] = "komentářů";
    $this->mTextsArray["util96"] = "Žádné komentáře.";
    $this->mTextsArray["util97"] = "Odpověď na komentář";
    $this->mTextsArray["util98"] = "Nový komentář";
    $this->mTextsArray["util99"] = "Opište prosím kód z obrázku (spam kontrola)";
    $this->mTextsArray["util9"] = "";
    $this->mTextsArray["util9"] = "";
    $this->mTextsArray["util9"] = "";
    $this->mTextsArray["util9"] = "";


    // errors
    $this->mTextsArray["err1"] = "Oops. Chybí číslo alba. Pokud potize pretrvaji, kontaktujte spravce.";
    $this->mTextsArray["err2"] = "Pro úpravu galerie je nutné přihlášení.";
    $this->mTextsArray["err3"] = "Neexistující album!";
    $this->mTextsArray["err4"] = "Nemáte právo editovat toto album.";
    $this->mTextsArray["err5"] = "Pro vložení článku je nutné přihlášení.";
    $this->mTextsArray["err6"] = "Některá z povinných položek není vyplněna.";
    $this->mTextsArray["err7"] = "Pokud je článek již určen pro zveřejnění, musí být vyplněna sekce, v které má být zobrazen.";
    $this->mTextsArray["err8"] = "Musí být vybrán alespoň jeden jazyk.";
    $this->mTextsArray["err9"] = 'OOps, neco se stalo. Pokud potize pretrvaji, kontaktujte spravce:';
    $this->mTextsArray["err10"] = "Článek vytvořen.";
    $this->mTextsArray["err11"] = "Oops. Chybí číslo článku. Pokud potize pretrvaji, kontaktujte spravce.";
    $this->mTextsArray["err12"] = "Pro úpravu článku je nutné přihlášení.";
    $this->mTextsArray["err13"] = "Neexistující článek!";
    $this->mTextsArray["err14"] = "Nemáte právo editovat tento článek.";
    $this->mTextsArray["err15"] = "Článek nenalezen";
    $this->mTextsArray["err16"] = "Nebyl nalezen příspěvek pro reakci, pravděpodobně byla ručně upravena URL stránky. Zkuste to prosím znovu.";
    $this->mTextsArray["err17"] = "Emailová adresa má neplatný tvar.";
    $this->mTextsArray["err18"] = "Příspěvek vložen";
    $this->mTextsArray["err19"] = "Mazat vlastní příspěvky lze pouze po přihlášení.";
    $this->mTextsArray["err20"] = "Nebyl nalezen příspěvek pro smazání.";
    $this->mTextsArray["err21"] = "Příspěvek nemůže být smazán, protože už na něj někdo reagoval.";
    $this->mTextsArray["err22"] = "Příspěvek byl smazán.";
    $this->mTextsArray["err23"] = "Fórum neobsahuje žádné příspěvky.";
    $this->mTextsArray["err24"] = "Neplatný formát odkazu na video.";
    $this->mTextsArray["err25"] = "Nejsou vloženy žádné fotografie.";
    $this->mTextsArray["err26"] = "Nepodařilo se nalézt odkazované video, zkontrolujte prosím vloženou adresu a zkuste to znovu.";
    $this->mTextsArray["err27"] = "Pro vložení galerie je nutné přihlášení.";
    $this->mTextsArray["err28"] = "Galerie vytvořena.";
    $this->mTextsArray["err29"] = "Galerie upravena.";
    $this->mTextsArray["err30"] = "Album zatím neobsahuje žádné fotky. Pro vložení fotek klikněte na";
    $this->mTextsArray["err31"] = "Nejste přihlášen";
    $this->mTextsArray["err32"] = "Chyba. Neplatný název složky.";
    $this->mTextsArray["err33"] = "Chyba. Složka s tímto názvem existuje.";
    $this->mTextsArray["err34"] = "Obrázek se nepodařilo nahrát.";
    $this->mTextsArray["err35"] = "Mazat vlastní komentáře lze pouze po přihlášení.";
    $this->mTextsArray["err36"] = "Nebyl nalezen komentář pro smazání.";
    $this->mTextsArray["err37"] = "Komentář byl smazán.";
    $this->mTextsArray["err38"] = "Komentář nemůže být smazán, protože už na něj někdo reagoval.";
    $this->mTextsArray["err39"] = "Formulář nebyl zpracován, protože vás systém považuje za spamovacího robota, pokud potíže přetrvají obraťte prosím na správce těchto stránek.";
    $this->mTextsArray["err40"] = "Nebyl správně opsán kód z obrázku, zkuste to prosím znovu.";
    $this->mTextsArray["err3"] = "";
    $this->mTextsArray["err3"] = "";
    $this->mTextsArray["err3"] = "";
    $this->mTextsArray["err3"] = "";
    $this->mTextsArray["err3"] = "";

    // filemanager
    $this->mTextsArray["fm1"] = "Vložen obrázek";
    $this->mTextsArray["fm2"] = "vytvořen obrázek pro náhled na celou šířku článku";
    $this->mTextsArray["fm3"] = "vytvořen obrázek pro náhled na polovinu šířky článku";
    $this->mTextsArray["fm4"] = "Správce souborů";
    $this->mTextsArray["fm5"] = "Vytvořit složku";
    $this->mTextsArray["fm6"] = "Vytvořit";
    $this->mTextsArray["fm7"] = "Vytvořit automaticky náhled fotky";
    $this->mTextsArray["fm8"] = "větší rozměr (na celou šířku)";
    $this->mTextsArray["fm9"] = "menší rozměr (na polovinu šířky)";
    $this->mTextsArray["fm10"] = "Aktuální cesta";
    $this->mTextsArray["fm11"] = "Vybraná fotka";
    $this->mTextsArray["fm12"] = "Jméno souboru";
    $this->mTextsArray["fm13"] = "Rozměry";
    $this->mTextsArray["fm14"] = "Vybrat";
    $this->mTextsArray["fm15"] = "Nebyla vybrána fotka";

  }

}

?>