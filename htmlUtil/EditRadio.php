<?php

/**
 * Element pro combo
 */
class EditRadio extends EditField {
  var $mRadios = null;
  var $mVertical = true;
  
  /**
   * Konstruktor
   */
  function __construct($aName, $aPrompt, $aPromptWidth, $aMandatory, $aVertical) {
    parent::__construct($aName, $aPrompt, $aPromptWidth, $aMandatory, 0, 0);
    
    $this->mVertical = $aVertical;
  }
  
  /***********************************************************************************************/
  /************************************** Pristup. metody ****************************************/
  
  /**
   * Prida radio-button
   */
  function addRadio($aName, $aValue) {
    if ($this->mRadios == null)
      $this->mRadios = array();
  
    $array = array($aName, $aValue);
    
    $this->mRadios[] = $array;
  }
  
  /***********************************************************************************************/
  /************************************* Prevod do HTML ******************************************/
  
  /**
   * Prevod vlastni polozky do html
   */
  function toHtml() {
    echo "<td style='width:".$this->mSize."px'>";
    
    if ($this->mVertical) {
      echo "<table style='width:100%'>";
      
			for ($i = 0; $i < count($this->mRadios); $i++) {
				$value = $this->mRadios[$i][1];
				$text = $this->mRadios[$i][0];
				
				echo "<tr><td>";
				echo "<input type='radio' name='".$this->mName."' id='".$this->mName.$i."'";
				echo " value='$value'".($_POST[$this->mName] == $value ? " checked=\"checked\"" : "");
				
				// atributy
				if ($this->mFieldAttrs != null)
					echo " ".$this->getAttrsStr($this->mFieldAttrs);
				
				echo "/>&nbsp;<label for='".$this->mName.$i."'>$text</label>";
				echo "</td></tr>";
			}
			echo "</table>";
    }
    else {
    
      for ($i = 0; $i < count($this->mRadios); $i++) {
				$value = $this->mRadios[$i][1];
				$text = $this->mRadios[$i][0];
				
				echo "<label for='".$this->mName.$i."'>$text</label>&nbsp;";
				echo "<input type='radio' name='".$this->mName."' id='".$this->mName.$i."'";
				echo " value='$value'".(@$_POST[$this->mName] == $value ? " checked=\"checked\"" : "");
				
				// atributy
				if ($this->mFieldAttrs != null)
					echo " ".$this->getAttrsStr($this->mFieldAttrs);
				
				echo "/>&nbsp;&nbsp;";
			}
    }
    
    echo "</td>";
  }
  
  /***********************************************************************************************/
  /**************************************   Validace    ******************************************/
  
  /**
   * Validace hodnoty
   */
  function validateField() {
    $this->mValid = true;
    
    if (!isset($_POST[$this->mName]))
      return true;
  
    return $this->mValid;
  }
}

?>