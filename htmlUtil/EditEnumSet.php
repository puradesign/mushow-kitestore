<?php

/**
 * Element pro mnozinu checkboxu
 */
class EditEnumSet extends EditField {
  var $mEnumTable;
  var $mCodeAttr;
  var $mDescrAttr;
  var $mItems = array();
  var $mVertical = true;
  var $mCond = null;
  
  /**
   * Konstruktor
   */
  function __construct($aName, $aPrompt, $aPromptWidth, $aMandatory, $aVertical, 
                       $aEnumTable, $aCodeAttr, $aDescrAttr, $aCond = null) {
    parent::__construct($aName, $aPrompt, $aPromptWidth, $aMandatory, 0, 0);
    
    $this->mVertical  = $aVertical;
    $this->mEnumTable = $aEnumTable;
    $this->mCodeAttr  = $aCodeAttr;
    $this->mDescrAttr = $aDescrAttr;
    $this->mCond = $aCond;
    
    // incializace polozek
    $query = "SELECT * FROM $aEnumTable ";
    if ($aCond != null && $aCond->getTotalCond() != "")
      $query .= "WHERE ".$aCond->getTotalCond();
    
    $result = $GLOBALS["db"]->query($query);
    
    while ($row = $result->fetch_assoc()) {
      $this->mItems[] = $row;
    }
  }
  
  /***********************************************************************************************/
  /************************************** Pristup. metody ****************************************/
  
  /**
   * Prida radio-button
   */
  /*function addRadio($aName, $aValue) {
    if ($this->mRadios == null)
      $this->mRadios = array();
  
    $array = array($aName, $aValue);
    
    $this->mRadios[] = $array;
  }*/
  
  /***********************************************************************************************/
  /************************************* Prevod do HTML ******************************************/
  
  /**
   * Prevod vlastni polozky do html
   */
  function toHtml() {
    echo "<td style='width:".$this->mSize."px'>";
    
    if ($this->mVertical) {
      echo "<table style='width:100%'>";
      
			for ($i = 0; $i < count($this->mItems); $i++) {
				$name = $this->mName.$this->mItems[$i][$this->mCodeAttr];
				$text = $this->mItems[$i][$this->mDescrAttr];
				
				echo "<tr><td>";
				echo "<input type='checkbox' name='$name' id='$name' ";
				echo (isset($_POST[$name]) ? " checked=\"checked\"" : "");
				
				if (!$this->mValid) echo "style='background-color:#dbb2b2'";
				
				// atributy
				if ($this->mFieldAttrs != null)
					echo " ".$this->getAttrsStr($this->mFieldAttrs);
				
				echo "/>&nbsp;<label for='$name' ";
				if (!$this->mValid) echo "style='background-color:#dbb2b2'";
				echo ">$text</label>";
				echo "</td></tr>";
			}
			echo "</table>";
    }
    else {
    
      for ($i = 0; $i < count($this->mItems); $i++) {
				$name = $this->mName.$this->mItems[$i][$this->mCodeAttr];
				$text = $this->mItems[$i][$this->mDescrAttr];
				
				echo "<label for='$name' ";
				if (!$this->mValid) echo "style='background-color:#dbb2b2'";
				echo ">$text</label>&nbsp;";
				
				echo "<input type='checkbox' name='$name' id='$name' ";
				echo (isset($_POST[$name]) ? " checked=\"checked\"" : "");
				
				// atributy
				if ($this->mFieldAttrs != null)
					echo " ".$this->getAttrsStr($this->mFieldAttrs);
				
				echo "/>&nbsp;&nbsp;&nbsp;&nbsp;";
			}
    }
    
    echo "</td>";
  }
  
  /***********************************************************************************************/
  /**************************************   Validace    ******************************************/
  
  /**
   * Validace hodnoty
   */
  function validateField() {
    $this->mValid = false;
    
    // pokud je položka povinná ověříme že je vybrána aspoň jedna možnost
    if ($this->mMandatory) {
       
      for ($i = 0; $i < count($this->mItems); $i++) {
        $name = $this->mName.$this->mItems[$i][$this->mCodeAttr];
        
        if (isset($_POST[$name]))
          $this->mValid = true;
      }
    }
    else
      $this->mValid = true;

  
    return $this->mValid;
  }
  
  /***********************************************************************************************/
  /*********************************   Ulozeni do DB    ****************************************/
  
  /**
   * Ulozeni hodnoty
   */
  function saveToDB($aRefTable, $aHeadAttr, $aEnumCodeAttr, $aHead) {
    
    // vytvoreni pole kodu
    $lEnumArray = array();
    for ($i = 0; $i < count($this->mItems); $i++)
      $lEnumArray[] = $this->mItems[$i][$this->mCodeAttr];
      
    $query  = "DELETE FROM $aRefTable WHERE $aHeadAttr=$aHead AND ";
    $query .= "$aEnumCodeAttr IN (".implode(",", $lEnumArray).")";
    $result = $GLOBALS["db"]->query($query);
				
		if (!$result)
			$GLOBALS["rv"]->addError("Chyba při mazání z DB.");
    
    // projdeme vsechny polozky
    for ($i = 0; $i < count($this->mItems); $i++) {
      $lItemCode = $this->mItems[$i][$this->mCodeAttr];
      $lName = $this->mName.$lItemCode;
      
      if (isset($_POST[$lName])) {
				$query  = "INSERT INTO $aRefTable (`$aHeadAttr`, `$aEnumCodeAttr`)";
				$query .= "VALUES ('$aHead', '$lItemCode')";
				
				$result = $GLOBALS["db"]->query($query);
				
				if (!$result)
				  $GLOBALS["rv"]->addError("Chyba při ukládání do DB.");
			}
    }
  }
  
  /***********************************************************************************************/
  /*********************************   Nacteni z DB    ****************************************/
  
  /**
   * Nacteni hodnoty
   */
  function loadFromDB($aRefTable, $aHeadAttr, $aEnumCodeAttr, $aHead) {
    
    // vytvoreni pole kodu
    $lEnumArray = array();
    for ($i = 0; $i < count($this->mItems); $i++)
      $lEnumArray[] = $this->mItems[$i][$this->mCodeAttr];
    
    $query = "SELECT $aEnumCodeAttr FROM $aRefTable WHERE $aHeadAttr=$aHead AND ";
    $query .= "$aEnumCodeAttr IN (".implode(",", $lEnumArray).")";
    $result = $GLOBALS["db"]->query($query);
    
    while ($row = $result->fetch_assoc()) {
      $lName = $this->mName.$row[$aEnumCodeAttr];
      $_POST[$lName] = "true";
    }
  }
  
  /***********************************************************************************************/
  
   /**
   * Pridani promptu do html
   */
  function addPrompt() {
    echo "<td style='width:".$this->mPromptWidth."px; text-align:right;vertical-align:top'>";
    
    if ($this->mPrompt != "") {
      echo "<label for='".$this->mName."'"./*($this->mValid ? "" : " style='color:#950000;'").*/">";
      echo ($this->mMandatory ? "<span style='color:#551310;font-weight:bold;font-size:14px;'>*</span>" : ""); 
      echo $this->mPrompt.":</label>";
    }
    
    echo "</td><td style='width:".$this->mGapWidth."px'></td>";
  }
}

?>