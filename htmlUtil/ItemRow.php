<?php

/**
 * Trida pro radek polozky
 */
class ItemRow {
  var $mToDelete = false;
  var $mCode = null;
  var $mOrder;
  
  /**
   * Konstruktor
   */
  function __construct($aOrder, $aCode = null) {
    
    $this->mOrder = $aOrder;
    $this->mCode = $aCode;
  }
  
  /**
   * Nastavi kod radku
   */
  function setCode($aCode) {
    $this->mCode = aCode;
  }
  
  /**
   * Vraci kod radku
   */
  function getRowCode() {
    return $this->mCode;
  }
  
  /**
   * Nastavi ze se ma radek smazat
   */
  function setForDelete() {
    $this->mToDelete = true;
  }
  
  /**
   * Bude se radek mazat
   */
  function isForDelete() {
    return $this->mToDelete;
  }
  
  /**
   * Bude radek videt
   */
  function isForShow() {
    return !$this->mToDelete;
  }
}

?>