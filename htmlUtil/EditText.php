<?php

/**
 * Element pro textarea
 */
class EditText extends EditField {
  var $mRows;
  
  /**
   * Konstruktor
   */
  function __construct($aName, $aPrompt, $aPromptWidth, $aMandatory,
                       $aSize, $aMaxLength,$aRows) {
    parent::__construct($aName, $aPrompt, $aPromptWidth, $aMandatory, $aSize, $aMaxLength);
    
    $this->mRows = $aRows;
  } 
  
  /***********************************************************************************************/
  /************************************* Prevod do HTML ******************************************/
  
  /**
   * Prevod vlastni polozky do html
   */
  function toHtml() {
    echo "<td style='width:".$this->mSize."px'>";
    echo "<textarea name='".$this->mName."' id='".$this->mName."'";
    
    if ($this->mMaxLength > 0)
      echo " maxlength='".$this->mMaxLength."' onkeyup='return ismaxlength(this)'";
      
    echo " cols='".$this->mSize."' rows='".$this->mRows."'";
    
    // atibuty
    if ($this->mFieldAttrs != null)
      echo " ".$this->getAttrsStr($this->mFieldAttrs);
    
    if (!$this->mValid)
      echo " style='background-color:#dbb2b2'";
    
    echo ">".@$_POST[$this->mName]."</textarea></td>";
  }
  
  /**
   * Pridani promptu do html
   */
  function addPrompt() {
    echo "<td style='width:".$this->mPromptWidth."px; text-align:right;vertical-align:top'>";
    
    if ($this->mPrompt != "") {
      echo "<label for='".$this->mName."'"./*($this->mValid ? "" : " style='color:#950000;'").*/">";
      echo ($this->mMandatory ? "<span style='color:#551310;font-weight:bold;font-size:14px;'>*</span>" : ""); 
      echo $this->mPrompt.":</label>";
    }
    
    echo "</td><td style='width:".$this->mGapWidth."px'></td>";
  }
}

?>