<?php

/**
 * Element pro combo
 */
class EditCodeCombo extends EditCombo {
  var $mViewName;
  var $mTextAttr;
  var $mOtherTextAttrs;
  var $mCodeAttr;
  var $mNullAllowed = true;
  var $mCond = null;
  var $mOrderBy = null;
  var $mAttrForStructure = null;
  
  /**
   * Konstruktor
   */
  function __construct($aName, $aPrompt, $aPromptWidth, $aMandatory, $aSize, 
                       $aViewName, $aCodeAttr, $aTextAttr, $aOtherTextAttrs = null) {
    parent::__construct($aName, $aPrompt, $aPromptWidth, $aMandatory, $aSize);
    
    $this->mViewName = $aViewName;
    $this->mTextAttr = $aTextAttr;
    $this->mCodeAttr = $aCodeAttr;
    $this->mOtherTextAttrs = $aOtherTextAttrs;
  }
  
  /***********************************************************************************************/
  /************************************** Pristup. metody ****************************************/
  
  /**
   * Nastavi podminku
   */
  function setCond($aCond) {
    $this->mCond = $aCond;
  }
  
  /**
   * Nastavi prizna, jestli se ma vlozit null hodnota
   */
  function setForNull($aBool) {
    $this->mNullAllowed = $aBool;
  }
  
  /**
   * Nastavi razeni
   */
  function setOrderBy($aOrderBy) {
    $this->mOrderBy = $aOrderBy;
  }
  
  /**
   * Nastavi atribut pro vypisovani strukturovane
   */
  function setAttrForStructure($aAttr) {
    $this->mAttrForStructure = $aAttr;
  }
  
  /**
   * Inicializace hodnot comba
   */
  function initOptions($nullText = "? ? ?") {
    $lSql = "SELECT * FROM ".$this->mViewName;
    
    // podminka
    $whereCond = $this->mCond;
    if ($whereCond != null && !$whereCond->isEmpty())
		  $lSql .= " WHERE ".$whereCond;
	 
	  // razeni
	  $orderBy = $this->mOrderBy;
	  if (!empty($orderBy))
		  $lSql .= " ORDER BY ".$orderBy;

    if ($this->mNullAllowed)
      $this->addSelect($nullText, 0);
 
	  $result = $GLOBALS["db"]->query($lSql);
	  while ($row = $result->fetch_assoc()) {
	    $text = $row[$this->mTextAttr];
	    
	    // pro vic text. atributu
	    if ($this->mOtherTextAttrs != null && !empty($this->mOtherTextAttrs)) {
	      for ($i=0; $i < count($this->mOtherTextAttrs); $i++)
	        $text .= " ".$row[$this->mOtherTextAttrs[$i]];
	    }
	    
	    if ($this->mAttrForStructure != null) {
	      $ident = substr_count($row[$this->mAttrForStructure], ".");
	      for ($i = 0; $i <= $ident; $i++)
	        $text = "&nbsp;&nbsp;".$text;
	    }
	  
	    $this->addSelect($text, $row[$this->mCodeAttr]);
	  }
  }
}

?>