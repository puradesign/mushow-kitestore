<?php

/**
 * Element pro combo
 */
class EditCombo extends EditField {
  var $mSelects = null;
  
  /**
   * Konstruktor
   */
  function __construct($aName, $aPrompt, $aPromptWidth, $aMandatory, $aSize) {
    parent::__construct($aName, $aPrompt, $aPromptWidth, $aMandatory, $aSize, 0);
    
  }
  
  /***********************************************************************************************/
  /************************************** Pristup. metody ****************************************/
  
  /**
   * Prida moznost
   */
  function addSelect($aName, $aValue) {
    if ($this->mSelects == null)
      $this->mSelects = array();
  
    $array = array($aName, $aValue);
    
    $this->mSelects[] = $array;
  }
  
  /***********************************************************************************************/
  /************************************* Prevod do HTML ******************************************/
  
  /**
   * Prevod vlastni polozky do html
   */
  function toHtml() {
    echo "<td style='width:".$this->mSize."px'>";
    echo "<select name='".$this->mName."' id='".$this->mName."'";
    
    echo " style='width:".($this->mSize > 0 ? $this->mSize."px" : "100%").";";
    if (!$this->mValid) echo "background-color:#dbb2b2;";
    echo "'";
    
    // atributy
    if ($this->mFieldAttrs != null)
      echo " ".$this->getAttrsStr($this->mFieldAttrs);
    
    echo ">";
    
    for ($i = 0; $i < count($this->mSelects); $i++) {
      $value = $this->mSelects[$i][1];
      $text = $this->mSelects[$i][0];
      
      echo "<option value='$value'".(@$_POST[$this->mName] == $value || @$_GET[$this->mName] == $value ? " selected='selected'" : "");
      echo ">$text</option>";
    }
    
    echo "</select></td>";
  }
  
  /***********************************************************************************************/
  /**************************************   Validace    ******************************************/
  
  /**
   * Validace hodnoty
   */
  function validateField() {
    $this->mValid = true;
    
    if (!isset($_POST[$this->mName]))
      return true;
    
    if ($this->mMandatory && is_numeric($_POST[$this->mName]) && $_POST[$this->mName] == 0)
      $this->mValid = false;
  
    return $this->mValid;
  }
}

?>