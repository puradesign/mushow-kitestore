<?php

/**
 * Zakladni element na prezentacni vrstve
 */
class EditField extends PresentElement {
  var $mPrompt;
  var $mPromptWidth = 100;
  var $mGapWidth = 20;
  var $mName;
  
  var $mAttrs = null;
  var $mStyles = null;
  var $mFieldAttrs = null;
  
  var $mMandatory = false;
  var $mSize = 20;
  var $mMaxLength = 100;
  var $mSuffix = null;
  var $mValue;
  
  var $mValid = true;
  
  var $mInputType = "text";
  
  var $mMinLength = 0;
  
  /**
   * Konstruktor
   */
  function __construct($aName, $aPrompt, $aPromptWidth, $aMandatory,
                       $aSize, $aMaxLength) {
    $this->mName = $aName;
    
    $this->mPrompt = $aPrompt;
    $this->mPromptWidth = $aPromptWidth;
    $this->mMandatory = $aMandatory;
    $this->mSize = $aSize;
    $this->mMaxLength = $aMaxLength;
   // $this->mValue = $aValue;
  }
  
  /***********************************************************************************************/
  /************************************** Pristup. metody ****************************************/
  
  /**
   * Nastavi sirku mezery
   */
  function setGapWidth($aWidth) {
    $this->mGapWidth = $aWidth;
  }
  
  /**
   * Nastavi viditelnost polozky
   */
  function setHidden() {
    $this->addStyle("visibility", "hidden");
    $this->addStyle("display", "none");
  }
  
  /**
   * Nastavi suffix polozky
   */
  function setSuffix($aSuffix) {
    $this->mSuffix = $aSuffix;
  }
  
  /**
   * Nastavi typ inputu
   */
  function setInputType($aType) {
    $this->mInputType = $aType;
  }
  
  /**
   * Nastavi sirku tabulky
   */
  function setWidthPX($aWidth) {
    $this->addStyle("width", $aWidth."px");
  }
  
  /**
   * Nastavi minimalni delku
   */
  function setMinLength($aLength) {
    $this->mMinLength = $aLength;
  }
  
  /**
   * Prida atribut
   */
  function addAttr($aName, $aValue) {
    if ($this->mAttrs == null)
      $this->mAttrs = array();
  
    $array = array($aName, $aValue);
    
    $this->mAttrs[] = $array;
  }
  
  /**
   * Vraci string atributu
   */
  function getAttrsStr($aAttrs) {
    $str = "";
    
    for ($i = 0; $i < count($aAttrs); $i++) {
      $str .= " ".$aAttrs[$i][0]."='";
      $str .= $aAttrs[$i][1]."'";
    }
    
    return $str;
  }
  
  /**
   * Prida atribut pro editacni polozku
   */
  function addFieldAttr($aName, $aValue) {
    if ($this->mFieldAttrs == null)
      $this->mFieldAttrs = array();
   
    $array = array($aName, $aValue);
    
    $this->mFieldAttrs[] = $array;
  }
  
  /**
   * Prida styl cele tabulky
   */
  function addStyle($aName, $aValue) {
    if ($this->mStyles == null)
      $this->mStyles = array();
      
    $array = array($aName, $aValue);
    
    $this->mStyles[] = $array;
  }
  
  /**
   * Vraci retezec stylu
   */
  function getStylesStr($aStyles) {
    $str = "";
    
    for ($i = 0; $i < count($aStyles); $i++) {
      if ($str != "")
        $str .= ";";

      $str .= $aStyles[$i][0].":";
      $str .= $aStyles[$i][1];
    }
    
    return "style='".$str."'";
  }
  
  /***********************************************************************************************/
  /**************************************Prevod do HTML ******************************************/
  
  /**
   * Vygenerovani html
   */
  function toHtmlLow() {
    echo "<table class='edit_table'";
    
    // id polozky
    if ($this->mID != null && $this->mID != "")
      echo " id='".$this->mID."'";
    
    // atributy
    if ($this->mAttrs != null)
      echo " ".$this->getAttrsStr($this->mAttrs);
    
    // styl
    if ($this->mStyles != null)
      echo " ".$this->getStylesStr($this->mStyles);
    
    echo "><tr>";
    
    // prompt
    if ($this->mPrompt != null && $this->mPromptWidth > 0)
      $this->addPrompt();
    
    // vlastni html polozky
    $this->toHtml();
    
    // suffix
    if ($this->mSuffix != null && $this->mSuffix != "")
      $this->addSuffix();
    
    if (count($this->mElements) > 0)
      $this->printSubElements();
    
    echo "<td></td></tr></table>";
  }
  
  /**
   * Prevod vlastni polozky do html
   */
  function toHtml() {
    echo "<td style='width:".$this->mSize."px'>";
    echo "<input type='".$this->mInputType."' style='width:".$this->mSize."px";
    if (!$this->mValid) echo ";background-color:#dbb2b2";
    echo "' maxlength='".$this->mMaxLength."'";
    echo " value='".@$_POST[$this->mName]."' name='".$this->mName."' id='".$this->mName."'";
    
    // atibuty
    if ($this->mFieldAttrs != null)
      echo " ".$this->getAttrsStr($this->mFieldAttrs);
    
    if (!$this->mValid)
      echo " style='background-color:#dbb2b2'";
    
    echo "/></td>";
  }
  
  /**
   * Pridani promptu do html
   */
  function addPrompt() {
    echo "<td style='width:".$this->mPromptWidth."px; text-align:right;vertical-align:middle'>";
    
    if ($this->mPrompt != "") {
      echo "<label for='".$this->mName."'"./*($this->mValid ? "" : " style='color:#950000;'").*/">";
      echo ($this->mMandatory ? "<span style='color:#551310;font-weight:bold;font-size:14px;'>*</span>" : ""); 
      echo $this->mPrompt.":</label>";
    }
    
    echo "</td><td style='width:".$this->mGapWidth."px'></td>";
  }
  
  /**
   * Pridani suffixu do html
   */
  function addSuffix() {
    echo "<td>";
  
    if ($this->mSuffix != "")
      echo "<label for='".$this->mName."'>".$this->mSuffix."</label>";
    
    echo "</td>";
  }
  
  /***********************************************************************************************/
  /**************************************   Validace    ******************************************/
  
  /**
   * Validace hodnoty
   */
  function validateField() {
    $this->mValid = true;
    
    if (!isset($_POST[$this->mName]))
      return true;
    
    if ($this->mMandatory && $_POST[$this->mName] == "")
      $this->mValid = false;
    
    elseif (strlen($_POST[$this->mName]) > 0 && strlen($_POST[$this->mName]) < $this->mMinLength) {
      $this->mValid = false;
      $GLOBALS["rv"]->addError("Položka '".$this->mPrompt."' musí mít minimálně ".$this->mMinLength." znaků.");
    }
  
    return $this->mValid;
  }
  
  /**
   * Vraci element podle jmena
   */
  function getElement($aName) {
    if ($aName == $this->mName)
      return $this;
    
    return parent::getElement($aName);
  }
}

?>