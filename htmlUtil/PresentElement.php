<?php

/**
 * Zakladni element na prezentacni vrstve
 */
class PresentElement {
  var $mText;
  var $mID = null;
  var $mElements = null;
  
  /**
   * Konstruktor
   */
  function __construct($aText) {
    
    $this->mText = $aText;
  }
  
  /**
   * Nastavi id elementu
   */
  function setID($aID) {
    $this->mID = aID;
  }
  
  /**
   * Pridani elementu
   */
  function addElement($aElem) {
    if ($aElem == null || !($aElem instanceof PresentElement))
      return false;
    
    if ($this->mElements == null)
      $this->mElements = array();
    
    $this->mElements[] = $aElem;
  }
  
  /**
   * Vypis podrizenych elementu
   */
  function printSubElements() {
  
    for ($i = 0; $i < count($this->mElements); $i++) {
      $this->mElements[$i].toHtmlLow();
    }
  }
  
  /**
   * Prevod do html
   */
  function toHtmlLow() {
   /* echo "<div class='present_el'";
    
    if ($this->mID != null && $this->mID != "")
      echo " id='".$this->mID."'";
 
    echo ">";*/
    
    
    echo $this->mText;
    
    if (count($this->mElements) > 0)
      $this->printSubElements();
    
    //echo "</div>";
  }
  
  /**
   * Vraci element podle jmena
   */
  function getElement($aName) {
  
    for ($i = 0; $i < count($this->mElements); $i++) {
      $lElem = $this->mElements[$i]->getElement($aName);
      
      if ($lElem != null)
        return $lElem;
    }
    
    return null;
  }
}

?>