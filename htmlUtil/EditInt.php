<?php

/**
 * Element pro combo
 */
class EditInt extends EditField {
  
  /**
   * Konstruktor
   */
  function __construct($aName, $aPrompt, $aPromptWidth, $aMandatory, $aSize, $aMaxLength, $aDecimal) {
    parent::__construct($aName, $aPrompt, $aPromptWidth, $aMandatory, $aSize, $aMaxLength);
    
    $lStr = "alert(\"V položce \\\"".$this->mPrompt."\\\" musí být platné";
    $lStr .= ($aDecimal ? " číslo" : " celé číslo").".\"); return false;";
    $this->addFieldAttr("onblur", "if (!validateNumber(this,".($aDecimal ? "true" : "false").")) ". 
      "{$lStr}");
  }
  
  /***********************************************************************************************/
  /************************************** Pristup. metody ****************************************/
  
  
  
  /***********************************************************************************************/
  /************************************* Prevod do HTML ******************************************/
  
  
  
  /***********************************************************************************************/
  /**************************************   Validace    ******************************************/
  
  /**
   * Validace hodnoty
   */
  function validateField() {
    if (!isset($_POST[$this->mName]))
      return true;
 
    $this->mValid = true;
    
    if (!$this->mMandatory && !is_numeric($_POST[$this->mName]))
      $_POST[$this->mName] = "";
    
    if (!$this->mMandatory)
      return true;
    
    // validace na cislo
    if (!is_numeric($_POST[$this->mName])) {
      //$_POST[$this->mName] = 1;
      $this->mValid = false;
    }
      
    if ($this->mMandatory && $_POST[$this->mName] == "")
      $this->mValid = false;
  
    return $this->mValid;
  }
}

?>