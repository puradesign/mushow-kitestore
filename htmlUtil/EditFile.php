<?php
require_once "SimpleImage.php";

/**
 * Element pro upload souboru
 */
class EditFile extends EditField {
  
  /**
   * Konstruktor
   */
  function __construct($aName, $aPrompt, $aPromptWidth, $aMandatory) {
    parent::__construct($aName, $aPrompt, $aPromptWidth, $aMandatory, 0, 0);
    
  } 
  
  /***********************************************************************************************/
  /************************************* Prevod do HTML ******************************************/
  
  /**
   * Prevod vlastni polozky do html
   */
  function toHtml() {
    echo "<td style='40px'>";
    echo "<input type='file' value='upload'";
    echo " name='".$this->mName."' id='".$this->mName."'";
    
    if (!$this->mValid) echo " style='background-color:#dbb2b2'";
    
    // atibuty
    if ($this->mFieldAttrs != null)
      echo " ".$this->getAttrsStr($this->mFieldAttrs);
    
    echo "/></td>";
  }
  
  /***********************************************************************************************/
  /**************************************   Validace    ******************************************/
  
  /**
   * Validace hodnoty
   */
  function validateField() {
    $this->mValid = true;
    
 /*   if (!isset($_FILES[$this->mName])) {
      $this->mValid = !$this->mMandatory;
    }
    else {    */
			$image = new ImageUploaded($_FILES[$this->mName]);
			$pic_ok = $image->validate($this->mMandatory);
			if (!$pic_ok)
				$this->mValid = false;
//		}
	
    return $this->mValid;
  }
}

?>