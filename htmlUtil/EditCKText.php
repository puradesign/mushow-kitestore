<?php

/**
 * Element pro textarea
 */
class EditCKText extends EditText {


  /**
   * Konstruktor
   */
  function __construct($aName, $aPrompt, $aMandatory) {
    parent::__construct($aName, $aPrompt, 0, $aMandatory, 2, 0, 2);
  }
  
  /***********************************************************************************************/
  /************************************* Prevod do HTML ******************************************/
  
  /**
   * Prevod vlastni polozky do html
   */
  function toHtmlLow() {
    echo "<div class='td_one'>";
    
    echo " <p><label for='".$this->mName."'>".($this->mMandatory ? "*" : "");
    echo $this->mPrompt.":</label></p>"; // Text příspěvku
    
    echo "<textarea name='".$this->mName."' id='".$this->mName."'";
    echo " maxlength='".$this->mMaxLength."' cols='2' rows='2' class='xckeditor'";
    
    // atibuty
    if ($this->mFieldAttrs != null)
      echo " ".$this->getAttrsStr($this->mFieldAttrs);
    
    echo ">".@$_POST[$this->mName]."</textarea></div>";
  }
}

?>