-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 04, 2010 at 12:01 PM
-- Server version: 5.1.37
-- PHP Version: 5.2.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `kitestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `fpost`
--

CREATE TABLE `fpost` (
  `f_code` int(11) NOT NULL AUTO_INCREMENT,
  `f_father` int(11) DEFAULT NULL,
  `f_user` int(11) DEFAULT NULL,
  `f_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `f_mail` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `f_web` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  `f_text` text COLLATE utf8_czech_ci NOT NULL,
  `f_date` datetime NOT NULL,
  `f_id` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `f_head` int(11) NOT NULL,
  `f_ip` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`f_code`),
  KEY `f_user` (`f_user`),
  KEY `f_father` (`f_father`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=37 ;

--
-- Dumping data for table `fpost`
--

INSERT INTO `fpost` VALUES(32, NULL, NULL, 'peter', 'p@k.cz', '', '<p>\r\n	heheeeeee</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	hehe</p>\r\n', '2010-08-01 01:09:34', '1', 1, '::1');
INSERT INTO `fpost` VALUES(33, NULL, NULL, 'testik', 'l@l.l', '', '<p>\r\n	luliiiiiiiiiii</p>\r\n<p>\r\n	nnnnnnnnnnnnnnnnnnnnnnnnnnnnn</p>\r\n', '2010-08-01 01:11:54', '2', 2, '::1');
INSERT INTO `fpost` VALUES(34, 33, NULL, 'pett', 'p@k.cz', '', '<p>\r\n	odpoveeeeeeeee<u>eeeeeeee</u>eeeeeed heh</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>heh</strong></p>\r\n', '2010-08-01 01:24:13', '2.1', 2, '::1');
INSERT INTO `fpost` VALUES(35, NULL, NULL, 'pett', 'p@k.cz', '', '<p>\r\n	terekkaaaa</p>\r\n', '2010-09-10 19:45:25', '3', 3, '::1');
INSERT INTO `fpost` VALUES(36, 35, NULL, 'ahoj', 'p@k.cz', '', '<p>\r\n	odpevode na terkaaaaaaa</p>\r\n', '2010-09-10 20:33:43', '3.1', 3, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `shop_item`
--

CREATE TABLE `shop_item` (
  `si_code` int(11) NOT NULL AUTO_INCREMENT,
  `si_title` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `si_descr` varchar(500) COLLATE utf8_czech_ci NOT NULL,
  `si_text` text COLLATE utf8_czech_ci,
  `si_type` int(11) NOT NULL,
  `si_brand` int(11) NOT NULL,
  `si_price` int(6) NOT NULL,
  `si_count` int(3) NOT NULL,
  `si_url` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `si_dcreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`si_code`),
  KEY `si_ibfk_1` (`si_type`),
  KEY `si_ibfk_2` (`si_brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `shop_item`
--

INSERT INTO `shop_item` VALUES(6, 'Slingshot Fuel 2010', 'Slingshot Fuel je legendární kite, který má za sebou již 9 leté období vývoje, tzn že první Fuel byl vyroben v roce 2001. Fuel je považován za zcela nejdokonalejší C kite na trhu vůbec. Mnohé z dokonalých vychytávek Fuelu přebraly i kity od jiných firem. Fuel 2010 navíc letos přichází i s novým řídícim systémem.', '<h2>\r\n	&Uacute;vod</h2>\r\n<p style="text-align: justify; ">\r\n	M&aacute;me tady konec roku 2009 a s n&iacute;m i nov&yacute; Slingshot Fuel 2010. Kite m&aacute; za sebou již děvet roků v&yacute;voje a na jeho vlastnostech je to patřičně zn&aacute;t. Možn&aacute; nevid&iacute;te rozd&iacute;ly mezi přede&scaron;l&yacute;m a aktu&aacute;ln&iacute;m modelem, ale rozdhodně ten rozd&iacute;l poc&iacute;t&iacute;te. Je&scaron;tě než se začtete do podrobn&eacute;ho popisu je třeba se zeptat, zda jste zač&aacute;tečnici, nebo pokročil&iacute; rideři s freestyle ambicema. Pokud zač&iacute;n&aacute;te, nem&aacute; smysl, aby jste v čten&iacute; pokračovali, protože nov&yacute; Fuel je hračka pouze pro velmi zku&scaron;en&eacute; jezdce až experty. A &scaron;el bych je&scaron;tě d&aacute;l, Fuel je kite určen&yacute; pro ridery s vyloženě freestylov&yacute;mi ambicemi a předev&scaron;&iacute;m pro jezdce vyzn&aacute;vaj&iacute;c&iacute; nekompromisn&iacute; wakestyle. Pokud se tedy řad&iacute;te do skupiny unhooked riderů, je v&aacute;m Fuel 2010 &scaron;it&yacute; přimo na m&iacute;ru.</p>\r\n<p>\r\n	<a href="http://localhost:8888/kiteshop/user_files/images/HMS_7147.JPG" rel="shadowbox[img]"><img alt="" height="261" src="http://localhost:8888/kiteshop/user_files/images/nahledVHMS_7147.JPG" width="530" /></a></p>\r\n', 1, 1, 28000, 0, 'slingshot-fuel-2010', '0000-00-00 00:00:00');
INSERT INTO `shop_item` VALUES(7, 'Slingshot RPM 2010', 'Slingshot RPM je full de-power Open &quot;C&quot; kite u kterého technici využili své znalosti z konstruování C-kites a kitů hybridních. RPM je tedy kite hybridního stylu s vlastnostmi C kitu a možností nastavení do maximálního výkonu pro sportovní, až závodní jízdu.', '<p>\r\n	<span class="Apple-style-span" style="font-size: 18px; font-weight: bold; ">&Uacute;vod</span></p>\r\n<p style="text-align: justify; ">\r\n	M&aacute;me tady konec roku 2009 a s n&iacute;m i nov&yacute; Slingshot Fuel 2010. Kite m&aacute; za sebou již děvet roků v&yacute;voje a na jeho vlastnostech je to patřičně zn&aacute;t. Možn&aacute; nevid&iacute;te rozd&iacute;ly mezi přede&scaron;l&yacute;m a aktu&aacute;ln&iacute;m modelem, ale rozdhodně ten rozd&iacute;l poc&iacute;t&iacute;te. Je&scaron;tě než se začtete do podrobn&eacute;ho popisu je třeba se zeptat, zda jste zač&aacute;tečnici, nebo pokročil&iacute; rideři s freestyle ambicema. Pokud zač&iacute;n&aacute;te, nem&aacute; smysl, aby jste v čten&iacute; pokračovali, protože nov&yacute; Fuel je hračka pouze pro velmi zku&scaron;en&eacute; jezdce až experty. A &scaron;el bych je&scaron;tě d&aacute;l, Fuel je kite určen&yacute; pro ridery s vyloženě freestylov&yacute;mi ambicemi a předev&scaron;&iacute;m pro jezdce vyzn&aacute;vaj&iacute;c&iacute; nekompromisn&iacute; wakestyle. Pokud se tedy řad&iacute;te do skupiny unhooked riderů, je v&aacute;m Fuel 2010 &scaron;it&yacute; přimo na m&iacute;ru.</p>\r\n<p>\r\n	<a href="http://localhost:8888/kiteshop/user_files/images/HMS_7147.JPG" rel="shadowbox[img]"><img alt="" height="261" src="http://localhost:8888/kiteshop/user_files/images/nahledVHMS_7147.JPG" style="cursor: default; " width="530" /></a></p>\r\n', 1, 1, 27000, 0, 'slingshot-rpm-2010', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `s_item_brand`
--

CREATE TABLE `s_item_brand` (
  `sib_code` int(11) NOT NULL AUTO_INCREMENT,
  `sib_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `sib_url` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`sib_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `s_item_brand`
--

INSERT INTO `s_item_brand` VALUES(1, 'SLINGSHOT', 'slingshot');
INSERT INTO `s_item_brand` VALUES(2, 'MUSHOW', 'mushow');
INSERT INTO `s_item_brand` VALUES(3, 'OZONE', 'ozone');
INSERT INTO `s_item_brand` VALUES(4, 'MYSTIC', 'mystic');
INSERT INTO `s_item_brand` VALUES(5, 'SP BOARDING', 'sp-boarding');
INSERT INTO `s_item_brand` VALUES(6, 'OAKLEY', 'oakley');
INSERT INTO `s_item_brand` VALUES(7, 'INFINITY', 'infinity');

-- --------------------------------------------------------

--
-- Table structure for table `s_item_row`
--

CREATE TABLE `s_item_row` (
  `sir_code` int(11) NOT NULL AUTO_INCREMENT,
  `sir_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `sir_price` int(6) NOT NULL,
  `sir_count` int(3) NOT NULL,
  `sir_head` int(11) NOT NULL,
  `sir_visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sir_code`),
  KEY `sir_ibfk_1` (`sir_head`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `s_item_row`
--

INSERT INTO `s_item_row` VALUES(2, '5m2', 27000, 0, 6, 1);
INSERT INTO `s_item_row` VALUES(3, '7m2', 30000, 6, 6, 1);
INSERT INTO `s_item_row` VALUES(4, '9m2', 31000, 6, 6, 1);
INSERT INTO `s_item_row` VALUES(5, '11m2', 32000, 6, 6, 1);
INSERT INTO `s_item_row` VALUES(6, '13m2', 33000, 6, 6, 1);
INSERT INTO `s_item_row` VALUES(7, '15ka', 3434343, 8, 6, 0);
INSERT INTO `s_item_row` VALUES(8, '9m', 28000, 2, 7, 1);
INSERT INTO `s_item_row` VALUES(9, '11m', 28000, 4, 7, 1);
INSERT INTO `s_item_row` VALUES(10, '13m', 28000, 4, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_item_type`
--

CREATE TABLE `s_item_type` (
  `sit_code` int(11) NOT NULL AUTO_INCREMENT,
  `sit_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `sit_head` int(11) DEFAULT NULL,
  `sit_brand` int(11) NOT NULL,
  `sit_id` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `sit_url` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`sit_code`),
  KEY `sit_ibfk_1` (`sit_head`),
  KEY `sit_ibfk_2` (`sit_brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `s_item_type`
--

INSERT INTO `s_item_type` VALUES(1, 'Kites', NULL, 1, '0001', 'kites');
INSERT INTO `s_item_type` VALUES(2, 'Boards', NULL, 1, '0002', 'boards');
INSERT INTO `s_item_type` VALUES(3, 'Waveboards', NULL, 1, '0003', 'waveboards');
INSERT INTO `s_item_type` VALUES(4, 'Wakeboarding', NULL, 1, '0004', 'wakeboarding');
INSERT INTO `s_item_type` VALUES(5, 'Boards', 4, 1, '0004.0005', 'wakeboards');
INSERT INTO `s_item_type` VALUES(6, 'Vázání', 4, 1, '0004.0006', 'vazani');
INSERT INTO `s_item_type` VALUES(7, 'Zvýhodněné komplety', NULL, 1, '0007', 'zvyhodnene-komplety');
INSERT INTO `s_item_type` VALUES(8, 'Accesories', NULL, 1, '0008', 'accesories');
INSERT INTO `s_item_type` VALUES(9, 'Pads + Straps', 8, 1, '0008.0009', 'pads-straps');
INSERT INTO `s_item_type` VALUES(10, 'Fins', 8, 1, '0008.0010', 'fins');
INSERT INTO `s_item_type` VALUES(11, 'Bags', NULL, 1, '0011', 'bags');
INSERT INTO `s_item_type` VALUES(12, 'Oblečení', NULL, 1, '0012', 'obleceni');
INSERT INTO `s_item_type` VALUES(13, 'Trička', 12, 1, '0012.0013', 'tricka');
INSERT INTO `s_item_type` VALUES(14, 'Mikiny', 12, 1, '0012.0014', 'Mikiny');
INSERT INTO `s_item_type` VALUES(15, 'Šortky', 12, 1, '0012.0015', 'sortky');
INSERT INTO `s_item_type` VALUES(16, 'Čepice', 12, 1, '0012.0016', 'cepice');
INSERT INTO `s_item_type` VALUES(18, 'Boards', NULL, 2, '0018', 'boards');
INSERT INTO `s_item_type` VALUES(19, 'SNK Boards', NULL, 2, '0019', 'snk-boards');
INSERT INTO `s_item_type` VALUES(20, 'Oblečení', NULL, 2, '0020', 'obleceni');
INSERT INTO `s_item_type` VALUES(22, 'Snowkites', NULL, 3, '0022', 'snowkites');
INSERT INTO `s_item_type` VALUES(23, 'Trapézy', NULL, 4, '0023', 'trapezy');
INSERT INTO `s_item_type` VALUES(24, 'Neoprény', NULL, 4, '0024', 'neopreny');
INSERT INTO `s_item_type` VALUES(25, 'Oblečení', NULL, 4, '0025', 'obleceni');
INSERT INTO `s_item_type` VALUES(26, 'Trička', 25, 4, '0025.0026', 'tricka');
INSERT INTO `s_item_type` VALUES(27, 'Mikiny', 25, 4, '0025.0027', 'mikiny');
INSERT INTO `s_item_type` VALUES(28, 'Šortky', 25, 4, '0025.0028', 'sortky');
INSERT INTO `s_item_type` VALUES(29, 'Pady', NULL, 5, '0029', 'pady');
INSERT INTO `s_item_type` VALUES(30, 'Strapy', NULL, 5, '0030', 'strapy');
INSERT INTO `s_item_type` VALUES(31, 'Handles', NULL, 5, '0031', 'Handles');
INSERT INTO `s_item_type` VALUES(32, 'Trapézy', NULL, 5, '0032', 'trapezy');

-- --------------------------------------------------------

--
-- Table structure for table `transport`
--

CREATE TABLE `transport` (
  `tr_code` int(11) NOT NULL AUTO_INCREMENT,
  `tr_name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `tr_price` int(6) NOT NULL,
  PRIMARY KEY (`tr_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `transport`
--

INSERT INTO `transport` VALUES(1, 'Česká pošta - obchodní balík', 150);
INSERT INTO `transport` VALUES(2, 'Osobní odběr - na MKB nebo jinde po dohodě', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_code` int(11) NOT NULL AUTO_INCREMENT,
  `u_nick` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `u_mail` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `u_date_create` datetime NOT NULL,
  `u_passwd` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `u_ip` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `u_main_addr` int(11) DEFAULT NULL,
  `u_activation` varchar(32) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`u_code`),
  UNIQUE KEY `u_nick` (`u_nick`),
  KEY `user_ibfk_1` (`u_main_addr`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` VALUES(1, 'admin', 'ds@ds.cs', '2010-09-14 09:17:19', 'ff26962881cd3866f57b13885382a811', '11', 1, NULL);
INSERT INTO `user` VALUES(4, 'terihei', 'terihei@gmail.com', '0000-00-00 00:00:00', '918cdca64d8b55ad150a81151c54a44d', '::1', 2, '966502086b55745c006e2ea79dbd6250');
INSERT INTO `user` VALUES(5, 'bambala', 's@s.s', '0000-00-00 00:00:00', 'a2c59a6418be444cabb8e67d89ff2f9c', '::1', 4, '9131d3d7c14debe57cdf4db04b292721');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `ua_code` int(11) NOT NULL AUTO_INCREMENT,
  `ua_abbr` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `ua_user` int(11) NOT NULL,
  `ua_name` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `ua_surname` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `ua_company` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ua_street` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `ua_town` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `ua_psc` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `ua_state` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `ua_tel` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `ua_ico` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `ua_dic` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`ua_code`),
  KEY `user_address_ibfk_1` (`ua_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` VALUES(1, 'main_addr_1', 1, 'admin', 'admin', '', 'adminova 4', 'hnolulu', '444', 'Česká republika', '434343', '', '');
INSERT INTO `user_address` VALUES(2, 'main_addr_4', 4, 'dsd', 'dsd', '', 'dsd', 'ds', 'ds', 'Česká republika', 'dsd', '', '');
INSERT INTO `user_address` VALUES(4, 'main_addr_5', 5, 'dsd', 'kj', '', 'j', 'kj', 'jkj', 'Česká republika', 'jk', '', '');
INSERT INTO `user_address` VALUES(5, 'nova adresa', 1, 'brouk', 'pytlik', '', 'honluu', 'hawaii', '444', 'Česká republika', '4444', '', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fpost`
--
ALTER TABLE `fpost`
  ADD CONSTRAINT `fpost_ibfk_1` FOREIGN KEY (`f_user`) REFERENCES `user` (`u_code`),
  ADD CONSTRAINT `fpost_ibfk_2` FOREIGN KEY (`f_father`) REFERENCES `fpost` (`f_code`);

--
-- Constraints for table `shop_item`
--
ALTER TABLE `shop_item`
  ADD CONSTRAINT `si_ibfk_1` FOREIGN KEY (`si_type`) REFERENCES `s_item_type` (`sit_code`),
  ADD CONSTRAINT `si_ibfk_2` FOREIGN KEY (`si_brand`) REFERENCES `s_item_brand` (`sib_code`);

--
-- Constraints for table `s_item_row`
--
ALTER TABLE `s_item_row`
  ADD CONSTRAINT `sir_ibfk_1` FOREIGN KEY (`sir_head`) REFERENCES `shop_item` (`si_code`);

--
-- Constraints for table `s_item_type`
--
ALTER TABLE `s_item_type`
  ADD CONSTRAINT `sit_ibfk_1` FOREIGN KEY (`sit_head`) REFERENCES `s_item_type` (`sit_code`),
  ADD CONSTRAINT `sit_ibfk_2` FOREIGN KEY (`sit_brand`) REFERENCES `s_item_brand` (`sib_code`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`u_main_addr`) REFERENCES `user_address` (`ua_code`) ON DELETE SET NULL;

--
-- Constraints for table `user_address`
--
ALTER TABLE `user_address`
  ADD CONSTRAINT `user_address_ibfk_1` FOREIGN KEY (`ua_user`) REFERENCES `user` (`u_code`) ON DELETE CASCADE;
