<?php

/**
 * Element pro checkbox
 */
class EditBool extends EditField {
  
  /**
   * Konstruktor
   */
  function __construct($aName, $aPrompt, $aPromptWidth) {
    parent::__construct($aName, $aPrompt, $aPromptWidth, false, 0, 0);
    
  } 
  
  /***********************************************************************************************/
  /************************************* Prevod do HTML ******************************************/
  
  /**
   * Prevod vlastni polozky do html
   */
  function toHtml() {
    echo "<td style='width:".$this->mSize."px'>";
    echo "<input type='checkbox' ".(isset($_POST[$this->mName]) ? "checked='true'" : "");
    echo " name='".$this->mName."' id='".$this->mName."'";
    
    // atributy
    if ($this->mFieldAttrs != null)
      echo " ".$this->getAttrsStr($this->mFieldAttrs);
    
    echo "/></td>";
  }
  
  /***********************************************************************************************/
  /**************************************   Validace    ******************************************/
  
  /**
   * Validace hodnoty
   */
  function validateField() {
    return true;
  }
}

?>