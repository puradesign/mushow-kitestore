<?php

/**
 * Zakladni element na prezentacni vrstve
 */
class ButtonImg extends EditField {
  var $mImgPath;
  var $mConfirmStr = null;
  
  /**
   * Konstruktor
   */
  function __construct($aName, $aText, $aImg) {
    parent::__construct($aName, $aText, 0, false, 0, 0);
    
    $this->mImgPath = $aImg;
  }
  
  /***********************************************************************************************/
  /************************************** Pristup. metody ****************************************/
  
  /**
   * Nastavi dotaz
   */
  function setConfirmStr($aStr) {
    $this->mConfirmStr = $aStr;
  }
  
  /***********************************************************************************************/
  /**************************************Prevod do HTML ******************************************/
  
  /**
   * Vygenerovani html
   */
  function toHtmlLow() {
    $lName = $this->mName;
    
    echo "<input type='submit' style='visibility:hidden; height: 0px; width:0px; ";
    echo "float:none; display:none' title='".$this->mPrompt."' id='$lName' name='$lName' value='".$this->mPrompt."'";
 
    // atributy
    if ($this->mAttrs != null)
      echo " ".$this->getAttrsStr($this->mAttrs);
    
    echo "/>";
    
    $query = "clickButton(\"$lName\");"; 
    
    if ($this->mConfirmStr != null)
      $query = "if (confirm(\"".$this->mConfirmStr."\"))".$query;
    
    // obrazek
    echo "<a href='' onclick='$query return false;' style='position:relative:display:block:float:left'>";
    echo "<img src='".$this->mImgPath."' alt='".$this->mPrompt."'";
    
    // id polozky
    if ($this->mID != null && $this->mID != "")
      echo " id='".$this->mID."'";
    
    // styl
    if ($this->mStyles != null)
      echo " ".$this->getStylesStr($this->mStyles);
    
    echo "/></a>";
  }
  
  
  
  /***********************************************************************************************/
  /**************************************   Validace    ******************************************/
  
  /**
   * Validace hodnoty
   */
  function validateField() {
    return true;
  }
}

?>