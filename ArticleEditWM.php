<?php
require_once "SimpleImage.php";
require_once "ArticleUtil.php";

class ArticleEditWM extends WebModule {
  var $mAction;
  var $mHeader = "Přidat článek";
  var $mActRow;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {

    if (!isset($_GET["action"]))
      $this->mAction = "add";
    else
      $this->mAction = $_GET["action"];

    if (($this->mAction != "add" && $this->mAction != "edit") ||
      ($this->mAction == "edit" && (!isset($_GET["item"]) || !is_numeric($_GET["item"])))) {
      $GLOBALS["rv"]->addError("Chyba. Pravděpodobně byla ručně upravena url stránky.");
      $this->setForOutput(false);
      return false;
    }

    // editace
    if ($this->mAction == "edit") {

      // kontrola prava na editaci
      if (!validateArticleEdit()) {
        $this->setForOutput(false);
        return false;
      }

      $query = "SELECT * FROM article WHERE a_code=" . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);

      $row = $result->fetch_assoc();

      $this->mActRow = $row;
      $this->mHeader = alterHtmlTextToPlain($row["a_title"]) . " - editace";

      if (empty($_POST)) {
        $_POST["a_title"] = alterHtmlTextToPlain($row["a_title"]);
        $_POST["a_description"] = alterHtmlTextToPlain($row["a_description"]);
        $_POST["a_text"] = $row["a_text"];
      }
    } elseif ($this->mAction == "add") {
      // kontroly
      if (!isLogged()) {
        $GLOBALS["rv"]->addError("Pro přidání článku musíte být přihlášen.");
        $this->setForOutput(false);
        return false;
      }

      // tady se nic nepridava
    }

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    $title = alterTextForDB($_POST["a_title"]);
    $descr = alterTextForDB($_POST["a_description"]);
    $text = $_POST["a_text"];

    $lCond = new WhereCondition();

    // vlozeni
    if ($this->mAction == "add") {

      // --

      return false;
    }

    // editace
    elseif ($this->mAction == "edit") {

      // upraveni odkazu na obrazky aby se otviraly v shadowboxu
      $text = addShadowboxToLinks($text);

      // update v DB
      $query = "UPDATE article SET `a_title` = '" . alterTextForDB($title) . "'";
      $query .= ", `a_description` = '" . alterTextForDB($descr) . "', `a_date_update` = NOW()";
      $query .= ", `a_text` = '" . addSlashes($text) . "'";

      $query .= " WHERE `a_code` = " . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError($query . 'OOps, neco se stalo. Pokud potize pretrvaji, kontaktujte spravce:' . $result->error);
        return true;
      }

      $GLOBALS["rv"]->addInfo("Změny uloženy.");

      return true;
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {

    return $this->mHeader;
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $whereCond = new WhereCondition();
    $lOffset = 100;

    // Nazev
    $lEF = new EditField("a_title", "Název", $lOffset, true,
      330, 50);
    $this->addElement($lEF);

    // Popis
    $lEF = new EditText("a_description", "Popis", $lOffset, true,
      46, 330, 5);
    $this->addElement($lEF);

    // Text
    $lEF = new EditCKText("a_text", "Text", true);
    $this->addElement($lEF);
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    //	 echo "<p>Přidání/editace inzerátu.</p>";

    echo "  <form method='post' id='edit_form' action='" . WR . "?m=" . A_EDIT . "&amp;action=" . $this->mAction;
    echo ($this->mAction != "add" ? "&amp;item=" . $_GET["item"] : "") . "'>";

    echo "  <fieldset class='form'>";
    $this->printElements();
    echo "  </fieldset>";

    echo "  <fieldset class='form' style='padding-top:5px; padding-bottom:3px'><div class='td_left' style='height:100%;width: 80px;'>";
    echo "  <input type='submit' class='submit' value='Uložit'/></div>";
    echo "<p>(<span style='color:#551310;font-weight:bold;font-size:14px;'>*</span>Položky označené hvězdičkou jsou povinné.)</p>";
    echo "  </fieldset></form>";
  }

  /**
   * Prida potrebne skripty modulu
   */
  function addScripts() {
    ?>

    <script type='text/javascript' src='<?= F_CKEDITOR ?>ckeditor.js'></script>
    <script type='text/javascript' src='<?= WR_SCRIPT ?>ckeditor_conf.js'></script>
    <script>
      $(document).ready(function () {
        CKEDITOR.config.customConfig = '<?= WR_SCRIPT ?>ckeditor_conf.js';
        CKEDITOR.replace('a_text');
      });
    </script>

    <?php
  }
}
?>