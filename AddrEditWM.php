<?php
require_once "StoreUtil.php";

class AddrEditWM extends WebModule {
  var $mAction;
  var $mHeader = "Dodací adresa";
  var $mActRow;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    // kontroly
    if (!isLogged()) {
      $GLOBALS["rv"]->addError("Pro vložení/editaci dodací adresy musíte být přihlášen.");
      $this->setForOutput(false);
      return false;
    }

    if (!isset($_GET["action"]))
      $this->mAction = "add";
    else
      $this->mAction = $_GET["action"];

    if (($this->mAction != "add" && $this->mAction != "edit") ||
      ($this->mAction == "edit" && (!isset($_GET["item"]) || !is_numeric($_GET["item"])))) {
      $GLOBALS["rv"]->addError("Chyba. Pravděpodobně byla ručně upravena url stránky.");
      $this->setForOutput(false);
      return false;
    }

    if ($this->mAction == "edit") {

      $query = "SELECT * FROM user_address WHERE ua_code=" . $_GET["item"];
      $result = $GLOBALS["db"]->query($query);

      $row = $result->fetch_assoc();

      if ($row["ua_user"] != $_SESSION[SN_CODE]) {
        $GLOBALS["rv"]->addError("Nemáte právo editovat tuto adresu.");
        $this->setForOutput(false);
        return false;
      }

      $this->mActRow = $row;
      $this->mHeader = alterHtmlTextToPlain($row["ua_abbr"]);

      if (empty($_POST)) {
        $_POST["ua_abbr"] = $row["ua_abbr"];
        $_POST["ua_company"] = $row["ua_company"];
        $_POST["ua_name"] = $row["ua_name"];
        $_POST["ua_surname"] = $row["ua_surname"];
        $_POST["ua_street"] = $row["ua_street"];
        $_POST["ua_town"] = $row["ua_town"];
        $_POST["ua_psc"] = $row["ua_psc"];
        $_POST["ua_state"] = $row["ua_state"];
        $_POST["ua_tel"] = $row["ua_tel"];
        $_POST["ua_ico"] = $row["ua_ico"];
        $_POST["ua_dic"] = $row["ua_dic"];
      }
    }

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    $abbr = alterTextForDB($_POST["ua_abbr"]);
    $company = alterTextForDB($_POST["ua_company"]);
    $name = alterTextForDB($_POST["ua_name"]);
    $surname = alterTextForDB($_POST["ua_surname"]);
    $street = alterTextForDB($_POST["ua_street"]);
    $town = alterTextForDB($_POST["ua_town"]);
    $psc = alterTextForDB($_POST["ua_psc"]);
    $state = alterTextForDB($_POST["ua_state"]);
    $tel = alterTextForDB($_POST["ua_tel"]);
    $ico = alterTextForDB($_POST["ua_ico"]);
    $dic = alterTextForDB($_POST["ua_dic"]);

    $lCond = new WhereCondition();

    // vlozeni
    if ($this->mAction == "add") {

      $lUserCode = $_SESSION[SN_CODE];

      // -----------------
      // vlozeni adresy

      $query = "INSERT INTO user_address (`ua_abbr`, `ua_user`, `ua_name`, `ua_surname`, `ua_company`";
      $query .= ", `ua_street`, `ua_town`, `ua_psc`, `ua_state`, `ua_tel`, `ua_ico`, `ua_dic`)";
      $query .= " VALUES ('$abbr', $lUserCode, '$name', '$surname', '$company'";
      $query .= ", '$street', '$town', '$psc', '$state', '$tel', '$ico', '$dic')";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);

        return true;
      }

      if ($GLOBALS['wm'] == $this) {
        $_POST = array();

        $GLOBALS["rv"]->addInfo("Adresa byla vytvořena.");

        // presmerovani na prihlaseni
        require_once "AddrsWM.php";
        $GLOBALS["wm"] = new AddrsWM(ADDRS);
        $GLOBALS["wm"]->reactOnActionLow();
      }

      return false;
    }

    // editace
    elseif ($this->mAction == "edit") {

      // update v DB fakturacnich udaju
      $query = "UPDATE user_address SET `ua_abbr` = '$abbr', `ua_company` = '$company', `ua_name` = '$name'";
      $query .= ", `ua_surname` = '$surname', `ua_street` = '$street', `ua_town` = '$town'";
      $query .= ", `ua_psc` = '$psc', `ua_state` = '$state', `ua_tel` = '$tel'";
      $query .= ", `ua_ico` = '$ico', `ua_dic` = '$dic'";
      $query .= " WHERE `ua_code` = " . $this->mActRow["ua_code"] . " LIMIT 1";
      $result = $GLOBALS["db"]->query($query);

      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba v SQL:" . $result->error);
        return true;
      }

      $GLOBALS["rv"]->addInfo("Adresa byla upravena.");

      // presmerovani na prehled adres
      require_once "AddrsWM.php";
      $GLOBALS["wm"] = new AddrsWM(ADDRS);
      $GLOBALS["wm"]->reactOnActionLow();

      return true;
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    $str = $this->mHeader . " - ";

    if ($this->mAction == "add")
      $str .= "Přidat";
    elseif ($this->mAction == "edit")
      $str .= "Editace";

    return $str;
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {
    $lPromptWidth = 120;
    $lInputWidth = 200;
    $lInputWidth2 = 120;
    $whereCond = new WhereCondition();
    $lIsEdit = $this->mAction == "edit";

    $lEF = new PresentElement("<fieldset class='form'>");
    $this->addElement($lEF);

    // Nazev
    $lEF = new EditField("ua_abbr", "Název (zkratka)", $lPromptWidth, true, $lInputWidth2, 20);
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset><fieldset class='form'>");
    $this->addElement($lEF);

    // Firma
    $lEF = new EditField("ua_company", "Firma", $lPromptWidth, false, $lInputWidth2, 50);
    $this->addElement($lEF);

    // Jmeno
    $lEF = new EditField("ua_name", "Jméno", $lPromptWidth, true, $lInputWidth2, 30);
    $this->addElement($lEF);

    // Prijmeni
    $lEF = new EditField("ua_surname", "Příjmení", $lPromptWidth, true, $lInputWidth2, 30);
    $this->addElement($lEF);

    // Ulice
    $lEF = new EditField("ua_street", "Ulice a č.p.", $lPromptWidth, true, $lInputWidth, 100);
    $this->addElement($lEF);

    // Mesto
    $lEF = new EditField("ua_town", " Město", $lPromptWidth, true, $lInputWidth2, 100);
    $this->addElement($lEF);

    // PSC
    $lEF = new EditField("ua_psc", "PSČ", $lPromptWidth, true, 50, 10);
    $this->addElement($lEF);

    // Stat
    $lEF = new EditCombo("ua_state", "Stát", $lPromptWidth, false, 200);
    fillStatesCombo($lEF);
    $this->addElement($lEF);

    // Telefon
    $lEF = new EditField("ua_tel", " Telefon", $lPromptWidth, true, $lInputWidth2, 20);
    $this->addElement($lEF);

    // ICO
    $lEF = new EditField("ua_ico", "IČO", $lPromptWidth, false, $lInputWidth2, 20);
    $this->addElement($lEF);

    // DIC
    $lEF = new EditField("ua_dic", "DIČ", $lPromptWidth, false, $lInputWidth2, 20);
    $this->addElement($lEF);

    $lEF = new PresentElement("</fieldset>");
    $this->addElement($lEF);
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    echo "<p>Přidání/editace dodací adresy.</p>";
    echo "  <form method='post' id='edit_form' action='" . WR . "?m=" . ADDR_EDIT . "&amp;action=" . $this->mAction;
    echo ($this->mAction != "add" ? "&amp;item=" . $_GET["item"] : "") . "'>";

    $this->printElements();

    echo "  <fieldset class='form'><div class='td_left' style='height:100%;width: 100px;'>";
    echo "  <input type='submit' class='submit' value='Potvrdit'/></div>";

    echo "  </fieldset></form>";

    echo "<p>(<span style='color:#551310;font-weight:bold;font-size:14px;'>*</span>Položky označené hvězdičkou jsou povinné.)</p>";

    echo "<p><a href='" . WR . "?m=" . ADDRS . "'>Zpět na přehled adres</a></p>";
  }
}
?>