<?php
// cesty ke slozkam
define("F_ROOT", dirname(__FILE__).DIRECTORY_SEPARATOR);
define("F_USER_FILES", F_ROOT."user_files".DIRECTORY_SEPARATOR);
define("F_ALBUMS", F_USER_FILES."albums".DIRECTORY_SEPARATOR);
define("F_USER_IMAGES", F_USER_FILES."images".DIRECTORY_SEPARATOR);
define("F_IMG",    F_ROOT."img".DIRECTORY_SEPARATOR);
define("F_ARTICLES", F_USER_FILES."articles".DIRECTORY_SEPARATOR);
define("F_HTML", F_ROOT."htmlUtil".DIRECTORY_SEPARATOR);
define("F_SHOP_ITEMS", F_USER_FILES."shop_items".DIRECTORY_SEPARATOR);
define("F_BAZAR", F_USER_FILES."bazar".DIRECTORY_SEPARATOR);

// *****************************************************************//
// SESSION konstanty
define("SN_CODE",    "ks_code");
define("SN_NICK",    "ks_nick");
define("SN_MAIL",    "ks_mail");
define("SN_LOGGED",  "ks_logged");
define("SN_HOMEDIR", "ks_homedir");
define("SN_LASTDIR", "ks_lastdir");
define("SN_ADMIN",   "ks_admin");
define("SN_AT",      "ks_at"); 
define("SN_LASTWM",  "ks_lastwm"); 
define("SN_LASTGET", "ks_lastget");
define("SN_TEXTS",   "ks_texts");
define("SN_LANG",    "ks_lang");
define("SN_ORDERBY", "ks_orderby");
define("SN_CART",    "ks_cart");
define("SN_MAIN_ADDR",    "ks_maddr");
define("SN_B_CATEG",  "ks_bcateg");
define("SN_B_TYPE",   "ks_btype");

// webove cesty
define("WR", "http://localhost/kiteshop/");
define("WRS", WR);
define("WR_PLAIN", WR);
define("F_SHADOWBOX", WR."lib/shadowbox/");
define("F_CKEDITOR", WR."lib/ckeditor/");
define("WR_SCRIPT", WR."script/");

define("WR_ART", WR."news/");
define("WR_GAL", WR."foto-a-video/");
define("WR_FOR", WR."forum/");
define("WR_IMG", WR."img/");
define("WR_IMAGES", WR."user_files/images/");
define("WR_CART", WR."shopping-cart");
define("WR_PROFILE", WR."edit-profile");
define("WR_CONTACT", WR."kontakt");
define("WR_BAZAR", WR."bazar");
define("WR_PODMINKY", WR."obchodni-podminky");
define("WR_GDPR", WR."zpracovani-osobnich-udaju");
define("WR_ORDER", WR."objednavky");
define("WR_SEARCH", WR."hledat");

define("WR_HOT_STUFF", WR."hot");
define("WR_ONSALE", WR."onsale");
define("WR_SHOP_NEWS", WR."shop-news");

// jednotlive moduly
define("NEWS",     0);
define("ARTICLES", 1);
define("GALLERY",  2);
define("A_VIEW",   3);
define("A_ADD",    4);
define("A_EDIT",      5);
define("A_DELETE",    6);

define("LOGIN",       10);
define("LOGOUT",      11);

define("G_ADD_PICS",  12);
define("G_EDIT_PICS", 13);
define("G_ADD",       14);
define("G_EDIT",      15);
define("G_DELETE",    16);
define("G_VIEW",      17);

define("FORUM",       18);
define("F_ADD_POST",  19);

define("CHANGE_LANG", 20);

define("S_CATEG",     21);
define("S_CATEG_EDIT",22);
define("S_CATEG_DEL", 23);

define("SHOP",        24);
define("SHOP_EDIT",   25);
define("SI_EDIT",     26);
define("SI_DELETE",   27);
define("SI_VIEW",     28);

define("REGISTER",    29);
define("PROF_EDIT",   30);
define("ADDRS",       31);
define("ADDR_EDIT",   32);

define("CART",        33);
define("FIX_TEXT",    34);

define("BAZAR",       35);
define("B_VIEW",      36);
define("B_EDIT",      37);

define("ORDERS",      38);
define("OR_VIEW",     39);
define("SHOP_NEWS",   40);
define("SKLAD",       41);
define("SEARCH",      42);
define("OR_EDIT",     43);

define("USERS",       44);

define("NS_BROWSER",  45);
define("NS_EDIT",     46);
define("NS_VIEW",     47);

define("CONDS_GDPR",     48);


// typy videa v_type v tabulce video
define("V_UNDEF", 0);
define("YOUTUBE", 1);
define("VIMEO",   2);

// sekce clanku
define("A_NONE",  0);
define("A_NEWS",  1);

// *****************************************************************//
// Konstanty pro polozky sessiony
define("F_LAST_PAGE", "f_page");

// *****************************************************************//
// Konstanty pro polozky menu
define("MENU_NONE",       0);
define("MENU_FORUM",      1);
define("MENU_NEWS",       2);
define("MENU_INTRO",      3);
define("MENU_SPOT",       4);
define("MENU_KITESCHOOL", 5);
define("MENU_TEST",       6);
define("MENU_REPAIR",     7);
define("MENU_ACCOM",      8);
define("MENU_TRANSPORT",  9);
define("MENU_TRIPS",      10);
define("MENU_CONTACTS",   11);
define("MENU_GALL",       12);


// *****************************************************************//
// GLOBALS konstanty
define("GL_BRAND",   "ks_brand");
define("GL_CATEGS",  "ks_categs");

// *****************************************************************//
// jazyky
define("LANG_EN", "en");
define("LANG_CS", "cs");
define("LANG_FR", "fr");

define("LANG_CODE_EN", 2);
define("LANG_CODE_CS", 3);
define("LANG_CODE_FR", 5);

?>
