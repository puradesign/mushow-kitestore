<?php
$lPath = isset($_SESSION["rel_path"]) ? $_SESSION["rel_path"] . "kitestore/" : "";

require_once $lPath . "connect.php";
require_once $lPath . "const.php";
require_once $lPath . "UtilFunctions.php";

$lStyle = "width:120px;float:left; height:232px;margin:5px 0px 5px 2px;";
$lStyle .= "background: url(\"" . WR_IMG . "ks_banner_bg.jpg\")";

if (isset($_GET["banner_stajla"]) && $_GET["banner_stajla"] == "wg")
  $lStyle = "width:120px;float:left; height:232px;margin:0px;background: url(\"" . WR_IMG . "ks_banner_wg.jpg\")";

echo "<div id='ks_banner' style='$lStyle'>";

$lTable = "shop_item LEFT JOIN s_item_type ON shop_item.si_type = s_item_type.sit_code";
$lTable .= " LEFT JOIN s_item_brand ON si_brand = s_item_brand.sib_code";

$query = "SELECT * FROM $lTable WHERE si_online = 1 AND si_banner=1 LIMIT 6";
$result = mysql_query($query);

if (!$result)
  echo "<p style='display:none'>" . $result->error . "</p>";

echo "<ul style='margin:61px 0px 0px 7px;height:146px;display:none;width:106px;padding:0px;list-style-type:none;letter-spacing:-1px;'>";

// projdou se vsechny polozky
while ($row = $result->fetch_assoc()) {

  $lUrl = WR . $row["sib_url"] . "/" . $row["sit_url"] . "/v/" . $row["si_url"];
  $lImgUrl = getBrowserPath(F_SHOP_ITEMS . "headfoto" . $row["si_code"] . ".jpg");
  $title = UTF8ToEntities($row["si_title"]);

  echo "<li style='padding:0px'><a href='$lUrl'><img src='$lImgUrl' height='106px' alt='" . $title . "' style='' title='" . $title . "'/></a>";
  echo "<p style='text-align:center; margin-top:4px;margin-bottom:0px'><a href='$lUrl' style='font-size:11px;color:#270101; font-weight:bold'>" . $title . "</a></p>";

  // puvodni cena
  $lOnSale = $row["si_fake_price"] != null && $row["si_fake_price"] > 0;

  //	echo "<div style='margin-top:".($lOnSale ? "4px" : "14px").";color:black;text-align:left;font-size:9px;font-family:Verdana, Geneva, Arial;width:112px;margin-left:-3px;'>";
  echo "<div style='margin-top:" . ($lOnSale ? "4px" : "14px") . ";color:black;text-align:left;font-size:9px;font-family:Verdana, Geneva, Arial;width:112px;margin-left:-3px;'>";

  $lCenaPrompt = "cena";
  if ($lOnSale) {
    echo "<p style='margin:0px'>p�vodn� cena: <span style='text-decoration:line-through;'>" . number_format($row["si_fake_price"], 0, "", " ") . " K�</span></p>";
    $lCenaPrompt = "nov� " . $lCenaPrompt;
  }

  echo "<p style='color:black;text-align:left;font-size:9px;margin:0px'>";
  echo $lCenaPrompt . ": <span style='font-size:12px;font-weight:bold'>" . number_format($row["si_price"], 0, "", " ") . " K�</span></p>";

  echo "</div></li>";
}

echo "</ul></div>";
?>