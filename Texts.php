<?php
/**
 * Trida pro praci s objekty
 */
class TextsObject {
  var mTextsArray = array();

  /**
	 * Inicializace textu
	 */
	function initTexts($lang = 2) {
	  mTextsArray = array();
	  
	  if ($lang == 2)
	    initTextsEN();
	  elseif ($lang == 3)
	    initTextsCS();
	  elseif ($lang == 5)
	    initTextsFR();
	}
	
	/**
	 * Vrat text
	 */
	function getText($id) {
		
		return this->mTextsArray[$id];
	}

	/**
	 * Inicializace ceskych textu
	 */
	private function initTextsCS() {
	  
	  // menu
	  mTextsArray['menu1'] = "News";
	  
	  
	  
	}
	
	/**
	 * Inicializace ceskych textu
	 */
	private function initTextsEN() {
	  
	  // menu
	  mTextsArray['menu1'] = "News";
	  
	  
	  
	}
	
	/**
	 * Inicializace ceskych textu
	 */
	private function initTextsFR() {
	  
	  // menu
	  mTextsArray['menu1'] = "Nouvelles";
	  
	  
	  
	}
}

?>