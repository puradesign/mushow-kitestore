<?php

/**
 * Trida pro praci s kosikem
 */
class CartObject {
  var $mItems = array(); // polozka [ 0=kod polozky, 1=kod varianty, 2=pocet kusu, 3=nazev, 4=cena, url]
  var $mItemsRef = array(); // pole klicu hlavniho pole

  var $mAddress = null; // vybrana dodaci adresa
  var $mTransport = null;
  var $mPayType = null;
  var $mDescr = null;
  var $mDiscount = 0;

  var $mUser = 0; // uzivatel
  var $mUserAddress = 0; // hlavni adresa uzivatele

  // ------------------------------------------------------- //
  // ------------------------------------------------------- //

  /**
   * Pridani polozky
   */
  function addItem($aItemCode, $aRowCode, $aName, $aPrice, $aUrl) {
    //$GLOBALS["rv"]->addInfo("pridani polozky ".$aVarCode);

    // pokud uz je v kosiku
    if (isset($this->mItems[$aRowCode])) {
      $this->mItems[$aRowCode][2] += 1;
      return;
    }
    // pokud nenajdem, pridame polozku
    else {
      /*$query = "SELECT * FROM s_item_row LEFT JOIN shop_item ON sir_head=shop_item.si_code WHERE sir_code=$aRowCode";
        $result = $GLOBALS["db"]->query($query);
        $row = $result->fetch_assoc();*/

      $this->mItems[$aRowCode] = array($aItemCode, $aRowCode, 1, $aName, $aPrice, $aUrl);
      $this->mItemsRef[] = $aRowCode;
    }
  }

  /**
   * Smazani polozky
   */
  function deleteItem($aOrder) {
    //$GLOBALS["rv"]->addInfo("pridani polozky ".$aVarCode);

    // pokud uz je v kosiku
    if (isset($this->mItemsRef[$aOrder])) {
      $lRowCode = $this->mItemsRef[$aOrder];

      $lNewArray = array();

      // vytvorime nove pole klicu bez vymazaneho
      for ($i = 0; $i < count($this->mItems); $i++) {
        if ($this->getRowCode($i) == $lRowCode)
          continue;

        $lNewArray[] = $this->getRowCode($i);
      }

      unset($this->mItems[$lRowCode]);
      $this->mItemsRef = $lNewArray;
    }
  }

  // ------------------------------------------------------- //

  /**
   * Vraceni pole hodnot polozky
   */
  function getItemByOrder($aOrder) {
    if ($aOrder > $this->getCount() - 1)
      return false;

    $lCode = $this->mItemsRef[$aOrder];

    return $this->mItems[$lCode];
  }

  /**
   * Vraci kod polozky na dane pozici
   */
  function getItemCode($aOrder) {
    $lItem = $this->getItemByOrder($aOrder);
    return $lItem[0];
  }

  /**
   * Vraci kod varianty polozky na dane pozici
   */
  function getRowCode($aOrder) {
    $lItem = $this->getItemByOrder($aOrder);
    return $lItem[1];
  }

  /**
   * Vraci pocet polozky na dane pozici
   */
  function getItemCount($aOrder) {
    $lItem = $this->getItemByOrder($aOrder);
    return $lItem[2];
  }

  /**
   * Nastavi pocet yboyi polozky na dane pozici
   */
  function setItemCount($aOrder, $aCount) {
    $lCode = $this->getRowCode($aOrder);
    $this->mItems[$lCode][2] = $aCount;
  }

  // ------------------------------------------------------- //

  /**
   * Vraci pocet polozek
   */
  function getCount() {
    return count($this->mItems);
  }

  /**
   * Vraci pocet vsech polozek i s poctem vyskytu
   */
  function getTotalCount() {
    $lTotal = 0;

    for ($i = 0; $i < count($this->mItems); $i++)
      $lTotal += $this->getItemCount($i);

    return $lTotal;
  }

  /**
   * Vraci prazdnost kosiku
   */
  function isEmpty() {
    return count($this->mItems) == 0;
  }

  // ------------------------------------------------------- //
  // ------------------------------------------------------- //

  /**
   * Vraci celkovou castku
   */
  function getCashTotal() {
    $lTotal = 0;

    for ($i = 0; $i < count($this->mItems); $i++) {
      $lItem = $this->getItemByOrder($i);

      $lTotal += $lItem[2] * $lItem[4];
    }

    return $lTotal - $lTotal * $this->mDiscount / 100;
  }

  /**
   * Nastavi kod adresy
   */
  function setAddress($aCode) {
    $this->mAddress = $aCode;
  }

  /**
   * Vraci kod adresy
   */
  function getAddress() {
    return $this->mAddress;
  }

  /**
   * Nastavi kod dopravy
   */
  function setTransport($aCode) {
    $this->mTransport = $aCode;
  }

  /**
   * Vraci kod dopravy
   */
  function getTransport() {
    return $this->mTransport;
  }

  /**
   * Nastavi kod platby
   */
  function setPayType($aCode) {
    $this->mPayType = $aCode;
  }

  /**
   * Vraci kod platby
   */
  function getPayType() {
    return $this->mPayType;
  }

  /**
   * Nastavi poynamku
   */
  function setDescr($aDescr) {
    $this->mDescr = $aDescr;
  }

  /**
   * Vraci poznamku
   */
  function getDescr() {
    return $this->mDescr;
  }

  /**
   * Nastavi slevu
   */
  function setDiscount($aDisc) {
    $this->mDiscount = $aDisc;
  }

  /**
   * Vraci slevu
   */
  function getDiscount() {
    return $this->mDiscount;
  }

  /**
   * Nastavi uzivatele
   */
  function setUser($aUser) {
    $this->mUser = $aUser;
  }

  /**
   * Vraci uzivatele
   */
  function getUser() {
    if ($this->mUser == 0 && isset($_SESSION[SN_CODE]))
      $this->mUser = $_SESSION[SN_CODE];

    return $this->mUser;
  }

  /**
   * Nastavi hlavni adresu uzivatele
   */
  function setUserAddress($aUserAddress) {
    $this->mUserAddress = $aUserAddress;
  }

  /**
   * Vraci hlavni adresu uzivatele
   */
  function getUserAddress() {
    if ($this->mUserAddress == 0 && isset($_SESSION[SN_MAIN_ADDR]))
      $this->mUserAddress = $_SESSION[SN_MAIN_ADDR];

    return $this->mUserAddress;
  }
}

?>