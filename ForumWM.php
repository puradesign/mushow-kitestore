<?php
require_once "ItemsBrowser.php";

class ForumWM extends WebModule {

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (isset($_GET["page"])) {
      $_SESSION[F_LAST_PAGE] = $_GET["page"];
    } elseif (isset($_SESSION[F_LAST_PAGE]))
      $_GET["page"] = $_SESSION[F_LAST_PAGE];

    if (isset($_GET["delete"]) && isset($_GET["post"])) {
      if (!isLogged()) {
        $GLOBALS["rv"]->addError(getRText("err19")); // "Mazat vlastní příspěvky lze pouze po přihlášení."
        return true;
      }

      $query = "SELECT f_code, f_user, f_id FROM fpost WHERE f_code =" . $_GET["post"];
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      if (!$row) {
        $GLOBALS["rv"]->addError(getRText("err20")); // "Nebyl nalezen příspěvek pro smazání."
        return true;
      }

      if (isLoggedAdmin()) {
        $query = "DELETE FROM fpost WHERE f_id LIKE '" . $row["f_id"] . "%' ORDER BY f_id DESC";
        $result = $GLOBALS["db"]->query($query);
      } elseif (!empty($row["f_user"]) && isLoggedUser($row["f_user"])) {
        $query = "SELECT f_code FROM fpost WHERE f_father = " . $_GET["post"] . " LIMIT 1";
        $result = $GLOBALS["db"]->query($query);
        $sonRow = $result->fetch_assoc();

        if ($sonRow) {
          $GLOBALS["rv"]->addError(getRText("err21")); // "Příspěvek nemůže být smazán, protože už na něj někdo reagoval."
          return true;
        }

        $query = "DELETE FROM fpost WHERE f_code = " . $_GET["post"] . " LIMIT 1";
        $result = $GLOBALS["db"]->query($query);

        $GLOBALS["rv"]->addInfo(getRText("err22")); // "Příspěvek byl smazán."
      }
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    return getRText("menu12"); // Fórum
  }

  function defineHeaderButton() {
    echo "<div class='right_top_button add_button'>";
    echo "<a href='" . WR_FOR . "?m=" . F_ADD_POST . "' style=''>";
    echo getRText("util36"); // "Nový příspěvek"
    echo "</a></div>";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/


  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput($searchCond = "", $search = false) {
    $limit = 30;
    $offset = 0;

    if (isset($_GET["page"]))
      $offset = $limit * $_GET["page"];

    // slozeni podminky
    $whereCond = new WhereCondition();

    if (!empty($searchCond))
      $whereCond->addCondStr($searchCond);

    // vytvoreni browseru a nacteni polozek pro zborazeni
    $itemsBrowser = new ItemsBrowser($limit, $offset);
    $result = $itemsBrowser->executeQuery("fpost", $whereCond, "f_head DESC, f_id ASC", "");
    if (!$result)
      die(getRText("err9") . $result->error);

    echo "<p>Zde máte příležitost napsat svoje dotazy, náměty, připomínky a problémy týkající se našich produktů. Administrátor stránek si vyhrazuje právo smazat urážlivé, nesmyslné nebo daných témat se netýkající příspěvky. Děkujeme, že na tomto fóru nepoužíváte sprostá slova!</p>";


    // nebyla nalezena zadna polozka
    if ($itemsBrowser->getCountAll() < 1) {
      if (!$search)
        echo "<br /><p>" . getRText("err23") . "</p>"; // Fórum neobsahuje žádné příspěvky.
      /* else
         echo "<br /><p class='searchCount'>Nebyly nalezeny žádné příspěvky.</p>";*/
    }

    // byla nalezena alespon jedna polozka
    else {
      // vykresleni ovladaci tabulky
      $itemsBrowser->printControlDiv($search ? WR . "?m=" . SEARCH : WR_FOR, getRText("util37")); // "příspěvků"

      // vykresleni prispevku fora na strance
      while ($row = $result->fetch_assoc()) {
        $this->printPost($row);
      }

      // vykresleni ovladaci tabulky
      $itemsBrowser->printControlDiv($search ? WR . "?m=" . SEARCH : WR_FOR, getRText("util37")); // "příspěvků"
    }
  }

  function printPost($row) {
    $indent = preg_match_all("/\./", $row["f_id"], $matches) * 20;

    echo "<div class='fpost' style='margin-left:" . $indent . "px; width: " . (490 - $indent) . "px; " . ($indent == 0 ? "margin-top: 10px;" : "") . "'>";
    echo "<a name='" . $row["f_code"] . "'></a>";

    $lName = $row["f_name"];
    if ($row["f_name"] == "admin" && $row["f_user"] == 1)
      $lName = "<a href='" . WR . "'>Mushow Kitestore</a>";

    $this->printPostHeader($lName, $row["f_web"], $row["f_user"]);

    $date = strtotime($row["f_date"]);
    $date = StrFTime("%d.%m.%Y, %H:%M:%S", $date);
    echo "<div class='rightup'>" . $date . "</div>";
    echo $row["f_text"];
    echo "<p class='post_link'>";

    if (isLoggedAdmin()) {
      echo "<a href='" . WR_FOR . "?m=" . F_ADD_POST . "&amp;post=" . $row["f_code"] . "'>";
      echo getRText("util38") . "</a>"; // Odpovědět
    }

    printDeleteLink($row["f_code"], $row["f_user"]);

    echo "</p></div>";
  }

  /**
   * Pro prepsani - vraci ID polozky v menu, ktera patri k tomuto WM (podle menu konstant)
   */
  function getMenuItemID() {
    return MENU_FORUM;
  }

  function printPostHeader($name, $web, $user) {
    echo "<div><span style='font-size: 14px; font-weight:bold'>" . $name . "</span>";

    /*	if (!empty($web)) {
         echo "<span class='note'> (".(!empty($web) ? ("<a href='".$web."'>www</a>") : "");
       //	echo (!empty($web) && !empty($user)) ? " | " : "";
       //	echo !empty($user) ? ("<a href='".WR_RID."?m=".PROFILE."&amp;id=".$user."'>profil</a>") : "";
         echo ")</span>\n";
       }*/

    echo "</div>\n";
  }
}
?>