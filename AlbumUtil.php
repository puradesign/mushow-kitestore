<?php

/*
 * Pristupove validace pro editaci alba
 */
function validateAlbumEdit() {

  if (!isset($_GET["aid"])) {
    $GLOBALS["rv"]->addError(getRText("err1")); // "Oops. Chybí číslo alba. Pokud potize pretrvaji, kontaktujte spravce."
    return false;
  } elseif (!@$_SESSION[SN_LOGGED]) {
    $GLOBALS["rv"]->addError(getRText("err2")); // "Pro úpravu galerie je nutné přihlášení."

    require_once F_ROOT . "LoginWM.php";
    $GLOBALS["wm"] = new LoginWM(LOGIN);

    return false;
  } else {
    $query = "SELECT al_user FROM album where al_code = " . $_GET["aid"];
    $result = $GLOBALS["db"]->query($query);

    $row = $result->fetch_assoc();
    if (empty($row)) {
      $GLOBALS["rv"]->addError(getRText("err3")); // "Neexistující album!"
      return false;
    }

    if ($row["al_user"] != $_SESSION[SN_CODE]) {
      $GLOBALS["rv"]->addError(getRText("err4")); // "Nemáte právo editovat toto album."
      return false;
    }
  }

  return true;
}

function deleteRow($table, $code) {
  $query = "DELETE FROM " . $table . " WHERE ph_code = " . $code;
  $result = $GLOBALS["db"]->query($query);
}

function deletePhotoRow($code) {
  $query = "SELECT * FROM photo WHERE ph_code=" . $code;
  $result = $GLOBALS["db"]->query($query);
  $row = $result->fetch_assoc();

  $filename = F_ALBUMS . $row["ph_album"] . DIRECTORY_SEPARATOR . $row["ph_filename"];
  if (file_exists($filename))
    unlink($filename);

  $filename = F_ALBUMS . $row["ph_album"] . DIRECTORY_SEPARATOR . "tb_" . $row["ph_filename"];
  if (file_exists($filename))
    unlink($filename);

  deleteRow("photo", $code);
}

/**
 * Vytiskne odkaz pro navrat na prehled galerii
 */
function printRefBackToGallery($code = 0, $edit = false) {
  $str = "<span class='content_block'><a href='" . WR_GAL;

  if ($code != 0)
    $str .= "?uid=" . $code;

  $str .= "'>" . getRText("util1"); // Zpět na přehled
  if ($edit)
    $str .= " (" . getRText("util2") . ")"; // bez uložení změn

  $str .= "</a></span>";

  echo $str;
}

/**
 * Vytiskne panel pro vyber akce s albumem
 */
function printEditPanel($actPanel) {
  echo "<div class='album_edit_panel'><ul>";

  if ($actPanel == 1)
    $str = "<li class='in'>";
  else
    $str = "<li>";
  echo $str . "<a href='" . WR . "?m=" . G_EDIT . "&amp;aid=" . $_GET["aid"] . "'>" . getRText("util3") . "</a></li>"; // Editace galerie

  if ($actPanel == 2)
    $str = "<li class='in'>";
  else
    $str = "<li>";
  echo $str . "<a href='" . WR . "?m=" . G_ADD_PICS . "&amp;aid=" . $_GET["aid"] . "'>" . getRText("util4") . "</a></li>"; // Přidat fotky

  if ($actPanel == 3)
    $str = "<li class='in'>";
  else
    $str = "<li>";
  echo $str . "<a href='" . WR . "?m=" . G_EDIT_PICS . "&amp;aid=" . $_GET["aid"] . "'>" . getRText("util5") . "</a></li>"; // Upravit fotky

  if ($actPanel == 4)
    $str = "<li class='in'>";
  else
    $str = "<li>";
  echo $str . "<a href='" . WR . "?m=" . G_DELETE . "&amp;aid=" . $_GET["aid"] . "'>" . getRText("util6") . "</a></li>"; // Smazat galerii

  echo "</ul></div>";
}

/**
 * Vytiskne panel pro vyber akce s albumem
 */
function printBrwEditPanel($aid) {
  echo "<div class='brw_edit_panel'><ul>";

  echo "<li><a href='" . WR . "?m=" . G_EDIT . "&amp;aid=$aid'>" . getRText("util3") . "</a></li>\n"; // Editace galerie
  echo "<li><a href='" . WR . "?m=" . G_ADD_PICS . "&amp;aid=$aid'>" . getRText("util4") . "</a></li>\n"; // Přidat fotky
  echo "<li><a href='" . WR . "?m=" . G_EDIT_PICS . "&amp;aid=$aid'>" . getRText("util5") . "</a></li>\n"; // Upravit fotky
  echo "<li><a href='" . WR . "?m=" . G_DELETE . "&amp;aid=$aid'>" . getRText("util6") . "</a></li>\n"; // Smazat galerii

  echo "</ul></div>";
}

function getAlbumTextForBrw($text) {
  $text = Str_Replace("<br />", " ", $text);
  $text = Str_Replace("<br>", " ", $text);

  if (strlen($text) < 200)
    return $text;
  else
    return substr($text, 0, 200) . "...";
}

/**
 *
 * Vytiskne jednu polozku browseru albumu
 */
function printOneGalleryBrw($row, $i = -1) {
  $filename = F_ALBUMS . $row["al_code"] . DIRECTORY_SEPARATOR . "album_head_pic.jpg";
  if (!file_exists($filename))
    $filename = F_IMG . "none.png";

  $image = new WebImage();
  $image->load($filename);
  $image->setFrameSize(95, 95);
  $imgStr = $image->getHtmlOutput($row["al_title"]);

  echo "<div class='album_td" . ($i == -1 ? " odd" : "") . "'>";
  echo "<div class='img_gallery_brw'><a href='" . WR_GAL . "v/" . $row["al_url"] . "'>" . $imgStr . "</a></div>";
  echo "<div class='text_gallery_brw'><h2><a href='" . WR_GAL . "v/" . $row["al_url"] . "'>" . $row["al_title"] . "</a></h2>";
  echo "<p>" . getAlbumTextForBrw($row["al_text"]) . "</p>";
  echo "</div>";

  // ikony
  /*$query = "SELECT 1 FROM photo WHERE ph_album=".$row["al_code"]." AND ph_is_video = '1'";
  $resIcon = mysql_query($query);
  $hasVideo = $resIcon->fetch_assoc();
  
  $query = "SELECT 1 FROM photo WHERE ph_album=".$row["al_code"]." AND ph_is_video = '0'";
  $resIcon = mysql_query($query);
  $hasPhoto = $resIcon->fetch_assoc();
  
  if ($hasPhoto || $hasVideo) {
    echo "<div class='album_td_icons'>";
    
    if ($hasPhoto)
      echo "<img src='".getBrowserPath(F_IMG."foto_icon.jpg")."' alt='foto ikona'/>";
      
    if ($hasVideo)
      echo "<img src='".getBrowserPath(F_IMG."video_icon.jpg")."' alt='foto ikona'/>";
  
    echo "</div>";
  }*/

  $time = strtotime($row["al_date_create"]);
  $date_create = $_SESSION[SN_LANG] == 3 ? StrFTime("%d.%m.%Y %H:%M", $time) : StrFTime("%d/%m/%Y %H:%M", $time);
  $time = strtotime($row["al_date_update"]);
  $date_update = $_SESSION[SN_LANG] == 3 ? StrFTime("%d.%m.%Y %H:%M", $time) : StrFTime("%d/%m/%Y %H:%M", $time);
  echo "<div class='brw_gallery_dates'>";
  echo "<p>" . getRText("util7") . " " . $date_create . "</p>"; // Přidáno
  echo "</div>";

  // pocet komentaru
  $query = "SELECT COUNT(c_code) AS pocet FROM comment WHERE c_head_type = " . G_VIEW . " AND c_head = " . $row["al_code"];
  $result = $GLOBALS["db"]->query($query);
  $comm_row = $result->fetch_assoc();
  $comments = $comm_row["pocet"];

  echo "<div class='rightdown'><a href='" . WR_GAL . "v/" . $row["al_url"] . "#comments'>$comments " . getRText("util95") . "</a> | "; // komentářů
  echo "<a href='" . WR_GAL . "v/" . $row["al_url"] . "?caction=add#add'>" . getRText("util94") . "</a>"; // Přidat komentář
  echo "</div>";

  echo "</div>";
}

/**
 * Vykresli odkaz na video
 */
function printVideoRef($code, $filename, $dir, $descr) {
  $query = "SELECT v_type, v_video_id FROM video WHERE v_code = $code";
  $result = mysql_query($query);
  $row = $result->fetch_assoc();

  if (empty($row))
    return;

  $image = new WebImage();
  $isOk = $image->load($dir . $filename);
  if (!$isOk)
    continue;

  $image->setFrameSize(130, 98);
  $imgStr = $image->getHtmlOutput($descr);

  // samotne vykresleni
  echo "<div class='pic_td'>\n";

  if ($row["v_type"] == YOUTUBE) {
    $video_ref = "<a href='http://www.youtube.com/v/" . $row["v_video_id"];
    $video_ref .= "' rel='shadowbox[img];width=640;height=386;player=swf'";
    $video_ref .= " title='$descr'>$imgStr</a>\n";

    echo $video_ref;
    echo "<img src='" . WR . "img/youtube.png' class='v_youtube' alt='$descr'/>";
  } elseif ($row["v_type"] == VIMEO) {
    $video_ref = "<a href='http://vimeo.com/moogaloop.swf'";
    $video_ref .= " rel='shadowbox[img];width=640;height=385;player=swf;";
    $video_ref .= "options={flashVars:{clip_id:" . $row["v_video_id"] . "}}'";
    $video_ref .= " title='$descr'>$imgStr</a>\n";

    echo $video_ref;
    echo "<img src='" . WR . "img/vimeo.png' class='v_vimeo' alt='$descr'/>";
  }

  echo "</div>\n";
}

/**
 * Vytvori text do url
 */
function createUrlTitleAlbum($title) {

  // prevod na UTF-8
  $title = getUTF($title);

  // odstraneni diakritiky
  $tbl = array("\xc3\xa1" => "a", "\xc3\xa4" => "a", "\xc4\x8d" => "c", "\xc4\x8f" => "d", "\xc3\xa9" => "e", "\xc4\x9b" => "e", "\xc3\xad" => "i", "\xc4\xbe" => "l", "\xc4\xba" => "l", "\xc5\x88" => "n", "\xc3\xb3" => "o", "\xc3\xb6" => "o", "\xc5\x91" => "o", "\xc3\xb4" => "o", "\xc5\x99" => "r", "\xc5\x95" => "r", "\xc5\xa1" => "s", "\xc5\xa5" => "t", "\xc3\xba" => "u", "\xc5\xaf" => "u", "\xc3\xbc" => "u", "\xc5\xb1" => "u", "\xc3\xbd" => "y", "\xc5\xbe" => "z", "\xc3\x81" => "A", "\xc3\x84" => "A", "\xc4\x8c" => "C", "\xc4\x8e" => "D", "\xc3\x89" => "E", "\xc4\x9a" => "E", "\xc3\x8d" => "I", "\xc4\xbd" => "L", "\xc4\xb9" => "L", "\xc5\x87" => "N", "\xc3\x93" => "O", "\xc3\x96" => "O", "\xc5\x90" => "O", "\xc3\x94" => "O", "\xc5\x98" => "R", "\xc5\x94" => "R", "\xc5\xa0" => "S", "\xc5\xa4" => "T", "\xc3\x9a" => "U", "\xc5\xae" => "U", "\xc3\x9c" => "U", "\xc5\xb0" => "U", "\xc3\x9d" => "Y", "\xc5\xbd" => "Z");
  $title = strtr($title, $tbl);

  // odstraneni nezadoucich znaku mezer atd.
  $title = preg_replace("/[^a-zA-Z0-9]+/", '-', $title);

  // melo by zrusit pomlcky na zacatku
  $title = preg_replace("/^-/", '', $title);

  if (strlen($title) > 49) {
    $title = substr($title, 0, 49);

    // melo by zrusit pomlcky na konci
    $title = preg_replace("/(.*[a-zA-Z0-9])[-]*$/", '$1', $title);
  }

  // kontrola jestli takova url uz neexistuje
  $num = 0;
  $newTitle = $title;
  while (true) {
    $query = "SELECT al_url FROM album WHERE al_url = '$newTitle'";
    $res = $GLOBALS["db"]->query($query);

    if (!$res->fetch_assoc())
      break;

    $num++;
    $newTitle = $title . "-" . $num;
  }

  return $newTitle;
}
?>