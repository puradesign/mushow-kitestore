<?php
require_once "StoreUtil.php";

class LoginWM extends WebModule {
  var $mAction = "login";

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    if (isset($_GET["a"]))
      $this->mAction = $_GET["a"];

    // aktivace
    if (isset($_GET["activate"])) {
      $lHash = addSlashes($_GET["activate"]);

      $query = "SELECT * FROM user WHERE u_activation='$lHash'";
      $result = $GLOBALS["db"]->query($query);
      $row = $result->fetch_assoc();

      if (!$row) {
        $GLOBALS["rv"]->addError("Účet je již aktivován, nebo byla ručně upravena url.");
      } else {
        $query = "UPDATE user SET u_activation=NULL WHERE u_code=" . $row["u_code"];
        $result = $GLOBALS["db"]->query($query);

        /* Vse probehlo v poradku */
        $_SESSION[SN_CODE] = $row["u_code"];
        $_SESSION[SN_NICK] = $row["u_nick"];
        $_SESSION[SN_MAIL] = $row["u_mail"];
        $_SESSION[SN_MAIN_ADDR] = $row["u_main_addr"];
        $_SESSION[SN_LOGGED] = true;
        $_SESSION[SN_HOMEDIR] = F_USER_IMAGES;
        $_SESSION[SN_ADMIN] = $row["u_role"] == 1;
        $_SESSION[SN_AT] = time();

        // nastaveni slevy
        if (!isLoggedAdmin()) {

          // id uzivatele do kosiku
          $_SESSION[SN_CART]->setUser($_SESSION[SN_CODE]);
          $_SESSION[SN_CART]->setUserAddress($_SESSION[SN_MAIN_ADDR]);
        }

        $GLOBALS["rv"]->addInfo("Účet je aktivován a byl jste automaticky přihlášen.");
      }

      $this->setForOutput(false);
      return true;
    }

    if (@$_SESSION[SN_LOGGED]) {
      $GLOBALS["rv"]->addError("Už jste přihlášen. Pro přihlášení pod jiným uživatelem se musíte nejdříve odhlásit.");

      $this->setForOutput(false);
      return true;
    }

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    if ($this->mAction == "login" && !@$_SESSION[SN_LOGGED]) {
      $login = addSlashes($_POST["username"]);
      $pwd = $_POST["password"];

      $pwd_md5 = MD5($pwd);

      /* test na existenci uziv. jmena */
      $query = "SELECT * FROM user WHERE u_nick='" . ($login) . "'";
      $result = $GLOBALS["db"]->query($query);
      if ($result->num_rows == 0) {
        $GLOBALS["rv"]->addError("Neexistující uživatelské jméno.");
        return true;
      }

      /* test na shodu uziv. jmena a hesla */
      $row = $result->fetch_assoc();

      if ($row["u_activation"] != null) {
        $GLOBALS["rv"]->addError("Tento účet není aktivován.");
        return true;
      }

      if ($row["u_passwd"] != $pwd_md5) {
        $GLOBALS["rv"]->addError("Nesprávné heslo.");
        return true;
      }

      /* Vse probehlo v poradku */
      $_SESSION[SN_CODE] = $row["u_code"];
      $_SESSION[SN_NICK] = $row["u_nick"];
      $_SESSION[SN_MAIL] = $row["u_mail"];
      $_SESSION[SN_MAIN_ADDR] = $row["u_main_addr"];
      $_SESSION[SN_LOGGED] = true;
      $_SESSION[SN_HOMEDIR] = F_USER_IMAGES;
      $_SESSION[SN_ADMIN] = $row["u_role"] == 1;
      $_SESSION[SN_AT] = time();

      // print_r($row);
      // print_r($_SESSION);

      // nastaveni slevy
      if (!isLoggedAdmin()) {
        $query = "SELECT * FROM shop_order WHERE or_order_state = 2 AND or_user=" . $row["u_code"];
        $query .= " AND or_date >= DATE_ADD( NOW(), INTERVAL -400 DAY)";
        $result = $GLOBALS["db"]->query($query);
        $lTotal = 0;
        while ($row = $result->fetch_assoc()) {
          $lTotal += $row["or_price_sum"];
        }

        $_SESSION[SN_CART]->setDiscount(getDiscount($lTotal));

        // id uzivatele do kosiku
        $_SESSION[SN_CART]->setUser($_SESSION[SN_CODE]);
        $_SESSION[SN_CART]->setUserAddress($_SESSION[SN_MAIN_ADDR]);
      }

      $GLOBALS["rv"]->addInfo("Přihlášen.");

      /*$this->setForOutput(false);
       
       return false;*/

      // presmerovani na posledni stranku
      if ($GLOBALS["wm"] == $this) {
        if (isset($_SESSION[SN_LASTWM]) && !strpos($_SESSION[SN_LASTWM], "?m=-1"))
          $url = "http://" . $_SERVER["HTTP_HOST"] . $_SESSION[SN_LASTWM];
        else
          $url = WR;

        $_SESSION["logged_now"] = "yes";

        echo "<html><head><meta http-equiv='refresh' content='0;url=$url'/></head><body>Probiha presmerovani na stranku " . $url . ". Pokud se stranka behem nekolika sekund nenacte, kliknete <a href='$url'>zde</a>.</body></html>";
        exit();
      }
    }

    // zapomenute heslo
    elseif ($this->mAction == "send_pswd") {
      $login = addSlashes($_POST["username"]);
      $email = addSlashes($_POST["email"]);

      /* test na existenci uziv. jmena */
      $query = "SELECT * FROM user WHERE u_nick='" . ($login) . "'";
      $result = $GLOBALS["db"]->query($query);
      if ($result->num_rows == 0) {
        $GLOBALS["rv"]->addError("Neexistující přihlašovací jméno.");
        return true;
      }

      /* test na shodu uziv. jmena a emailu */
      $row = $result->fetch_assoc();
      if ($row["u_mail"] != $email) {
        $GLOBALS["rv"]->addError("Pro tento účet byla zadána jiná emailová adresa.");
        return true;
      }

      $newPass = $this->genNewPassword();
      $oldPassMD5 = $row["u_passwd"];

      $query = "UPDATE user SET u_passwd = '" . MD5($newPass) . "' WHERE u_code = " . $row["u_code"];
      $result = $GLOBALS["db"]->query($query);
      if (!$result) {
        $GLOBALS["rv"]->addError("Chyba při uložení nového hesla: " . $result->error);
        return true;
      }

      $subj = "Nové přihlašovací údaje k účtu " . $row["u_nick"];

      $lPlain = "Dobrý den,\n\n";
      $lPlain .= "bylo vám vygenerováno nové heslo pro přístup na stránky mushow.cz/kitestore\n";
      $lPlain .= "Doporučujeme si heslo ihned po příhlášení změnit.\n\n";
      $lPlain .= "Nové přihlašovací údaje:\n";
      $lPlain .= "Login: " . $row["u_nick"] . "\n";
      $lPlain .= "Heslo: " . $newPass;

      $lHtml = "<p>Dobrý den,<br/><br/>";
      $lHtml .= "bylo vám vygenerováno nové heslo pro přístup na stránky mushow.cz/kitestore<br/>";
      $lHtml .= "Doporučujeme si heslo ihned po příhlášení změnit.<br/><br/>";
      $lHtml .= "Nové přihlašovací údaje:<br/>";
      $lHtml .= "<b>Login:</b> " . $row["u_nick"] . "<br/>";
      $lHtml .= "<b>Heslo:</b> " . $newPass . "</p>";

      if (sendMail($email, $subj, $lPlain, $lHtml)) {
        $GLOBALS["rv"]->addInfo("Nové heslo úspěšně odesláno na zadanou emailovou adresu.");
      } else {
        $GLOBALS["rv"]->addError("Nepodařilo se odeslat email s novým heslem.");

        $query = "UPDATE user SET u_passwd = '" . $oldPassMD5 . "' WHERE u_code = " . $row["u_code"];
        $result = $GLOBALS["db"]->query($query);
      }
    }

    return true;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    if ($this->mAction == "login")
      return "Login";
    elseif ($this->mAction == "send_pswd")
      return "Zapomenuté heslo";
  }


  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/

  /**
   * Vytvoreni elementu formulare
   */
  function defineElements() {

    if ($this->mAction == "login") {
      $lEF = new EditField("username", getRText("util62"), 100, true, // Login
        100, 20);
      $this->addElement($lEF);

      $lEF = new EditField("password", getRText("util63"), 100, true, // Heslo
        100, 20);
      $lEF->setInputType("password");
      $this->addElement($lEF);
    } elseif ($this->mAction == "send_pswd") {
      $lEF = new EditField("username", getRText("util62"), 100, true, // Login
        100, 20);
      $this->addElement($lEF);

      $lEF = new EditField("email", "Email", 100, true, // Email
        150, 50);
      $this->addElement($lEF);
    }
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    if ($this->mAction == "login") {
      echo "<p>Formulář pro přihlášení existujícího uživatele. Pokud nemáte vytvořený účet";
      echo ", můžete se <a href='" . WR . "registrace'>zde zaregistrovat</a>.</p>";
    } elseif ($this->mAction == "send_pswd") {
      echo "<p>Po potvrzení formuláře vám bude vytvořeno nové heslo a zasláno na email zadaný ";
      echo "při registraci. Pokud nemáte vytvořený účet";
      echo ", můžete se <a href='" . WR . "registrace'>zde zaregistrovat</a>.</p>";
    }

    echo "<form method='post' name='login' action='" . WR . "login?a=" . $this->mAction . "'>";
    echo "<fieldset class='form' style='margin-bottom:5px;margin-top:3px;'>";

    $this->printElements();

    $lButtPrompt = ($this->mAction == "login" ? "Přihlásit se" : "Odeslat");
    echo "	<input type='submit' value='$lButtPrompt' style='padding:5px;'/>";
    echo "</fieldset></form>";

    if ($this->mAction == "login") {
      echo "<p style='float:left;margin-bottom:30px;'>Pokud si nepamatujete svoje heslo, můžete si nechat vytvořit nové heslo ";
      echo "<a href='" . WR . "login?a=send_pswd'>zde</a>.</p>";
    }
  }

  /**
   * Vygeneruje nove heslo
   */
  function genNewPassword() {
    $length = 8;
    $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
    $string = "";

    for ($p = 0; $p < $length; $p++) {
      $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }
    return $string;
  }
}
?>