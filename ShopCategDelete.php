<?php

class ShopCategDeleteWM extends WebModule {
  var $mAction;
  var $mHeader = "Kategorie";
  var $mActRow;

  /**
   * Reaguje na akci vyvolanou uzivatelem - pro prepsani
   */
  function beforeAction() {
    // kontroly
    if (!isLoggedAdmin()) {
      $GLOBALS["rv"]->addError("Nemáte právo vstupu do této sekce.");
      $this->setForOutput(false);
      return false;
    }

    if (!isset($_GET["item"]) || !is_numeric($_GET["item"])) {
      $GLOBALS["rv"]->addError("Chyba. Pravděpodobně byla ručně upravena url stránky.");
      $this->setForOutput(false);
      return false;
    }

    $query = "SELECT * FROM s_item_type WHERE sit_code=" . $_GET["item"];
    $result = $GLOBALS["db"]->query($query);

    $row = $result->fetch_assoc();
    $this->mActRow = $row;
    $this->mHeader = alterHtmlTextToPlain($row["sit_name"]);

    return true;
  }

  /**
   * Volano pro vykonne akce - po odeslani formulare
   */
  function processAction() {
    $query = "DELETE FROM s_item_typ WHERE `sit_code`=" . $this->mActRow["sit_code"];
    $result = $GLOBALS["db"]->query($query);

    if (!$result) {
      $GLOBALS["rv"]->addError("Kategorii se nepodařilo smaazat, pravděpdobně už v ní jsou nějaké položky nebo obsahuje jiné podkategorie.");
      return true;
    }

    // presmerovani na prehled kategorii
    require_once "ShopCategWM.php";
    $GLOBALS["wm"] = new ShopCategWM(S_CATEG);
    $GLOBALS["wm"]->reactOnActionLow();

    return false;
  }

  /* ------------------------------------------------------------------------*/
  /* ------------------------------------------------------------------------*/
  /**
   * Definuje hlavicku obsahu - pro prepsani
   */
  function getHeader() {
    $str = $this->mHeader . " - Smazat";

    return $str;
  }

  /**
   * Definovani vlastniho obsahu - pro prepsani
   */
  function defineHtmlOutput() {
    echo "<fieldset class='form' id='delete'>";
    echo "  <p>Opravdu smazat tuto kategorii?</p>";
    echo "  <form method='post' action='" . WR . "?m=" . S_CATEG_DEL . "&amp;item=" . $_GET["item"] . "'>";
    echo "    <input type='hidden' name='delete'/>";
    echo "    <input type='submit' class='submit' value='" . getRText("util23") . "'/>"; // Smazat
    echo "  </form>";
    echo "</fieldset>";
  }
}
?>